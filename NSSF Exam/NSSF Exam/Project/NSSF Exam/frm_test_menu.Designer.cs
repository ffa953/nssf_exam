﻿namespace NSSF_Exam
{
    partial class frm_test_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_test_menu));
            this.btn_eq = new System.Windows.Forms.Button();
            this.btn_iq = new System.Windows.Forms.Button();
            this.btn_mcq = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbl_title = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbl_id = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_eq
            // 
            this.btn_eq.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_eq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_eq.FlatAppearance.BorderSize = 0;
            this.btn_eq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_eq.Font = new System.Drawing.Font("Khmer OS Bokor", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_eq.ForeColor = System.Drawing.Color.Transparent;
            this.btn_eq.Location = new System.Drawing.Point(57, 30);
            this.btn_eq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_eq.Name = "btn_eq";
            this.btn_eq.Size = new System.Drawing.Size(275, 47);
            this.btn_eq.TabIndex = 0;
            this.btn_eq.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (EQ)";
            this.btn_eq.UseVisualStyleBackColor = true;
            this.btn_eq.Click += new System.EventHandler(this.btn_eq_Click);
            // 
            // btn_iq
            // 
            this.btn_iq.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_iq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_iq.FlatAppearance.BorderSize = 0;
            this.btn_iq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_iq.Font = new System.Drawing.Font("Khmer OS Bokor", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_iq.ForeColor = System.Drawing.Color.Transparent;
            this.btn_iq.Location = new System.Drawing.Point(57, 97);
            this.btn_iq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_iq.Name = "btn_iq";
            this.btn_iq.Size = new System.Drawing.Size(275, 47);
            this.btn_iq.TabIndex = 1;
            this.btn_iq.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (IQ)";
            this.btn_iq.UseVisualStyleBackColor = false;
            this.btn_iq.Click += new System.EventHandler(this.btn_iq_Click);
            // 
            // btn_mcq
            // 
            this.btn_mcq.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_mcq.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_mcq.FlatAppearance.BorderSize = 0;
            this.btn_mcq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_mcq.Font = new System.Drawing.Font("Khmer OS Bokor", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_mcq.ForeColor = System.Drawing.Color.Transparent;
            this.btn_mcq.Location = new System.Drawing.Point(57, 164);
            this.btn_mcq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_mcq.Name = "btn_mcq";
            this.btn_mcq.Size = new System.Drawing.Size(275, 47);
            this.btn_mcq.TabIndex = 2;
            this.btn_mcq.Text = "សំនួរជ្រើសរើស (MCQ)";
            this.btn_mcq.UseVisualStyleBackColor = false;
            this.btn_mcq.Click += new System.EventHandler(this.btn_mcq_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Khmer OS Content", 9F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl_title,
            this.lbl_id});
            this.statusStrip1.Location = new System.Drawing.Point(0, 244);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(394, 27);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.Transparent;
            this.lbl_title.Font = new System.Drawing.Font("Khmer OS Content", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lbl_title.ForeColor = System.Drawing.Color.Navy;
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(120, 22);
            this.lbl_title.Text = "លេខសម្គាល់បេក្ខជនៈ";
            // 
            // lbl_id
            // 
            this.lbl_id.BackColor = System.Drawing.Color.Transparent;
            this.lbl_id.Font = new System.Drawing.Font("Stencil", 10F);
            this.lbl_id.ForeColor = System.Drawing.Color.Navy;
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(64, 22);
            this.lbl_id.Text = "1901934";
            // 
            // frm_test_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(394, 271);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btn_mcq);
            this.Controls.Add(this.btn_iq);
            this.Controls.Add(this.btn_eq);
            this.Font = new System.Drawing.Font("Khmer OS Content", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_test_menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "----ជ្រើសរើសវិញ្ញាសារប្រឡង----";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_test_menu_FormClosed);
            this.Load += new System.EventHandler(this.frm_test_menu_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_eq;
        private System.Windows.Forms.Button btn_iq;
        private System.Windows.Forms.Button btn_mcq;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel lbl_title;
        public System.Windows.Forms.ToolStripStatusLabel lbl_id;
    }
}