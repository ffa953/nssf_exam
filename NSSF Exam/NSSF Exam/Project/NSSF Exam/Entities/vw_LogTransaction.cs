//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSSF_Exam.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_LogTransaction
    {
        public Nullable<int> UserId { get; set; }
        public string UserName { get; set; }
        public long LogId { get; set; }
        public Nullable<System.DateTime> LogDate { get; set; }
        public Nullable<short> LogTypeId { get; set; }
        public string IP { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string AreaCode { get; set; }
        public Nullable<double> Latitude { get; set; }
        public Nullable<double> Longitude { get; set; }
        public Nullable<System.DateTime> DateLogin { get; set; }
        public Nullable<System.DateTime> DateLogout { get; set; }
        public string LogType { get; set; }
        public string Address { get; set; }
    }
}
