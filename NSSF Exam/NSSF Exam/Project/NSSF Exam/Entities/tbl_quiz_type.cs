//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSSF_Exam.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_quiz_type
    {
        public tbl_quiz_type()
        {
            this.tbl_skill = new HashSet<tbl_skill>();
            this.tbl_subject_quiz = new HashSet<tbl_subject_quiz>();
        }
    
        public int type_id { get; set; }
        public string type_name { get; set; }
    
        public virtual ICollection<tbl_skill> tbl_skill { get; set; }
        public virtual ICollection<tbl_subject_quiz> tbl_subject_quiz { get; set; }
    }
}
