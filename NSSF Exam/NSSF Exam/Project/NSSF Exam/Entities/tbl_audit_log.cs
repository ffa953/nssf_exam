//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSSF_Exam.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_audit_log
    {
        public long log_id { get; set; }
        public Nullable<System.DateTime> log_date_in { get; set; }
        public Nullable<System.DateTime> log_date_out { get; set; }
        public Nullable<int> use_id { get; set; }
        public Nullable<int> mac_id { get; set; }
        public Nullable<bool> log_status { get; set; }
        public string div_id { get; set; }
    
        public virtual tbl_audit_machine tbl_audit_machine { get; set; }
        public virtual tbl_user tbl_user { get; set; }
    }
}
