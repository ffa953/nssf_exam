﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_student_edit : Form
    {
        public int userID​ = 0;
        public frm_student_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "បញ្ចូលព័ត៌មានបេក្ខជន");
        }

        private void frm_add_stu_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_add);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_combobox(cbo_branch);
            ClsSetting.sty_combobox(cbo_cert_skill);
            ClsSetting.sty_combobox(cbo_certificate);
            ClsSetting.sty_combobox(cbo_gender);
            ClsSetting.sty_combobox(cbo_nat);
            ClsSetting.sty_combobox(cbo_skill);
            ClsSetting.sty_combobox(cbo_status);

            bds_gender.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            bds_skill.DataSource = ClsConfig.glo_local_db.tbl_skill.Where(s => s.skill_active == true).ToList();
            bds_nat.DataSource = ClsConfig.glo_local_db.vw_nationality.Where(w => w.nat_id >= 1).OrderBy(o => o.nat_order).ToList();
            bds_status.DataSource = ClsConfig.glo_local_db.tbl_user_type.ToList();
            bds_certificate.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            bds_cert_skill.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();
            bds_stu_type.DataSource = ClsConfig.glo_local_db.tbl_student_type.ToList();
          

            if (userID > 0)
            {
                        
               var user = ClsConfig.glo_local_db.UserProfiles.Find(userID);

               bds_place.DataSource = (from b in ClsConfig.glo_local_db.tbl_exam_place where b.pla_id == user.bra_id select b).ToList();

               txt_name_kh.Text = user.Name_khmer;
               txt_name_en.Text = user.Name_latin;
               cbo_gender.SelectedValue = user.GenderId;
               dt_dob.Value = Convert.ToDateTime(user.DateOfBirth);
               cbo_nat.SelectedValue = user.nat_id;
               cbo_status.SelectedValue = user.status_id;
               txt_phone.Text = user.Phone;
               dt_off_start_date.Value = Convert.ToDateTime(user.off_start_date);
               cbo_certificate.SelectedValue = user.certificate_id;
               cbo_cert_skill.SelectedValue = user.cert_skill_id;
               cbo_skill.SelectedValue = user.skill_id;
               cbo_branch.SelectedValue = user.bra_id;
               cbo_stu_type_id.SelectedValue = user.stu_type_id;

            }
            else
            {

                cbo_skill.SelectedIndex = -1;
                cbo_certificate.SelectedIndex = -1;
                cbo_cert_skill.SelectedIndex = -1;
                cbo_status.SelectedIndex = -1;
                cbo_stu_type_id.SelectedIndex = -1;

            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_name_kh_Enter(object sender, EventArgs e)
        {
            ClsSetting.ChangeKeyToKH();
        }

        private void txt_name_en_Enter(object sender, EventArgs e)
        {
            ClsSetting.ChangeKeyToEN();
        }

        private void cbo_skill_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_skill.SelectedIndex > -1) { 

                int skillId = (int)cbo_skill.SelectedValue;
                bds_place.DataSource = ClsConfig.glo_local_db.tbl_skill_branch.Where(b => b.skill_id == skillId).Select(b => b.tbl_exam_place).ToList();  

            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (txt_name_kh.Text == "")
            {
                msgString += "- សូមបំពេញឈ្មោះជាអក្សរខ្មែរ!\n";
            }
            if (txt_name_en.Text == "")
            {
                msgString += "- សូមបំពេញឈ្មោះជាអក្សរឡាតាំង!\n";
            }

            if (cbo_status.Text == "")
            {
                msgString += "- សូមជ្រើសរើសស្ថានភាព!\n";
            }

            if (txt_phone.Text == "")
            {
                msgString += "- សូមបំពេញលេខទូរស័ព្ទ!\n";
            }
            if (cbo_certificate.Text == "")
            {
                msgString += "- សូមជ្រើសរើសសញ្ញាប័ត្រ!\n";
            }
            if (cbo_cert_skill.Text == "")
            {
                msgString += "- សូមជ្រើសរើសឯកទេស!\n";
            }
            if (cbo_skill.Text == "")
            {
                msgString += "- សូមជ្រើសរើសផ្នែក!\n";
            }

            if (ClsFun.CheckAge(DateTime.Now.Date, dt_dob.Value.Date))
            {
                msgString += "- បេក្ខជនត្រូវមានអាយុចាប់ពី ១៨ ឆ្នាំ";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
          try{

              if (IsFieldValidate()) {

                  if (userID > 0)
                  {

                      var user = ClsConfig.glo_local_db.UserProfiles.Find(userID);

                      user.Name_khmer = txt_name_kh.Text;
                      user.Name_latin = txt_name_en.Text;
                      user.GenderId = Convert.ToInt16(cbo_gender.SelectedValue);
                      user.DateOfBirth = dt_dob.Value;
                      user.nat_id = Convert.ToInt32(cbo_nat.SelectedValue);
                      user.status_id = Convert.ToInt32(cbo_status.SelectedValue);
                      user.Phone = txt_phone.Text;
                      user.certificate_id = Convert.ToInt32(cbo_certificate.SelectedValue);
                      user.cert_skill_id = Convert.ToInt32(cbo_cert_skill.SelectedValue);
                      user.skill_id = Convert.ToInt32(cbo_skill.SelectedValue);
                      user.bra_id = Convert.ToInt32(cbo_branch.SelectedValue);
                      user.LastActivityDate = ClsFun.getCurrentDate();
                      user.off_start_date = dt_off_start_date.Value;
                      user.stu_type_id = (int)cbo_stu_type_id.SelectedValue;
                      user.user_id = ClsConfig.glo_use_id;

                      ClsConfig.glo_local_db.SaveChanges();

                      ClsMsg.Success(ClsMsg.STR_SUCCESS);
                      
                  }
                  else
                  {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required))
                    {
                        try {
                            UserProfile us = new UserProfile();
                            var examId = (from ex in ClsConfig.glo_local_db.tbl_exam_date where ex.exam_active == true select ex).FirstOrDefault();

                            us.UserName = "NSSF";
                            us.Name_khmer = txt_name_kh.Text;
                            us.Name_latin = txt_name_en.Text;
                            us.GenderId = Convert.ToInt16(cbo_gender.SelectedValue);
                            us.DateOfBirth = dt_dob.Value;
                            us.Phone = txt_phone.Text;
                            us.IsActive = true;
                            us.skill_id = Convert.ToInt16(cbo_skill.SelectedValue);
                            us.exam_id = examId.exam_id;
                            us.certificate_id = Convert.ToInt32(cbo_certificate.SelectedValue);
                            us.cert_skill_id = Convert.ToInt32(cbo_cert_skill.SelectedValue);
                            us.nat_id = Convert.ToInt32(cbo_nat.SelectedValue);
                            us.status_id = Convert.ToInt32(cbo_status.SelectedValue);
                            us.bra_id = Convert.ToInt32(cbo_branch.SelectedValue);
                            us.LastActivityDate = ClsFun.getCurrentDate();
                            us.off_start_date = dt_off_start_date.Value;
                            us.stu_type_id = (int)cbo_stu_type_id.SelectedValue;
                            us.user_id = ClsConfig.glo_use_id;

                            ClsConfig.glo_local_db.UserProfiles.Add(us);
                            ClsConfig.glo_local_db.SaveChanges();

                            var user = (from u in ClsConfig.glo_local_db.UserProfiles where u.UserId == us.UserId select u).FirstOrDefault();
                            user.UserName = cbo_gender.SelectedValue + dt_dob.Value.Date.Year.ToString().Substring(2, 2) + us.UserId;
                            ClsConfig.glo_local_db.SaveChanges();
                            webpages_UsersInRoles web_us = new webpages_UsersInRoles();
                            web_us.UserId = us.UserId;
                            web_us.RoleId = 2;
                            ClsConfig.glo_local_db.webpages_UsersInRoles.Add(web_us);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success("លេខសំគាល់​៖ " + us.UserName.ToString());
                            tran.Complete();
                        }
                        catch (Exception ex) {
                            tran.Dispose();
                            ClsMsg.Error(ex.Message);
                        }
                    }
                  }

                  this.Close();
              }

          }
          catch (Exception ex)
          {
              ClsMsg.Error("" + ex);
          }
        }

       
      
    }
}
