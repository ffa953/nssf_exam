﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_section_in_subject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.cbo_subject = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.chk_active = new System.Windows.Forms.CheckBox();
            this.txt_section_title = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rad_subject_no_sct = new System.Windows.Forms.RadioButton();
            this.rad_subject_have_sct = new System.Windows.Forms.RadioButton();
            this.rad_all = new System.Windows.Forms.RadioButton();
            this.col_edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_lr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sct_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sub_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sct_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sct_active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 460);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1003, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // cbo_subject
            // 
            this.cbo_subject.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_subject.DisplayMember = "skill";
            this.cbo_subject.DropDownHeight = 120;
            this.cbo_subject.FormattingEnabled = true;
            this.cbo_subject.IntegralHeight = false;
            this.cbo_subject.Location = new System.Drawing.Point(95, 27);
            this.cbo_subject.Name = "cbo_subject";
            this.cbo_subject.Size = new System.Drawing.Size(248, 30);
            this.cbo_subject.TabIndex = 38;
            this.cbo_subject.ValueMember = "skill_id";
            this.cbo_subject.SelectionChangeCommitted += new System.EventHandler(this.cbo_subject_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 22);
            this.label1.TabIndex = 37;
            this.label1.Text = "មុខវិជ្ជា";
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvlst.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_edit,
            this.col_lr,
            this.col_sct_id,
            this.col_sub_id,
            this.col_sct_title,
            this.col_sct_active});
            this.dgvlst.Location = new System.Drawing.Point(12, 120);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(979, 326);
            this.dgvlst.TabIndex = 40;
            this.dgvlst.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellClick);
            this.dgvlst.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvlst_RowsAdded);
            this.dgvlst.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvlst_RowsRemoved);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(451, 67);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(100, 34);
            this.btn_clear.TabIndex = 42;
            this.btn_clear.Tag = "1";
            this.btn_clear.Text = "button1";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(451, 27);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 41;
            this.btn_add.Tag = "1";
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // chk_active
            // 
            this.chk_active.AutoSize = true;
            this.chk_active.Location = new System.Drawing.Point(352, 29);
            this.chk_active.Name = "chk_active";
            this.chk_active.Size = new System.Drawing.Size(74, 26);
            this.chk_active.TabIndex = 43;
            this.chk_active.Text = "ដំណើរការ";
            this.chk_active.UseVisualStyleBackColor = true;
            // 
            // txt_section_title
            // 
            this.txt_section_title.BackColor = System.Drawing.SystemColors.Info;
            this.txt_section_title.Location = new System.Drawing.Point(95, 71);
            this.txt_section_title.MaxLength = 100;
            this.txt_section_title.Name = "txt_section_title";
            this.txt_section_title.Size = new System.Drawing.Size(329, 30);
            this.txt_section_title.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 22);
            this.label2.TabIndex = 45;
            this.label2.Text = "ផ្នែកនៃមុខវិជ្ជា";
            // 
            // rad_subject_no_sct
            // 
            this.rad_subject_no_sct.AutoSize = true;
            this.rad_subject_no_sct.Location = new System.Drawing.Point(699, 75);
            this.rad_subject_no_sct.Name = "rad_subject_no_sct";
            this.rad_subject_no_sct.Size = new System.Drawing.Size(106, 26);
            this.rad_subject_no_sct.TabIndex = 48;
            this.rad_subject_no_sct.TabStop = true;
            this.rad_subject_no_sct.Text = "មុខវិជ្ជាពុំមានផ្នែក";
            this.rad_subject_no_sct.UseVisualStyleBackColor = true;
            this.rad_subject_no_sct.CheckedChanged += new System.EventHandler(this.rad_subject_no_sct_CheckedChanged);
            // 
            // rad_subject_have_sct
            // 
            this.rad_subject_have_sct.AutoSize = true;
            this.rad_subject_have_sct.Location = new System.Drawing.Point(815, 75);
            this.rad_subject_have_sct.Name = "rad_subject_have_sct";
            this.rad_subject_have_sct.Size = new System.Drawing.Size(98, 26);
            this.rad_subject_have_sct.TabIndex = 49;
            this.rad_subject_have_sct.TabStop = true;
            this.rad_subject_have_sct.Text = "មុខវិជ្ជាមានផ្នែក";
            this.rad_subject_have_sct.UseVisualStyleBackColor = true;
            this.rad_subject_have_sct.CheckedChanged += new System.EventHandler(this.rad_subject_have_sct_CheckedChanged);
            // 
            // rad_all
            // 
            this.rad_all.AutoSize = true;
            this.rad_all.Location = new System.Drawing.Point(923, 75);
            this.rad_all.Name = "rad_all";
            this.rad_all.Size = new System.Drawing.Size(68, 26);
            this.rad_all.TabIndex = 50;
            this.rad_all.TabStop = true;
            this.rad_all.Text = "ទាំងអស់";
            this.rad_all.UseVisualStyleBackColor = true;
            this.rad_all.CheckedChanged += new System.EventHandler(this.rad_all_CheckedChanged);
            // 
            // col_edit
            // 
            this.col_edit.FillWeight = 60F;
            this.col_edit.HeaderText = "";
            this.col_edit.Name = "col_edit";
            this.col_edit.ReadOnly = true;
            this.col_edit.Text = "កែប្រែ";
            this.col_edit.UseColumnTextForButtonValue = true;
            this.col_edit.Width = 60;
            // 
            // col_lr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_lr.FillWeight = 50F;
            this.col_lr.HeaderText = "ល.រ";
            this.col_lr.Name = "col_lr";
            this.col_lr.ReadOnly = true;
            this.col_lr.Width = 50;
            // 
            // col_sct_id
            // 
            this.col_sct_id.DataPropertyName = "sct_id";
            this.col_sct_id.HeaderText = "sct_id";
            this.col_sct_id.Name = "col_sct_id";
            this.col_sct_id.ReadOnly = true;
            this.col_sct_id.Visible = false;
            // 
            // col_sub_id
            // 
            this.col_sub_id.DataPropertyName = "sub_id";
            this.col_sub_id.HeaderText = "sub_id";
            this.col_sub_id.Name = "col_sub_id";
            this.col_sub_id.ReadOnly = true;
            this.col_sub_id.Visible = false;
            // 
            // col_sct_title
            // 
            this.col_sct_title.DataPropertyName = "sct_title";
            this.col_sct_title.FillWeight = 250F;
            this.col_sct_title.HeaderText = "ផ្នែកនៃមុខវិជ្ជា";
            this.col_sct_title.Name = "col_sct_title";
            this.col_sct_title.ReadOnly = true;
            this.col_sct_title.Width = 300;
            // 
            // col_sct_active
            // 
            this.col_sct_active.DataPropertyName = "sct_active";
            this.col_sct_active.HeaderText = "ដំណើរការ";
            this.col_sct_active.Name = "col_sct_active";
            this.col_sct_active.ReadOnly = true;
            this.col_sct_active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_sct_active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frm_section_in_subject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 482);
            this.Controls.Add(this.rad_all);
            this.Controls.Add(this.rad_subject_have_sct);
            this.Controls.Add(this.rad_subject_no_sct);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_section_title);
            this.Controls.Add(this.chk_active);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.dgvlst);
            this.Controls.Add(this.cbo_subject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_section_in_subject";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ផ្នែកនៃមុខវិជ្ជា";
            this.Load += new System.EventHandler(this.frm_section_in_subject_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ComboBox cbo_subject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.CheckBox chk_active;
        private System.Windows.Forms.TextBox txt_section_title;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rad_subject_no_sct;
        private System.Windows.Forms.RadioButton rad_subject_have_sct;
        private System.Windows.Forms.RadioButton rad_all;
        private System.Windows.Forms.DataGridViewButtonColumn col_edit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sct_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sub_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sct_title;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_sct_active;
    }
}