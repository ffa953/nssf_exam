﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Forms.Finger;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_student : Form
    {
        //03-10-2017
        public frm_student()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ពត៍មានបេក្ខជន");
        }

        private void frm_student_Load(object sender, EventArgs e)
        {
            ClsSetting.OpenPermission(this, this.Name, ClsConfig.glo_use_privilege);

            ClsSetting.sty_dgv(dgv_student);
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_btn_print(btn_export);
            ClsSetting.sty_btn_update(btn_edit);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_btn_search(btn_search);

            GenBindingSource.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            SkillBindingSource.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();          
            CertBindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            CertSkill_BindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();
            NatBindindSource.DataSource = ClsConfig.glo_local_db.vw_nationality.ToList();
            StatusBindingSource.DataSource = ClsConfig.glo_local_db.tbl_user_type.ToList();
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_user_by_role(2).OrderByDescending(f => f.UserId).ToList();
        }

        private void dgv_student_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_student.RowCount; i++)
            {
                dgv_student.Rows[i].Cells["col"].Value = i + 1;             
            }
            dgv_student.ClearSelection();
        }

       

        private void btn_add_Click(object sender, EventArgs e)
        {
            frm_student_edit frm = new frm_student_edit();
            frm.ShowDialog();
            frm_student_Load(null, null);
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            int rowindex = dgv_student.CurrentCell.RowIndex;
            int id = Convert.ToInt32(dgv_student.Rows[rowindex].Cells[0].Value);
            frm_student_edit frm = new frm_student_edit();
            frm.userID = id;
            frm.ShowDialog();
            frm_student_Load(null, null);
        }

        private void dgv_student_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_student.Columns[e.ColumnIndex].Name == "finger")
            {
                if (dgv_student.Rows[e.RowIndex].Cells["finger"].Value != null)
                {

                    e.CellStyle.BackColor = SystemColors.Info;

                }
                else
                {
                    e.CellStyle.BackColor = SystemColors.Control;
                  
                }
            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            string search_text = txt_search.Text;
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_user_by_role(2).Where(m => m.Name_khmer.Contains(search_text) || m.Name_latin.Contains(search_text) || m.UserName.Contains(search_text) || m.skill_note.Contains(search_text)).OrderByDescending(u => u.UserId).ToList();
        }


        private void btn_export_Click(object sender, EventArgs e)
        {
            int stu_type_id;
            frm_export_choice frm_ex = new frm_export_choice();        
            frm_ex.ShowDialog();


            if (frm_ex.tr)
            {
                if (frm_ex.stu_id == 1)
                {

                    stu_type_id = 1;
                }
                else
                {
                    stu_type_id = 2;
                }


                var products = new System.Data.DataTable("user");



                products.Columns.Add("ល.រ", typeof(int));
                products.Columns.Add("លេខតុ", typeof(string));
                products.Columns.Add("គោត្តនាម និងនាម", typeof(string));
                products.Columns.Add("អក្សរឡាតាំង", typeof(string));
                products.Columns.Add("ភេទ", typeof(string));
                products.Columns.Add("ថ្ងៃខែឆ្នាំកំណើត", typeof(string));
                products.Columns.Add("សញ្ញាប័ត្រ", typeof(string));
                products.Columns.Add("ឯកទេស", typeof(string));
                products.Columns.Add("លេខទូរស័ព្ទ", typeof(string));
                products.Columns.Add("ស្ថានភាព", typeof(string));
                products.Columns.Add("ផ្នែក", typeof(string));
                products.Columns.Add("ទីតាំង", typeof(string));
                products.Columns.Add("ថ្ងៃតាំងស៊ប់",typeof(string));

                var emp = ClsConfig.glo_local_db.pro_user_detail(stu_type_id).ToList();
                int j = 0;
                foreach (var i in emp)
                {
                    j++;

                    string off_date ="";

                    string date = Convert.ToDateTime(i.DateOfBirth).ToString("dd.MM.yyyy");

                    if (stu_type_id == 1)
                    {
                       off_date = Convert.ToDateTime(i.off_start_date).ToString("dd.MM.yyyy");
                    }

                    else
                    {
                        off_date = "គ្មាន";
                    }

                    products.Rows.Add(j, i.skill_note, i.Name_khmer, i.Name_latin, i.GenderKH, date, i.certificate_name, i.cert_skill_name, "Tel: " + i.Phone, i.status_discribe, i.skill, i.bra_name, off_date);
                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Execl files (*.xls)|*.xls";
                saveFileDialog.FilterIndex = 0;
                saveFileDialog.RestoreDirectory = true;

                saveFileDialog.Title = "Export Excel File To";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    ClsExcell.ExportToExcel(products, saveFileDialog.FileName);
                }
            }
  
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_student_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                frm_registration_finger frm = new frm_registration_finger();
                frm.user_id = (int)dgv_student.CurrentRow.Cells[0].Value;
                frm.ShowDialog();
                frm_student_Load(null, null);
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 3)
            {
                frm_registration_finger_info frm = new frm_registration_finger_info();
                frm.user_id = (int)dgv_student.CurrentRow.Cells[0].Value;
                frm.ShowDialog();
            }
          
        }

        private void frm_student_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }
    }
}
