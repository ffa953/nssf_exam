﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_subject_edit_ : Form
    {
        public frm_subject_edit_()
        {
            InitializeComponent();
        }
        int auto_id;

        void Load_Record()
        {
            var query = ClsConfig.glo_local_db.vw_select_subject_in_skill.AsNoTracking().ToList();
            dgvlst.DataSource = query;
        }
        void Load_Subject()
        {
            var query = ClsConfig.glo_local_db.tbl_subject.ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            cbo_subject.SelectedIndex = -1;
        }
        void Load_Skill()
        {
            var query = ClsConfig.glo_local_db.tbl_skill.ToList();
            cbo_skill.DataSource = query;
            cbo_skill.DisplayMember = "skill";
            cbo_skill.ValueMember = "skill_id";
            cbo_skill.SelectedIndex = -1;
        }

        bool Duplicate_Record()
        {
            int skill_id=Convert.ToInt32(cbo_skill.SelectedValue);
            int sub_id=Convert.ToInt32(cbo_subject.SelectedValue);
            var query = ClsConfig.glo_local_db.tbl_subject_in_skill.Where(s => s.skill_id == skill_id && s.sub_id == sub_id && s.auto_id != auto_id).ToList();
            if (query.Count > 0)
            {
                ClsMsg.Warning("ជំនាញៈ "+cbo_skill.Text+"\n"+"មុខវិជ្ជាៈ "+cbo_subject.Text+"\n"+"បានបញ្ចូលរូចម្តងហើយ!");
                return true;
            }
            else { return false; }
        }

        bool Existing_Ctrl()
        {
            string str = "";
            bool result = false;
            if (cbo_skill.SelectedIndex == -1 || string.IsNullOrEmpty(cbo_skill.Text.Trim()))
            {
                str += "- សូមជ្រើសរើសជំនាញ!"+"\n";
            }
            if (cbo_subject.SelectedIndex == -1 || string.IsNullOrEmpty(cbo_subject.Text.Trim()))
            {
                str += "- សូមជ្រើសរើសមុខវិជ្ជា!"+"\n";
            }
            if (string.IsNullOrEmpty(txt_answer_num.Text.Trim()))
            {
                str += "- សូមបំពេញចំនួនសំនួរ!"+"\n";
            }
            if (!string.IsNullOrEmpty(str))
            {
                result = true;
                ClsMsg.Warning(str);
            }
            return result;
        }

        private void frm_subject_edit__Load(object sender, EventArgs e)
        {
                ClsSetting.sty_btn_add(btn_add);
                ClsSetting.sty_btn_close(btn_clear);
                ClsSetting.sty_dgv(dgvlst);
                Load_Record();
                Load_Skill();
                //Load_Subject();
        }

        private void dgvlst_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(btn_add.Tag) == 1) 
            {
                if (!Existing_Ctrl())
                {
                        try
                        {
                            tbl_subject_in_skill sk = new tbl_subject_in_skill();
                            sk.skill_id = (int)cbo_skill.SelectedValue;
                            sk.sub_id = (int)cbo_subject.SelectedValue;
                            sk.quiz_limit = Convert.ToInt32(txt_answer_num.Text.Trim());
                            ClsConfig.glo_local_db.tbl_subject_in_skill.Add(sk);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            Load_Record();
                            cbo_subject.DataSource = null;
                            cbo_skill.SelectedIndex = -1;
                            txt_answer_num.Clear();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                }
            } 
            else if (Convert.ToInt32(btn_add.Tag) == 2) 
            {
                if (!string.IsNullOrEmpty(txt_answer_num.Text.Trim()))
                {
                    if (auto_id != 0)
                    {
                        if (!Duplicate_Record())
                        {
                            if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                            {
                                try
                                {
                                    tbl_subject_in_skill sbk = ClsConfig.glo_local_db.tbl_subject_in_skill.Where(s => s.auto_id == auto_id).Single();
                                    sbk.quiz_limit = Convert.ToInt32(txt_answer_num.Text.Trim());
                                    sbk.sub_id = Convert.ToInt32(cbo_subject.SelectedValue);
                                    ClsConfig.glo_local_db.SaveChanges();
                                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                                    Load_Record();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void cbo_skill_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int skill_id = Convert.ToInt32(cbo_skill.SelectedValue);
            var query = (from st in ClsConfig.glo_local_db.tbl_subject where !(from x in ClsConfig.glo_local_db.tbl_subject_in_skill where x.skill_id == skill_id select x.sub_id).Contains(st.sub_id) && st.sub_active==true select st).ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            //cbo_subject.SelectedIndex = -1;
        }

        private void dgvlst_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvlst.Cursor = e.ColumnIndex == 0 && e.RowIndex > -1 ? Cursors.Hand : Cursors.Default;
        }

        private void dgvlst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == dgvlst.Columns["col_edit"].Index)
            {
                Load_Subject();
                DataGridViewRow row = dgvlst.CurrentRow;
                auto_id = Convert.ToInt32(row.Cells["col_auto_id"].Value);
                cbo_skill.SelectedValue = row.Cells["col_skill_id"].Value;
                cbo_subject.SelectedValue = row.Cells["col_sub_id"].Value;
                txt_answer_num.Text = row.Cells["col_question"].Value.ToString();
                cbo_skill.Enabled = false;
                ClsSetting.sty_btn_save(btn_add);
                ClsSetting.sty_btn_cancel(btn_clear);
                btn_clear.Tag = 2;
                btn_add.Tag = 2;
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(btn_clear.Tag) == 1)
            {
                this.Close();
            }
            else  if (Convert.ToInt32(btn_clear.Tag) == 2)
            {
                ClsSetting.sty_btn_close(btn_clear);
                ClsSetting.sty_btn_add(btn_add);
                btn_clear.Tag = 1;
                btn_add.Tag = 1;
                txt_answer_num.Clear();
                cbo_skill.Enabled = true;
                cbo_subject.DataSource = null;
                cbo_skill.SelectedIndex = -1;
            }
        }
    }
}
