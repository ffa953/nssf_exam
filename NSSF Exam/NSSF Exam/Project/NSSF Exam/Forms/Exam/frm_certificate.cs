﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_certificate : Form
    {
        public frm_certificate()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this,"ពត៍មានសញ្ញាប័ត្រ");
        }

        private void frm_certificate_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_certificate);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            bds_certificate.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_certificate_edit frm = new frm_certificate_edit();
            frm.ShowDialog();
            frm_certificate_Load(null, null);
        }

        private void dgv_certificate_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_certificate_edit frm = new frm_certificate_edit();
                frm.cert_id = (int)dgv_certificate.CurrentRow.Cells["certificate_id"].Value;
                frm.ShowDialog();
                frm_certificate_Load(null, null);
            }
        }

        private void frm_certificate_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }
    }
}
