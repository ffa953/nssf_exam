﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_subject_edit : Form
    {
        public int sub_id = 0;
        int subID = 0;
        int row_index;
        public frm_subject_edit()
        {
            InitializeComponent();
        }

        private void frm_subject_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_dgv(dgv_sub);
            ClsSetting.sty_btn_clear(btn_clear);
            bds_skill.DataSource = ClsConfig.glo_local_db.tbl_skill.Where(m=>m.skill_active==true).ToList();
            cbo_skill.SelectedIndex = -1;
            chk_active.Checked = true;

            if(sub_id>0){

                var sub = ClsConfig.glo_local_db.tbl_subject.Find(sub_id);
                txt_subtitle.Text = sub.sub_title;
                chk_active.Checked =(bool)sub.sub_active;

                bds_subject_skill.DataSource = (from sk in ClsConfig.glo_local_db.tbl_subject_in_skill where sk.sub_id==sub_id select sk).ToList();

            }
           
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            try
            {
                if (sub_id > 0)
                {
                    TransactionScope tran = new TransactionScope(TransactionScopeOption.Required);

                    var sub = ClsConfig.glo_local_db.tbl_subject.Find(sub_id);
                    sub.sub_title = txt_subtitle.Text;
                    sub.sub_active = (chk_active.Checked) ? true : false;
                    sub.user_id = ClsConfig.glo_use_id;
                    sub.user_edit = ClsFun.getCurrentDate();
                    ClsConfig.glo_local_db.SaveChanges();

                    var sub_sk = (from sk in ClsConfig.glo_local_db.tbl_subject_in_skill where sk.sub_id==sub.sub_id select sk).ToList();
                    foreach (var i in sub_sk)
                    {
                        ClsConfig.glo_local_db.tbl_subject_in_skill.Remove(i);
                        ClsConfig.glo_local_db.SaveChanges();
                    }

                    foreach (tbl_subject_in_skill i in bds_subject_skill)
                    {
                        tbl_subject_in_skill sub_skill = new tbl_subject_in_skill();
                        sub_skill.skill_id = i.skill_id;
                        sub_skill.quiz_limit = i.quiz_limit;
                        sub_skill.sub_id = sub.sub_id;
                        ClsConfig.glo_local_db.tbl_subject_in_skill.Add(sub_skill);
                        ClsConfig.glo_local_db.SaveChanges();
                    }

                    tran.Complete();
                    tran.Dispose();

                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សារទុក។");
                    this.Close();
                }
                else
                {
                    if(IsFieldValidate()){
                        TransactionScope tran = new TransactionScope(TransactionScopeOption.Required);

                        tbl_subject sub = new tbl_subject();
                        sub.sub_title = txt_subtitle.Text;
                        sub.sub_active = (chk_active.Checked) ? true : false;
                        sub.user_id = ClsConfig.glo_use_id;
                        sub.user_edit = ClsFun.getCurrentDate();
                        ClsConfig.glo_local_db.tbl_subject.Add(sub);
                        ClsConfig.glo_local_db.SaveChanges();

                        foreach(tbl_subject_in_skill i in bds_subject_skill)
                        {
                            tbl_subject_in_skill sub_skill = new tbl_subject_in_skill();
                            sub_skill.skill_id = i.skill_id;
                            sub_skill.quiz_limit = i.quiz_limit;
                            sub_skill.sub_id = sub.sub_id;
                            ClsConfig.glo_local_db.tbl_subject_in_skill.Add(sub_skill);
                            ClsConfig.glo_local_db.SaveChanges();
                        }

                        tran.Complete();
                        tran.Dispose();

                        txt_subtitle.Text = "";
                        cbo_skill.SelectedIndex = -1;
                        txt_answer_num.Text = "";
                        bds_subject_skill.DataSource = null;
                        ClsMsg.Success("ពត៍មានត្រូវបានរក្សារទុក។");
                    }
                }
            }
            catch(Exception ex){
                ClsMsg.Success("" + ex);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {

            if (subID > 0)
            {
                tbl_subject_in_skill sub = new tbl_subject_in_skill();
                sub.skill_id = Convert.ToInt32(cbo_skill.SelectedValue);
                sub.quiz_limit = Convert.ToInt32(txt_answer_num.Text);
                bds_subject_skill.Insert(row_index, sub);
                bds_subject_skill.RemoveAt(row_index + 1);

                btn_add.Text = "បន្ថែមថ្មី";
                txt_answer_num.Text = "";
                subID = 0;
                cbo_skill.SelectedIndex = -1; ;
            }
            else
            {


                if (IsFieldValidate())
                {
                    if (bds_subject_skill.Count != 0)
                    {
                        foreach (tbl_subject_in_skill i in bds_subject_skill)
                        {
                            if (i.skill_id == (int)cbo_skill.SelectedValue)
                            {
                                ClsMsg.Warning("ជំនាញនេះមានរួចហើយ!");
                                return;
                            }

                        }

                        if (IsFieldValidate())
                        {
                            tbl_subject_in_skill sub = new tbl_subject_in_skill();
                            sub.skill_id = Convert.ToInt32(cbo_skill.SelectedValue);
                            sub.quiz_limit = Convert.ToInt32(txt_answer_num.Text);
                            bds_subject_skill.Add(sub);

                            cbo_skill.SelectedIndex = -1;
                            txt_answer_num.Text = "";
                        }
                    }
                    else
                    {
                        tbl_subject_in_skill sub = new tbl_subject_in_skill();
                        sub.skill_id = Convert.ToInt32(cbo_skill.SelectedValue);
                        sub.quiz_limit = Convert.ToInt32(txt_answer_num.Text);
                        bds_subject_skill.Add(sub);
                    }
                }
            }
        }

       

        private bool IsFieldValidate()
        {
            string msgString = "";
            if (txt_subtitle.Text == "")
            {
                msgString += "- មុខវិជ្ជាត្រូវតែបញ្ចូល\n";
            }
          
            if (txt_answer_num.Text == "")
            {
                msgString += "- ចំនួនសំនួរត្រូវតែបញ្ចូល\n";
            }

            if(cbo_skill.SelectedItem == null)
            {
                msgString += "- ជំនាញត្រូវតែបញ្ជូល\n";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void dgv_sub_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                bds_subject_skill.RemoveAt(e.RowIndex);
                btn_add.Text = "បន្ថែមថ្មី";
                txt_answer_num.Text = "";
                cbo_skill.SelectedIndex = -1; ;
                subID = 0;
            }
            else
            {
                cbo_skill.SelectedValue = dgv_sub.Rows[e.RowIndex].Cells["skill_id"].Value;
                txt_answer_num.Text = dgv_sub.Rows[e.RowIndex].Cells["quiz_limit"].Value.ToString().Trim();
                subID = 1;
                row_index = e.RowIndex;
                btn_add.Text="កែប្រែ";

            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            btn_add.Text = "បន្ថែមថ្មី";
            txt_answer_num.Text = "";
            subID = 0;
            cbo_skill.SelectedIndex = -1; ;
        }
    }
}
