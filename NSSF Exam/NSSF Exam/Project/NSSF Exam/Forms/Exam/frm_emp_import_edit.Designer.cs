﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_emp_import_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label3;
            System.Windows.Forms.Label Div_idLabel;
            System.Windows.Forms.Label Pos_idLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_emp = new System.Windows.Forms.Label();
            this.cbo_division = new System.Windows.Forms.ComboBox();
            this.bds_division = new System.Windows.Forms.BindingSource(this.components);
            this.cbo_branch = new System.Windows.Forms.ComboBox();
            this.bds_branch = new System.Windows.Forms.BindingSource(this.components);
            this.cbo_pos = new System.Windows.Forms.ComboBox();
            this.bds_position = new System.Windows.Forms.BindingSource(this.components);
            this.dt_start_date = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            Label3 = new System.Windows.Forms.Label();
            Div_idLabel = new System.Windows.Forms.Label();
            Pos_idLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_division)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_position)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label3
            // 
            Label3.AutoSize = true;
            Label3.Location = new System.Drawing.Point(13, 76);
            Label3.Name = "Label3";
            Label3.Size = new System.Drawing.Size(123, 24);
            Label3.TabIndex = 29;
            Label3.Text = "កាលបរិច្ឆេទចូលធ្វើការ";
            // 
            // Div_idLabel
            // 
            Div_idLabel.AutoSize = true;
            Div_idLabel.Location = new System.Drawing.Point(19, 218);
            Div_idLabel.Name = "Div_idLabel";
            Div_idLabel.Size = new System.Drawing.Size(71, 24);
            Div_idLabel.TabIndex = 31;
            Div_idLabel.Text = "ការិយាល័យ";
            // 
            // Pos_idLabel
            // 
            Pos_idLabel.AutoSize = true;
            Pos_idLabel.Location = new System.Drawing.Point(19, 127);
            Pos_idLabel.Name = "Pos_idLabel";
            Pos_idLabel.Size = new System.Drawing.Size(37, 24);
            Pos_idLabel.TabIndex = 33;
            Pos_idLabel.Text = "ឋានៈ";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 28);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(112, 24);
            label1.TabIndex = 34;
            label1.Text = "ចំនួនបុគ្គលិក ឬមន្រ្ដី";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(19, 170);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(110, 24);
            label2.TabIndex = 35;
            label2.Text = "ទីស្នាក់ការ​ ប.ស.ស";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_emp);
            this.groupBox1.Controls.Add(this.cbo_division);
            this.groupBox1.Controls.Add(this.cbo_branch);
            this.groupBox1.Controls.Add(this.cbo_pos);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(Pos_idLabel);
            this.groupBox1.Controls.Add(Div_idLabel);
            this.groupBox1.Controls.Add(this.dt_start_date);
            this.groupBox1.Controls.Add(Label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(367, 270);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lb_emp
            // 
            this.lb_emp.AutoSize = true;
            this.lb_emp.Location = new System.Drawing.Point(152, 28);
            this.lb_emp.Name = "lb_emp";
            this.lb_emp.Size = new System.Drawing.Size(42, 24);
            this.lb_emp.TabIndex = 41;
            this.lb_emp.Text = "label4";
            // 
            // cbo_division
            // 
            this.cbo_division.DataSource = this.bds_division;
            this.cbo_division.DisplayMember = "div_name_khmer";
            this.cbo_division.DropDownHeight = 100;
            this.cbo_division.FormattingEnabled = true;
            this.cbo_division.IntegralHeight = false;
            this.cbo_division.Location = new System.Drawing.Point(154, 210);
            this.cbo_division.Name = "cbo_division";
            this.cbo_division.Size = new System.Drawing.Size(202, 32);
            this.cbo_division.TabIndex = 40;
            this.cbo_division.ValueMember = "div_id";
            // 
            // bds_division
            // 
            this.bds_division.DataSource = typeof(NSSF_Exam.Entities.vie_division);
            // 
            // cbo_branch
            // 
            this.cbo_branch.DataSource = this.bds_branch;
            this.cbo_branch.DisplayMember = "bra_name_khmer";
            this.cbo_branch.DropDownHeight = 100;
            this.cbo_branch.FormattingEnabled = true;
            this.cbo_branch.IntegralHeight = false;
            this.cbo_branch.Location = new System.Drawing.Point(154, 162);
            this.cbo_branch.Name = "cbo_branch";
            this.cbo_branch.Size = new System.Drawing.Size(202, 32);
            this.cbo_branch.TabIndex = 39;
            this.cbo_branch.ValueMember = "bra_id";
            this.cbo_branch.SelectedIndexChanged += new System.EventHandler(this.cbo_branch_SelectedIndexChanged);
            this.cbo_branch.SelectionChangeCommitted += new System.EventHandler(this.cbo_branch_SelectionChangeCommitted);
            // 
            // bds_branch
            // 
            this.bds_branch.DataSource = typeof(NSSF_Exam.Entities.vw_branch);
            // 
            // cbo_pos
            // 
            this.cbo_pos.DataSource = this.bds_position;
            this.cbo_pos.DisplayMember = "pos_description";
            this.cbo_pos.DropDownHeight = 100;
            this.cbo_pos.FormattingEnabled = true;
            this.cbo_pos.IntegralHeight = false;
            this.cbo_pos.Location = new System.Drawing.Point(154, 119);
            this.cbo_pos.Name = "cbo_pos";
            this.cbo_pos.Size = new System.Drawing.Size(202, 32);
            this.cbo_pos.TabIndex = 38;
            this.cbo_pos.ValueMember = "pos_id";
            // 
            // bds_position
            // 
            this.bds_position.DataSource = typeof(NSSF_Exam.Entities.vw_position);
            // 
            // dt_start_date
            // 
            this.dt_start_date.CustomFormat = "dd/MM/yyyy";
            this.dt_start_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_start_date.Location = new System.Drawing.Point(154, 70);
            this.dt_start_date.Name = "dt_start_date";
            this.dt_start_date.Size = new System.Drawing.Size(202, 32);
            this.dt_start_date.TabIndex = 28;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 291);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(391, 64);
            this.panel1.TabIndex = 1;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(277, 15);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 3;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save.Location = new System.Drawing.Point(167, 15);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(100, 34);
            this.btn_save.TabIndex = 2;
            this.btn_save.Text = "button1";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // frm_emp_import_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 355);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_emp_import_edit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ទាញយកទិន្ន័យបុគ្គលិក ឬមន្រ្ដី";
            this.Load += new System.EventHandler(this.frm_emp_import_edit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_division)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_position)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.DateTimePicker dt_start_date;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.BindingSource bds_position;
        private System.Windows.Forms.BindingSource bds_branch;
        private System.Windows.Forms.BindingSource bds_division;
        private System.Windows.Forms.ComboBox cbo_division;
        private System.Windows.Forms.ComboBox cbo_branch;
        private System.Windows.Forms.ComboBox cbo_pos;
        private System.Windows.Forms.Label lb_emp;
    }
}