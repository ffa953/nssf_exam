﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_skill : Form
    {
        public frm_skill()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this,"ពត៍មានផ្នែក");
        }

        private void frm_skill_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_skill);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            bds_skill.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_skill_edit frm = new frm_skill_edit();
            frm.ShowDialog();
        }

        private void dgv_skill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_skill_edit frm = new frm_skill_edit();
                frm.skill_id = (int)dgv_skill.CurrentRow.Cells["skill_id"].Value;             
                frm.ShowDialog();
                frm_skill_Load(null, null);
            }
        }

        private void frm_skill_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

       
    }
}
