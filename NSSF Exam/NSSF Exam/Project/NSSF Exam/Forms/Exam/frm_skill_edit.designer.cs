﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_skill_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_skill = new System.Windows.Forms.TextBox();
            this.txt_skill_note = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_quiz_type = new System.Windows.Forms.ComboBox();
            this.bds_quiz_type = new System.Windows.Forms.BindingSource(this.components);
            this.chk_box = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bds_quiz_type)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "ផ្នែក";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "អក្សរកាត់";
            // 
            // txt_skill
            // 
            this.txt_skill.BackColor = System.Drawing.SystemColors.Info;
            this.txt_skill.Location = new System.Drawing.Point(131, 47);
            this.txt_skill.Name = "txt_skill";
            this.txt_skill.Size = new System.Drawing.Size(190, 32);
            this.txt_skill.TabIndex = 1;
            // 
            // txt_skill_note
            // 
            this.txt_skill_note.BackColor = System.Drawing.SystemColors.Info;
            this.txt_skill_note.Location = new System.Drawing.Point(131, 94);
            this.txt_skill_note.Name = "txt_skill_note";
            this.txt_skill_note.Size = new System.Drawing.Size(190, 32);
            this.txt_skill_note.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "ប្រភេទសំនួរ";
            // 
            // cbo_quiz_type
            // 
            this.cbo_quiz_type.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_quiz_type.DataSource = this.bds_quiz_type;
            this.cbo_quiz_type.DisplayMember = "type_name";
            this.cbo_quiz_type.FormattingEnabled = true;
            this.cbo_quiz_type.Location = new System.Drawing.Point(131, 142);
            this.cbo_quiz_type.Name = "cbo_quiz_type";
            this.cbo_quiz_type.Size = new System.Drawing.Size(190, 32);
            this.cbo_quiz_type.TabIndex = 5;
            this.cbo_quiz_type.ValueMember = "type_id";
            // 
            // bds_quiz_type
            // 
            this.bds_quiz_type.DataSource = typeof(NSSF_Exam.Entities.tbl_quiz_type);
            // 
            // chk_box
            // 
            this.chk_box.AutoSize = true;
            this.chk_box.Location = new System.Drawing.Point(46, 203);
            this.chk_box.Name = "chk_box";
            this.chk_box.Size = new System.Drawing.Size(81, 28);
            this.chk_box.TabIndex = 7;
            this.chk_box.Text = "ដំណើរការ";
            this.chk_box.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 254);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(371, 69);
            this.panel1.TabIndex = 8;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(258, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(148, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // frm_skill_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 323);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chk_box);
            this.Controls.Add(this.cbo_quiz_type);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_skill_note);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_skill);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_skill_edit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ពត៍មានផ្នែក";
            this.Load += new System.EventHandler(this.frm_skill_edit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bds_quiz_type)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_skill;
        private System.Windows.Forms.TextBox txt_skill_note;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_quiz_type;
        private System.Windows.Forms.CheckBox chk_box;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.BindingSource bds_quiz_type;
    }
}