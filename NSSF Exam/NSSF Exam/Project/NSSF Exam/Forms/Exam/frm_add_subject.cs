﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_add_subject : Form
    {
        public frm_add_subject(int sub_id,string subject,bool sub_active,short load_type)
        {
            InitializeComponent();
            txt_subtitle.Text = subject;
            txt_subtitle.Tag = sub_id;
            chk_active.Checked = sub_active;
            this.load_type = load_type;
        }
        public frm_add_subject()
        {
            InitializeComponent();
        }

        public bool is_inserted = false;
        short load_type=1;

        private void frm_add_subject_Load(object sender, EventArgs e)
        {
            if (load_type == 1) 
            {
                ClsSetting.sty_btn_add(btn_add);
                btn_add.Tag=1;
            }
            else
            {
                ClsSetting.sty_btn_save(btn_add);
                btn_add.Tag=2;
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txt_subtitle.Text.Trim()))
            {
                if (Convert.ToInt32(btn_add.Tag) == 1)
                {
                    var sub = ClsConfig.glo_local_db.tbl_subject.Where(s => s.sub_title.Trim().ToLower().Equals(txt_subtitle.Text.Trim().ToLower())).ToList();
                    if (sub.Count == 0)
                    {
                        try
                        {
                            tbl_subject sbj = new tbl_subject();
                            sbj.sub_title = txt_subtitle.Text.Trim();
                            sbj.sub_active = chk_active.Checked;
                            sbj.user_id = ClsConfig.glo_use_id;
                            sbj.user_edit = ClsFun.getCurrentDate();
                            ClsConfig.glo_local_db.tbl_subject.Add(sbj);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_inserted = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else { ClsMsg.Warning("ឈ្មោះមុខវិជ្ជានេះមានរួចម្តងហើយ!"); }
                }
                else if(Convert.ToInt32(btn_add.Tag)==2)
                {
                    int sub_id = Convert.ToInt32(txt_subtitle.Tag);
                    var query = ClsConfig.glo_local_db.tbl_subject.Where(s => s.sub_title.Trim().ToLower().Equals(txt_subtitle.Text.Trim().ToLower()) && s.sub_id != sub_id).ToList();
                    if (query.Count == 0)
                    {
                        if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែឈ្មោះមុខវិជ្ជានេះមែនឬទេ?") == true)
                        {
                            try
                            {
                                tbl_subject sub = ClsConfig.glo_local_db.tbl_subject.Where(s => s.sub_id == sub_id).Single();
                                sub.sub_title = txt_subtitle.Text.Trim();
                                sub.sub_active = chk_active.Checked;
                                ClsConfig.glo_local_db.SaveChanges();
                                ClsMsg.Success(ClsMsg.STR_SUCCESS);
                                is_inserted = true;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            else { ClsMsg.Warning("សូមបញ្ចូលមុខវិជ្ជាអោយបានត្រឹមត្រូវ!"); }
        }
    }
}
