﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;
using System.Text.RegularExpressions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_typing_edit : Form
    {
        public int typ_id=0;
        public frm_typing_edit()
        {
            InitializeComponent();
        }

        private void frm_typing_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            if(typ_id>0)
            {
                var type = (from t in ClsConfig.glo_local_db.tbl_typing where t.typ_id==typ_id select t).Single();
                txt_title.Text = type.title;
                txt_score.Text = type.score.ToString();
                chk_active.Checked =(bool)type.active;
                txt_topic.Text = Regex.Replace(type.description.Trim(), "<.*?>", " ").Trim();                          
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {

            try
            {
                
                if(typ_id>0)
                {
                    var type = (from t in ClsConfig.glo_local_db.tbl_typing where t.typ_id == typ_id select t).Single();

                    type.title = txt_title.Text;

                    string[] arr_str = txt_topic.Text.Split(' ');
                    var descr = "";

                    int i = 0;
                    foreach (string str in arr_str)
                    {
                        if (str != "")
                        {
                            if (i == 0)
                            {
                                descr += "<span class='letter active' data-id='" + i + "'>" + str + "</span>";
                            }
                            else
                            {
                                descr += "<span class='letter' data-id='" + i + "'>" + str + "</span>";
                            }
                            i++;
                        }

                    }

                    type.description = descr;
                    type.score = Convert.ToInt32(txt_score.Text);
                    type.active = (chk_active.Checked) ? true : false;
                    type.user_id = ClsConfig.glo_use_id;
                    type.user_edit = ClsFun.getCurrentDate();                  
                    ClsConfig.glo_local_db.SaveChanges();

                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");

                    this.Close();
                }
                else
                {
                    if (IsFieldValidate())
                    {
                        string[] arr_str = txt_topic.Text.Split(' ');
                        var descr = "";

                        int i = 0;
                        foreach (string str in arr_str)
                        {
                            if (str != "")
                            {
                                if (i == 0)
                                {
                                    descr += "<span class='letter active' data-id='" + i + "'>" + str + "</span>";
                                }
                                else
                                {
                                    descr += "<span class='letter' data-id='" + i + "'>" + str + "</span>";
                                }
                                i++;
                            }

                        }

                        tbl_typing typing = new tbl_typing();
                        typing.title = txt_title.Text;
                        typing.description = descr;
                        typing.score = Convert.ToInt32(txt_score.Text);
                        typing.active = (chk_active.Checked) ? true : false;
                        typing.user_id = ClsConfig.glo_use_id;
                        typing.user_edit = ClsFun.getCurrentDate();
                        ClsConfig.glo_local_db.tbl_typing.Add(typing);
                        ClsConfig.glo_local_db.SaveChanges();

                        ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");

                        txt_title.Text = "";
                        txt_score.Text = "";
                        txt_topic.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                ClsMsg.Success(""+ex);
            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";
            if (txt_title.Text == "")
            {
                msgString += "- ចំណងជើងត្រូវតែបញ្ចូល\n";
            }

            if (txt_score.Text == "")
            {
                msgString += "- ពិន្ទុត្រូវតែបញ្ចូល\n";
            }

            if (txt_topic .Text == "")
            {
                msgString += "- អត្ថបទត្រូវតែបញ្ជូល\n";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
