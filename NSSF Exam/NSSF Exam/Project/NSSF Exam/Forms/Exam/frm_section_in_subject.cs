﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_section_in_subject : Form
    {
        public frm_section_in_subject()
        {
            InitializeComponent();
        }
        int sct_id;

        void Load_Record(int sub_id)
        {
            var query = ClsConfig.glo_local_db.tbl_section_in_subject.Where(s=>s.sub_id==sub_id).ToList();
            dgvlst.Rows.Clear();
            foreach (var item in query)
            {
                dgvlst.Rows.Add(null,null,item.sct_id,item.sub_id,item.sct_title,item.sct_active);
            }
        }

        private void frm_section_in_subject_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_btn_close(btn_clear);
            ClsSetting.sty_dgv(dgvlst);
            //Load_Subject();
            rad_all.Checked = true;
            rad_all_CheckedChanged(null, null);
        }

        bool Existing_Ctrl()
        {
            bool result = false;
            string str = "";
            if (string.IsNullOrEmpty(cbo_subject.Text.Trim()) || cbo_subject.SelectedIndex == -1)
            {
                str += "- សូមជ្រើសរើសមុខវិជ្ជា!\n";
            }
            if (string.IsNullOrEmpty(txt_section_title.Text.Trim()))
            {
                str += "- សូមបំពេញផ្នែកនៃមុខវិជ្ជា!";
            }
            if (!string.IsNullOrEmpty(str))
            {
                ClsMsg.Warning(str);
                result = true;
            }
            return result;
        }

        bool Duplicate_Section()
        {
            var query = ClsConfig.glo_local_db.tbl_section_in_subject.Where(s => s.sct_title.Trim().ToLower().Equals(txt_section_title.Text.Trim().ToLower())).ToList();
            if (sct_id != 0) { query = query.Where(s=>s.sct_id != sct_id).ToList(); }
            return query.Count > 0 ? true : false;
        }

        void Load_Subject()
        {
            var query = ClsConfig.glo_local_db.tbl_subject.ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            cbo_subject.SelectedIndex = -1;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (!Existing_Ctrl()) 
            {
                if (!Duplicate_Section())
                {
                    if (Convert.ToInt32(btn_add.Tag) == 1)
                    {
                        tbl_section_in_subject sct = new tbl_section_in_subject();
                        sct.sub_id = Convert.ToInt32(cbo_subject.SelectedValue);
                        sct.sct_title = txt_section_title.Text.Trim();
                        sct.sct_active = chk_active.Checked;
                        ClsConfig.glo_local_db.tbl_section_in_subject.Add(sct);
                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success(ClsMsg.STR_SUCCESS);
                        cbo_subject_SelectionChangeCommitted(null, null);
                    }
                    else if(Convert.ToInt32(btn_add.Tag)==2)
                    {
                        if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                        {
                            tbl_section_in_subject sct = ClsConfig.glo_local_db.tbl_section_in_subject.Where(s => s.sct_id == sct_id).Single();
                            sct.sct_title = txt_section_title.Text.Trim();
                            sct.sct_active = chk_active.Checked;
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            cbo_subject_SelectionChangeCommitted(null, null);
                        }
                    }
                }
                else { ClsMsg.Warning("ផ្នែក: "+txt_section_title.Text.Trim()+"\nមុខវិជ្ជា: "+cbo_subject.Text+"\nមានរួចម្តងហើយ!"); }
            }
        }

        private void dgvlst_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void dgvlst_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void cbo_subject_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Load_Record(Convert.ToInt32(cbo_subject.SelectedValue));
        }

        private void rad_subject_no_sct_CheckedChanged(object sender, EventArgs e)
        {
            var query = (from st in ClsConfig.glo_local_db.tbl_subject where !(from x in ClsConfig.glo_local_db.tbl_section_in_subject select x.sub_id).Contains(st.sub_id) && st.sub_active == true select st).ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            cbo_subject.SelectedIndex = -1;
            txt_section_title.Clear();
            dgvlst.Rows.Clear();
        }

        private void rad_subject_have_sct_CheckedChanged(object sender, EventArgs e)
        {
            var  query = (from st in ClsConfig.glo_local_db.tbl_subject where (from x in ClsConfig.glo_local_db.tbl_section_in_subject select x.sub_id).Contains(st.sub_id) && st.sub_active == true select st).ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            cbo_subject.SelectedIndex = -1;
            txt_section_title.Clear();
            dgvlst.Rows.Clear();
        }

        private void rad_all_CheckedChanged(object sender, EventArgs e)
        {
            var query = (from st in ClsConfig.glo_local_db.tbl_subject select st).ToList();
            cbo_subject.DataSource = query;
            cbo_subject.DisplayMember = "sub_title";
            cbo_subject.ValueMember = "sub_id";
            cbo_subject.SelectedIndex = -1;
            txt_section_title.Clear();
            dgvlst.Rows.Clear();
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(btn_clear.Tag) == 1) 
            {
                this.Close();
            }
            else if (Convert.ToInt32(btn_clear.Tag) == 2)
            {
                ClsSetting.sty_btn_close(btn_clear);
                ClsSetting.sty_btn_add(btn_add);
                btn_clear.Tag = 1;
                btn_add.Tag = 1;
                txt_section_title.Clear();
                cbo_subject.SelectedIndex = -1;
                dgvlst.Rows.Clear();
                chk_active.Checked = false;
                rad_all.Enabled = rad_subject_have_sct.Enabled = rad_subject_no_sct.Enabled = true;
                if (!rad_all.Checked)
                {
                    rad_all.Checked = true;
                    rad_all_CheckedChanged(null, null);
                }
            }

        }

        private void dgvlst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvlst.Columns["col_edit"].Index && e.RowIndex > -1)
            {
                txt_section_title.Text = dgvlst.CurrentRow.Cells["col_sct_title"].Value.ToString();
                sct_id = Convert.ToInt32(dgvlst.CurrentRow.Cells["col_sct_id"].Value);
                chk_active.Checked = Convert.ToBoolean(dgvlst.CurrentRow.Cells["col_sct_active"].Value);
                ClsSetting.sty_btn_save(btn_add);
                ClsSetting.sty_btn_cancel(btn_clear);
                btn_add.Tag = 2;
                btn_clear.Tag = 2;
                rad_all.Enabled = rad_subject_have_sct.Enabled = rad_subject_no_sct.Enabled = false;
            }
        }
    }
}
