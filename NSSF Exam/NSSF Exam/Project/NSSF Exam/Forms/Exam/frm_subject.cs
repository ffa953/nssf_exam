﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_subject : Form
    {
        public frm_subject()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "កំណត់មុខវិជ្ជា");
        }

        private void frm_subject_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_dgv(dgv_subject);
            bds_subject.DataSource = ClsConfig.glo_local_db.tbl_subject.ToList();
        }

        private void frm_subject_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_subject_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgv_subject.ClearSelection();
        }

        private void dgv_subject_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_subject.Columns[e.ColumnIndex].Name == "sub_active")
            {
                if ((bool)dgv_subject.Rows[e.RowIndex].Cells["sub_active"].Value == false)
                {
                    e.Value = "មិនដំណើរការ";
                }
                else
                {
                    e.Value = "ដំណើរការ";
                }
            }
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_add_subject frm = new frm_add_subject();
            frm.ShowDialog();
            if (frm.is_inserted) 
            {
                bds_subject.DataSource = ClsConfig.glo_local_db.tbl_subject.ToList();
            }
        }

        private void dgv_subject_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_add_subject frm = new frm_add_subject((int)dgv_subject.CurrentRow.Cells["sub_id"].Value, dgv_subject.CurrentRow.Cells["subject"].Value.ToString(),(bool)dgv_subject.CurrentRow.Cells["sub_active"].Value,2);
                frm.ShowDialog();
            }
        }
    }
}
