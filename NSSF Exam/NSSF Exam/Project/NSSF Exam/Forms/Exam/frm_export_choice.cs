﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_export_choice : Form
    {
        public int stu_id;
        public bool tr;
        public frm_export_choice()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if(rd_off.Checked){
                stu_id =1;
            }

            if(rd_st.Checked){
                stu_id = 2;
            }
            tr = true;
            this.Close();
          
        }

        private void frm_export_choice_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (tr != true)
            {
                tr = false;
            }
        }
    }
}
