﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_student_import_edit : Form
    {
        public int userID;
        public frm_student_import_edit()
        {
            InitializeComponent();
        }

        private void frm_student_import_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_add);
            ClsSetting.sty_btn_close(btn_close);

            bds_sex.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            bds_cert.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            bds_cert_skill.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();
            bds_exam_date.DataSource = ClsConfig.glo_local_db.tbl_exam_date.OrderByDescending(m => m.exam_id).ToList();
            bds_skill.DataSource = ClsConfig.glo_local_db.tbl_skill.Where(m => m.skill_active == true).ToList();

            cbo_cert_id.SelectedIndex = -1;
            cbo_cert_skill_id.SelectedIndex = -1;
            cbo_exam_date_id.SelectedIndex = -1;
            cbo_skill_id.SelectedIndex = -1;

            if(userID>0)
            {
                var stu = ClsConfig.glo_local_db.UserProfiles.Find(userID);
                txt_stu_name.Text = stu.Name_khmer;
                cbo_sex.SelectedValue = stu.GenderId;
                dt_stu_dob.Value = (DateTime)stu.DateOfBirth;
                txt_phone.Text = stu.Phone;
                cbo_cert_id.SelectedValue = stu.certificate_id;
                cbo_cert_skill_id.SelectedValue = stu.cert_skill_id;
                
                txt_stu_name.Enabled = false;
                cbo_sex.Enabled = false;
                dt_stu_dob.Enabled = false;
                txt_phone.Enabled = false;
                cbo_cert_id.Enabled = false;
                cbo_cert_skill_id.Enabled = false;
                

            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e) 
        {
            try
            {
                int sex_id =  Convert.ToInt32(cbo_sex.SelectedValue);
                int exam_id = Convert.ToInt32(cbo_exam_date_id.SelectedValue);
                var stu = (from st in ClsConfig.glo_local_db.UserProfiles where st.exam_id==exam_id && st.Name_khmer.Trim()==txt_stu_name.Text.Trim() && st.GenderId==sex_id && st.DateOfBirth==dt_stu_dob.Value.Date select st).ToList();
                if (stu.Count == 0)
                {
                    if (IsFieldValidate())
                    {
                        var stu_import = ClsConfig.glo_local_db.UserProfiles.Find(userID);
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required))
                        {
                            try
                            {
                                UserProfile us = new UserProfile();
                                var examId = (from ex in ClsConfig.glo_local_db.tbl_exam_date where ex.exam_active == true select ex).FirstOrDefault();

                                us.UserName = "NSSF";
                                us.Name_khmer = stu_import.Name_khmer;
                                us.Name_latin = stu_import.Name_latin;
                                us.GenderId = stu_import.GenderId;
                                us.DateOfBirth = stu_import.DateOfBirth;
                                us.Phone = stu_import.Phone;
                                us.IsActive = true;
                                us.skill_id = Convert.ToInt16(cbo_skill_id.SelectedValue);
                                us.exam_id = Convert.ToInt32(cbo_exam_date_id.SelectedValue);
                                us.certificate_id = stu_import.certificate_id;
                                us.cert_skill_id = stu_import.cert_skill_id;
                                us.nat_id = stu_import.nat_id;
                                us.status_id = stu_import.status_id;
                                us.bra_id = Convert.ToInt32(cbo_branch.SelectedValue);
                                us.LastActivityDate = ClsFun.getCurrentDate();
                                us.off_start_date = stu_import.off_start_date;
                                us.stu_type_id = stu_import.stu_type_id;
                                us.user_id = ClsConfig.glo_use_id;

                                ClsConfig.glo_local_db.UserProfiles.Add(us);
                                ClsConfig.glo_local_db.SaveChanges();



                                var user = (from u in ClsConfig.glo_local_db.UserProfiles where u.UserId == us.UserId select u).FirstOrDefault();
                                user.UserName = cbo_sex.SelectedValue + dt_stu_dob.Value.Date.Year.ToString().Substring(2, 2) + us.UserId;
                                ClsConfig.glo_local_db.SaveChanges();


                                webpages_UsersInRoles web_us = new webpages_UsersInRoles();
                                web_us.UserId = us.UserId;
                                web_us.RoleId = 2;
                                ClsConfig.glo_local_db.webpages_UsersInRoles.Add(web_us);
                                ClsConfig.glo_local_db.SaveChanges();

                                ClsMsg.Success("លេខសំគាល់​៖ " + us.UserName.ToString());

                                var us_finger = ClsConfig.glo_local_db.tbl_user_fingerprint.Find(userID);
                                if (us_finger != null)
                                {
                                    tbl_user_fingerprint fg = new tbl_user_fingerprint();
                                    fg.UserId = us.UserId;
                                    fg.use_finger_left = us_finger.use_finger_left;
                                    fg.use_finger_left_index = us_finger.use_finger_left_index;
                                    fg.use_finger_right = us_finger.use_finger_right;
                                    fg.use_finger_right_index = us_finger.use_finger_right_index;
                                    fg.user_id = ClsConfig.glo_use_id;
                                    fg.user_edit = ClsFun.getCurrentDate();
                                    ClsConfig.glo_local_db.tbl_user_fingerprint.Add(fg);
                                    ClsConfig.glo_local_db.SaveChanges();

                                    tran.Complete();
                                    this.Close();
                                }
                                else
                                {
                                    ClsMsg.Warning("បេក្ខជនពុំមានស្នាមម្រាមដៃ!!!");

                                }


                            }
                            catch (Exception ex)
                            {
                                tran.Dispose();
                                ClsMsg.Error(ex.Message);
                            }


                        }
                    }

                }
                else
                {
                    ClsMsg.Warning("បេក្ខជនមានរួចហើយ!");
                }
            }
            catch(Exception ex)
            {
                ClsMsg.Error(""+ex);
            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (cbo_exam_date_id.SelectedItem == null)
            {
                msgString += "- សូមជ្រើសរើសសម័យប្រលង!\n";
            }
            if (cbo_skill_id.SelectedItem == null)
            {
                msgString += "- សូមជ្រើសរើសផ្នែក!\n";
            }
          

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cbo_skill_id_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_skill_id.SelectedIndex > -1)
            {

                int skillId = (int)cbo_skill_id.SelectedValue;
                bds_place.DataSource = ClsConfig.glo_local_db.tbl_skill_branch.Where(b => b.skill_id == skillId).Select(b => b.tbl_exam_place).ToList();
            }
        }
    }
}
