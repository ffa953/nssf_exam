﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class sub_edit_eq_section_with_question : Form
    {
        public int sct_id;

        public sub_edit_eq_section_with_question()
        {
            InitializeComponent();
        }
        void Load_Record()
        {
            var query = (from st in ClsConfig.glo_local_db.tbl_eq_question where !(from eq in ClsConfig.glo_local_db.tbl_eq_detail select eq.eq_question_id).Contains(st.eq_question_id) select st).ToList();
            foreach (var item in query)
            {
                dgv_question_2.Rows.Add(null,null,item.eq_question_id,item.question);
            }
        }
        void Select_Question() 
        {
            var query = (from eq_d in ClsConfig.glo_local_db.tbl_eq_detail join qs in ClsConfig.glo_local_db.tbl_eq_question on eq_d.eq_question_id equals qs.eq_question_id 
                         where eq_d.eq_sct_id==this.sct_id
                         select new {qs.eq_question_id,qs.question }).ToList();
            foreach (var item in query)
            {
                dgv_question_1.Rows.Add(null,null,item.eq_question_id,item.question);
            }
        }

        private void sub_edit_eq_section_with_question_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_cancel);
            ClsSetting.sty_dgv(dgv_question_2);
            ClsSetting.sty_dgv(dgv_question_1);
            Load_Record();
            Select_Question();
            ClsSetting.rowGrid_AutoNum(dgv_question_1, "col_lr_1");
            ClsSetting.rowGrid_AutoNum(dgv_question_2, "col_lr_2");
        }

        private void dgv_question_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            this.dgv_question_2.Sort(this.dgv_question_2.Columns["col_lr_2"], ListSortDirection.Ascending);
        }

        private void dgv_question_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            this.dgv_question_2.Sort(this.dgv_question_2.Columns["col_lr_2"], ListSortDirection.Ascending);
        }

        private void dgv_question_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dgv_question_2.Columns["col_check_2"].Index)
                {
                    if (Convert.ToBoolean(dgv_question_2.CurrentRow.Cells["col_check_2"].Value) == false)
                    {
                        dgv_question_2.CurrentRow.Cells["col_check_2"].Value = true;
                    }
                    else if (Convert.ToBoolean(dgv_question_2.CurrentRow.Cells["col_check_2"].Value) == true)
                    {
                        dgv_question_2.CurrentRow.Cells["col_check_2"].Value = false;
                    }
                }
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgv_question_2.RowCount; i++)
            {
                DataGridViewRow row=dgv_question_2.Rows[i];
                if (Convert.ToBoolean(row.Cells["col_check_2"].Value) == true)
                {
                    dgv_question_1.Rows.Add(null, row.Cells["col_lr_2"].Value, row.Cells["col_question_id_2"].Value, row.Cells["col_question_2"].Value);
                }
            }
            List<DataGridViewRow> row_delete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgv_question_2.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["col_check_2"];
                if (Convert.ToBoolean(chk.Value) == true)
                {
                    row_delete.Add(row);
                }
            }
            foreach (DataGridViewRow item in row_delete)
            {
                dgv_question_2.Rows.Remove(item);
            }
        }

        private void dgv_question_1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            this.dgv_question_1.Sort(this.dgv_question_1.Columns["col_lr_1"], ListSortDirection.Ascending);
        }

        private void dgv_question_1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            this.dgv_question_1.Sort(this.dgv_question_1.Columns["col_lr_1"], ListSortDirection.Ascending);
        }

        private void btn_remove_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgv_question_1.RowCount; i++)
            {
                DataGridViewRow row = dgv_question_1.Rows[i];
                if (Convert.ToBoolean(row.Cells["col_check_1"].Value) == true)
                {
                    dgv_question_2.Rows.Add(null, row.Cells["col_lr_1"].Value, row.Cells["col_question_id_1"].Value, row.Cells["col_question_1"].Value);
                }
            }
            List<DataGridViewRow> row_delete = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgv_question_1.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["col_check_1"];
                if (Convert.ToBoolean(chk.Value) == true)
                {
                    row_delete.Add(row);
                }
            }
            foreach (DataGridViewRow item in row_delete)
            {
                dgv_question_1.Rows.Remove(item);
            }
        }

        private void dgv_question_1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dgv_question_1.Columns["col_check_1"].Index)
                {
                    if (Convert.ToBoolean(dgv_question_1.CurrentRow.Cells["col_check_1"].Value) == false)
                    {
                        dgv_question_1.CurrentRow.Cells["col_check_1"].Value = true;
                    }
                    else if (Convert.ToBoolean(dgv_question_1.CurrentRow.Cells["col_check_1"].Value) == true)
                    {
                        dgv_question_1.CurrentRow.Cells["col_check_1"].Value = false;
                    }
                }
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                var query = ClsConfig.glo_local_db.tbl_eq_detail.Where(eq => eq.eq_sct_id == this.sct_id).ToList();
                foreach (var item in query)
                {
                    ClsConfig.glo_local_db.tbl_eq_detail.Remove(item);
                    ClsConfig.glo_local_db.SaveChanges();
                }
                tbl_eq_detail tb_eq = new tbl_eq_detail();
                tb_eq.eq_sct_id = this.sct_id;
                for (int i = 0; i < dgv_question_1.RowCount; i++)
                {
                    int q_id = Convert.ToInt32(dgv_question_1.Rows[i].Cells["col_question_id_1"].Value);
                    tb_eq.eq_question_id = q_id;
                    ClsConfig.glo_local_db.tbl_eq_detail.Add(tb_eq);
                    ClsConfig.glo_local_db.SaveChanges();
                }
                ClsMsg.Success(ClsMsg.STR_SUCCESS);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
