﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class sub_eq_question : Form
    {
        public bool is_updated;
        public short load_type;
        public int id;

        public sub_eq_question()
        {
            InitializeComponent();
        }

        private void sub_eq_question_Load(object sender, EventArgs e)
        {
            if (load_type == 1) { this.Text = "បន្ថែមថ្មី"; btn_save.Tag = 1; } else { this.Text = "កែប្រែ"; btn_save.Tag = 2; }
            ClsSetting.sty_btn_save(btn_save);
        }
        bool Duplicate()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_question.Where(eq => eq.question.Equals(txt_question.Text.Trim()) && eq.eq_question_id != id).ToList();
            return query.Count > 0 ? true : false;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(txt_question.Text.Trim()))
            {
                if (!Duplicate())
                {
                    if (Convert.ToInt32(btn_save.Tag) == 1)
                    {
                        try
                        {
                            tbl_eq_question eq = new tbl_eq_question();
                            eq.question = txt_question.Text.Trim();
                            ClsConfig.glo_local_db.tbl_eq_question.Add(eq);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_updated = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else if (Convert.ToInt32(btn_save.Tag) == 2)
                    {
                        if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                        {
                            tbl_eq_question query = ClsConfig.glo_local_db.tbl_eq_question.Where(eq => eq.eq_question_id == id).Single();
                            query.question = txt_question.Text.Trim();
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_updated = true;
                            this.Close();
                        }
                    }
                }
                else { ClsMsg.Warning("សំនួរនេះមានរួចម្តងហើយ!"); }
            }
            else { ClsMsg.Warning("សូមបញ្ចូលសំនួរជាមុនសិន!"); }
        }
    }
}
