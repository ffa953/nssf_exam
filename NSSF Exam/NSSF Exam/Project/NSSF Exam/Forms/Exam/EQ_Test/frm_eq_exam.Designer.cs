﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_eq_exam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_eq_exam));
            this.lbl_mode = new System.Windows.Forms.Label();
            this.pan_mode = new System.Windows.Forms.Panel();
            this.dgv_ans = new System.Windows.Forms.DataGridView();
            this.lbl = new System.Windows.Forms.Label();
            this.pan = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_eq_desc = new System.Windows.Forms.Label();
            this.btnprevious = new System.Windows.Forms.Button();
            this.btnnext = new System.Windows.Forms.Button();
            this.lbl_section_id = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_section = new System.Windows.Forms.Label();
            this.lbl_inc = new System.Windows.Forms.Label();
            this.container = new System.Windows.Forms.FlowLayoutPanel();
            this.pan_mode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ans)).BeginInit();
            this.pan.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_mode
            // 
            this.lbl_mode.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_mode.Font = new System.Drawing.Font("Khmer OS Muol Light", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mode.Location = new System.Drawing.Point(0, 0);
            this.lbl_mode.Name = "lbl_mode";
            this.lbl_mode.Size = new System.Drawing.Size(1271, 70);
            this.lbl_mode.TabIndex = 0;
            this.lbl_mode.Text = "ការតេស្តភាពវៃឆ្លាតនៅក្នុងការគ្រប់គ្រងអារម្មណ៍\r\n( Emotional Intelligence Testing )" +
    "";
            this.lbl_mode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pan_mode
            // 
            this.pan_mode.Controls.Add(this.dgv_ans);
            this.pan_mode.Controls.Add(this.lbl);
            this.pan_mode.Controls.Add(this.pan);
            this.pan_mode.Dock = System.Windows.Forms.DockStyle.Top;
            this.pan_mode.Location = new System.Drawing.Point(0, 70);
            this.pan_mode.Name = "pan_mode";
            this.pan_mode.Size = new System.Drawing.Size(1271, 220);
            this.pan_mode.TabIndex = 7;
            // 
            // dgv_ans
            // 
            this.dgv_ans.AllowUserToDeleteRows = false;
            this.dgv_ans.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_ans.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_ans.ColumnHeadersHeight = 30;
            this.dgv_ans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_ans.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgv_ans.EnableHeadersVisualStyles = false;
            this.dgv_ans.Location = new System.Drawing.Point(0, 162);
            this.dgv_ans.Name = "dgv_ans";
            this.dgv_ans.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ans.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ans.RowHeadersVisible = false;
            this.dgv_ans.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Transparent;
            this.dgv_ans.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgv_ans.RowTemplate.Height = 30;
            this.dgv_ans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_ans.Size = new System.Drawing.Size(1271, 58);
            this.dgv_ans.TabIndex = 8;
            // 
            // lbl
            // 
            this.lbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl.Font = new System.Drawing.Font("Khmer OS Muol Light", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(0, 141);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(1271, 21);
            this.lbl.TabIndex = 7;
            this.lbl.Text = "សូមចំណាំ៖";
            this.lbl.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pan
            // 
            this.pan.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.pan.ColumnCount = 3;
            this.pan.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.906938F));
            this.pan.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.09306F));
            this.pan.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.pan.Controls.Add(this.lbl_eq_desc, 1, 0);
            this.pan.Controls.Add(this.btnprevious, 2, 0);
            this.pan.Dock = System.Windows.Forms.DockStyle.Top;
            this.pan.Location = new System.Drawing.Point(0, 0);
            this.pan.Name = "pan";
            this.pan.RowCount = 1;
            this.pan.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.43305F));
            this.pan.Size = new System.Drawing.Size(1271, 141);
            this.pan.TabIndex = 6;
            // 
            // lbl_eq_desc
            // 
            this.lbl_eq_desc.AutoSize = true;
            this.lbl_eq_desc.Font = new System.Drawing.Font("Khmer OS Content", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_eq_desc.Location = new System.Drawing.Point(65, 3);
            this.lbl_eq_desc.Name = "lbl_eq_desc";
            this.lbl_eq_desc.Size = new System.Drawing.Size(1081, 135);
            this.lbl_eq_desc.TabIndex = 1;
            this.lbl_eq_desc.Text = resources.GetString("lbl_eq_desc.Text");
            this.lbl_eq_desc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnprevious
            // 
            this.btnprevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnprevious.Location = new System.Drawing.Point(1166, 54);
            this.btnprevious.Name = "btnprevious";
            this.btnprevious.Size = new System.Drawing.Size(99, 33);
            this.btnprevious.TabIndex = 0;
            this.btnprevious.Text = "⮝";
            this.btnprevious.UseVisualStyleBackColor = true;
            this.btnprevious.Click += new System.EventHandler(this.btnprevious_Click);
            // 
            // btnnext
            // 
            this.btnnext.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnnext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnnext.Location = new System.Drawing.Point(1175, 6);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(90, 33);
            this.btnnext.TabIndex = 1;
            this.btnnext.Text = "ផ្នែកបន្ទាប់ ⮞⮞";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // lbl_section_id
            // 
            this.lbl_section_id.AutoSize = true;
            this.lbl_section_id.Location = new System.Drawing.Point(12, 9);
            this.lbl_section_id.Name = "lbl_section_id";
            this.lbl_section_id.Size = new System.Drawing.Size(62, 22);
            this.lbl_section_id.TabIndex = 11;
            this.lbl_section_id.Text = "section_id";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnnext);
            this.panel1.Controls.Add(this.lbl_section);
            this.panel1.Controls.Add(this.lbl_inc);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 290);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1271, 38);
            this.panel1.TabIndex = 12;
            // 
            // lbl_section
            // 
            this.lbl_section.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_section.Font = new System.Drawing.Font("Khmer OS Muol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_section.Location = new System.Drawing.Point(78, 0);
            this.lbl_section.Name = "lbl_section";
            this.lbl_section.Size = new System.Drawing.Size(821, 38);
            this.lbl_section.TabIndex = 1;
            this.lbl_section.Text = "NA";
            this.lbl_section.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_inc
            // 
            this.lbl_inc.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_inc.Font = new System.Drawing.Font("Khmer OS Muol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_inc.Location = new System.Drawing.Point(0, 0);
            this.lbl_inc.Name = "lbl_inc";
            this.lbl_inc.Size = new System.Drawing.Size(78, 38);
            this.lbl_inc.TabIndex = 0;
            this.lbl_inc.Text = "ផ្នែកទី៖១";
            this.lbl_inc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // container
            // 
            this.container.BackColor = System.Drawing.SystemColors.Control;
            this.container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.container.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.container.Location = new System.Drawing.Point(0, 328);
            this.container.Margin = new System.Windows.Forms.Padding(0);
            this.container.Name = "container";
            this.container.Size = new System.Drawing.Size(1271, 199);
            this.container.TabIndex = 13;
            // 
            // frm_eq_exam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 527);
            this.Controls.Add(this.container);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbl_section_id);
            this.Controls.Add(this.pan_mode);
            this.Controls.Add(this.lbl_mode);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_eq_exam";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_eq_exam_FormClosed);
            this.Load += new System.EventHandler(this.frm_eq_exam_Load);
            this.pan_mode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ans)).EndInit();
            this.pan.ResumeLayout(false);
            this.pan.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_mode;
        private System.Windows.Forms.Panel pan_mode;
        private System.Windows.Forms.TableLayoutPanel pan;
        private System.Windows.Forms.Label lbl_eq_desc;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.DataGridView dgv_ans;
        private System.Windows.Forms.Label lbl_section_id;
        private System.Windows.Forms.Button btnprevious;
        private System.Windows.Forms.Button btnnext;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_inc;
        private System.Windows.Forms.FlowLayoutPanel container;
        private System.Windows.Forms.Label lbl_section;
    }
}