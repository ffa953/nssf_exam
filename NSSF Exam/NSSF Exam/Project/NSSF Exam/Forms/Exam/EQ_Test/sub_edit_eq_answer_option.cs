﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class sub_edit_eq_answer_option : Form
    {
        public bool is_updated = false;
        public short load_type;
        public string id;
        public string answer;

        public sub_edit_eq_answer_option()
        {
            InitializeComponent();
        }

        bool Duplicate()
        {
            int ans_id = Convert.ToInt32(txt_id.Text.Trim());
            var query = ClsConfig.glo_local_db.tbl_eq_answer_type.Where(s => s.eq_ans_type_id == ans_id).ToList();
            return query.Count > 0 ? true : false;
        }

        bool Existing_Ctrl()
        {
            string str = "";
            bool result = false;
            if (string.IsNullOrEmpty(txt_id.Text.Trim()))
            {
                str += "- សូមបញ្ចូលលេខសម្គាល់!";
            }
            if (string.IsNullOrEmpty(txt_answer.Text.Trim()))
            {
                str += "\n- សូមបញ្ចូលកំណត់ចំណាំ!";
            }
            if (!string.IsNullOrEmpty(str))
            {
                ClsMsg.Warning(str);
                result = true;
            }
            return result;
        }

        private void sub_edit_eq_answer_option_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            if (load_type == 1)
            {
                txt_id.ReadOnly = false;
                btn_save.Tag = 1;
                this.Text = "បន្ថែមថ្មី";
            }
            else
            {
                txt_id.ReadOnly = true;
                txt_id.Text = id;
                txt_answer.Text = answer;
                btn_save.Tag = 2;
                this.Text = "កែប្រែ";
            }
        }
        private void btn_save_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(btn_save.Tag) == 1) 
            {
                if (!Existing_Ctrl())
                {
                    if (!Duplicate())
                    {
                        try
                        {
                            tbl_eq_answer_type eq = new tbl_eq_answer_type();
                            eq.eq_ans_type_id = Convert.ToInt32(txt_id.Text);
                            eq.eq_answer = txt_answer.Text;
                            ClsConfig.glo_local_db.tbl_eq_answer_type.Add(eq);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_updated = true;
                           
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                  
                    }
                    else { ClsMsg.Warning("លេខសម្គាល់នេះមានរួចម្តងហើយ!");}
                }
            }
            else if (Convert.ToInt32(btn_save.Tag) == 2)
            {
                try
                {
                    if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                    {
                        int ans_id = Convert.ToInt32(id);
                        tbl_eq_answer_type tb = ClsConfig.glo_local_db.tbl_eq_answer_type.Where(s => s.eq_ans_type_id == ans_id).Single();
                        tb.eq_answer = txt_answer.Text.Trim();
                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success(ClsMsg.STR_SUCCESS);
                        is_updated = true;
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
