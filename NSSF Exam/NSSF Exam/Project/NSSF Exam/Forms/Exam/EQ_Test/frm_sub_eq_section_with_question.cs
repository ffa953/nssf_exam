﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_sub_eq_section_with_question : Form
    {
        public frm_sub_eq_section_with_question()
        {
            InitializeComponent();
        }
        void Load_Record()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_section.AsNoTracking().ToList();
            dgvlst.DataSource = query;
        }

        void Select_Question(int sct_id)
        {
            var query = (from eq_d in ClsConfig.glo_local_db.tbl_eq_detail
                         join qs in ClsConfig.glo_local_db.tbl_eq_question on eq_d.eq_question_id equals qs.eq_question_id
                         where eq_d.eq_sct_id == sct_id
                         select new { qs.eq_question_id, qs.question }).ToList();
            dgv_question.DataSource = query;
        }

        private void frm_sub_eq_section_with_question_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_dgv(dgvlst);
            ClsSetting.sty_dgv(dgv_question);
            Load_Record();
        }

        private void dgvlst_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void dgvlst_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void dgvlst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dgvlst.Columns["col_check"].Index)
                {
                    foreach (DataGridViewRow item in dgvlst.Rows)
                    {
                        item.Cells["col_check"].Value = false;
                    }
                     if (Convert.ToBoolean(dgvlst.CurrentRow.Cells["col_check"].Value) == false)
                    {
                        dgvlst.CurrentRow.Cells["col_check"].Value = true;
                        btn_add.Enabled = true;
                        Select_Question(Convert.ToInt32(dgvlst.CurrentRow.Cells["col_sct_id"].Value));
                    }
                }
            }
        }

        private void dgvlst_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvlst.Cursor = (e.ColumnIndex==0 && e.RowIndex >-1) ? Cursors.Hand : Cursors.Default;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            sub_edit_eq_section_with_question frm = new sub_edit_eq_section_with_question();
            foreach (DataGridViewRow item in dgvlst.Rows)
            {
                if (Convert.ToBoolean(item.Cells["col_check"].Value) == true) 
                {
                    frm.sct_id = Convert.ToInt32(item.Cells["col_sct_id"].Value);
                    frm.txt_section.Text = item.Cells["col_section"].Value.ToString();
                    break;
                }
            }
            frm.ShowDialog();
        }

        private void dgv_question_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgv_question, "col_lr_1");
        }

        private void dgv_question_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgv_question, "col_lr_1");
        }

        private void dgv_question_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int sct_id = 0;
                foreach (DataGridViewRow item in dgvlst.Rows)
                {
                    if (Convert.ToBoolean(item.Cells["col_check"].Value) == true)
                    {
                        sct_id = Convert.ToInt32(item.Cells["col_sct_id"].Value);
                        break;
                    }
                }
                if (e.ColumnIndex == dgv_question.Columns["col_remove"].Index)
                {
                    int question_id = Convert.ToInt32(dgv_question.CurrentRow.Cells["col_question_id"].Value);
                    var query = ClsConfig.glo_local_db.tbl_eq_detail.Where(eq => eq.eq_sct_id == sct_id && eq.eq_question_id == question_id).Single();
                    if (query != null)
                    {
                        if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់ដកចេញមែនឬទេ?") == true)
                        {
                            ClsConfig.glo_local_db.tbl_eq_detail.Remove(query);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            Select_Question(sct_id);
                        }
                    }

                }
            }
        }

    }
}
