﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_sub_eq_section : Form
    {
       
        public frm_sub_eq_section()
        {
            InitializeComponent();
        }

        void Load_Record()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_section.AsNoTracking().ToList();
            dgvlst.DataSource = query;
        }

        private void frm_sub_eq_section_Load(object sender, EventArgs e)
        {
            Load_Record();
            ClsSetting.sty_dgv(dgvlst);
            ClsSetting.sty_btn_add(btn_add);
        }

        private void dgvlst_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void dgvlst_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgvlst, "col_lr");
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            sub_edit_eq_section frm = new sub_edit_eq_section();
            frm.load_type = 1;
            frm.ShowDialog();
            if (frm.is_update)
            {
                Load_Record();
            }
        }

        private void dgvlst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dgvlst.Columns["col_edit"].Index) 
                {
                    sub_edit_eq_section frm = new sub_edit_eq_section();
                    frm.txt_answer.Text = dgvlst.CurrentRow.Cells["col_section"].Value.ToString();
                    frm.id = Convert.ToInt32(dgvlst.CurrentRow.Cells["col_sct_id"].Value);
                    frm.load_type = 2;
                    frm.ShowDialog();
                    if (frm.is_update)
                    {
                        Load_Record();
                    }
                }
                else if (e.ColumnIndex == dgvlst.Columns["col_delete"].Index) 
                {
                    if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់លុបមែនឬទេ?") == true)
                    {
                        int id = Convert.ToInt32(dgvlst.CurrentRow.Cells["col_sct_id"].Value);
                        var query = ClsConfig.glo_local_db.tbl_eq_section.Where(eq => eq.eq_sct_id == id).Single();
                        ClsConfig.glo_local_db.tbl_eq_section.Remove(query);
                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success(ClsMsg.STR_SUCCESS);
                        Load_Record();
                    }
                }
            }
        }
    }
}
