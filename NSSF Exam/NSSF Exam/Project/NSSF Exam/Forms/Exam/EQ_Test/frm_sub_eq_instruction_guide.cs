﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_sub_eq_instruction_guide : Form
    {
        public frm_sub_eq_instruction_guide()
        {
            InitializeComponent();
        }

        void Load_Record() 
        {
            var query=ClsConfig.glo_local_db.tbl_eq_test.ToList();
            txt_instr.Text = query[0].eq_description;
            txt_instr.Tag = query[0].eq_id;
        }

        private void frm_sub_eq_instruction_guide_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_add);
            Load_Record();
         
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                {
                    int eq_id = Convert.ToInt32(txt_instr.Tag);
                    tbl_eq_test eq = ClsConfig.glo_local_db.tbl_eq_test.Where(q => q.eq_id == eq_id).Single();
                    eq.eq_description = txt_instr.Text.Trim();
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
        }

        private void txt_instr_TextChanged(object sender, EventArgs e)
        {
            int lenght = 4000 - txt_instr.Text.Length;
            lbl_text_lenght.Text = "សរុបៈ " + txt_instr.Text.Length + "/4000 ពាក្យ";
            lbl_text.Text = "សល់ៈ " + lenght+" ពាក្យ";
        }
    }
}
