﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_eq_exam : Form
    {
        public frm_eq_exam()
        {
            InitializeComponent();
        }

        BindingSource bds_section = new BindingSource();

        void Load_EQ_Desc()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_test.Single();
            lbl_eq_desc.Text = query.eq_description;
          
        }
        DataGridView dgv;
        List<string> col_name = new List<string>();

        void Load_Answer()
        {
            var ans = ClsConfig.glo_local_db.tbl_eq_answer_type.ToList();
            foreach (var item_ans in ans)
            {
                string val_inc = item_ans.eq_ans_type_id.ToString();
                DataGridViewTextBoxColumn Obj = new DataGridViewTextBoxColumn();
                Obj.Name = item_ans.eq_answer;
                Obj.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                
                Obj.DataPropertyName = item_ans.eq_answer;
                Obj.Width = 80;
                Obj.HeaderText = ClsSetting.toKhmerNum(item_ans.eq_ans_type_id.ToString());
                dgv_ans.Columns.Add(Obj);
                dgv_ans.Rows[0].Cells[item_ans.eq_answer].Value=item_ans.eq_answer;
                dgv_ans.Columns[item_ans.eq_answer].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgv_ans.Rows[0].Cells[item_ans.eq_answer].Selected = false;
            }
            
        }

        void Load_Question_By_Section()
        {
            var ans = ClsConfig.glo_local_db.tbl_eq_answer_type.ToList();
            foreach (var item_ in ans)
            {
                col_name.Add(item_.eq_ans_type_id.ToString());
            }
            int sct_id = Convert.ToInt32(lbl_section_id.Text);
            var sub_query = (from ed in ClsConfig.glo_local_db.tbl_eq_detail
                             join qu in ClsConfig.glo_local_db.tbl_eq_question on ed.eq_question_id equals qu.eq_question_id
                             where ed.eq_sct_id == sct_id
                             select qu).ToList();
            dgv = new DataGridView();
           
            DataGridViewTextBoxColumn auto_num = new DataGridViewTextBoxColumn();
            dgv.RowsAdded += dgv_RowsAdded;
            auto_num.Name = "col_auto";
            auto_num.HeaderText = "ល.រ";
            auto_num.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            auto_num.Width = 70;
            dgv.Columns.Add(auto_num);

            short val = 0;
            foreach (var item_ans in ans)
            {
                string val_inc = item_ans.eq_ans_type_id.ToString();
                DataGridViewCheckBoxColumn Obj = new DataGridViewCheckBoxColumn();
                Obj.Name = item_ans.eq_ans_type_id.ToString();
                Obj.Width = 80;
                Obj.HeaderText = ClsSetting.toKhmerNum(val.ToString());
                Obj.ReadOnly = false;
                dgv.Columns.Add(Obj);
                val++;
            }
           
            dgv.CellClick += new DataGridViewCellEventHandler(dgv_CellClick);
       
            DataGridViewTextBoxColumn question = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn question_id = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn ishas_Check = new DataGridViewTextBoxColumn();

            question.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            question.SortMode = DataGridViewColumnSortMode.NotSortable;

            ishas_Check.Name = "col_has_check";
            question.DataPropertyName = "question";
            question_id.DataPropertyName = "eq_question_id";
            question_id.Visible = false;
           
            dgv.Columns.Add(question);
            dgv.Columns.Add(question_id);
            dgv.Columns.Add(ishas_Check);

            dgv.Width = container.Width - 10;
            dgv.Height = container.Height - 10;
            ClsSetting.sty_dgv(dgv);
            dgv.DataSource = sub_query;
            foreach (Control item in container.Controls)
            {
                if (item is DataGridView) 
                {
                    container.Controls.Remove(item);
                }
            }
            container.Controls.Add(dgv);
            container.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            container.Dock = System.Windows.Forms.DockStyle.Fill;
            container.AutoScroll = true;
            container.Margin = new Padding(0, 0, 0, 0);
            setDefaultValue();
        }

        void dgv_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgv, "col_auto");
        }

        void setDefaultValue()
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].Cells["col_has_check"].Value = -1;
            }
        }
            

        void Load_Exam()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_section.ToList();
            bds_section.DataSource = query;
            FlowLayoutPanel pan = new FlowLayoutPanel();
            pan.AutoSize = true;
            lbl_section.DataBindings.Add(new Binding("Text", bds_section, "eq_section_name"));
            lbl_section_id.DataBindings.Add(new Binding("Text", bds_section, "eq_sct_id"));
            lbl_inc.Text="ផ្នែកទី" + ClsSetting.toKhmerNum((bds_section.Position + 1).ToString())+"៖";
            Load_Question_By_Section();
        }

        void Check_Select(int row_index)
        {
            foreach (DataGridViewColumn dc in dgv.Columns)
            {
                    if (dc is DataGridViewCheckBoxColumn)
                    {
                        if (Convert.ToBoolean(dgv.Rows[row_index].Cells[dc.Name].Value) == true) 
                        {
                            dgv.Rows[row_index].Cells[dc.Name].Value = false;
                        }
                    }
            }
        }

        bool isAllCell_Checked()
        {
            bool result=true;
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (Convert.ToInt32(dgv.Rows[i].Cells["col_has_check"].Value) == -1) 
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
                if (e.RowIndex > -1)
                {
                        if (dgv.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn)
                        {
                            if (Convert.ToBoolean(dgv.CurrentRow.Cells[e.ColumnIndex].Value) == false)
                            {
                                Check_Select(e.RowIndex);
                                dgv.CurrentRow.Cells[e.ColumnIndex].Value = true;
                                dgv.CurrentRow.Cells["col_has_check"].Value = e.ColumnIndex-1;
                            }
                            else if (Convert.ToBoolean(dgv.CurrentRow.Cells[e.ColumnIndex].Value) == true)
                            {
                                dgv.CurrentRow.Cells[e.ColumnIndex].Value = false;
                                dgv.CurrentRow.Cells["col_has_check"].Value = -1;
                            }
                        }
            }
        }

        private void frm_eq_exam_Load(object sender, EventArgs e)
        {
            bds_section.PositionChanged += bds_section_PositionChanged;
            Load_Exam();
            Load_EQ_Desc();
            Load_Answer();
            ClsSetting.sty_btn_next_eq(btnnext);
        }

        void bds_section_PositionChanged(object sender, EventArgs e)
        {
            btnnext.Enabled = (bds_section.Position + 1 == bds_section.Count) ? false : true;
            btnprevious.Enabled = (bds_section.Position == 0) ? false : true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pan_mode.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pan_mode.Visible = true;
        }
       
        private void btnnext_Click(object sender, EventArgs e)
        {
            if (isAllCell_Checked())
            {
                bds_section.MoveNext();
                lbl_inc.Text = "ផ្នែកទី៖" + ClsSetting.toKhmerNum((bds_section.Position + 1).ToString());
                Load_Question_By_Section();
            }
            else { ClsMsg.Warning("សូមជ្រើសរើសចម្លើយអោយបានគ្រប់ មុនទៅផ្នែកបន្ទាប់!"); }
        }

        private void btnprevious_Click(object sender, EventArgs e)
        {
                bds_section.MovePrevious();
                lbl_inc.Text = "ផ្នែកទី៖" + ClsSetting.toKhmerNum((bds_section.Position + 1).ToString());
                Load_Question_By_Section();
        }

        private void frm_eq_exam_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm_test_menu frm = new frm_test_menu();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

    }
}
