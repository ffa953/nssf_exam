﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_sub_question : Form
    {
        public frm_sub_question()
        {
            InitializeComponent();
        }

        private void frm_sub_question_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_dgv(dgv_question);
            Load_Record();
        }
        void Load_Record()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_question.AsNoTracking().ToList();
            dgv_question.DataSource = query;
        }


        private void btn_add_Click(object sender, EventArgs e)
        {
            sub_eq_question frm = new sub_eq_question();
            frm.load_type = 1;
            frm.ShowDialog();
            if (frm.is_updated)
            {
                Load_Record();
            }
        }

        private void dgv_question_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgv_question, "col_lr");
        }

        private void dgv_question_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ClsSetting.rowGrid_AutoNum(dgv_question, "col_lr");
        }

        private void dgv_question_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1) 
            { 
                if (e.ColumnIndex == dgv_question.Columns["col_edit"].Index) 
                {
                    sub_eq_question frm = new sub_eq_question();
                    frm.txt_question.Text = dgv_question.CurrentRow.Cells["col_question"].Value.ToString();
                    frm.id = Convert.ToInt32(dgv_question.CurrentRow.Cells["col_question_id"].Value);
                    frm.load_type = 2;
                    frm.ShowDialog();
                    if (frm.is_updated) 
                    {
                        Load_Record();
                    }
                }
                else if (e.ColumnIndex == dgv_question.Columns["col_delete"].Index) 
                {
                    if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់លុបមែនឬទេ?") == true)
                    {
                        try
                        {
                            int id = Convert.ToInt32(dgv_question.CurrentRow.Cells["col_question_id"].Value);
                            tbl_eq_question query = ClsConfig.glo_local_db.tbl_eq_question.Where(eq => eq.eq_question_id == id).Single();
                            ClsConfig.glo_local_db.tbl_eq_question.Remove(query);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            Load_Record();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);    
                        }
                    }
                }
            }
        }
    }
}
