﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_sub_eq_answer_option
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.col_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_edit = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvlst.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_edit,
            this.col_delete,
            this.col_num,
            this.col_note});
            this.dgvlst.Location = new System.Drawing.Point(97, 113);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(951, 309);
            this.dgvlst.TabIndex = 0;
            this.dgvlst.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellClick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Muol Light", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(457, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "បញ្ចូលកំណត់ចំណាំចម្លើយ";
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(943, 72);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // col_note
            // 
            this.col_note.DataPropertyName = "eq_answer";
            this.col_note.HeaderText = "កំណត់ចំណាំ";
            this.col_note.Name = "col_note";
            this.col_note.ReadOnly = true;
            // 
            // col_num
            // 
            this.col_num.DataPropertyName = "eq_ans_type_id";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_num.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_num.HeaderText = "លេខសម្គាល់";
            this.col_num.Name = "col_num";
            this.col_num.ReadOnly = true;
            // 
            // col_delete
            // 
            this.col_delete.HeaderText = "";
            this.col_delete.Name = "col_delete";
            this.col_delete.ReadOnly = true;
            this.col_delete.Text = "លុប";
            this.col_delete.UseColumnTextForButtonValue = true;
            this.col_delete.Width = 50;
            // 
            // col_edit
            // 
            this.col_edit.HeaderText = "";
            this.col_edit.Name = "col_edit";
            this.col_edit.ReadOnly = true;
            this.col_edit.Text = "កែប្រែ";
            this.col_edit.UseColumnTextForButtonValue = true;
            this.col_edit.Width = 60;
            // 
            // frm_sub_eq_answer_option
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 521);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvlst);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_sub_eq_answer_option";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_sub_eq_answer_option_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.DataGridViewButtonColumn col_edit;
        private System.Windows.Forms.DataGridViewButtonColumn col_delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_note;
    }
}