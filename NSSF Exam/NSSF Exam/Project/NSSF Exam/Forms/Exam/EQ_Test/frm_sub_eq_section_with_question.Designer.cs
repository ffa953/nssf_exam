﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_sub_eq_section_with_question
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.col_check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.col_sct_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_section = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_add = new System.Windows.Forms.Button();
            this.dgv_question = new System.Windows.Forms.DataGridView();
            this.col_remove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_lr_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Khmer OS Muol Light", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1133, 125);
            this.label1.TabIndex = 3;
            this.label1.Text = "កំណត់សំនួរក្នុងផ្នែកនៃEQតេស្ត";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_check,
            this.col_sct_id,
            this.col_lr,
            this.col_section});
            this.dgvlst.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvlst.Location = new System.Drawing.Point(0, 125);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(343, 392);
            this.dgvlst.TabIndex = 5;
            this.dgvlst.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellClick);
            this.dgvlst.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellMouseEnter);
            this.dgvlst.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvlst_RowsAdded);
            this.dgvlst.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvlst_RowsRemoved);
            // 
            // col_check
            // 
            this.col_check.HeaderText = "";
            this.col_check.Name = "col_check";
            this.col_check.ReadOnly = true;
            this.col_check.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_check.Width = 50;
            // 
            // col_sct_id
            // 
            this.col_sct_id.DataPropertyName = "eq_sct_id";
            this.col_sct_id.HeaderText = "sct_id";
            this.col_sct_id.Name = "col_sct_id";
            this.col_sct_id.ReadOnly = true;
            this.col_sct_id.Visible = false;
            // 
            // col_lr
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr.DefaultCellStyle = dataGridViewCellStyle9;
            this.col_lr.HeaderText = "ល.រ";
            this.col_lr.Name = "col_lr";
            this.col_lr.ReadOnly = true;
            this.col_lr.Width = 60;
            // 
            // col_section
            // 
            this.col_section.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.col_section.DataPropertyName = "eq_section_name";
            this.col_section.HeaderText = "ផ្នែក";
            this.col_section.Name = "col_section";
            this.col_section.ReadOnly = true;
            this.col_section.Width = 54;
            // 
            // btn_add
            // 
            this.btn_add.Enabled = false;
            this.btn_add.Location = new System.Drawing.Point(1021, 81);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 7;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // dgv_question
            // 
            this.dgv_question.AllowUserToAddRows = false;
            this.dgv_question.AllowUserToDeleteRows = false;
            this.dgv_question.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_question.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_question.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_remove,
            this.col_lr_1,
            this.col_question_id,
            this.col_question});
            this.dgv_question.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_question.Location = new System.Drawing.Point(343, 125);
            this.dgv_question.Name = "dgv_question";
            this.dgv_question.ReadOnly = true;
            this.dgv_question.Size = new System.Drawing.Size(790, 392);
            this.dgv_question.TabIndex = 9;
            this.dgv_question.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_question_CellClick);
            this.dgv_question.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgv_question_RowsAdded);
            this.dgv_question.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgv_question_RowsRemoved);
            // 
            // col_remove
            // 
            this.col_remove.HeaderText = "";
            this.col_remove.Name = "col_remove";
            this.col_remove.ReadOnly = true;
            this.col_remove.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_remove.Text = "ដកចេញ";
            this.col_remove.UseColumnTextForButtonValue = true;
            this.col_remove.Width = 60;
            // 
            // col_lr_1
            // 
            this.col_lr_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr_1.DefaultCellStyle = dataGridViewCellStyle10;
            this.col_lr_1.FillWeight = 68.02721F;
            this.col_lr_1.HeaderText = "ល.រ";
            this.col_lr_1.Name = "col_lr_1";
            this.col_lr_1.ReadOnly = true;
            this.col_lr_1.Width = 53;
            // 
            // col_question_id
            // 
            this.col_question_id.DataPropertyName = "eq_question_id";
            this.col_question_id.HeaderText = "question_id";
            this.col_question_id.Name = "col_question_id";
            this.col_question_id.ReadOnly = true;
            this.col_question_id.Visible = false;
            // 
            // col_question
            // 
            this.col_question.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_question.DataPropertyName = "question";
            this.col_question.FillWeight = 131.9728F;
            this.col_question.HeaderText = "សំនួរ";
            this.col_question.Name = "col_question";
            this.col_question.ReadOnly = true;
            // 
            // frm_sub_eq_section_with_question
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 517);
            this.ControlBox = false;
            this.Controls.Add(this.dgv_question);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.dgvlst);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_sub_eq_section_with_question";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_sub_eq_section_with_question_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_check;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sct_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_section;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.DataGridView dgv_question;
        private System.Windows.Forms.DataGridViewButtonColumn col_remove;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question;
    }
}