﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_main_eq_test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("១. សេចក្តីណែនាំនៃតេស្តEQ", 3, 3);
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("២. កំណត់ចំណាំចម្លើយ", 5, 5);
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("៣. ផ្នែកផ្សេងក្នុងEQតេស្ត", 4, 4);
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("៤. បញ្ចូលសំនួរEQតេស្ត");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("៥. កំណត់សំនួរក្នុងផ្នែកនៃEQតេស្ត");
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_close = new System.Windows.Forms.ToolStripButton();
            this.tree_view = new System.Windows.Forms.TreeView();
            this.pan_main = new System.Windows.Forms.Panel();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Font = new System.Drawing.Font("Khmer OS Content", 9F);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_close});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1218, 33);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_close
            // 
            this.btn_close.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btn_close.AutoSize = false;
            this.btn_close.Image = global::NSSF_Exam.Properties.Resources.btn_delete_color;
            this.btn_close.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(81, 35);
            this.btn_close.Text = "ចាកចេញ";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // tree_view
            // 
            this.tree_view.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tree_view.Cursor = System.Windows.Forms.Cursors.Default;
            this.tree_view.Dock = System.Windows.Forms.DockStyle.Left;
            this.tree_view.Font = new System.Drawing.Font("Khmer OS Content", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tree_view.Indent = 25;
            this.tree_view.ItemHeight = 35;
            this.tree_view.Location = new System.Drawing.Point(0, 33);
            this.tree_view.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tree_view.Name = "tree_view";
            treeNode1.ImageIndex = 3;
            treeNode1.Name = "nd_Product";
            treeNode1.SelectedImageIndex = 3;
            treeNode1.Text = "១. សេចក្តីណែនាំនៃតេស្តEQ";
            treeNode2.ImageIndex = 5;
            treeNode2.Name = "Node1";
            treeNode2.SelectedImageIndex = 5;
            treeNode2.Text = "២. កំណត់ចំណាំចម្លើយ";
            treeNode3.ImageIndex = 4;
            treeNode3.Name = "Node2";
            treeNode3.SelectedImageIndex = 4;
            treeNode3.Text = "៣. ផ្នែកផ្សេងក្នុងEQតេស្ត";
            treeNode4.Name = "Node0";
            treeNode4.Text = "៤. បញ្ចូលសំនួរEQតេស្ត";
            treeNode5.Name = "Node1";
            treeNode5.Text = "៥. កំណត់សំនួរក្នុងផ្នែកនៃEQតេស្ត";
            this.tree_view.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            this.tree_view.Size = new System.Drawing.Size(219, 610);
            this.tree_view.TabIndex = 4;
            this.tree_view.TabStop = false;
            this.tree_view.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tree_view_AfterSelect);
            // 
            // pan_main
            // 
            this.pan_main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pan_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_main.Location = new System.Drawing.Point(219, 33);
            this.pan_main.Name = "pan_main";
            this.pan_main.Size = new System.Drawing.Size(999, 610);
            this.pan_main.TabIndex = 5;
            // 
            // frm_main_eq_test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1218, 643);
            this.ControlBox = false;
            this.Controls.Add(this.pan_main);
            this.Controls.Add(this.tree_view);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_main_eq_test";
            this.ShowIcon = false;
            this.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (EQ)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_main_eq_test_FormClosed);
            this.Load += new System.EventHandler(this.frm_eq_test_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_close;
        public System.Windows.Forms.TreeView tree_view;
        private System.Windows.Forms.Panel pan_main;

    }
}