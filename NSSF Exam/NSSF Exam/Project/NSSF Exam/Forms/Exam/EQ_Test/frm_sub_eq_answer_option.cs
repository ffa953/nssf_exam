﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_sub_eq_answer_option : Form
    {
        public frm_sub_eq_answer_option()
        {
            InitializeComponent();
        }

        void Load_Record()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_answer_type.AsNoTracking().ToList();
            dgvlst.DataSource = query;
        }

        private void frm_sub_eq_answer_option_Load(object sender, EventArgs e)
        {
            Load_Record();
            ClsSetting.sty_dgv(dgvlst);
            ClsSetting.sty_btn_add(btn_add);
        }

        private void dgvlst_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvlst.Columns["col_edit"].Index) 
            {
                sub_edit_eq_answer_option frm=new sub_edit_eq_answer_option();
                frm.id=dgvlst.CurrentRow.Cells["col_num"].Value.ToString();
                frm.answer = dgvlst.CurrentRow.Cells["col_note"].Value.ToString();
                frm.load_type = 2;
                frm.ShowDialog();
                if (frm.is_updated)
                {
                    Load_Record();
                }
            }
            else if(e.ColumnIndex==dgvlst.Columns["col_delete"].Index)
            {
                if (ClsMsg.Question("តើលោកអ្នកពិជាចង់លុបមែនឬទេ?") == true)
                {
                    int ans_id = Convert.ToInt32(dgvlst.CurrentRow.Cells["col_num"].Value);
                    tbl_eq_answer_type eq = ClsConfig.glo_local_db.tbl_eq_answer_type.Where(s => s.eq_ans_type_id == ans_id).Single();
                    ClsConfig.glo_local_db.tbl_eq_answer_type.Remove(eq);
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    Load_Record();
                }
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            sub_edit_eq_answer_option frm = new sub_edit_eq_answer_option();
            frm.load_type = 1;
            frm.ShowDialog();
            if (frm.is_updated) { Load_Record(); }
        }
    }
}
