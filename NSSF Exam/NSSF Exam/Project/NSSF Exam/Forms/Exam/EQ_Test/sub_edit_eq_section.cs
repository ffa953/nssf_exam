﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class sub_edit_eq_section : Form
    {
        public sub_edit_eq_section()
        {
            InitializeComponent();
        }
        public short load_type;
        public bool is_update;
        public int id;

        bool Duplicate()
        {
            var query = ClsConfig.glo_local_db.tbl_eq_section.Where(eq => eq.eq_section_name.Equals(txt_answer.Text.Trim()) && eq.eq_sct_id !=id).ToList();
            return query.Count > 0 ? true : false;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txt_answer.Text.Trim()))
            {
                if (!Duplicate())
                {
                    if (Convert.ToInt32(btn_save.Tag) == 1)
                    {
                        try
                        {
                            tbl_eq_section eq = new tbl_eq_section();
                            eq.eq_section_name = txt_answer.Text.Trim();
                            ClsConfig.glo_local_db.tbl_eq_section.Add(eq);
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_update = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else if (Convert.ToInt32(btn_save.Tag) == 2)
                    {
                        if (ClsMsg.Question("តើលោកអ្នកពិតជាចង់កែប្រែមែនឬទេ?") == true)
                        {
                            tbl_eq_section eq = ClsConfig.glo_local_db.tbl_eq_section.Where(s => s.eq_sct_id == id).Single();
                            eq.eq_section_name = txt_answer.Text.Trim();
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            is_update = true;
                            this.Close();
                        }
                    }
                }
                else { ClsMsg.Warning("ផ្នែកនៃសំនួរក្នុងEQតេស្តនេះបានបញ្ចូលរួចម្តងហើយ!"); }
            }
            else { ClsMsg.Warning("សូមបញ្ចូលផ្នែកនៃសំនួរក្នុងEQតេស្ត!"); }
        }

        private void sub_edit_eq_section_Load(object sender, EventArgs e)
        {
            if (load_type == 1) { btn_save.Tag = 1; this.Text = "បន្ថែមថ្មី"; } else { btn_save.Tag = 2; this.Text = "កែប្រែ"; }
            ClsSetting.sty_btn_save(btn_save);
        }
    }
}
