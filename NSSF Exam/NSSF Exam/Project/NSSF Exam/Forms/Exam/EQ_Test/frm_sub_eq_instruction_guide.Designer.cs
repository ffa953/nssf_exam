﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_sub_eq_instruction_guide
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_instr = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_text_lenght = new System.Windows.Forms.Label();
            this.lbl_text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_instr
            // 
            this.txt_instr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_instr.Font = new System.Drawing.Font("Khmer OS Content", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_instr.Location = new System.Drawing.Point(75, 147);
            this.txt_instr.MaxLength = 4000;
            this.txt_instr.Multiline = true;
            this.txt_instr.Name = "txt_instr";
            this.txt_instr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_instr.Size = new System.Drawing.Size(1005, 342);
            this.txt_instr.TabIndex = 2;
            this.txt_instr.TextChanged += new System.EventHandler(this.txt_instr_TextChanged);
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_add.Location = new System.Drawing.Point(980, 105);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Muol Light", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(392, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(352, 34);
            this.label1.TabIndex = 1;
            this.label1.Text = "បញ្ចូលសេចក្តីណែនាំសម្រាប់តេស្តEQ";
            // 
            // lbl_text_lenght
            // 
            this.lbl_text_lenght.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_text_lenght.AutoSize = true;
            this.lbl_text_lenght.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_text_lenght.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_text_lenght.Location = new System.Drawing.Point(80, 499);
            this.lbl_text_lenght.Name = "lbl_text_lenght";
            this.lbl_text_lenght.Size = new System.Drawing.Size(137, 22);
            this.lbl_text_lenght.TabIndex = 5;
            this.lbl_text_lenght.Text = "សរុបៈ 651/4000 ពាក្យ";
            // 
            // lbl_text
            // 
            this.lbl_text.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_text.AutoSize = true;
            this.lbl_text.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_text.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_text.Location = new System.Drawing.Point(958, 499);
            this.lbl_text.Name = "lbl_text";
            this.lbl_text.Size = new System.Drawing.Size(46, 22);
            this.lbl_text.TabIndex = 6;
            this.lbl_text.Text = "សរុបៈ ";
            // 
            // frm_sub_eq_instruction_guide
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 580);
            this.Controls.Add(this.lbl_text);
            this.Controls.Add(this.lbl_text_lenght);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.txt_instr);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_sub_eq_instruction_guide";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_sub_eq_instruction_guide_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_instr;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_text_lenght;
        private System.Windows.Forms.Label lbl_text;
    }
}