﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class sub_edit_eq_section_with_question
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_section = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            this.dgv_question_2 = new System.Windows.Forms.DataGridView();
            this.col_check_2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.col_lr_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question_id_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_question_1 = new System.Windows.Forms.DataGridView();
            this.col_check_1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.col_lr_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question_id_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question_1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_cancel);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Controls.Add(this.txt_section);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1357, 58);
            this.panel1.TabIndex = 0;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cancel.Location = new System.Drawing.Point(1225, 10);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(100, 35);
            this.btn_cancel.TabIndex = 4;
            this.btn_cancel.Text = "button1";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save.Location = new System.Drawing.Point(1119, 10);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(100, 35);
            this.btn_save.TabIndex = 3;
            this.btn_save.Text = "button2";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txt_section
            // 
            this.txt_section.Location = new System.Drawing.Point(109, 12);
            this.txt_section.Name = "txt_section";
            this.txt_section.ReadOnly = true;
            this.txt_section.Size = new System.Drawing.Size(263, 30);
            this.txt_section.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Content", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "ផ្នែកនៃEQតេស្ត";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 527);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1357, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // btn_add
            // 
            this.btn_add.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_add.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.Maroon;
            this.btn_add.Location = new System.Drawing.Point(630, 240);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(47, 35);
            this.btn_add.TabIndex = 5;
            this.btn_add.Text = "<<";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_remove
            // 
            this.btn_remove.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btn_remove.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_remove.ForeColor = System.Drawing.Color.Navy;
            this.btn_remove.Location = new System.Drawing.Point(630, 281);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(47, 35);
            this.btn_remove.TabIndex = 6;
            this.btn_remove.Text = ">>";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // dgv_question_2
            // 
            this.dgv_question_2.AllowUserToAddRows = false;
            this.dgv_question_2.AllowUserToDeleteRows = false;
            this.dgv_question_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_question_2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_question_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_question_2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_check_2,
            this.col_lr_2,
            this.col_question_id_2,
            this.col_question_2});
            this.dgv_question_2.Location = new System.Drawing.Point(685, 87);
            this.dgv_question_2.Name = "dgv_question_2";
            this.dgv_question_2.ReadOnly = true;
            this.dgv_question_2.Size = new System.Drawing.Size(641, 412);
            this.dgv_question_2.TabIndex = 8;
            this.dgv_question_2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_question_CellClick);
            this.dgv_question_2.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgv_question_RowsAdded);
            this.dgv_question_2.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgv_question_RowsRemoved);
            // 
            // col_check_2
            // 
            this.col_check_2.HeaderText = "";
            this.col_check_2.Name = "col_check_2";
            this.col_check_2.ReadOnly = true;
            this.col_check_2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_check_2.Width = 50;
            // 
            // col_lr_2
            // 
            this.col_lr_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr_2.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_lr_2.FillWeight = 68.02721F;
            this.col_lr_2.HeaderText = "ល.រ";
            this.col_lr_2.Name = "col_lr_2";
            this.col_lr_2.ReadOnly = true;
            this.col_lr_2.Width = 53;
            // 
            // col_question_id_2
            // 
            this.col_question_id_2.DataPropertyName = "eq_question_id";
            this.col_question_id_2.HeaderText = "question_id";
            this.col_question_id_2.Name = "col_question_id_2";
            this.col_question_id_2.ReadOnly = true;
            this.col_question_id_2.Visible = false;
            // 
            // col_question_2
            // 
            this.col_question_2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.col_question_2.DataPropertyName = "question";
            this.col_question_2.FillWeight = 131.9728F;
            this.col_question_2.HeaderText = "សំនួរ";
            this.col_question_2.Name = "col_question_2";
            this.col_question_2.ReadOnly = true;
            this.col_question_2.Width = 58;
            // 
            // dgv_question_1
            // 
            this.dgv_question_1.AllowUserToAddRows = false;
            this.dgv_question_1.AllowUserToDeleteRows = false;
            this.dgv_question_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_question_1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_question_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_question_1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_check_1,
            this.col_lr_1,
            this.col_question_id_1,
            this.col_question_1});
            this.dgv_question_1.Location = new System.Drawing.Point(32, 87);
            this.dgv_question_1.Name = "dgv_question_1";
            this.dgv_question_1.ReadOnly = true;
            this.dgv_question_1.Size = new System.Drawing.Size(589, 412);
            this.dgv_question_1.TabIndex = 9;
            this.dgv_question_1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_question_1_CellClick);
            this.dgv_question_1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgv_question_1_RowsAdded);
            this.dgv_question_1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgv_question_1_RowsRemoved);
            // 
            // col_check_1
            // 
            this.col_check_1.HeaderText = "";
            this.col_check_1.Name = "col_check_1";
            this.col_check_1.ReadOnly = true;
            this.col_check_1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_check_1.Width = 50;
            // 
            // col_lr_1
            // 
            this.col_lr_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr_1.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_lr_1.HeaderText = "ល.រ";
            this.col_lr_1.Name = "col_lr_1";
            this.col_lr_1.ReadOnly = true;
            this.col_lr_1.Width = 53;
            // 
            // col_question_id_1
            // 
            this.col_question_id_1.DataPropertyName = "eq_question_id";
            this.col_question_id_1.HeaderText = "question_id";
            this.col_question_id_1.Name = "col_question_id_1";
            this.col_question_id_1.ReadOnly = true;
            this.col_question_id_1.Visible = false;
            // 
            // col_question_1
            // 
            this.col_question_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.col_question_1.DataPropertyName = "question";
            this.col_question_1.HeaderText = "សំនួរ";
            this.col_question_1.Name = "col_question_1";
            this.col_question_1.ReadOnly = true;
            this.col_question_1.Width = 58;
            // 
            // sub_edit_eq_section_with_question
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1357, 549);
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.dgv_question_1);
            this.Controls.Add(this.dgv_question_2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "sub_edit_eq_section_with_question";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.sub_edit_eq_section_with_question_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_question_1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_remove;
        private System.Windows.Forms.DataGridView dgv_question_2;
        private System.Windows.Forms.DataGridView dgv_question_1;
        public System.Windows.Forms.TextBox txt_section;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_check_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question_id_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question_1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_check_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question_id_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question_2;
    }
}