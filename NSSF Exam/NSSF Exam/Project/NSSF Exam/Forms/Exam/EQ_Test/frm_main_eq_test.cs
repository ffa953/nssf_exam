﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    public partial class frm_main_eq_test : Form
    {
        public frm_main_eq_test()
        {
            InitializeComponent();
        }

        void OpenForm(Form frm,Panel pan)
        {
            frm.TopLevel = false;
            pan.Controls.Clear();
            pan.Controls.Add(frm);
            frm.Show();
        }

        private void frm_eq_test_Load(object sender, EventArgs e)
        {
          
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_main_eq_test_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void tree_view_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tree_view.Nodes[0].IsSelected)
            {
                OpenForm(new frm_sub_eq_instruction_guide(), pan_main);
            }
            else if (tree_view.Nodes[1].IsSelected)
            {
                OpenForm(new frm_sub_eq_answer_option(), pan_main);
            }
            else if (tree_view.Nodes[2].IsSelected)
            {
                OpenForm(new frm_sub_eq_section(), pan_main);
            }
            else if (tree_view.Nodes[3].IsSelected)
            {
                OpenForm(new frm_sub_question(), pan_main);
            }
            else if (tree_view.Nodes[4].IsSelected)
            {
                OpenForm(new frm_sub_eq_section_with_question(), pan_main);
            }
        }
    }
}
