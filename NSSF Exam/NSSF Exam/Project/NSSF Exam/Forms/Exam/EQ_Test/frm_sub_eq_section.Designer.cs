﻿namespace NSSF_Exam.Forms.Exam.EQ_Test
{
    partial class frm_sub_eq_section
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.col_edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_sct_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_section = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(942, 72);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 6;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Muol Light", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(457, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 34);
            this.label1.TabIndex = 5;
            this.label1.Text = "បញ្ចូលផ្នែកនៃសំនួរក្នុងEQតេស្ត";
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvlst.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_edit,
            this.col_delete,
            this.col_sct_id,
            this.col_lr,
            this.col_section});
            this.dgvlst.Location = new System.Drawing.Point(97, 113);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(951, 309);
            this.dgvlst.TabIndex = 4;
            this.dgvlst.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellClick);
            this.dgvlst.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvlst_RowsAdded);
            this.dgvlst.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvlst_RowsRemoved);
            // 
            // col_edit
            // 
            this.col_edit.HeaderText = "";
            this.col_edit.Name = "col_edit";
            this.col_edit.ReadOnly = true;
            this.col_edit.Text = "កែប្រែ";
            this.col_edit.UseColumnTextForButtonValue = true;
            this.col_edit.Width = 60;
            // 
            // col_delete
            // 
            this.col_delete.HeaderText = "";
            this.col_delete.Name = "col_delete";
            this.col_delete.ReadOnly = true;
            this.col_delete.Text = "លុប";
            this.col_delete.UseColumnTextForButtonValue = true;
            this.col_delete.Width = 50;
            // 
            // col_sct_id
            // 
            this.col_sct_id.DataPropertyName = "eq_sct_id";
            this.col_sct_id.HeaderText = "sct_id";
            this.col_sct_id.Name = "col_sct_id";
            this.col_sct_id.ReadOnly = true;
            this.col_sct_id.Visible = false;
            // 
            // col_lr
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_lr.HeaderText = "ល.រ";
            this.col_lr.Name = "col_lr";
            this.col_lr.ReadOnly = true;
            this.col_lr.Width = 60;
            // 
            // col_section
            // 
            this.col_section.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.col_section.DataPropertyName = "eq_section_name";
            this.col_section.HeaderText = "ផ្នែក";
            this.col_section.Name = "col_section";
            this.col_section.ReadOnly = true;
            this.col_section.Width = 54;
            // 
            // frm_sub_eq_section
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 529);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvlst);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_sub_eq_section";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_sub_eq_section_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.DataGridViewButtonColumn col_edit;
        private System.Windows.Forms.DataGridViewButtonColumn col_delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sct_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_section;
    }
}