﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_student_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbo_stu_type_id = new System.Windows.Forms.ComboBox();
            this.bds_stu_type = new System.Windows.Forms.BindingSource(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.dt_off_start_date = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbo_branch = new System.Windows.Forms.ComboBox();
            this.bds_place = new System.Windows.Forms.BindingSource(this.components);
            this.label11 = new System.Windows.Forms.Label();
            this.cbo_skill = new System.Windows.Forms.ComboBox();
            this.bds_skill = new System.Windows.Forms.BindingSource(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.cbo_cert_skill = new System.Windows.Forms.ComboBox();
            this.bds_cert_skill = new System.Windows.Forms.BindingSource(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.cbo_certificate = new System.Windows.Forms.ComboBox();
            this.bds_certificate = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbo_status = new System.Windows.Forms.ComboBox();
            this.bds_status = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.cbo_nat = new System.Windows.Forms.ComboBox();
            this.bds_nat = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.dt_dob = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbo_gender = new System.Windows.Forms.ComboBox();
            this.bds_gender = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txt_name_en = new System.Windows.Forms.TextBox();
            this.txt_name_kh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_stu_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_place)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_certificate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_nat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_gender)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.cbo_stu_type_id);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.dt_off_start_date);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.cbo_branch);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cbo_skill);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.cbo_cert_skill);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cbo_certificate);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txt_phone);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cbo_status);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cbo_nat);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dt_dob);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbo_gender);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt_name_en);
            this.panel1.Controls.Add(this.txt_name_kh);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(739, 404);
            this.panel1.TabIndex = 0;
            // 
            // cbo_stu_type_id
            // 
            this.cbo_stu_type_id.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_stu_type_id.DataSource = this.bds_stu_type;
            this.cbo_stu_type_id.DisplayMember = "stu_type_name";
            this.cbo_stu_type_id.DropDownHeight = 150;
            this.cbo_stu_type_id.DropDownWidth = 120;
            this.cbo_stu_type_id.FormattingEnabled = true;
            this.cbo_stu_type_id.IntegralHeight = false;
            this.cbo_stu_type_id.Location = new System.Drawing.Point(143, 292);
            this.cbo_stu_type_id.Name = "cbo_stu_type_id";
            this.cbo_stu_type_id.Size = new System.Drawing.Size(194, 32);
            this.cbo_stu_type_id.TabIndex = 29;
            this.cbo_stu_type_id.ValueMember = "stu_type_id";
            // 
            // bds_stu_type
            // 
            this.bds_stu_type.DataSource = typeof(NSSF_Exam.Entities.tbl_student_type);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(28, 300);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 24);
            this.label13.TabIndex = 28;
            this.label13.Text = "ប្រភេទបេក្ខជន";
            // 
            // dt_off_start_date
            // 
            this.dt_off_start_date.CustomFormat = "dd-MM-yyyy";
            this.dt_off_start_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_off_start_date.Location = new System.Drawing.Point(512, 244);
            this.dt_off_start_date.Name = "dt_off_start_date";
            this.dt_off_start_date.Size = new System.Drawing.Size(194, 32);
            this.dt_off_start_date.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(397, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 24);
            this.label1.TabIndex = 26;
            this.label1.Text = "ថ្ងៃខែឆ្នាំតាំងស៊ប់";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 250);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 24);
            this.label12.TabIndex = 23;
            this.label12.Text = "ទីតាំង";
            // 
            // cbo_branch
            // 
            this.cbo_branch.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_branch.DataSource = this.bds_place;
            this.cbo_branch.DisplayMember = "pla_name";
            this.cbo_branch.DropDownHeight = 150;
            this.cbo_branch.DropDownWidth = 450;
            this.cbo_branch.FormattingEnabled = true;
            this.cbo_branch.IntegralHeight = false;
            this.cbo_branch.Location = new System.Drawing.Point(143, 245);
            this.cbo_branch.Name = "cbo_branch";
            this.cbo_branch.Size = new System.Drawing.Size(195, 32);
            this.cbo_branch.TabIndex = 22;
            this.cbo_branch.ValueMember = "pla_id";
            // 
            // bds_place
            // 
            this.bds_place.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_place);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(396, 206);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 24);
            this.label11.TabIndex = 21;
            this.label11.Text = "ផ្នែក";
            // 
            // cbo_skill
            // 
            this.cbo_skill.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_skill.DataSource = this.bds_skill;
            this.cbo_skill.DisplayMember = "skill";
            this.cbo_skill.FormattingEnabled = true;
            this.cbo_skill.Location = new System.Drawing.Point(511, 203);
            this.cbo_skill.Name = "cbo_skill";
            this.cbo_skill.Size = new System.Drawing.Size(194, 32);
            this.cbo_skill.TabIndex = 20;
            this.cbo_skill.ValueMember = "skill_id";
            this.cbo_skill.SelectedIndexChanged += new System.EventHandler(this.cbo_skill_SelectedIndexChanged);
            // 
            // bds_skill
            // 
            this.bds_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 24);
            this.label10.TabIndex = 19;
            this.label10.Text = "ឯកទេស";
            // 
            // cbo_cert_skill
            // 
            this.cbo_cert_skill.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_cert_skill.DataSource = this.bds_cert_skill;
            this.cbo_cert_skill.DisplayMember = "cert_skill_name";
            this.cbo_cert_skill.DropDownHeight = 100;
            this.cbo_cert_skill.FormattingEnabled = true;
            this.cbo_cert_skill.IntegralHeight = false;
            this.cbo_cert_skill.Location = new System.Drawing.Point(144, 200);
            this.cbo_cert_skill.Name = "cbo_cert_skill";
            this.cbo_cert_skill.Size = new System.Drawing.Size(194, 32);
            this.cbo_cert_skill.TabIndex = 18;
            this.cbo_cert_skill.ValueMember = "cert_skill_id";
            // 
            // bds_cert_skill
            // 
            this.bds_cert_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate_skill);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(396, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 24);
            this.label9.TabIndex = 17;
            this.label9.Text = "សញ្ញាប័ត្រ";
            // 
            // cbo_certificate
            // 
            this.cbo_certificate.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_certificate.DataSource = this.bds_certificate;
            this.cbo_certificate.DisplayMember = "certificate_name";
            this.cbo_certificate.FormattingEnabled = true;
            this.cbo_certificate.Location = new System.Drawing.Point(511, 159);
            this.cbo_certificate.Name = "cbo_certificate";
            this.cbo_certificate.Size = new System.Drawing.Size(194, 32);
            this.cbo_certificate.TabIndex = 16;
            this.cbo_certificate.ValueMember = "certificate_id";
            // 
            // bds_certificate
            // 
            this.bds_certificate.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 24);
            this.label8.TabIndex = 15;
            this.label8.Text = "លេខទូរស័ព្ទ";
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.SystemColors.Info;
            this.txt_phone.Location = new System.Drawing.Point(144, 156);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(194, 32);
            this.txt_phone.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(395, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 24);
            this.label7.TabIndex = 13;
            this.label7.Text = "ស្ថានភាព";
            // 
            // cbo_status
            // 
            this.cbo_status.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_status.DataSource = this.bds_status;
            this.cbo_status.DisplayMember = "status_discribe";
            this.cbo_status.FormattingEnabled = true;
            this.cbo_status.Location = new System.Drawing.Point(511, 116);
            this.cbo_status.Name = "cbo_status";
            this.cbo_status.Size = new System.Drawing.Size(194, 32);
            this.cbo_status.TabIndex = 12;
            this.cbo_status.ValueMember = "status_id";
            // 
            // bds_status
            // 
            this.bds_status.DataSource = typeof(NSSF_Exam.Entities.tbl_user_type);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 24);
            this.label6.TabIndex = 11;
            this.label6.Text = "សញ្ជាតិ";
            // 
            // cbo_nat
            // 
            this.cbo_nat.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_nat.DataSource = this.bds_nat;
            this.cbo_nat.DisplayMember = "nat_name_kh";
            this.cbo_nat.DropDownHeight = 100;
            this.cbo_nat.FormattingEnabled = true;
            this.cbo_nat.IntegralHeight = false;
            this.cbo_nat.Location = new System.Drawing.Point(143, 113);
            this.cbo_nat.Name = "cbo_nat";
            this.cbo_nat.Size = new System.Drawing.Size(194, 32);
            this.cbo_nat.TabIndex = 10;
            this.cbo_nat.ValueMember = "nat_id";
            // 
            // bds_nat
            // 
            this.bds_nat.DataSource = typeof(NSSF_Exam.Entities.vw_nationality);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(395, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "ថ្ងៃខែឆ្នាំកំណើត";
            // 
            // dt_dob
            // 
            this.dt_dob.CustomFormat = "dd-MM-yyyy";
            this.dt_dob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_dob.Location = new System.Drawing.Point(511, 72);
            this.dt_dob.Name = "dt_dob";
            this.dt_dob.Size = new System.Drawing.Size(194, 32);
            this.dt_dob.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 24);
            this.label4.TabIndex = 7;
            this.label4.Text = "ភេទ";
            // 
            // cbo_gender
            // 
            this.cbo_gender.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_gender.DataSource = this.bds_gender;
            this.cbo_gender.DisplayMember = "GenderKH";
            this.cbo_gender.FormattingEnabled = true;
            this.cbo_gender.Location = new System.Drawing.Point(143, 69);
            this.cbo_gender.Name = "cbo_gender";
            this.cbo_gender.Size = new System.Drawing.Size(194, 32);
            this.cbo_gender.TabIndex = 6;
            this.cbo_gender.ValueMember = "GenderId";
            // 
            // bds_gender
            // 
            this.bds_gender.DataSource = typeof(NSSF_Exam.Entities.tbl_gender);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(395, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "អក្សរឡាតាំង";
            // 
            // txt_name_en
            // 
            this.txt_name_en.BackColor = System.Drawing.SystemColors.Info;
            this.txt_name_en.Location = new System.Drawing.Point(511, 28);
            this.txt_name_en.Name = "txt_name_en";
            this.txt_name_en.Size = new System.Drawing.Size(194, 32);
            this.txt_name_en.TabIndex = 4;
            this.txt_name_en.Enter += new System.EventHandler(this.txt_name_en_Enter);
            // 
            // txt_name_kh
            // 
            this.txt_name_kh.BackColor = System.Drawing.SystemColors.Info;
            this.txt_name_kh.Location = new System.Drawing.Point(143, 25);
            this.txt_name_kh.Name = "txt_name_kh";
            this.txt_name_kh.Size = new System.Drawing.Size(194, 32);
            this.txt_name_kh.TabIndex = 3;
            this.txt_name_kh.Enter += new System.EventHandler(this.txt_name_kh_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "នាមជាខ្មែរ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Controls.Add(this.btn_add);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 347);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(739, 57);
            this.panel2.TabIndex = 1;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(623, 12);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_add
            // 
            this.btn_add.BackgroundImage = global::NSSF_Exam.Properties.Resources._801b2c49b2713c8c6957a345db5fff93;
            this.btn_add.Location = new System.Drawing.Point(513, 12);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 0;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // frm_student_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 404);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_student_edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ពត៍មានបេក្ខជន";
            this.Load += new System.EventHandler(this.frm_add_stu_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_stu_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_place)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_certificate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_nat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_gender)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbo_nat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dt_dob;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbo_gender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_name_en;
        private System.Windows.Forms.TextBox txt_name_kh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbo_branch;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbo_skill;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbo_cert_skill;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbo_certificate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbo_status;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.BindingSource bds_gender;
        private System.Windows.Forms.BindingSource bds_skill;
        private System.Windows.Forms.BindingSource bds_place;
        private System.Windows.Forms.BindingSource bds_nat;
        private System.Windows.Forms.BindingSource bds_status;
        private System.Windows.Forms.BindingSource bds_certificate;
        private System.Windows.Forms.BindingSource bds_cert_skill;
        private System.Windows.Forms.ComboBox cbo_stu_type_id;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dt_off_start_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bds_stu_type;
    }
}