﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_quiz_edit : Form
    {
        public int quiz_id = 0;
        int answer_id = 0;
        int row_index;
        public frm_quiz_edit()
        {
            InitializeComponent();
        }

        private void frm_quiz_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_add);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_dgv(dgv_ans);
            ClsSetting.sty_btn_clear(btn_clear);
            chk_active.Checked = true;

            bds_sub.DataSource = ClsConfig.glo_local_db.tbl_subject.Where(m => m.sub_active == true).ToList();
            cbo_subject.SelectedIndex = -1;

            bds_type.DataSource = ClsConfig.glo_local_db.tbl_quiz_type.ToList();
            cbo_type.SelectedIndex = -1;

            if (quiz_id > 0)
            {
                var quiz = ClsConfig.glo_local_db.tbl_subject_quiz.Find(quiz_id);
                cbo_subject.SelectedValue = quiz.sub_id;
                cbo_type.SelectedValue = quiz.type_id;
                txt_quiz_title.Text = quiz.quiz_title;
                txt_score.Text = quiz.score.ToString();
                chk_active.Checked = (bool) quiz.active;

                bds_quiz_ans.DataSource=(from q in ClsConfig.glo_local_db.tbl_quiz_answer where q.quiz_id==quiz_id select q).ToList();
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (quiz_id > 0)
                {
                    TransactionScope tran = new TransactionScope(TransactionScopeOption.Required);

                    var sub_quiz = (from q in ClsConfig.glo_local_db.tbl_subject_quiz where q.quiz_id == quiz_id select q).SingleOrDefault();
                    sub_quiz.quiz_title = txt_quiz_title.Text;
                    sub_quiz.sub_id = Convert.ToInt32(cbo_subject.SelectedValue);
                    sub_quiz.score = Convert.ToInt32(txt_score.Text);
                    sub_quiz.active = chk_active.Checked ? true : false;
                    sub_quiz.type_id = Convert.ToInt32(cbo_type.SelectedValue);
                  
                    ClsConfig.glo_local_db.SaveChanges();

                    var quiz_answer =(from q in ClsConfig.glo_local_db.tbl_quiz_answer where q.quiz_id==quiz_id select q).ToList();
                    foreach(tbl_quiz_answer i in quiz_answer)
                    {
                        ClsConfig.glo_local_db.tbl_quiz_answer.Remove(i);
                        ClsConfig.glo_local_db.SaveChanges();
                    }


                    int j = 0;
                    foreach (tbl_quiz_answer i in bds_quiz_ans)
                    {
                        j++;
                        tbl_quiz_answer quiz_ans = new tbl_quiz_answer();
                        quiz_ans.ans_title = i.ans_title;
                        quiz_ans.ans_choice = i.ans_choice;
                        quiz_ans.quiz_id = sub_quiz.quiz_id;
                        quiz_ans.active = chk_active.Checked ? true : false;
                        quiz_ans.ans_order = j;
                        ClsConfig.glo_local_db.tbl_quiz_answer.Add(quiz_ans);
                        ClsConfig.glo_local_db.SaveChanges();

                    }


                    tran.Complete();
                    tran.Dispose();

                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សារទុក។");
                    this.Close();


                }
                else
                {
                    if(IsFieldValidate()){

                    TransactionScope tran = new TransactionScope(TransactionScopeOption.Required);

                    tbl_subject_quiz sub_quiz = new tbl_subject_quiz();
                    sub_quiz.quiz_title = txt_quiz_title.Text;
                    sub_quiz.sub_id =Convert.ToInt32(cbo_subject.SelectedValue);
                    sub_quiz.score = Convert.ToInt32( txt_score.Text);
                    sub_quiz.active = chk_active.Checked ? true : false;
                    sub_quiz.type_id = Convert.ToInt32(cbo_type.SelectedValue);
                    ClsConfig.glo_local_db.tbl_subject_quiz.Add(sub_quiz);
                    ClsConfig.glo_local_db.SaveChanges();

                    int j = 0;
                    foreach(tbl_quiz_answer i in bds_quiz_ans){
                        j++;
                        tbl_quiz_answer quiz_ans = new tbl_quiz_answer();
                        quiz_ans.ans_title = i.ans_title;
                        quiz_ans.ans_choice = i.ans_choice;
                        quiz_ans.quiz_id = sub_quiz.quiz_id;
                        quiz_ans.active = chk_active.Checked ? true : false;
                        quiz_ans.ans_order = j;
                        ClsConfig.glo_local_db.tbl_quiz_answer.Add(quiz_ans);
                        ClsConfig.glo_local_db.SaveChanges();

                    }

                    tran.Complete();
                    tran.Dispose();

                    cbo_subject.SelectedIndex = -1;
                    cbo_type.SelectedIndex = -1;
                    txt_ans_title.Text = "";
                    txt_quiz_title.Text = "";
                    txt_score.Text = "";
                    bds_quiz_ans.DataSource = null;

                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សារទុក។");


                    }
                }
            }
            catch (Exception ex)
            {
                ClsMsg.Error("" + ex);
            }
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            if (answer_id > 0)
            {
                tbl_quiz_answer quiz_ans = new tbl_quiz_answer();
                quiz_ans.ans_title = txt_ans_title.Text;
                quiz_ans.ans_choice = (rb_true.Checked) ? true : false;

                bds_quiz_ans.Insert(row_index, quiz_ans);
                bds_quiz_ans.RemoveAt(row_index + 1);

                btn_new.Text = "បន្ថែមថ្មី";
                txt_ans_title.Text = "";
                answer_id = 0;
            }
            else
            {
                tbl_quiz_answer quiz_ans = new tbl_quiz_answer();
                quiz_ans.ans_title = txt_ans_title.Text;
                quiz_ans.ans_choice = (rb_true.Checked) ? true : false;
                bds_quiz_ans.Add(quiz_ans);
                txt_ans_title.Text = "";
               
            }
        }

        private void dgv_ans_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_ans.Columns[e.ColumnIndex].Name == "ans_choice")
            {
                if ((bool)dgv_ans.Rows[e.RowIndex].Cells["ans_choice"].Value == false)
                {

                    e.Value = "មិនត្រឹមត្រូវ";

                }
                else
                {
                    e.Value = "ត្រឹមត្រូវ";

                }
            }
        }

        private void dgv_ans_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                bds_quiz_ans.RemoveAt(e.RowIndex);
                answer_id = 0;
                btn_new.Text = "បន្ថែមថ្មី";
                txt_ans_title.Text = "";
            }
            else
            {
                row_index = e.RowIndex;
                txt_ans_title.Text = dgv_ans.Rows[e.RowIndex].Cells["ans_title"].Value.ToString().Trim();
                answer_id = 1;
                if ((bool)dgv_ans.Rows[e.RowIndex].Cells["ans_choice"].Value == true)
                {
                    rb_true.Checked = true;
                }
                else
                {
                    rb_false.Checked = true;
                }
                btn_new.Text = "កែប្រែ";
            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (cbo_subject.SelectedItem == null)
            {
                msgString += "-មុខវិជ្ជាត្រូវតែបញ្ចូល\n";
            }

            if (cbo_type.SelectedItem == null)
            {
                msgString += "-ប្រភេទសំនួរត្រូវតែបញ្ចូល\n";
            }

            if (txt_quiz_title.Text == "")
            {
                msgString += "-សំនួរត្រូវតែបញ្ចូល\n";
            }

            if (txt_score.Text == "")
            {
                msgString += "-ពិន្ទុត្រូវតែបញ្ចូល\n";
            }
            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            btn_new.Text = "បន្ថែមថ្មី";
            txt_ans_title.Text = "";
            answer_id = 0;
        }

      
    }
}
