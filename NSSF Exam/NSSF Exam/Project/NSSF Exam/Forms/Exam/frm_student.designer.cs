﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_student = new System.Windows.Forms.DataGridView();
            this.GenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.NatBindindSource = new System.Windows.Forms.BindingSource(this.components);
            this.SkillBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.StuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txt_search = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_search = new System.Windows.Forms.Button();
            this.btn_export = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.CertSkill_BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.StatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bds_user = new System.Windows.Forms.BindingSource(this.components);
            this.userIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.verify = new System.Windows.Forms.DataGridViewImageColumn();
            this.finger = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.skill_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namekhmerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namelatinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.natidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dateOfBirthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skillidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.certificateidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cert_skill_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_student)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatBindindSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkillBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StuBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CertSkill_BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_user)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_student
            // 
            this.dgv_student.AutoGenerateColumns = false;
            this.dgv_student.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_student.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userIdDataGridViewTextBoxColumn,
            this.col,
            this.Column1,
            this.verify,
            this.finger,
            this.skill_note,
            this.userNameDataGridViewTextBoxColumn,
            this.namekhmerDataGridViewTextBoxColumn,
            this.namelatinDataGridViewTextBoxColumn,
            this.genderIdDataGridViewTextBoxColumn,
            this.natidDataGridViewTextBoxColumn,
            this.dateOfBirthDataGridViewTextBoxColumn,
            this.phoneDataGridViewTextBoxColumn,
            this.skillidDataGridViewTextBoxColumn,
            this.certificateidDataGridViewTextBoxColumn,
            this.cert_skill_name,
            this.user_id});
            this.dgv_student.DataSource = this.StuBindingSource;
            this.dgv_student.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_student.Location = new System.Drawing.Point(0, 0);
            this.dgv_student.Name = "dgv_student";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_student.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_student.Size = new System.Drawing.Size(1072, 501);
            this.dgv_student.TabIndex = 0;
            this.dgv_student.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_student_CellClick);
            this.dgv_student.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_student_CellFormatting);
            this.dgv_student.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_student_DataBindingComplete);
            // 
            // GenBindingSource
            // 
            this.GenBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_gender);
            // 
            // NatBindindSource
            // 
            this.NatBindindSource.DataSource = typeof(NSSF_Exam.Entities.vw_nationality);
            // 
            // SkillBindingSource
            // 
            this.SkillBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // CertBindingSource
            // 
            this.CertBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate);
            // 
            // StuBindingSource
            // 
            this.StuBindingSource.DataSource = typeof(NSSF_Exam.Entities.fun_user_by_role_Result);
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.SystemColors.Info;
            this.txt_search.Location = new System.Drawing.Point(21, 16);
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(244, 32);
            this.txt_search.TabIndex = 1;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_add.Location = new System.Drawing.Point(630, 17);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_edit.Location = new System.Drawing.Point(739, 17);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(100, 35);
            this.btn_edit.TabIndex = 5;
            this.btn_edit.Text = "button3";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_search);
            this.panel1.Controls.Add(this.btn_export);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.txt_search);
            this.panel1.Controls.Add(this.btn_edit);
            this.panel1.Controls.Add(this.btn_add);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 65);
            this.panel1.TabIndex = 6;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(271, 15);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(100, 35);
            this.btn_search.TabIndex = 8;
            this.btn_search.Text = "button3";
            this.btn_search.UseVisualStyleBackColor = true;
            // 
            // btn_export
            // 
            this.btn_export.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_export.Location = new System.Drawing.Point(847, 17);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(100, 35);
            this.btn_export.TabIndex = 7;
            this.btn_export.Text = "button3";
            this.btn_export.UseVisualStyleBackColor = true;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(956, 17);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 6;
            this.btn_close.Text = "button1";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_student);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1072, 501);
            this.panel2.TabIndex = 7;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កំណត់ស្នាមម្រាមដៃ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.fingerprint;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 120;
            // 
            // CertSkill_BindingSource
            // 
            this.CertSkill_BindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate_skill);
            // 
            // StatusBindingSource
            // 
            this.StatusBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_user_type);
            // 
            // bds_user
            // 
            this.bds_user.DataSource = typeof(NSSF_Exam.Entities.tbl_user);
            // 
            // userIdDataGridViewTextBoxColumn
            // 
            this.userIdDataGridViewTextBoxColumn.DataPropertyName = "UserId";
            this.userIdDataGridViewTextBoxColumn.HeaderText = "UserId";
            this.userIdDataGridViewTextBoxColumn.Name = "userIdDataGridViewTextBoxColumn";
            this.userIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // col
            // 
            this.col.HeaderText = "ល.រ";
            this.col.Name = "col";
            this.col.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កំណត់ស្នាមម្រាមដៃ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.fingerprint;
            this.Column1.Name = "Column1";
            this.Column1.Width = 120;
            // 
            // verify
            // 
            this.verify.HeaderText = "ផ្ទៀងផ្ទាត់";
            this.verify.Image = global::NSSF_Exam.Properties.Resources.btn_refresh;
            this.verify.Name = "verify";
            this.verify.Width = 80;
            // 
            // finger
            // 
            this.finger.DataPropertyName = "finger";
            this.finger.HeaderText = "ស្គែនរួច";
            this.finger.Name = "finger";
            this.finger.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.finger.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.finger.Width = 80;
            // 
            // skill_note
            // 
            this.skill_note.DataPropertyName = "skill_note";
            this.skill_note.HeaderText = "លេខតុ";
            this.skill_note.Name = "skill_note";
            this.skill_note.Width = 70;
            // 
            // userNameDataGridViewTextBoxColumn
            // 
            this.userNameDataGridViewTextBoxColumn.DataPropertyName = "UserName";
            this.userNameDataGridViewTextBoxColumn.HeaderText = "លេខសំគាល់";
            this.userNameDataGridViewTextBoxColumn.Name = "userNameDataGridViewTextBoxColumn";
            // 
            // namekhmerDataGridViewTextBoxColumn
            // 
            this.namekhmerDataGridViewTextBoxColumn.DataPropertyName = "Name_khmer";
            this.namekhmerDataGridViewTextBoxColumn.HeaderText = "នាមជាខ្មែរ";
            this.namekhmerDataGridViewTextBoxColumn.Name = "namekhmerDataGridViewTextBoxColumn";
            this.namekhmerDataGridViewTextBoxColumn.Width = 150;
            // 
            // namelatinDataGridViewTextBoxColumn
            // 
            this.namelatinDataGridViewTextBoxColumn.DataPropertyName = "Name_latin";
            this.namelatinDataGridViewTextBoxColumn.HeaderText = "អក្សរឡាតាំង";
            this.namelatinDataGridViewTextBoxColumn.Name = "namelatinDataGridViewTextBoxColumn";
            this.namelatinDataGridViewTextBoxColumn.Width = 150;
            // 
            // genderIdDataGridViewTextBoxColumn
            // 
            this.genderIdDataGridViewTextBoxColumn.DataPropertyName = "GenderId";
            this.genderIdDataGridViewTextBoxColumn.DataSource = this.GenBindingSource;
            this.genderIdDataGridViewTextBoxColumn.DisplayMember = "GenderKH";
            this.genderIdDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.genderIdDataGridViewTextBoxColumn.HeaderText = "ភេទ";
            this.genderIdDataGridViewTextBoxColumn.Name = "genderIdDataGridViewTextBoxColumn";
            this.genderIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.genderIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.genderIdDataGridViewTextBoxColumn.ValueMember = "GenderId";
            this.genderIdDataGridViewTextBoxColumn.Width = 80;
            // 
            // natidDataGridViewTextBoxColumn
            // 
            this.natidDataGridViewTextBoxColumn.DataPropertyName = "nat_id";
            this.natidDataGridViewTextBoxColumn.DataSource = this.NatBindindSource;
            this.natidDataGridViewTextBoxColumn.DisplayMember = "nat_name_kh";
            this.natidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.natidDataGridViewTextBoxColumn.HeaderText = "សញ្ជាតិ";
            this.natidDataGridViewTextBoxColumn.Name = "natidDataGridViewTextBoxColumn";
            this.natidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.natidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.natidDataGridViewTextBoxColumn.ValueMember = "nat_id";
            this.natidDataGridViewTextBoxColumn.Width = 80;
            // 
            // dateOfBirthDataGridViewTextBoxColumn
            // 
            this.dateOfBirthDataGridViewTextBoxColumn.DataPropertyName = "DateOfBirth";
            dataGridViewCellStyle1.Format = "dd-MM-yyyy";
            this.dateOfBirthDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.dateOfBirthDataGridViewTextBoxColumn.HeaderText = "ថ្ងៃខែកំណើត";
            this.dateOfBirthDataGridViewTextBoxColumn.Name = "dateOfBirthDataGridViewTextBoxColumn";
            this.dateOfBirthDataGridViewTextBoxColumn.Width = 130;
            // 
            // phoneDataGridViewTextBoxColumn
            // 
            this.phoneDataGridViewTextBoxColumn.DataPropertyName = "Phone";
            this.phoneDataGridViewTextBoxColumn.HeaderText = "លេខទូរស័ព្ទ";
            this.phoneDataGridViewTextBoxColumn.Name = "phoneDataGridViewTextBoxColumn";
            this.phoneDataGridViewTextBoxColumn.Width = 150;
            // 
            // skillidDataGridViewTextBoxColumn
            // 
            this.skillidDataGridViewTextBoxColumn.DataPropertyName = "skill_id";
            this.skillidDataGridViewTextBoxColumn.DataSource = this.SkillBindingSource;
            this.skillidDataGridViewTextBoxColumn.DisplayMember = "skill";
            this.skillidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.skillidDataGridViewTextBoxColumn.HeaderText = "ផ្នែក";
            this.skillidDataGridViewTextBoxColumn.Name = "skillidDataGridViewTextBoxColumn";
            this.skillidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.skillidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.skillidDataGridViewTextBoxColumn.ValueMember = "skill_id";
            this.skillidDataGridViewTextBoxColumn.Width = 220;
            // 
            // certificateidDataGridViewTextBoxColumn
            // 
            this.certificateidDataGridViewTextBoxColumn.DataPropertyName = "certificate_id";
            this.certificateidDataGridViewTextBoxColumn.DataSource = this.CertBindingSource;
            this.certificateidDataGridViewTextBoxColumn.DisplayMember = "certificate_name";
            this.certificateidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.certificateidDataGridViewTextBoxColumn.HeaderText = "សញ្ញាប័ត្រ";
            this.certificateidDataGridViewTextBoxColumn.Name = "certificateidDataGridViewTextBoxColumn";
            this.certificateidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.certificateidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.certificateidDataGridViewTextBoxColumn.ValueMember = "certificate_id";
            this.certificateidDataGridViewTextBoxColumn.Width = 150;
            // 
            // cert_skill_name
            // 
            this.cert_skill_name.DataPropertyName = "cert_skill_name";
            this.cert_skill_name.HeaderText = "ឯកទេស";
            this.cert_skill_name.Name = "cert_skill_name";
            this.cert_skill_name.Width = 170;
            // 
            // user_id
            // 
            this.user_id.DataPropertyName = "user_id";
            this.user_id.DataSource = this.bds_user;
            this.user_id.DisplayMember = "use_username";
            this.user_id.HeaderText = "អ្នកបញ្ជូល";
            this.user_id.Name = "user_id";
            this.user_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.user_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frm_student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 566);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_student";
            this.Text = "frm_student";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_student_FormClosed);
            this.Load += new System.EventHandler(this.frm_student_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_student)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatBindindSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkillBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StuBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CertSkill_BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_user)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_student;
        private System.Windows.Forms.BindingSource StuBindingSource;
        private System.Windows.Forms.BindingSource GenBindingSource;
        private System.Windows.Forms.BindingSource SkillBindingSource;
        private System.Windows.Forms.BindingSource CertBindingSource;
        private System.Windows.Forms.BindingSource CertSkill_BindingSource;
        private System.Windows.Forms.BindingSource NatBindindSource;
        private System.Windows.Forms.BindingSource StatusBindingSource;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_export;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.BindingSource bds_user;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn verify;
        private System.Windows.Forms.DataGridViewCheckBoxColumn finger;
        private System.Windows.Forms.DataGridViewTextBoxColumn skill_note;
        private System.Windows.Forms.DataGridViewTextBoxColumn userNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namekhmerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namelatinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn genderIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn natidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateOfBirthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn skillidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn certificateidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cert_skill_name;
        private System.Windows.Forms.DataGridViewComboBoxColumn user_id;
    }
}