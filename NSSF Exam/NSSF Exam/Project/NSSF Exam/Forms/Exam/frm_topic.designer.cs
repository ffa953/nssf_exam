﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_topic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_topic = new System.Windows.Forms.DataGridView();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.ex_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.extypenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ex_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ex_active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_topic = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_topic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_topic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(810, 69);
            this.panel1.TabIndex = 3;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(697, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(587, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_topic
            // 
            this.dgv_topic.AutoGenerateColumns = false;
            this.dgv_topic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_topic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ex_type_id,
            this.extypenameDataGridViewTextBoxColumn,
            this.ex_time,
            this.ex_active});
            this.dgv_topic.DataSource = this.bds_topic;
            this.dgv_topic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_topic.Location = new System.Drawing.Point(0, 69);
            this.dgv_topic.Name = "dgv_topic";
            this.dgv_topic.Size = new System.Drawing.Size(810, 415);
            this.dgv_topic.TabIndex = 4;
            this.dgv_topic.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_topic_CellClick);
            this.dgv_topic.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_topic_CellFormatting);
            this.dgv_topic.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_topic_DataBindingComplete);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Width = 90;
            // 
            // ex_type_id
            // 
            this.ex_type_id.DataPropertyName = "ex_type_id";
            this.ex_type_id.HeaderText = "ex_type_id";
            this.ex_type_id.Name = "ex_type_id";
            this.ex_type_id.Visible = false;
            // 
            // extypenameDataGridViewTextBoxColumn
            // 
            this.extypenameDataGridViewTextBoxColumn.DataPropertyName = "ex_type_name";
            this.extypenameDataGridViewTextBoxColumn.HeaderText = "វិញ្ញាសារ";
            this.extypenameDataGridViewTextBoxColumn.Name = "extypenameDataGridViewTextBoxColumn";
            this.extypenameDataGridViewTextBoxColumn.Width = 250;
            // 
            // ex_time
            // 
            this.ex_time.DataPropertyName = "ex_time";
            this.ex_time.HeaderText = "រយះពេល(នាទី)";
            this.ex_time.Name = "ex_time";
            this.ex_time.Width = 200;
            // 
            // ex_active
            // 
            this.ex_active.DataPropertyName = "ex_active";
            this.ex_active.HeaderText = "ដំណើរការ";
            this.ex_active.Name = "ex_active";
            this.ex_active.Width = 220;
            // 
            // bds_topic
            // 
            this.bds_topic.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_type);
            // 
            // frm_topic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 484);
            this.Controls.Add(this.dgv_topic);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_topic";
            this.Text = "frm_topic";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_topic_FormClosed);
            this.Load += new System.EventHandler(this.frm_topic_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_topic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_topic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.DataGridView dgv_topic;
        private System.Windows.Forms.BindingSource bds_topic;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ex_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn extypenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ex_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn ex_active;
    }
}