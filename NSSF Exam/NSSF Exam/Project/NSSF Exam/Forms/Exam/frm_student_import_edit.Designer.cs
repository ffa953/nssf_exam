﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_student_import_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_stu_name = new System.Windows.Forms.TextBox();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dt_stu_dob = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_sex = new System.Windows.Forms.ComboBox();
            this.bds_sex = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbo_skill_id = new System.Windows.Forms.ComboBox();
            this.bds_skill = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.cbo_exam_date_id = new System.Windows.Forms.ComboBox();
            this.bds_exam_date = new System.Windows.Forms.BindingSource(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.cbo_cert_skill_id = new System.Windows.Forms.ComboBox();
            this.bds_cert_skill = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.cbo_cert_id = new System.Windows.Forms.ComboBox();
            this.bds_cert = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bds_place = new System.Windows.Forms.BindingSource(this.components);
            this.cbo_branch = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_sex)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_place)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txt_stu_name);
            this.panel1.Controls.Add(this.txt_phone);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dt_stu_dob);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbo_sex);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(737, 126);
            this.panel1.TabIndex = 0;
            // 
            // txt_stu_name
            // 
            this.txt_stu_name.BackColor = System.Drawing.SystemColors.Info;
            this.txt_stu_name.Location = new System.Drawing.Point(148, 27);
            this.txt_stu_name.Name = "txt_stu_name";
            this.txt_stu_name.Size = new System.Drawing.Size(209, 32);
            this.txt_stu_name.TabIndex = 8;
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.SystemColors.Info;
            this.txt_phone.Location = new System.Drawing.Point(493, 76);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.Size = new System.Drawing.Size(209, 32);
            this.txt_phone.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(414, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "លេខទូរស័ព្ទ";
            // 
            // dt_stu_dob
            // 
            this.dt_stu_dob.CustomFormat = "dd-MM-yyyy";
            this.dt_stu_dob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_stu_dob.Location = new System.Drawing.Point(148, 78);
            this.dt_stu_dob.Name = "dt_stu_dob";
            this.dt_stu_dob.Size = new System.Drawing.Size(209, 32);
            this.dt_stu_dob.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "ថ្ងៃខែឆ្នាំកំណើត";
            // 
            // cbo_sex
            // 
            this.cbo_sex.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_sex.DataSource = this.bds_sex;
            this.cbo_sex.DisplayMember = "GenderKH";
            this.cbo_sex.FormattingEnabled = true;
            this.cbo_sex.Location = new System.Drawing.Point(493, 27);
            this.cbo_sex.Name = "cbo_sex";
            this.cbo_sex.Size = new System.Drawing.Size(209, 32);
            this.cbo_sex.TabIndex = 3;
            this.cbo_sex.ValueMember = "GenderId";
            // 
            // bds_sex
            // 
            this.bds_sex.DataSource = typeof(NSSF_Exam.Entities.tbl_gender);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(414, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "ភេទ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "គោត្តនាម និងនាម";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 273);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(737, 53);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_close);
            this.panel4.Controls.Add(this.btn_add);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, -4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(737, 57);
            this.panel4.TabIndex = 2;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(602, 11);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(492, 11);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 0;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbo_branch);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.cbo_skill_id);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.cbo_exam_date_id);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.cbo_cert_skill_id);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.cbo_cert_id);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 126);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 147);
            this.panel3.TabIndex = 2;
            // 
            // cbo_skill_id
            // 
            this.cbo_skill_id.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_skill_id.DataSource = this.bds_skill;
            this.cbo_skill_id.DisplayMember = "skill";
            this.cbo_skill_id.FormattingEnabled = true;
            this.cbo_skill_id.Location = new System.Drawing.Point(493, 53);
            this.cbo_skill_id.Name = "cbo_skill_id";
            this.cbo_skill_id.Size = new System.Drawing.Size(209, 32);
            this.cbo_skill_id.TabIndex = 8;
            this.cbo_skill_id.ValueMember = "skill_id";
            this.cbo_skill_id.SelectedIndexChanged += new System.EventHandler(this.cbo_skill_id_SelectedIndexChanged);
            // 
            // bds_skill
            // 
            this.bds_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(414, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 24);
            this.label8.TabIndex = 7;
            this.label8.Text = "ផ្នែក";
            // 
            // cbo_exam_date_id
            // 
            this.cbo_exam_date_id.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_exam_date_id.DataSource = this.bds_exam_date;
            this.cbo_exam_date_id.DisplayMember = "exam_date";
            this.cbo_exam_date_id.FormatString = "dd-MM-yyyy";
            this.cbo_exam_date_id.FormattingEnabled = true;
            this.cbo_exam_date_id.Location = new System.Drawing.Point(148, 53);
            this.cbo_exam_date_id.Name = "cbo_exam_date_id";
            this.cbo_exam_date_id.Size = new System.Drawing.Size(209, 32);
            this.cbo_exam_date_id.TabIndex = 6;
            this.cbo_exam_date_id.ValueMember = "exam_id";
            // 
            // bds_exam_date
            // 
            this.bds_exam_date.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_date);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 24);
            this.label7.TabIndex = 5;
            this.label7.Text = "សម័យប្រលង";
            // 
            // cbo_cert_skill_id
            // 
            this.cbo_cert_skill_id.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_cert_skill_id.DataSource = this.bds_cert_skill;
            this.cbo_cert_skill_id.DisplayMember = "cert_skill_name";
            this.cbo_cert_skill_id.FormattingEnabled = true;
            this.cbo_cert_skill_id.Location = new System.Drawing.Point(493, 1);
            this.cbo_cert_skill_id.Name = "cbo_cert_skill_id";
            this.cbo_cert_skill_id.Size = new System.Drawing.Size(209, 32);
            this.cbo_cert_skill_id.TabIndex = 4;
            this.cbo_cert_skill_id.ValueMember = "cert_skill_id";
            // 
            // bds_cert_skill
            // 
            this.bds_cert_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate_skill);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(414, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 24);
            this.label6.TabIndex = 3;
            this.label6.Text = "ឯកទេស";
            // 
            // cbo_cert_id
            // 
            this.cbo_cert_id.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_cert_id.DataSource = this.bds_cert;
            this.cbo_cert_id.DisplayMember = "certificate_name";
            this.cbo_cert_id.FormattingEnabled = true;
            this.cbo_cert_id.Location = new System.Drawing.Point(148, 1);
            this.cbo_cert_id.Name = "cbo_cert_id";
            this.cbo_cert_id.Size = new System.Drawing.Size(209, 32);
            this.cbo_cert_id.TabIndex = 2;
            this.cbo_cert_id.ValueMember = "certificate_id";
            // 
            // bds_cert
            // 
            this.bds_cert.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "សញ្ញាប័ត្រ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "ទីតាំង";
            // 
            // bds_place
            // 
            this.bds_place.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_place);
            // 
            // cbo_branch
            // 
            this.cbo_branch.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_branch.DataSource = this.bds_place;
            this.cbo_branch.DisplayMember = "pla_name";
            this.cbo_branch.DropDownHeight = 150;
            this.cbo_branch.DropDownWidth = 450;
            this.cbo_branch.FormattingEnabled = true;
            this.cbo_branch.IntegralHeight = false;
            this.cbo_branch.Location = new System.Drawing.Point(148, 97);
            this.cbo_branch.Name = "cbo_branch";
            this.cbo_branch.Size = new System.Drawing.Size(209, 32);
            this.cbo_branch.TabIndex = 23;
            this.cbo_branch.ValueMember = "pla_id";
            // 
            // frm_student_import_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 326);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_student_import_edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ទាញយកបេក្ខជន";
            this.Load += new System.EventHandler(this.frm_student_import_edit_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_sex)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_cert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_place)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txt_phone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dt_stu_dob;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_sex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.ComboBox cbo_skill_id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbo_exam_date_id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbo_cert_skill_id;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbo_cert_id;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_stu_name;
        private System.Windows.Forms.BindingSource bds_sex;
        private System.Windows.Forms.BindingSource bds_cert;
        private System.Windows.Forms.BindingSource bds_cert_skill;
        private System.Windows.Forms.BindingSource bds_exam_date;
        private System.Windows.Forms.BindingSource bds_skill;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.BindingSource bds_place;
        private System.Windows.Forms.ComboBox cbo_branch;
    }
}