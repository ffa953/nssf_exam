﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_topic : Form
    {
        public frm_topic()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "វិញ្ញសារ");
        }

        private void frm_topic_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_dgv(dgv_topic);
            bds_topic.DataSource = ClsConfig.glo_local_db.tbl_exam_type.ToList();
        }

       

        private void dgv_topic_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_topic.Columns[e.ColumnIndex].Name == "ex_active")
            {
                if ((bool)dgv_topic.Rows[e.RowIndex].Cells["ex_active"].Value == false)
                {
                    e.Value = "មិនដំណើរការ";
                }
                else
                {
                    e.Value = "ដំណើរការ";
                }              
            }

            if (dgv_topic.Columns[e.ColumnIndex].Name == "ex_time")
            {
                if (Convert.ToInt32(dgv_topic.Rows[e.RowIndex].Cells["ex_time"].Value) !=0)
                {
                    int time =Convert.ToInt32(dgv_topic.Rows[e.RowIndex].Cells["ex_time"].Value);
                    e.Value = time / 60;
                }
            }

        }

        private void dgv_topic_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgv_topic.ClearSelection();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_topic_edit frm = new frm_topic_edit();
            frm.ShowDialog();
            frm_topic_Load(null, null);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_topic_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_topic_edit frm = new frm_topic_edit();
                frm.ex_type_id = (int)dgv_topic.CurrentRow.Cells["ex_type_id"].Value;
                frm.ShowDialog();
                frm_topic_Load(null, null);
            }
        }

        private void frm_topic_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }
    }
}
