﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_typing : Form
    {
        public frm_typing()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this,"ពត៍មានវាយអត្ថបទ");
        }

        private void frm_typing_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_typing);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            bds_typing.DataSource = ClsConfig.glo_local_db.tbl_typing.Where(m => m.active == true).ToList();
        }

        private void frm_typing_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_typing_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_typing.RowCount; i++)
            {
                dgv_typing.Rows[i].Cells["col"].Value = i + 1;
            }
            dgv_typing.ClearSelection();
        }

        private void dgv_typing_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_typing.Columns[e.ColumnIndex].Name == "active")
            {
                if ((bool)dgv_typing.Rows[e.RowIndex].Cells["active"].Value == false)
                {
                    e.Value = "មិនដំណើរការ";
                }
                else
                {
                    e.Value = "ដំណើរការ";
                }
            }
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_typing_edit frm = new frm_typing_edit();
            frm.ShowDialog();
            frm_typing_Load(null, null);
        }

        private void dgv_typing_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_typing_edit frm = new frm_typing_edit();
                frm.typ_id = (int)dgv_typing.CurrentRow.Cells["typ_id"].Value;
                frm.ShowDialog();
                frm_typing_Load(null, null);
            }
        }
    }
}
