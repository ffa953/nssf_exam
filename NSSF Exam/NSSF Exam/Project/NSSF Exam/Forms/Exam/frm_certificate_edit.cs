﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_certificate_edit : Form
    {
        public int cert_id=0;
        public frm_certificate_edit()
        {
            InitializeComponent();
        }

        private void frm_certificate_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            if (cert_id > 0)
            {
                var cert = (from c in ClsConfig.glo_local_db.tbl_certificate where c.certificate_id==cert_id select c).SingleOrDefault();
                txt_certificate.Text = cert.certificate_name;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            try
            {
                if (cert_id > 0)
                {
                    var cert = (from c in ClsConfig.glo_local_db.tbl_certificate where c.certificate_id==cert_id select c).SingleOrDefault();
                    cert.certificate_name = txt_certificate.Text;
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");
                    this.Close();
                }
                else
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required))
                    {
                ​​​        if(IsFieldValidate()){

                            tbl_certificate cert = new tbl_certificate();
                            cert.certificate_name = txt_certificate.Text;
                            ClsConfig.glo_local_db.tbl_certificate.Add(cert);
                            ClsConfig.glo_local_db.SaveChanges();
                       

                            var cer = (from c in ClsConfig.glo_local_db.tbl_certificate where c.certificate_id == cert.certificate_id select c).SingleOrDefault();
                            cer.certificate_order = cert.certificate_id;
                            ClsConfig.glo_local_db.SaveChanges();

                            tran.Complete();
                            txt_certificate.Text = "";
                            ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");

                             

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ClsMsg.Error("" + ex);
            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";
            if (txt_certificate.Text == "")
            {
                msgString += "- សញ្ញាប័ត្រត្រូវតែបញ្ចូល\n";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
