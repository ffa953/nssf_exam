﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_certificate_skill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_certificate = new System.Windows.Forms.DataGridView();
            this.bds_certificate_skill = new System.Windows.Forms.BindingSource(this.components);
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.cert_skill_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.certskillnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.certskillorderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userProfilesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_certificate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_certificate_skill)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 69);
            this.panel1.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(690, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(580, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_certificate
            // 
            this.dgv_certificate.AutoGenerateColumns = false;
            this.dgv_certificate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_certificate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.cert_skill_id,
            this.certskillnameDataGridViewTextBoxColumn,
            this.certskillorderDataGridViewTextBoxColumn,
            this.userProfilesDataGridViewTextBoxColumn});
            this.dgv_certificate.DataSource = this.bds_certificate_skill;
            this.dgv_certificate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_certificate.Location = new System.Drawing.Point(0, 69);
            this.dgv_certificate.Name = "dgv_certificate";
            this.dgv_certificate.Size = new System.Drawing.Size(803, 369);
            this.dgv_certificate.TabIndex = 1;
            this.dgv_certificate.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_certificate_CellClick);
            // 
            // bds_certificate_skill
            // 
            this.bds_certificate_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate_skill);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Width = 90;
            // 
            // cert_skill_id
            // 
            this.cert_skill_id.DataPropertyName = "cert_skill_id";
            this.cert_skill_id.HeaderText = "ល.រ";
            this.cert_skill_id.Name = "cert_skill_id";
            this.cert_skill_id.Visible = false;
            this.cert_skill_id.Width = 70;
            // 
            // certskillnameDataGridViewTextBoxColumn
            // 
            this.certskillnameDataGridViewTextBoxColumn.DataPropertyName = "cert_skill_name";
            this.certskillnameDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះជំនាញ";
            this.certskillnameDataGridViewTextBoxColumn.Name = "certskillnameDataGridViewTextBoxColumn";
            this.certskillnameDataGridViewTextBoxColumn.Width = 300;
            // 
            // certskillorderDataGridViewTextBoxColumn
            // 
            this.certskillorderDataGridViewTextBoxColumn.DataPropertyName = "cert_skill_order";
            this.certskillorderDataGridViewTextBoxColumn.HeaderText = "cert_skill_order";
            this.certskillorderDataGridViewTextBoxColumn.Name = "certskillorderDataGridViewTextBoxColumn";
            this.certskillorderDataGridViewTextBoxColumn.Visible = false;
            // 
            // userProfilesDataGridViewTextBoxColumn
            // 
            this.userProfilesDataGridViewTextBoxColumn.DataPropertyName = "UserProfiles";
            this.userProfilesDataGridViewTextBoxColumn.HeaderText = "UserProfiles";
            this.userProfilesDataGridViewTextBoxColumn.Name = "userProfilesDataGridViewTextBoxColumn";
            this.userProfilesDataGridViewTextBoxColumn.Visible = false;
            // 
            // frm_certificate_skill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 438);
            this.Controls.Add(this.dgv_certificate);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_certificate_skill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_certificate";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_certificate_FormClosed);
            this.Load += new System.EventHandler(this.frm_certificate_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_certificate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_certificate_skill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_certificate;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.BindingSource bds_certificate_skill;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cert_skill_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn certskillnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn certskillorderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userProfilesDataGridViewTextBoxColumn;
    }
}