﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_student_import : Form
    {
        public frm_student_import()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this,"ទាញយកបេក្ខជនចាស់");
        }

        private void frm_student_history_Load(object sender, EventArgs e)
        {
            ClsSetting.OpenPermission(this, this.Name, ClsConfig.glo_use_privilege);
            ClsSetting.sty_btn_search(btn_search);
            ClsSetting.sty_btn_refresh(btn_refresh);
            ClsSetting.sty_btn_close(btn_exit);
            ClsSetting.sty_dgv(dgv_stu_his);

            GenBindingSource.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            bds_exam_date.DataSource = ClsConfig.glo_local_db.tbl_exam_date.OrderByDescending(m => m.exam_id).ToList();
            SkillBindingSource.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();
            CertBindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            CertSkill_BindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();

            cbo_exam_date.SelectedIndex = -1;
           
        }

        private void frm_student_history_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
          StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_get_stuDate(2,Convert.ToInt32(cbo_exam_date.SelectedValue));
        }

        private void dgv_stu_his_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_stu_his.RowCount; i++)
            {
                dgv_stu_his.Rows[i].Cells["col"].Value = i + 1;
            }
            dgv_stu_his.ClearSelection();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            string search_text = txt_search.Text;
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_get_stuDate(2,Convert.ToInt32(cbo_exam_date.SelectedValue)).Where(m => m.Name_khmer.Contains(search_text) || m.Name_latin.Contains(search_text) || m.UserName.Contains(search_text) || m.skill_note.Contains(search_text)).OrderByDescending(u => u.UserId).ToList();

           
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            if (dgv_stu_his.Rows.Count > 0)
            {
                int rowindex = dgv_stu_his.CurrentCell.RowIndex;
                int id = (int)dgv_stu_his.Rows[rowindex].Cells[0].Value;
                frm_student_import_edit frm = new frm_student_import_edit();
                frm.userID = id;
                frm.ShowDialog();
            }
            else
            {
                ClsMsg.Warning("ទិន្នន័យទទេរ!!!");
            }
        }

        private void cbo_exam_date_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
