﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_subject_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_subtitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv_sub = new System.Windows.Forms.DataGridView();
            this.del = new System.Windows.Forms.DataGridViewImageColumn();
            this.skill_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bds_skill = new System.Windows.Forms.BindingSource(this.components);
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quiz_limit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_subject_skill = new System.Windows.Forms.BindingSource(this.components);
            this.chk_active = new System.Windows.Forms.CheckBox();
            this.cbo_skill = new System.Windows.Forms.ComboBox();
            this.txt_answer_num = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_subject_skill)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 411);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(806, 69);
            this.panel1.TabIndex = 3;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(676, 12);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(566, 12);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "ឈ្មោះមុខវិជ្ជា";
            // 
            // txt_subtitle
            // 
            this.txt_subtitle.BackColor = System.Drawing.SystemColors.Info;
            this.txt_subtitle.Location = new System.Drawing.Point(119, 42);
            this.txt_subtitle.Name = "txt_subtitle";
            this.txt_subtitle.Size = new System.Drawing.Size(230, 32);
            this.txt_subtitle.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "ជំនាញ";
            // 
            // dgv_sub
            // 
            this.dgv_sub.AutoGenerateColumns = false;
            this.dgv_sub.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_sub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sub.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.del,
            this.skill_id,
            this.id,
            this.quiz_limit});
            this.dgv_sub.DataSource = this.bds_subject_skill;
            this.dgv_sub.GridColor = System.Drawing.SystemColors.Control;
            this.dgv_sub.Location = new System.Drawing.Point(26, 198);
            this.dgv_sub.Name = "dgv_sub";
            this.dgv_sub.Size = new System.Drawing.Size(750, 207);
            this.dgv_sub.TabIndex = 7;
            this.dgv_sub.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_sub_CellClick);
            // 
            // del
            // 
            this.del.HeaderText = "លុប";
            this.del.Image = global::NSSF_Exam.Properties.Resources.btn_delete_color;
            this.del.Name = "del";
            // 
            // skill_id
            // 
            this.skill_id.DataPropertyName = "skill_id";
            this.skill_id.DataSource = this.bds_skill;
            this.skill_id.DisplayMember = "skill";
            this.skill_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.skill_id.HeaderText = "ជំនាញ";
            this.skill_id.Name = "skill_id";
            this.skill_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.skill_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.skill_id.ValueMember = "skill_id";
            this.skill_id.Width = 370;
            // 
            // bds_skill
            // 
            this.bds_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // id
            // 
            this.id.DataPropertyName = "sub_id";
            this.id.HeaderText = "sub_id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // quiz_limit
            // 
            this.quiz_limit.DataPropertyName = "quiz_limit";
            this.quiz_limit.HeaderText = "ចំនួនសំនួរ";
            this.quiz_limit.Name = "quiz_limit";
            this.quiz_limit.Width = 250;
            // 
            // bds_subject_skill
            // 
            this.bds_subject_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_subject_in_skill);
            // 
            // chk_active
            // 
            this.chk_active.AutoSize = true;
            this.chk_active.Location = new System.Drawing.Point(437, 46);
            this.chk_active.Name = "chk_active";
            this.chk_active.Size = new System.Drawing.Size(81, 28);
            this.chk_active.TabIndex = 23;
            this.chk_active.Text = "ដំណើរការ";
            this.chk_active.UseVisualStyleBackColor = true;
            // 
            // cbo_skill
            // 
            this.cbo_skill.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_skill.DataSource = this.bds_skill;
            this.cbo_skill.DisplayMember = "skill";
            this.cbo_skill.DropDownHeight = 120;
            this.cbo_skill.FormattingEnabled = true;
            this.cbo_skill.IntegralHeight = false;
            this.cbo_skill.Location = new System.Drawing.Point(119, 98);
            this.cbo_skill.Name = "cbo_skill";
            this.cbo_skill.Size = new System.Drawing.Size(230, 32);
            this.cbo_skill.TabIndex = 24;
            this.cbo_skill.ValueMember = "skill_id";
            // 
            // txt_answer_num
            // 
            this.txt_answer_num.BackColor = System.Drawing.SystemColors.Info;
            this.txt_answer_num.Location = new System.Drawing.Point(546, 98);
            this.txt_answer_num.Name = "txt_answer_num";
            this.txt_answer_num.Size = new System.Drawing.Size(230, 32);
            this.txt_answer_num.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(442, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 24);
            this.label3.TabIndex = 25;
            this.label3.Text = "ចំនួនសំនួរ";
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_add.Location = new System.Drawing.Point(567, 158);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 27;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_clear.Location = new System.Drawing.Point(677, 158);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(100, 34);
            this.btn_clear.TabIndex = 28;
            this.btn_clear.Text = "button1";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // frm_subject_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 480);
            this.ControlBox = false;
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.txt_answer_num);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbo_skill);
            this.Controls.Add(this.chk_active);
            this.Controls.Add(this.dgv_sub);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_subtitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_subject_edit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "កំណត់មុខវីជ្ជា";
            this.Load += new System.EventHandler(this.frm_subject_edit_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_subject_skill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_subtitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv_sub;
        private System.Windows.Forms.CheckBox chk_active;
        private System.Windows.Forms.ComboBox cbo_skill;
        private System.Windows.Forms.TextBox txt_answer_num;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.BindingSource bds_skill;
        private System.Windows.Forms.BindingSource bds_subject_skill;
        private System.Windows.Forms.DataGridViewImageColumn del;
        private System.Windows.Forms.DataGridViewComboBoxColumn skill_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn quiz_limit;
        private System.Windows.Forms.Button btn_clear;
    }
}