﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_quiz_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_subject = new System.Windows.Forms.ComboBox();
            this.bds_sub = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_type = new System.Windows.Forms.ComboBox();
            this.bds_type = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txt_score = new System.Windows.Forms.TextBox();
            this.chk_active = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_clear = new System.Windows.Forms.Button();
            this.rb_false = new System.Windows.Forms.RadioButton();
            this.btn_new = new System.Windows.Forms.Button();
            this.rb_true = new System.Windows.Forms.RadioButton();
            this.dgv_ans = new System.Windows.Forms.DataGridView();
            this.del = new System.Windows.Forms.DataGridViewImageColumn();
            this.ans_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ans_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ans_choice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_quiz_ans = new System.Windows.Forms.BindingSource(this.components);
            this.txt_quiz_title = new System.Windows.Forms.TextBox();
            this.txt_ans_title = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.bds_sub)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_type)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_quiz_ans)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "មុខវិជ្ជា";
            // 
            // cbo_subject
            // 
            this.cbo_subject.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_subject.DataSource = this.bds_sub;
            this.cbo_subject.DisplayMember = "sub_title";
            this.cbo_subject.DropDownHeight = 120;
            this.cbo_subject.FormattingEnabled = true;
            this.cbo_subject.IntegralHeight = false;
            this.cbo_subject.Location = new System.Drawing.Point(97, 38);
            this.cbo_subject.Name = "cbo_subject";
            this.cbo_subject.Size = new System.Drawing.Size(312, 32);
            this.cbo_subject.TabIndex = 1;
            this.cbo_subject.ValueMember = "sub_id";
            // 
            // bds_sub
            // 
            this.bds_sub.DataSource = typeof(NSSF_Exam.Entities.tbl_subject);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_add);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 555);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 69);
            this.panel1.TabIndex = 9;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(640, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 3;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_add.Location = new System.Drawing.Point(534, 14);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 29;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "សំនួរ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(435, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 24);
            this.label3.TabIndex = 12;
            this.label3.Text = "ប្រភេទសំនួរ";
            // 
            // cbo_type
            // 
            this.cbo_type.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_type.DataSource = this.bds_type;
            this.cbo_type.DisplayMember = "type_name";
            this.cbo_type.FormattingEnabled = true;
            this.cbo_type.Location = new System.Drawing.Point(512, 38);
            this.cbo_type.Name = "cbo_type";
            this.cbo_type.Size = new System.Drawing.Size(228, 32);
            this.cbo_type.TabIndex = 13;
            this.cbo_type.ValueMember = "type_id";
            // 
            // bds_type
            // 
            this.bds_type.DataSource = typeof(NSSF_Exam.Entities.tbl_quiz_type);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(435, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 24);
            this.label4.TabIndex = 14;
            this.label4.Text = "ពិន្ទុ";
            // 
            // txt_score
            // 
            this.txt_score.BackColor = System.Drawing.SystemColors.Info;
            this.txt_score.Location = new System.Drawing.Point(512, 86);
            this.txt_score.Name = "txt_score";
            this.txt_score.Size = new System.Drawing.Size(228, 32);
            this.txt_score.TabIndex = 15;
            // 
            // chk_active
            // 
            this.chk_active.AutoSize = true;
            this.chk_active.Location = new System.Drawing.Point(439, 159);
            this.chk_active.Name = "chk_active";
            this.chk_active.Size = new System.Drawing.Size(81, 28);
            this.chk_active.TabIndex = 16;
            this.chk_active.Text = "ដំណើរការ";
            this.chk_active.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 24);
            this.label5.TabIndex = 18;
            this.label5.Text = "ចម្លើយ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(435, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 24);
            this.label6.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_clear);
            this.panel2.Controls.Add(this.rb_false);
            this.panel2.Controls.Add(this.btn_new);
            this.panel2.Controls.Add(this.rb_true);
            this.panel2.Location = new System.Drawing.Point(439, 206);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(301, 100);
            this.panel2.TabIndex = 20;
            // 
            // btn_clear
            // 
            this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_clear.Location = new System.Drawing.Point(106, 66);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(100, 34);
            this.btn_clear.TabIndex = 22;
            this.btn_clear.Text = "button1";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // rb_false
            // 
            this.rb_false.AutoSize = true;
            this.rb_false.Location = new System.Drawing.Point(85, 6);
            this.rb_false.Name = "rb_false";
            this.rb_false.Size = new System.Drawing.Size(85, 28);
            this.rb_false.TabIndex = 21;
            this.rb_false.TabStop = true;
            this.rb_false.Text = "មិនត្រឹមត្រូវ";
            this.rb_false.UseVisualStyleBackColor = true;
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(0, 66);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 2;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // rb_true
            // 
            this.rb_true.AutoSize = true;
            this.rb_true.Location = new System.Drawing.Point(3, 6);
            this.rb_true.Name = "rb_true";
            this.rb_true.Size = new System.Drawing.Size(67, 28);
            this.rb_true.TabIndex = 0;
            this.rb_true.TabStop = true;
            this.rb_true.Text = "ត្រឹមត្រូវ";
            this.rb_true.UseVisualStyleBackColor = true;
            // 
            // dgv_ans
            // 
            this.dgv_ans.AutoGenerateColumns = false;
            this.dgv_ans.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv_ans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.del,
            this.ans_id,
            this.ans_title,
            this.ans_choice});
            this.dgv_ans.DataSource = this.bds_quiz_ans;
            this.dgv_ans.GridColor = System.Drawing.SystemColors.Control;
            this.dgv_ans.Location = new System.Drawing.Point(36, 327);
            this.dgv_ans.Name = "dgv_ans";
            this.dgv_ans.Size = new System.Drawing.Size(704, 227);
            this.dgv_ans.TabIndex = 31;
            this.dgv_ans.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ans_CellClick);
            this.dgv_ans.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_ans_CellFormatting);
            // 
            // del
            // 
            this.del.HeaderText = "លុប";
            this.del.Image = global::NSSF_Exam.Properties.Resources.btn_delete_color;
            this.del.Name = "del";
            this.del.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.del.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ans_id
            // 
            this.ans_id.DataPropertyName = "ans_id";
            this.ans_id.HeaderText = "ans_id";
            this.ans_id.Name = "ans_id";
            this.ans_id.Visible = false;
            // 
            // ans_title
            // 
            this.ans_title.DataPropertyName = "ans_title";
            this.ans_title.HeaderText = "ចម្លើយ";
            this.ans_title.Name = "ans_title";
            this.ans_title.Width = 480;
            // 
            // ans_choice
            // 
            this.ans_choice.DataPropertyName = "ans_choice";
            this.ans_choice.HeaderText = "ត្រឹមត្រូវ";
            this.ans_choice.Name = "ans_choice";
            this.ans_choice.Width = 120;
            // 
            // bds_quiz_ans
            // 
            this.bds_quiz_ans.DataSource = typeof(NSSF_Exam.Entities.tbl_quiz_answer);
            // 
            // txt_quiz_title
            // 
            this.txt_quiz_title.BackColor = System.Drawing.SystemColors.Info;
            this.txt_quiz_title.Location = new System.Drawing.Point(97, 86);
            this.txt_quiz_title.Multiline = true;
            this.txt_quiz_title.Name = "txt_quiz_title";
            this.txt_quiz_title.Size = new System.Drawing.Size(312, 101);
            this.txt_quiz_title.TabIndex = 32;
            // 
            // txt_ans_title
            // 
            this.txt_ans_title.BackColor = System.Drawing.SystemColors.Info;
            this.txt_ans_title.Location = new System.Drawing.Point(97, 205);
            this.txt_ans_title.Multiline = true;
            this.txt_ans_title.Name = "txt_ans_title";
            this.txt_ans_title.Size = new System.Drawing.Size(312, 101);
            this.txt_ans_title.TabIndex = 33;
            // 
            // frm_quiz_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 624);
            this.ControlBox = false;
            this.Controls.Add(this.txt_ans_title);
            this.Controls.Add(this.txt_quiz_title);
            this.Controls.Add(this.dgv_ans);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chk_active);
            this.Controls.Add(this.txt_score);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbo_type);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbo_subject);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_quiz_edit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "   ";
            this.Load += new System.EventHandler(this.frm_quiz_edit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bds_sub)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bds_type)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_quiz_ans)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_subject;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_type;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_score;
        private System.Windows.Forms.CheckBox chk_active;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rb_false;
        private System.Windows.Forms.RadioButton rb_true;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.DataGridView dgv_ans;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.BindingSource bds_sub;
        private System.Windows.Forms.BindingSource bds_type;
        private System.Windows.Forms.BindingSource bds_quiz_ans;
        private System.Windows.Forms.DataGridViewImageColumn del;
        private System.Windows.Forms.DataGridViewTextBoxColumn ans_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ans_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn ans_choice;
        private System.Windows.Forms.TextBox txt_quiz_title;
        private System.Windows.Forms.TextBox txt_ans_title;
        private System.Windows.Forms.Button btn_clear;
    }
}