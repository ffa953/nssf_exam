﻿namespace NSSF_Exam.Forms.Exam.IQ_Test
{
    partial class frm_iq_exam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_timer = new System.Windows.Forms.Label();
            this.lbl_skill = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_subject = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_table_no = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_id = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_dob = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.quiz_timer = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl_question = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbl_question_pos = new System.Windows.Forms.Label();
            this.lbl_quiz = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_answer = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.bds_iq_question = new System.Windows.Forms.BindingSource(this.components);
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_previous = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbl = new System.Windows.Forms.Label();
            this.lbl_question_id = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_iq_question)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_timer
            // 
            this.lbl_timer.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl_timer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_timer.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbl_timer.Font = new System.Drawing.Font("Stencil", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timer.ForeColor = System.Drawing.Color.Navy;
            this.lbl_timer.Location = new System.Drawing.Point(1014, 0);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(158, 93);
            this.lbl_timer.TabIndex = 42;
            this.lbl_timer.Text = "00:00:00";
            this.lbl_timer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_skill
            // 
            this.lbl_skill.AutoSize = true;
            this.lbl_skill.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_skill.Location = new System.Drawing.Point(474, 54);
            this.lbl_skill.Name = "lbl_skill";
            this.lbl_skill.Size = new System.Drawing.Size(101, 22);
            this.lbl_skill.TabIndex = 11;
            this.lbl_skill.Text = "វិទ្យាសាស្រ្តកុំព្យូទ័រ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(414, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 24);
            this.label10.TabIndex = 10;
            this.label10.Text = "ឯកទេស:";
            // 
            // lbl_subject
            // 
            this.lbl_subject.AutoSize = true;
            this.lbl_subject.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_subject.Location = new System.Drawing.Point(474, 15);
            this.lbl_subject.Name = "lbl_subject";
            this.lbl_subject.Size = new System.Drawing.Size(72, 22);
            this.lbl_subject.TabIndex = 9;
            this.lbl_subject.Text = "ពត៌មានវិទ្យា";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(436, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 24);
            this.label12.TabIndex = 8;
            this.label12.Text = "ផ្នែក:";
            // 
            // lbl_table_no
            // 
            this.lbl_table_no.AutoSize = true;
            this.lbl_table_no.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_table_no.Location = new System.Drawing.Point(313, 14);
            this.lbl_table_no.Name = "lbl_table_no";
            this.lbl_table_no.Size = new System.Drawing.Size(43, 22);
            this.lbl_table_no.TabIndex = 7;
            this.lbl_table_no.Text = "IT-89";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(264, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 24);
            this.label8.TabIndex = 6;
            this.label8.Text = "លេខតុ:";
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.Location = new System.Drawing.Point(99, 15);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(74, 22);
            this.lbl_id.TabIndex = 5;
            this.lbl_id.Text = "00124545";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 24);
            this.label6.TabIndex = 4;
            this.label6.Text = "លេខសំគាល់:";
            // 
            // lbl_dob
            // 
            this.lbl_dob.AutoSize = true;
            this.lbl_dob.Font = new System.Drawing.Font("Stencil", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dob.Location = new System.Drawing.Point(313, 56);
            this.lbl_dob.Name = "lbl_dob";
            this.lbl_dob.Size = new System.Drawing.Size(82, 17);
            this.lbl_dob.TabIndex = 3;
            this.lbl_dob.Text = "10/04/1992";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(99, 54);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(93, 22);
            this.lbl_name.TabIndex = 2;
            this.lbl_name.Text = "ហេង មួយឡាយ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(212, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "ថ្ងៃ/ខែ/ឆ្នាំកំណើត:";
            // 
            // quiz_timer
            // 
            this.quiz_timer.Enabled = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "គោតនាម/នាម:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_timer);
            this.panel1.Controls.Add(this.lbl_skill);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lbl_subject);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lbl_table_no);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbl_id);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lbl_dob);
            this.panel1.Controls.Add(this.lbl_name);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1174, 95);
            this.panel1.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(581, 385);
            this.panel4.TabIndex = 31;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::NSSF_Exam.Properties.Resources.photo_2018_06_06_11_54_30;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(581, 385);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lbl_question);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(581, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(591, 270);
            this.flowLayoutPanel1.TabIndex = 41;
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_question.Font = new System.Drawing.Font("Khmer OS Muol Light", 15F);
            this.lbl_question.Location = new System.Drawing.Point(3, 198);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Padding = new System.Windows.Forms.Padding(120, 0, 0, 0);
            this.lbl_question.Size = new System.Drawing.Size(530, 72);
            this.lbl_question.TabIndex = 40;
            this.lbl_question.Text = " ​តើប្រាសាទ​ព្រះវិហារ​ត្រូវបាន​​​ចុះបញ្ជី​ជា​បេតិកភណ្ឌ​​ពិភពលោកនៅថ្ងៃខែឆ្នាំណា?";
            this.lbl_question.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lbl_question_pos);
            this.panel5.Controls.Add(this.lbl_quiz);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 139);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(530, 56);
            this.panel5.TabIndex = 41;
            // 
            // lbl_question_pos
            // 
            this.lbl_question_pos.BackColor = System.Drawing.Color.Transparent;
            this.lbl_question_pos.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_question_pos.Font = new System.Drawing.Font("Khmer OS Muol Light", 15F);
            this.lbl_question_pos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_question_pos.Location = new System.Drawing.Point(116, 0);
            this.lbl_question_pos.Name = "lbl_question_pos";
            this.lbl_question_pos.Size = new System.Drawing.Size(31, 56);
            this.lbl_question_pos.TabIndex = 42;
            this.lbl_question_pos.Text = "១";
            this.lbl_question_pos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_quiz
            // 
            this.lbl_quiz.BackColor = System.Drawing.Color.Transparent;
            this.lbl_quiz.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_quiz.Font = new System.Drawing.Font("Khmer OS Muol Light", 15F);
            this.lbl_quiz.ForeColor = System.Drawing.Color.Navy;
            this.lbl_quiz.Location = new System.Drawing.Point(0, 0);
            this.lbl_quiz.Name = "lbl_quiz";
            this.lbl_quiz.Size = new System.Drawing.Size(116, 56);
            this.lbl_quiz.TabIndex = 41;
            this.lbl_quiz.Text = "សំនួរទី៖";
            this.lbl_quiz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 95);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1174, 387);
            this.panel2.TabIndex = 7;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(581, 270);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(591, 124);
            this.panel6.TabIndex = 43;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.txt_answer);
            this.panel8.Controls.Add(this.btn_save);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(591, 100);
            this.panel8.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Khmer OS Muol Light", 15F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(9, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 32);
            this.label3.TabIndex = 43;
            this.label3.Text = "ចំលើយ៖";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_answer
            // 
            this.txt_answer.Location = new System.Drawing.Point(134, 37);
            this.txt_answer.Name = "txt_answer";
            this.txt_answer.Size = new System.Drawing.Size(174, 32);
            this.txt_answer.TabIndex = 0;
            // 
            // btn_save
            // 
            this.btn_save.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_save.ForeColor = System.Drawing.Color.Red;
            this.btn_save.Location = new System.Drawing.Point(314, 35);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(100, 35);
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "បន្ត >>";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // bds_iq_question
            // 
            this.bds_iq_question.DataSource = typeof(NSSF_Exam.Entities.vw_iq_question);
            // 
            // btn_next
            // 
            this.btn_next.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_next.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_next.FlatAppearance.BorderSize = 0;
            this.btn_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_next.Image = global::NSSF_Exam.Properties.Resources.play_symbol;
            this.btn_next.Location = new System.Drawing.Point(650, 25);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(47, 35);
            this.btn_next.TabIndex = 0;
            this.btn_next.UseVisualStyleBackColor = false;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_previous
            // 
            this.btn_previous.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_previous.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_previous.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_previous.FlatAppearance.BorderSize = 0;
            this.btn_previous.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_previous.Image = global::NSSF_Exam.Properties.Resources.previous;
            this.btn_previous.Location = new System.Drawing.Point(591, 25);
            this.btn_previous.Name = "btn_previous";
            this.btn_previous.Size = new System.Drawing.Size(47, 35);
            this.btn_previous.TabIndex = 1;
            this.btn_previous.UseVisualStyleBackColor = false;
            this.btn_previous.Click += new System.EventHandler(this.btn_previous_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lbl);
            this.panel3.Controls.Add(this.btn_previous);
            this.panel3.Controls.Add(this.btn_next);
            this.panel3.Controls.Add(this.lbl_question_id);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 482);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1174, 89);
            this.panel3.TabIndex = 6;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(3, 36);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(49, 24);
            this.lbl.TabIndex = 43;
            this.lbl.Text = "label14";
            // 
            // lbl_question_id
            // 
            this.lbl_question_id.AutoSize = true;
            this.lbl_question_id.Location = new System.Drawing.Point(3, 4);
            this.lbl_question_id.Name = "lbl_question_id";
            this.lbl_question_id.Size = new System.Drawing.Size(49, 24);
            this.lbl_question_id.TabIndex = 19;
            this.lbl_question_id.Text = "label14";
            // 
            // frm_iq_exam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 571);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_iq_exam";
            this.Text = "frm_iq_exam";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_iq_exam_FormClosed);
            this.Load += new System.EventHandler(this.frm_iq_exam_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_iq_question)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_timer;
        private System.Windows.Forms.Label lbl_skill;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_subject;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_table_no;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_dob;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer quiz_timer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource bds_iq_question;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbl_question_pos;
        private System.Windows.Forms.Label lbl_quiz;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txt_answer;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_previous;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label lbl_question_id;
    }
}