﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Properties;
using NSSF_Exam.Entities;
using System.IO;
using System.Transactions;

namespace NSSF_Exam.Forms.Exam.IQ_Test
{
    public partial class frm_iq_question_edit : Form
    {
        public int iq_id =0;
        public frm_iq_question_edit()
        {
            InitializeComponent();       
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_iq_question_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_close);
     
             if(iq_id>0)
             {
                 tbl_iq_question qu = ClsConfig.glo_local_db.tbl_iq_question.Find(iq_id);
                 txt_question.Text = qu.iq_question;

                 tbl_image im = (from i in ClsConfig.glo_local_db.tbl_image where i.iq_id == qu.iq_id select i).SingleOrDefault();
                 if(im != null)
                 {
                     Byte[] data = new Byte[0];
                     data = (Byte[])(im.im_image);
                     MemoryStream mem = new MemoryStream(data);
                     pictureBox1.Image = Image.FromStream(mem);
                 }

                 tbl_iq_answer ans = (from a in ClsConfig.glo_local_db.tbl_iq_answer where a.iq_id == qu.iq_id select a).SingleOrDefault();
                 txt_answer.Text = ans.ans_answer;
                 txt_score.Text = Convert.ToString( ans.score );
             }
          
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
          
        }
        public static byte[] ImageToByte2(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {

            if (iq_id == 0)
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        tbl_iq_question ques = new tbl_iq_question();
                        ques.iq_question = txt_question.Text.TrimStart().TrimEnd();
                        ques.use_id = ClsConfig.glo_use_id;
                        ques.use_edit = DateTime.Now;
                        ClsConfig.glo_local_db.tbl_iq_question.Add(ques);

                        if (pictureBox1.Image != null)
                        {
                            tbl_image img = new tbl_image();
                            img.iq_id = ques.iq_id;
                            Image im = pictureBox1.Image;
                            img.im_image = ImageToByte2(im);
                            ClsConfig.glo_local_db.tbl_image.Add(img);
                        }

                        tbl_iq_answer ans = new tbl_iq_answer();
                        ans.iq_id = ques.iq_id;
                        ans.ans_answer = txt_answer.Text;
                        ans.score =Convert.ToInt32( txt_score.Text );
                        ClsConfig.glo_local_db.tbl_iq_answer.Add(ans);

                        tran.Complete();
                        tran.Dispose();
                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        ClsMsg.Error(ex.ToString());
                    }
                }
            }
            else
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        tbl_iq_question ques = ClsConfig.glo_local_db.tbl_iq_question.Find(iq_id);
                        ques.iq_question = txt_question.Text.TrimStart().TrimEnd();
                        ques.use_id = ClsConfig.glo_use_id;
                        ques.use_edit = DateTime.Now;

                        int id = ques.iq_id;
                        tbl_image img = (from i in ClsConfig.glo_local_db.tbl_image where i.iq_id == id select i).SingleOrDefault();
                        if (img != null)
                        {
                            img.iq_id = ques.iq_id;
                            Image im = pictureBox1.Image;
                            img.im_image = ImageToByte2(im);
                        }

                        tbl_iq_answer ans =  (from an in ClsConfig.glo_local_db.tbl_iq_answer where an.iq_id == id select an).SingleOrDefault();
                        ans.iq_id = ques.iq_id;
                        ans.ans_answer = txt_answer.Text;
                        ans.score = Convert.ToInt32 ( txt_score.Text );

                        tran.Complete();
                        tran.Dispose();

                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    }
                    catch (Exception ex)
                    {
                        tran.Dispose();
                        ClsMsg.Error(ex.ToString());
                    }
                }
            }
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.png) | *.jpg;  *.png";
            dialog.FilterIndex = 0;
            dialog.RestoreDirectory = true;
            dialog.Title = "Please select an image.";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = new Bitmap(dialog.FileName);
            }   
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.pictureBox1, "Double click to browse image !!!");
        }
    }
}
