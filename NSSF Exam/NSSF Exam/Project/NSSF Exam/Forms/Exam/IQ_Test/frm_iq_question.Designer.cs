﻿namespace NSSF_Exam.Forms.Exam.IQ_Test
{
    partial class frm_iq_question
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.bds_iq_question = new System.Windows.Forms.BindingSource(this.components);
            this.iqidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edit = new System.Windows.Forms.DataGridViewImageColumn();
            this.col_lr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iqquestionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_iq_question)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_add.Location = new System.Drawing.Point(878, 74);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 35);
            this.btn_add.TabIndex = 8;
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Muol Light", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(354, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(294, 34);
            this.label1.TabIndex = 7;
            this.label1.Text = "បញ្ចូលផ្នែកនៃសំនួរក្នុងIQតេស្ត";
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvlst.AutoGenerateColumns = false;
            this.dgvlst.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iqidDataGridViewTextBoxColumn,
            this.edit,
            this.col_lr,
            this.iqquestionDataGridViewTextBoxColumn});
            this.dgvlst.DataSource = this.bds_iq_question;
            this.dgvlst.Location = new System.Drawing.Point(27, 115);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(951, 309);
            this.dgvlst.TabIndex = 9;
            this.dgvlst.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellContentClick);
            this.dgvlst.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvlst_DataBindingComplete);
            // 
            // bds_iq_question
            // 
            this.bds_iq_question.DataSource = typeof(NSSF_Exam.Entities.tbl_iq_question);
            // 
            // iqidDataGridViewTextBoxColumn
            // 
            this.iqidDataGridViewTextBoxColumn.DataPropertyName = "iq_id";
            this.iqidDataGridViewTextBoxColumn.HeaderText = "iq_id";
            this.iqidDataGridViewTextBoxColumn.Name = "iqidDataGridViewTextBoxColumn";
            this.iqidDataGridViewTextBoxColumn.ReadOnly = true;
            this.iqidDataGridViewTextBoxColumn.Visible = false;
            // 
            // edit
            // 
            this.edit.HeaderText = "កែប្រែ";
            this.edit.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.edit.Name = "edit";
            this.edit.ReadOnly = true;
            this.edit.Width = 80;
            // 
            // col_lr
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_lr.HeaderText = "ល.រ";
            this.col_lr.Name = "col_lr";
            this.col_lr.ReadOnly = true;
            this.col_lr.Width = 60;
            // 
            // iqquestionDataGridViewTextBoxColumn
            // 
            this.iqquestionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.iqquestionDataGridViewTextBoxColumn.DataPropertyName = "iq_question";
            this.iqquestionDataGridViewTextBoxColumn.HeaderText = "សំនួរ";
            this.iqquestionDataGridViewTextBoxColumn.Name = "iqquestionDataGridViewTextBoxColumn";
            this.iqquestionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // frm_iq_question
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 436);
            this.Controls.Add(this.dgvlst);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_iq_question";
            this.Text = "frm_iq_question";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frm_iq_question_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_iq_question)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.BindingSource bds_iq_question;
        private System.Windows.Forms.DataGridViewTextBoxColumn iqidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn edit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr;
        private System.Windows.Forms.DataGridViewTextBoxColumn iqquestionDataGridViewTextBoxColumn;
    }
}