﻿namespace NSSF_Exam.Forms.Exam.IQ_Quiz
{
    partial class frm_iq_testing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("១. បញ្ចូលសំនួរIQតេស្ត");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("២. កំណត់ចំណាំចម្លើយ", 5, 5);
            this.tree_view = new System.Windows.Forms.TreeView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.pan_main = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tree_view
            // 
            this.tree_view.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tree_view.Cursor = System.Windows.Forms.Cursors.Default;
            this.tree_view.Dock = System.Windows.Forms.DockStyle.Left;
            this.tree_view.Font = new System.Drawing.Font("Khmer OS Content", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tree_view.Indent = 25;
            this.tree_view.ItemHeight = 35;
            this.tree_view.Location = new System.Drawing.Point(0, 33);
            this.tree_view.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tree_view.Name = "tree_view";
            treeNode3.Name = "Node0";
            treeNode3.Text = "១. បញ្ចូលសំនួរIQតេស្ត";
            treeNode4.ImageIndex = 5;
            treeNode4.Name = "Node1";
            treeNode4.SelectedImageIndex = 5;
            treeNode4.Text = "២. កំណត់ចំណាំចម្លើយ";
            this.tree_view.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4});
            this.tree_view.Size = new System.Drawing.Size(219, 357);
            this.tree_view.TabIndex = 7;
            this.tree_view.TabStop = false;
            this.tree_view.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tree_view_AfterSelect);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Font = new System.Drawing.Font("Khmer OS Content", 9F);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_close});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(951, 33);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // pan_main
            // 
            this.pan_main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pan_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan_main.Location = new System.Drawing.Point(219, 33);
            this.pan_main.Name = "pan_main";
            this.pan_main.Size = new System.Drawing.Size(732, 357);
            this.pan_main.TabIndex = 8;
            // 
            // btn_close
            // 
            this.btn_close.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btn_close.AutoSize = false;
            this.btn_close.Image = global::NSSF_Exam.Properties.Resources.btn_delete_color;
            this.btn_close.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(81, 35);
            this.btn_close.Text = "ចាកចេញ";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // frm_iq_testing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 390);
            this.ControlBox = false;
            this.Controls.Add(this.pan_main);
            this.Controls.Add(this.tree_view);
            this.Controls.Add(this.toolStrip1);
            this.Name = "frm_iq_testing";
            this.ShowIcon = false;
            this.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (IQ)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_iq_testing_FormClosed);
            this.Load += new System.EventHandler(this.frm_iq_testing_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TreeView tree_view;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_close;
        private System.Windows.Forms.Panel pan_main;
    }
}