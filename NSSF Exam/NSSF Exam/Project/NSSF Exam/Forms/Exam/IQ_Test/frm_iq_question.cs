﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam.IQ_Test
{
    public partial class frm_iq_question : Form
    {
        public frm_iq_question()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ពត៍មានសំនួរIQ");
        }

        private void frm_iq_question_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_dgv(dgvlst);

            bds_iq_question.DataSource = (from iq in ClsConfig.glo_local_db.tbl_iq_question select iq).ToList();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            new frm_iq_question_edit().ShowDialog();
            frm_iq_question_Load(null, null);
        }

        private void dgvlst_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex > -1 && e.ColumnIndex == 1)
            {
                frm_iq_question_edit iq = new frm_iq_question_edit();
                iq.iq_id = Convert.ToInt32( dgvlst.CurrentRow.Cells[0].Value );
                iq.ShowDialog();
                frm_iq_question_Load(null, null);
            }
        }

        private void dgvlst_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgvlst.RowCount;i++ )
            {
                dgvlst.Rows[i].Cells[2].Value = i + 1;
            }
        }
    }
}
