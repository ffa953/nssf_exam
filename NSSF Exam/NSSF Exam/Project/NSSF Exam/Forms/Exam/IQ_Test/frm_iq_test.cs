﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Forms.Exam.IQ_Quiz;
using NSSF_Exam.Forms.Exam.IQ_Test;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam.IQ_Quiz
{
    public partial class frm_iq_testing : Form
    {
        public frm_iq_testing()
        {
            InitializeComponent();
        }

     
        private void tree_view_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tree_view.Nodes[0].IsSelected)
            {
                OpenForm(new frm_iq_question(), pan_main);
            }
            else if (tree_view.Nodes[1].IsSelected)
            {
              
            }
        }

        void OpenForm(Form frm, Panel pan)
        {
            frm.TopLevel = false;
            pan.Controls.Clear();
            pan.Controls.Add(frm);
            frm.Show();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_iq_testing_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void frm_iq_testing_Load(object sender, EventArgs e)
        {

        }

    }
}
