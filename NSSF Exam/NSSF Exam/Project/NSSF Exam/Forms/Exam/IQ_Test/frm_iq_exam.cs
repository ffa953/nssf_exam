﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using NSSF_Exam.Classes.Config;
using System.IO;

namespace NSSF_Exam.Forms.Exam.IQ_Test
{
    public partial class frm_iq_exam : Form
    {
        int i = 0;
        public frm_iq_exam()
        {
            InitializeComponent();
        }

        private void frm_iq_exam_Load(object sender, EventArgs e)
        {
          
            lbl_question_id.Visible = false;
            lbl.Visible = false;
            btn_next.Visible = false;
            btn_previous.Visible = false;

            display_stu_info();
            bds_iq_question.DataSource = (from v in ClsConfig.glo_local_db.vw_iq_question  select v).ToList();
            lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_iq_question.Position + 1).ToString());
            lbl_question.DataBindings.Add(new Binding("Text", bds_iq_question, "iq_question"));
            lbl_question_id.DataBindings.Add(new Binding("Text", bds_iq_question, "iq_id"));

            int iq_id =Convert.ToInt32( lbl_question_id.Text );
            var query = ClsConfig.glo_local_db.vw_iq_question.Find(iq_id);
            if (query.im_id != null)
            {
                Byte[] data = new Byte[0];
                data = (Byte[])(query.im_image);
                MemoryStream mem = new MemoryStream(data);
                pictureBox1.Image = Image.FromStream(mem);
            }
            else
            {
                pictureBox1.Image = null;
                //pictureBox1.Image = NSSF_Exam.Properties.Resources.photo_2018_06_06_11_54_30;
                //pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }


        }

        void display_stu_info()
        {
            var stu = (from s in ClsConfig.glo_local_db.UserProfiles where s.UserId==ClsConfig.glo_stu_id select s).SingleOrDefault();
            lbl_id.Text = stu.UserName;
            lbl_name.Text = stu.Name_khmer;
            lbl_table_no.Text = (from t in ClsConfig.glo_local_db.fun_user_by_role(2) where t.UserId == stu.UserId select t.skill_note).SingleOrDefault();
            lbl_dob.Text = Convert.ToDateTime( stu.DateOfBirth).ToString("dd-MM-yyyy");
            lbl_subject.Text = (from s in ClsConfig.glo_local_db.tbl_skill where s.skill_id == stu.skill_id select s.skill).SingleOrDefault();
            lbl_skill.Text = (from sk in ClsConfig.glo_local_db.tbl_certificate_skill where sk.cert_skill_id == stu.cert_skill_id select sk.cert_skill_name).SingleOrDefault();
        }

        void load_picture()
        {
            int iq_id = Convert.ToInt32(lbl_question_id.Text);
            var query = ClsConfig.glo_local_db.vw_iq_question.Find(iq_id);
            if (query.im_id != null)
            {
                Byte[] data = new Byte[0];
                data = (Byte[])(query.im_image);
                MemoryStream mem = new MemoryStream(data);
                pictureBox1.Image = Image.FromStream(mem);
            }
            else
            {
                pictureBox1.Image = null;
                //pictureBox1.Image = NSSF_Exam.Properties.Resources.photo_2018_06_06_11_54_30;
                //pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;              
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            if (bds_iq_question.Count != Convert.ToInt32(bds_iq_question.Position + 1))
            {
                bds_iq_question.MoveNext();
                lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_iq_question.Position + 1).ToString());
                load_picture();
            }
            else
            {
                frm_congratgulation frm = new frm_congratgulation("ការប្រឡងតេស្តភាពវៃឆ្លាត (IQ) ត្រូវបានបញ្ចប់");
                frm.ShowDialog();
            }
        }

        private void btn_previous_Click(object sender, EventArgs e)
        {
            bds_iq_question.MovePrevious();
            lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_iq_question.Position + 1).ToString());
            load_picture();
        }

        private void frm_iq_exam_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm_test_menu frm = new frm_test_menu();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_answer.Text != "")
                {
                    tbl_iq_student_answer stu = new tbl_iq_student_answer();
                    stu.iq_id = Convert.ToInt32(lbl_question_id.Text);
                    stu.UserId = ClsConfig.glo_stu_id;
                    stu.answer = txt_answer.Text;

                    ClsConfig.glo_local_db.tbl_iq_student_answer.Add(stu);
                    ClsConfig.glo_local_db.SaveChanges();
                    txt_answer.Text = "";
                    btn_next_Click(null, null);
                }
                else
                {
                    ClsMsg.Warning("សូមបំពេញចម្លើយ !!!");
                }
            }
            catch(Exception ex)
            {
                ClsMsg.Error(ex.ToString());
            }
        }
    }
}
