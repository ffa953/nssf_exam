﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_export_choice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rd_st = new System.Windows.Forms.RadioButton();
            this.rd_off = new System.Windows.Forms.RadioButton();
            this.btn_ok = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rd_st);
            this.groupBox1.Controls.Add(this.rd_off);
            this.groupBox1.Location = new System.Drawing.Point(12, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(177, 78);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rd_st
            // 
            this.rd_st.AutoSize = true;
            this.rd_st.Location = new System.Drawing.Point(91, 26);
            this.rd_st.Name = "rd_st";
            this.rd_st.Size = new System.Drawing.Size(75, 31);
            this.rd_st.TabIndex = 1;
            this.rd_st.TabStop = true;
            this.rd_st.Text = "បុគ្គលិក";
            this.rd_st.UseVisualStyleBackColor = true;
            // 
            // rd_off
            // 
            this.rd_off.AutoSize = true;
            this.rd_off.Location = new System.Drawing.Point(19, 26);
            this.rd_off.Name = "rd_off";
            this.rd_off.Size = new System.Drawing.Size(54, 31);
            this.rd_off.TabIndex = 0;
            this.rd_off.TabStop = true;
            this.rd_off.Text = "មន្រ្តី";
            this.rd_off.UseVisualStyleBackColor = true;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(12, 82);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(177, 38);
            this.btn_ok.TabIndex = 1;
            this.btn_ok.Text = "យល់ព្រម";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // frm_export_choice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(201, 125);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_export_choice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ជ្រើសរើស";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_export_choice_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rd_st;
        private System.Windows.Forms.RadioButton rd_off;
        private System.Windows.Forms.Button btn_ok;
    }
}