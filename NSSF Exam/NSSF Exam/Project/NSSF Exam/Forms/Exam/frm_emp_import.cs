﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_emp_import : Form
    {
        List<int> lst = new List<int>();
        public frm_emp_import()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ទាញយកបុគ្គលិក");
        }

        private void frm_emp_import_Load(object sender, EventArgs e)
        {
            ClsSetting.OpenPermission(this, this.Name, ClsConfig.glo_use_privilege);
            ClsSetting.sty_btn_search(btn_search);
            ClsSetting.sty_btn_refresh(btn_refresh);
            ClsSetting.sty_btn_close(btn_exit);
            ClsSetting.sty_dgv(dgv_emp_import);
            ClsSetting.sty_btn_submit(btn_submit);

            GenBindingSource.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            bds_exam_date.DataSource = ClsConfig.glo_local_db.tbl_exam_date.OrderByDescending(m => m.exam_id).ToList();
            SkillBindingSource.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();
            CertBindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            CertSkill_BindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();

            cbo_exam_date.SelectedIndex = -1;
        }

        private void dgv_stu_his_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_emp_import.RowCount; i++)
            {
                dgv_emp_import.Rows[i].Cells["col"].Value = i + 1;
 
            }
            dgv_emp_import.ClearSelection();
         
           
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
          
        }

        private void frm_emp_import_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_get_stuDate(2, Convert.ToInt32(cbo_exam_date.SelectedValue)).OrderByDescending(u => u.UserId);
            lst.Clear();
            
        }

        private void dgv_emp_import_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 5)
            {
                if (Convert.ToBoolean(dgv_emp_import.CurrentRow.Cells["choose"].Value) == false)
                {
                    dgv_emp_import.CurrentRow.Cells["choose"].Value = true;                
                    lst.Add(Convert.ToInt32(dgv_emp_import.CurrentRow.Cells[0].Value));
                    dgv_emp_import.ClearSelection();

                }
                else if (Convert.ToBoolean(dgv_emp_import.CurrentRow.Cells["choose"].Value) == true)
                {
                    dgv_emp_import.CurrentRow.Cells["choose"].Value = false;
                    for (int i = 0; i < lst.Count;i++ )
                    {
                        if (lst[i] == Convert.ToInt32(dgv_emp_import.CurrentRow.Cells[0].Value))
                        {
                            lst.Remove(lst[i]);
                        }
                    }
                    
                    dgv_emp_import.ClearSelection();
                }
            }
     
        }

        private void dgv_emp_import_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (Convert.ToBoolean(dgv_emp_import.CurrentRow.Cells["choose"].Value) == true)
            {
                dgv_emp_import.CurrentRow.DefaultCellStyle.BackColor = Color.Green;
                dgv_emp_import.CurrentRow.DefaultCellStyle.ForeColor = Color.White;
                dgv_emp_import.CurrentRow.DefaultCellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
            }

            if (Convert.ToBoolean(dgv_emp_import.CurrentRow.Cells["choose"].Value) == false)
            {
                dgv_emp_import.CurrentRow.DefaultCellStyle.BackColor = Color.Empty;
                dgv_emp_import.CurrentRow.DefaultCellStyle.ForeColor = Color.Empty;
                dgv_emp_import.CurrentRow.DefaultCellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Regular);
            }

          
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            string search_text = txt_search.Text;
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_get_stuDate(2, Convert.ToInt32(cbo_exam_date.SelectedValue)).Where(m => m.Name_khmer.Contains(search_text) || m.Name_latin.Contains(search_text) || m.UserName.Contains(search_text) || m.skill_note.Contains(search_text)).OrderByDescending(u => u.UserId).ToList();
             foreach (int item in lst)
            {
                for (int i = 0; i < dgv_emp_import.RowCount; i++)
                {
                    if (Convert.ToInt32(dgv_emp_import.Rows[i].Cells[0].Value) == item) {
                        dgv_emp_import.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                        dgv_emp_import.Rows[i].DefaultCellStyle.ForeColor = Color.White;
                        dgv_emp_import.Rows[i].Cells["choose"].Value = true;
                        dgv_emp_import.Rows[i].DefaultCellStyle.Font = new Font("Khmer OS Content", 10, FontStyle.Bold);
                    }
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if (lst.Count > 0)
            {
                frm_emp_import_edit frm = new frm_emp_import_edit();
                frm.emp_lst = lst;
                frm.ShowDialog();
            }
            else
            {
                ClsMsg.Warning("សូមជ្រើសរើសបុគ្គលិកជាមុខសិន !!!");
            }
        }
    }
}
