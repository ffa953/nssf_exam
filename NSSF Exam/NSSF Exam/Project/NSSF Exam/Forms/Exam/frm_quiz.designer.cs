﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_quiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_quiz = new System.Windows.Forms.DataGridView();
            this.quiz_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quiz_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ans_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_quiz)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(891, 69);
            this.panel1.TabIndex = 3;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(778, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(668, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_quiz
            // 
            this.dgv_quiz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_quiz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.quiz_id,
            this.Column1,
            this.col,
            this.quiz_title,
            this.ans_title,
            this.score,
            this.type_name,
            this.active});
            this.dgv_quiz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_quiz.Location = new System.Drawing.Point(0, 69);
            this.dgv_quiz.Name = "dgv_quiz";
            this.dgv_quiz.Size = new System.Drawing.Size(891, 400);
            this.dgv_quiz.TabIndex = 4;
            this.dgv_quiz.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_quiz_CellClick);
            this.dgv_quiz.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_quiz_CellFormatting);
            this.dgv_quiz.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_quiz_DataBindingComplete);
            // 
            // quiz_id
            // 
            this.quiz_id.DataPropertyName = "quiz_id";
            this.quiz_id.HeaderText = "quiz_id";
            this.quiz_id.Name = "quiz_id";
            this.quiz_id.Visible = false;
            // 
            // col
            // 
            this.col.HeaderText = "ល.រ";
            this.col.Name = "col";
            // 
            // quiz_title
            // 
            this.quiz_title.DataPropertyName = "quiz_title";
            this.quiz_title.HeaderText = "សំនួរ";
            this.quiz_title.Name = "quiz_title";
            this.quiz_title.Width = 550;
            // 
            // ans_title
            // 
            this.ans_title.DataPropertyName = "ans_title";
            this.ans_title.HeaderText = "ចម្លើយត្រឹមត្រូវ";
            this.ans_title.Name = "ans_title";
            this.ans_title.Width = 500;
            // 
            // score
            // 
            this.score.DataPropertyName = "score";
            this.score.HeaderText = "ពិន្ទុ";
            this.score.Name = "score";
            // 
            // type_name
            // 
            this.type_name.DataPropertyName = "type_name";
            this.type_name.HeaderText = "ប្រភេទសំនួរ";
            this.type_name.Name = "type_name";
            this.type_name.Width = 120;
            // 
            // active
            // 
            this.active.DataPropertyName = "active";
            this.active.HeaderText = "ដំណើរការ";
            this.active.Name = "active";
            this.active.Width = 115;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Width = 90;
            // 
            // frm_quiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 469);
            this.Controls.Add(this.dgv_quiz);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_quiz";
            this.Text = "frm_quiz";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_quiz_FormClosed);
            this.Load += new System.EventHandler(this.frm_quiz_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_quiz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.DataGridView dgv_quiz;
        private System.Windows.Forms.DataGridViewTextBoxColumn quiz_id;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col;
        private System.Windows.Forms.DataGridViewTextBoxColumn quiz_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn ans_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn score;
        private System.Windows.Forms.DataGridViewTextBoxColumn type_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn active;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
    }
}