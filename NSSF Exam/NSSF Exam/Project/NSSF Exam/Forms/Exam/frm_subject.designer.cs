﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_subject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_subject = new System.Windows.Forms.DataGridView();
            this.bds_subject = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.sub_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sub_active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_subject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_subject)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(741, 69);
            this.panel1.TabIndex = 1;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(628, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(518, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_subject
            // 
            this.dgv_subject.AutoGenerateColumns = false;
            this.dgv_subject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_subject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.sub_id,
            this.subject,
            this.sub_active});
            this.dgv_subject.DataSource = this.bds_subject;
            this.dgv_subject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_subject.Location = new System.Drawing.Point(0, 69);
            this.dgv_subject.Name = "dgv_subject";
            this.dgv_subject.Size = new System.Drawing.Size(741, 415);
            this.dgv_subject.TabIndex = 2;
            this.dgv_subject.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_subject_CellClick);
            this.dgv_subject.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_subject_CellFormatting);
            this.dgv_subject.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_subject_DataBindingComplete);
            // 
            // bds_subject
            // 
            this.bds_subject.DataSource = typeof(NSSF_Exam.Entities.tbl_subject);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Width = 90;
            // 
            // sub_id
            // 
            this.sub_id.DataPropertyName = "sub_id";
            this.sub_id.HeaderText = "sub_id";
            this.sub_id.Name = "sub_id";
            this.sub_id.Visible = false;
            // 
            // subject
            // 
            this.subject.DataPropertyName = "sub_title";
            this.subject.HeaderText = "មុខវិជ្ជា";
            this.subject.Name = "subject";
            this.subject.Width = 300;
            // 
            // sub_active
            // 
            this.sub_active.DataPropertyName = "sub_active";
            this.sub_active.HeaderText = "ដំណើរការ";
            this.sub_active.Name = "sub_active";
            this.sub_active.Width = 300;
            // 
            // frm_subject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 484);
            this.Controls.Add(this.dgv_subject);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_subject";
            this.Text = "frm_subject";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_subject_FormClosed);
            this.Load += new System.EventHandler(this.frm_subject_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_subject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_subject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.DataGridView dgv_subject;
        private System.Windows.Forms.BindingSource bds_subject;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sub_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn sub_active;
    }
}