﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_skill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_skill = new System.Windows.Forms.DataGridView();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.skill_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skillDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skillnoteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_skill = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_skill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(804, 69);
            this.panel1.TabIndex = 2;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(691, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(581, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_skill
            // 
            this.dgv_skill.AutoGenerateColumns = false;
            this.dgv_skill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_skill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.skill_id,
            this.skillDataGridViewTextBoxColumn,
            this.skillnoteDataGridViewTextBoxColumn});
            this.dgv_skill.DataSource = this.bds_skill;
            this.dgv_skill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_skill.Location = new System.Drawing.Point(0, 69);
            this.dgv_skill.Name = "dgv_skill";
            this.dgv_skill.Size = new System.Drawing.Size(804, 378);
            this.dgv_skill.TabIndex = 3;
            this.dgv_skill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_skill_CellClick);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Width = 90;
            // 
            // skill_id
            // 
            this.skill_id.DataPropertyName = "skill_id";
            this.skill_id.HeaderText = "skill_id";
            this.skill_id.Name = "skill_id";
            this.skill_id.Visible = false;
            // 
            // skillDataGridViewTextBoxColumn
            // 
            this.skillDataGridViewTextBoxColumn.DataPropertyName = "skill";
            this.skillDataGridViewTextBoxColumn.HeaderText = "ផ្នែក";
            this.skillDataGridViewTextBoxColumn.Name = "skillDataGridViewTextBoxColumn";
            this.skillDataGridViewTextBoxColumn.Width = 200;
            // 
            // skillnoteDataGridViewTextBoxColumn
            // 
            this.skillnoteDataGridViewTextBoxColumn.DataPropertyName = "skill_note";
            this.skillnoteDataGridViewTextBoxColumn.HeaderText = "អក្សរកាត់";
            this.skillnoteDataGridViewTextBoxColumn.Name = "skillnoteDataGridViewTextBoxColumn";
            this.skillnoteDataGridViewTextBoxColumn.Width = 200;
            // 
            // bds_skill
            // 
            this.bds_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // frm_skill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 447);
            this.Controls.Add(this.dgv_skill);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_skill";
            this.Text = "frm_specialty";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_skill_FormClosed);
            this.Load += new System.EventHandler(this.frm_skill_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_skill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.DataGridView dgv_skill;
        private System.Windows.Forms.BindingSource bds_skill;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn skill_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn skillDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn skillnoteDataGridViewTextBoxColumn;
    }
}