﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_topic_edit : Form
    {
        public int ex_type_id;
        public frm_topic_edit()
        {
            InitializeComponent();
        }

        private void frm_topic_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_add);
            ClsSetting.sty_btn_close(btn_close);
            chk_active.Checked = true;

            if(ex_type_id>0){
                var exam_type = ClsConfig.glo_local_db.tbl_exam_type.Find(ex_type_id);
                txt_ex_type_name.Text = exam_type.ex_type_name;
                txt_ex_time.Text = exam_type.ex_time.ToString();
                chk_active.Checked =(bool)exam_type.ex_active;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (ex_type_id > 0)
                {

                    var exam_type = ClsConfig.glo_local_db.tbl_exam_type.Find(ex_type_id);
                    exam_type.ex_type_name = txt_ex_type_name.Text;
                    exam_type.ex_time = Convert.ToInt32(txt_ex_time.Text);
                    exam_type.ex_active = (chk_active.Checked) ? true : false;                 
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");
                    this.Close();
                }
                else
                {

                    tbl_exam_type exam_type = new tbl_exam_type();
                    exam_type.ex_type_name = txt_ex_type_name.Text;
                    exam_type.ex_time = Convert.ToInt32(txt_ex_time.Text);
                    exam_type.ex_active = (chk_active.Checked) ? true : false;
                    ClsConfig.glo_local_db.tbl_exam_type.Add(exam_type);
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក។");

                }
            }
            catch (Exception ex) {

                ClsMsg.Error("" + ex);
            }
        }
    }
}
