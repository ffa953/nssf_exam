﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_exam : Form
    {
        public frm_exam()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "សម័យប្រឡង");
        }

        private void frm_exam_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_exam);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            bds_exam.DataSource = ClsConfig.glo_local_db.tbl_exam_date.ToList();

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_exam_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void dgv_exam_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_exam_edit frm = new frm_exam_edit();
                frm.exam_id = (int)dgv_exam.CurrentRow.Cells["exam_id"].Value;
                frm.ShowDialog();
                frm_exam_Load(null, null);
            }
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_exam_edit frm = new frm_exam_edit();
            frm.ShowDialog();
            frm_exam_Load(null, null);
        }

       
    }
}
