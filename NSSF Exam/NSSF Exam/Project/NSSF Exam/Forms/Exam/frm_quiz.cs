﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_quiz : Form
    {
        public frm_quiz()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "កំណត់សំនូរ");
        }

        private void frm_quiz_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_quiz);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            var quiz = (from q in ClsConfig.glo_local_db.tbl_subject_quiz
                        join a in ClsConfig.glo_local_db.tbl_quiz_answer on q.quiz_id equals a.quiz_id
                        join t in ClsConfig.glo_local_db.tbl_quiz_type on q.type_id equals t.type_id
                        where a.ans_choice == true && q.active==true
                        select new { q.quiz_id,q.quiz_title , a.ans_title ,q.score ,t.type_name ,q.active}).ToList();

            dgv_quiz.DataSource = quiz;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_quiz_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_quiz.RowCount; i++)
            {
                dgv_quiz.Rows[i].Cells["col"].Value = i + 1;
            }
            dgv_quiz.ClearSelection();
        }

        private void dgv_quiz_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_quiz.Columns[e.ColumnIndex].Name == "active")
            {
                if ((bool)dgv_quiz.Rows[e.RowIndex].Cells["active"].Value == false)
                {
                    e.Value = "មិនដំណើរការ";
                }
                else
                {
                    e.Value = "ដំណើរការ";
                }
            }
        }

        private void frm_quiz_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_quiz_edit frm = new frm_quiz_edit();
            frm.ShowDialog();
            frm_quiz_Load(null, null);
        }

        private void dgv_quiz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_quiz_edit frm = new frm_quiz_edit();
                frm.quiz_id = (int)dgv_quiz.CurrentRow.Cells["quiz_id"].Value;
                frm.ShowDialog();
                frm_quiz_Load(null, null);
            }
            
        }
    }
}
