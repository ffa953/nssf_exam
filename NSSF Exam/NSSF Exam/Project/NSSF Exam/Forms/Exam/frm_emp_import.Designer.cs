﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_emp_import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_submit = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.cbo_exam_date = new System.Windows.Forms.ComboBox();
            this.bds_exam_date = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.btn_search = new System.Windows.Forms.Button();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_emp_import = new System.Windows.Forms.DataGridView();
            this.userIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.verify = new System.Windows.Forms.DataGridViewImageColumn();
            this.finger = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.choose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.skill_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namekhmerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namelatinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.natnamekhDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateOfBirthDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skillidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SkillBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.certificateidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cert_skill_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btn_import = new System.Windows.Forms.ToolStripMenuItem();
            this.StuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CertSkill_BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_emp_import)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkillBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBindingSource)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertSkill_BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_submit);
            this.panel1.Controls.Add(this.btn_exit);
            this.panel1.Controls.Add(this.btn_refresh);
            this.panel1.Controls.Add(this.cbo_exam_date);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_search);
            this.panel1.Controls.Add(this.txt_search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1175, 66);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btn_submit
            // 
            this.btn_submit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_submit.Location = new System.Drawing.Point(955, 16);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(100, 35);
            this.btn_submit.TabIndex = 14;
            this.btn_submit.Text = "button3";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.Location = new System.Drawing.Point(1061, 16);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(100, 35);
            this.btn_exit.TabIndex = 13;
            this.btn_exit.Text = "button3";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_refresh.Location = new System.Drawing.Point(798, 16);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(100, 35);
            this.btn_refresh.TabIndex = 12;
            this.btn_refresh.Text = "button3";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // cbo_exam_date
            // 
            this.cbo_exam_date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_exam_date.DataSource = this.bds_exam_date;
            this.cbo_exam_date.DisplayMember = "exam_date";
            this.cbo_exam_date.FormatString = "dd-MM-yyyy";
            this.cbo_exam_date.FormattingEnabled = true;
            this.cbo_exam_date.Location = new System.Drawing.Point(615, 17);
            this.cbo_exam_date.Name = "cbo_exam_date";
            this.cbo_exam_date.Size = new System.Drawing.Size(177, 32);
            this.cbo_exam_date.TabIndex = 11;
            this.cbo_exam_date.ValueMember = "exam_id";
            // 
            // bds_exam_date
            // 
            this.bds_exam_date.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_date);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(506, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "ជ្រើសរើសជំនាន់";
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(230, 14);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(100, 35);
            this.btn_search.TabIndex = 9;
            this.btn_search.Text = "button3";
            this.btn_search.UseVisualStyleBackColor = true;
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.SystemColors.Info;
            this.txt_search.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_search.Location = new System.Drawing.Point(12, 16);
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(216, 32);
            this.txt_search.TabIndex = 0;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_emp_import);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1175, 418);
            this.panel2.TabIndex = 2;
            // 
            // dgv_emp_import
            // 
            this.dgv_emp_import.AutoGenerateColumns = false;
            this.dgv_emp_import.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_emp_import.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.userIdDataGridViewTextBoxColumn,
            this.col,
            this.Column1,
            this.verify,
            this.finger,
            this.choose,
            this.skill_note,
            this.userNameDataGridViewTextBoxColumn,
            this.namekhmerDataGridViewTextBoxColumn,
            this.namelatinDataGridViewTextBoxColumn,
            this.genderIdDataGridViewTextBoxColumn,
            this.natnamekhDataGridViewTextBoxColumn,
            this.dateOfBirthDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.phoneDataGridViewTextBoxColumn,
            this.skillidDataGridViewTextBoxColumn,
            this.certificateidDataGridViewTextBoxColumn,
            this.cert_skill_name});
            this.dgv_emp_import.ContextMenuStrip = this.contextMenuStrip1;
            this.dgv_emp_import.DataSource = this.StuBindingSource;
            this.dgv_emp_import.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_emp_import.Location = new System.Drawing.Point(0, 0);
            this.dgv_emp_import.Name = "dgv_emp_import";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_emp_import.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_emp_import.Size = new System.Drawing.Size(1175, 418);
            this.dgv_emp_import.TabIndex = 2;
            this.dgv_emp_import.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_emp_import_CellClick);
            this.dgv_emp_import.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_emp_import_CellFormatting);
            this.dgv_emp_import.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_stu_his_DataBindingComplete);
            // 
            // userIdDataGridViewTextBoxColumn
            // 
            this.userIdDataGridViewTextBoxColumn.DataPropertyName = "UserId";
            this.userIdDataGridViewTextBoxColumn.HeaderText = "UserId";
            this.userIdDataGridViewTextBoxColumn.Name = "userIdDataGridViewTextBoxColumn";
            this.userIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // col
            // 
            this.col.HeaderText = "ល.រ";
            this.col.Name = "col";
            this.col.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កំណត់ស្នាមម្រាមដៃ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.fingerprint;
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 120;
            // 
            // verify
            // 
            this.verify.HeaderText = "ផ្ទៀងផ្ទាត់";
            this.verify.Image = global::NSSF_Exam.Properties.Resources.btn_refresh;
            this.verify.Name = "verify";
            this.verify.Visible = false;
            this.verify.Width = 80;
            // 
            // finger
            // 
            this.finger.DataPropertyName = "finger";
            this.finger.HeaderText = "ស្គែនរួច";
            this.finger.Name = "finger";
            this.finger.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.finger.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.finger.Visible = false;
            this.finger.Width = 80;
            // 
            // choose
            // 
            this.choose.HeaderText = "ជ្រើសរើស";
            this.choose.Name = "choose";
            this.choose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.choose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // skill_note
            // 
            this.skill_note.DataPropertyName = "skill_note";
            this.skill_note.HeaderText = "លេខតុ";
            this.skill_note.Name = "skill_note";
            this.skill_note.Width = 70;
            // 
            // userNameDataGridViewTextBoxColumn
            // 
            this.userNameDataGridViewTextBoxColumn.DataPropertyName = "UserName";
            this.userNameDataGridViewTextBoxColumn.HeaderText = "លេខសំគាល់";
            this.userNameDataGridViewTextBoxColumn.Name = "userNameDataGridViewTextBoxColumn";
            // 
            // namekhmerDataGridViewTextBoxColumn
            // 
            this.namekhmerDataGridViewTextBoxColumn.DataPropertyName = "Name_khmer";
            this.namekhmerDataGridViewTextBoxColumn.HeaderText = "គោត្តនាម និងនាម";
            this.namekhmerDataGridViewTextBoxColumn.Name = "namekhmerDataGridViewTextBoxColumn";
            this.namekhmerDataGridViewTextBoxColumn.Width = 200;
            // 
            // namelatinDataGridViewTextBoxColumn
            // 
            this.namelatinDataGridViewTextBoxColumn.DataPropertyName = "Name_latin";
            this.namelatinDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះជាឡាតាំង";
            this.namelatinDataGridViewTextBoxColumn.Name = "namelatinDataGridViewTextBoxColumn";
            this.namelatinDataGridViewTextBoxColumn.Width = 200;
            // 
            // genderIdDataGridViewTextBoxColumn
            // 
            this.genderIdDataGridViewTextBoxColumn.DataPropertyName = "GenderId";
            this.genderIdDataGridViewTextBoxColumn.DataSource = this.GenBindingSource;
            this.genderIdDataGridViewTextBoxColumn.DisplayMember = "GenderKH";
            this.genderIdDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.genderIdDataGridViewTextBoxColumn.HeaderText = "ភេទ";
            this.genderIdDataGridViewTextBoxColumn.Name = "genderIdDataGridViewTextBoxColumn";
            this.genderIdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.genderIdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.genderIdDataGridViewTextBoxColumn.ValueMember = "GenderId";
            // 
            // GenBindingSource
            // 
            this.GenBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_gender);
            // 
            // natnamekhDataGridViewTextBoxColumn
            // 
            this.natnamekhDataGridViewTextBoxColumn.DataPropertyName = "nat_name_kh";
            this.natnamekhDataGridViewTextBoxColumn.HeaderText = "សញ្ជាតិ";
            this.natnamekhDataGridViewTextBoxColumn.Name = "natnamekhDataGridViewTextBoxColumn";
            this.natnamekhDataGridViewTextBoxColumn.Width = 120;
            // 
            // dateOfBirthDataGridViewTextBoxColumn
            // 
            this.dateOfBirthDataGridViewTextBoxColumn.DataPropertyName = "DateOfBirth";
            dataGridViewCellStyle1.Format = "dd-MM-yyyy";
            this.dateOfBirthDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.dateOfBirthDataGridViewTextBoxColumn.HeaderText = "ថ្ងៃខែឆ្នាំកំណើត";
            this.dateOfBirthDataGridViewTextBoxColumn.Name = "dateOfBirthDataGridViewTextBoxColumn";
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            this.emailDataGridViewTextBoxColumn.Visible = false;
            // 
            // phoneDataGridViewTextBoxColumn
            // 
            this.phoneDataGridViewTextBoxColumn.DataPropertyName = "Phone";
            this.phoneDataGridViewTextBoxColumn.HeaderText = "លេខទូរស័ព្ទ";
            this.phoneDataGridViewTextBoxColumn.Name = "phoneDataGridViewTextBoxColumn";
            this.phoneDataGridViewTextBoxColumn.Width = 150;
            // 
            // skillidDataGridViewTextBoxColumn
            // 
            this.skillidDataGridViewTextBoxColumn.DataPropertyName = "skill_id";
            this.skillidDataGridViewTextBoxColumn.DataSource = this.SkillBindingSource;
            this.skillidDataGridViewTextBoxColumn.DisplayMember = "skill";
            this.skillidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.skillidDataGridViewTextBoxColumn.HeaderText = "ផ្នែក";
            this.skillidDataGridViewTextBoxColumn.Name = "skillidDataGridViewTextBoxColumn";
            this.skillidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.skillidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.skillidDataGridViewTextBoxColumn.ValueMember = "skill_id";
            this.skillidDataGridViewTextBoxColumn.Width = 200;
            // 
            // SkillBindingSource
            // 
            this.SkillBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // certificateidDataGridViewTextBoxColumn
            // 
            this.certificateidDataGridViewTextBoxColumn.DataPropertyName = "certificate_id";
            this.certificateidDataGridViewTextBoxColumn.DataSource = this.CertBindingSource;
            this.certificateidDataGridViewTextBoxColumn.DisplayMember = "certificate_name";
            this.certificateidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.certificateidDataGridViewTextBoxColumn.HeaderText = "សញ្ញាប័ត្រ";
            this.certificateidDataGridViewTextBoxColumn.Name = "certificateidDataGridViewTextBoxColumn";
            this.certificateidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.certificateidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.certificateidDataGridViewTextBoxColumn.ValueMember = "certificate_id";
            this.certificateidDataGridViewTextBoxColumn.Width = 150;
            // 
            // CertBindingSource
            // 
            this.CertBindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate);
            // 
            // cert_skill_name
            // 
            this.cert_skill_name.DataPropertyName = "cert_skill_name";
            this.cert_skill_name.HeaderText = "ឯកទេស";
            this.cert_skill_name.Name = "cert_skill_name";
            this.cert_skill_name.Width = 200;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_import});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(126, 32);
            // 
            // btn_import
            // 
            this.btn_import.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_import.Image = global::NSSF_Exam.Properties.Resources.Accept;
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(125, 28);
            this.btn_import.Text = "ទាញយក";
            // 
            // StuBindingSource
            // 
            this.StuBindingSource.DataSource = typeof(NSSF_Exam.Entities.fun_user_by_role_Result);
            // 
            // CertSkill_BindingSource
            // 
            this.CertSkill_BindingSource.DataSource = typeof(NSSF_Exam.Entities.tbl_certificate_skill);
            // 
            // frm_emp_import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 484);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_emp_import";
            this.Text = "frm_emp_import";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_emp_import_FormClosed);
            this.Load += new System.EventHandler(this.frm_emp_import_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_emp_import)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkillBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBindingSource)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertSkill_BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.ComboBox cbo_exam_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource bds_exam_date;
        private System.Windows.Forms.BindingSource StuBindingSource;
        private System.Windows.Forms.BindingSource GenBindingSource;
        private System.Windows.Forms.BindingSource SkillBindingSource;
        private System.Windows.Forms.BindingSource CertSkill_BindingSource;
        private System.Windows.Forms.BindingSource CertBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btn_import;
        private System.Windows.Forms.DataGridView dgv_emp_import;
        private System.Windows.Forms.DataGridViewTextBoxColumn userIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn verify;
        private System.Windows.Forms.DataGridViewCheckBoxColumn finger;
        private System.Windows.Forms.DataGridViewCheckBoxColumn choose;
        private System.Windows.Forms.DataGridViewTextBoxColumn skill_note;
        private System.Windows.Forms.DataGridViewTextBoxColumn userNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namekhmerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namelatinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn genderIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn natnamekhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateOfBirthDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn skillidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn certificateidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cert_skill_name;
        private System.Windows.Forms.Button btn_submit;

    }
}