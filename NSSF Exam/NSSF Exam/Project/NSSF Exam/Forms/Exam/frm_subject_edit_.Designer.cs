﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_subject_edit_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.dgvlst = new System.Windows.Forms.DataGridView();
            this.cbo_skill = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_subject = new System.Windows.Forms.ComboBox();
            this.txt_answer_num = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.col_auto_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.col_lr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_skill_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_skill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_sub_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_question = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 460);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1003, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // dgvlst
            // 
            this.dgvlst.AllowUserToAddRows = false;
            this.dgvlst.AllowUserToDeleteRows = false;
            this.dgvlst.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvlst.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvlst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvlst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_auto_id,
            this.col_edit,
            this.col_lr,
            this.col_skill_id,
            this.col_skill,
            this.col_sub_id,
            this.col_subject,
            this.col_question});
            this.dgvlst.Location = new System.Drawing.Point(12, 124);
            this.dgvlst.Name = "dgvlst";
            this.dgvlst.ReadOnly = true;
            this.dgvlst.Size = new System.Drawing.Size(979, 326);
            this.dgvlst.TabIndex = 1;
            this.dgvlst.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellClick);
            this.dgvlst.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvlst_CellMouseEnter);
            this.dgvlst.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvlst_RowsAdded);
            // 
            // cbo_skill
            // 
            this.cbo_skill.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_skill.DisplayMember = "skill";
            this.cbo_skill.DropDownHeight = 120;
            this.cbo_skill.FormattingEnabled = true;
            this.cbo_skill.IntegralHeight = false;
            this.cbo_skill.Location = new System.Drawing.Point(71, 24);
            this.cbo_skill.Name = "cbo_skill";
            this.cbo_skill.Size = new System.Drawing.Size(230, 30);
            this.cbo_skill.TabIndex = 28;
            this.cbo_skill.ValueMember = "skill_id";
            this.cbo_skill.SelectionChangeCommitted += new System.EventHandler(this.cbo_skill_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 22);
            this.label2.TabIndex = 27;
            this.label2.Text = "ជំនាញ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 22);
            this.label1.TabIndex = 25;
            this.label1.Text = "មុខវិជ្ជា";
            // 
            // cbo_subject
            // 
            this.cbo_subject.BackColor = System.Drawing.SystemColors.Info;
            this.cbo_subject.DisplayMember = "skill";
            this.cbo_subject.DropDownHeight = 120;
            this.cbo_subject.FormattingEnabled = true;
            this.cbo_subject.IntegralHeight = false;
            this.cbo_subject.Location = new System.Drawing.Point(71, 73);
            this.cbo_subject.Name = "cbo_subject";
            this.cbo_subject.Size = new System.Drawing.Size(230, 30);
            this.cbo_subject.TabIndex = 29;
            this.cbo_subject.ValueMember = "skill_id";
            // 
            // txt_answer_num
            // 
            this.txt_answer_num.BackColor = System.Drawing.SystemColors.Info;
            this.txt_answer_num.Location = new System.Drawing.Point(378, 73);
            this.txt_answer_num.MaxLength = 3;
            this.txt_answer_num.Name = "txt_answer_num";
            this.txt_answer_num.Size = new System.Drawing.Size(69, 30);
            this.txt_answer_num.TabIndex = 32;
            this.txt_answer_num.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(315, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 22);
            this.label3.TabIndex = 31;
            this.label3.Text = "ចំនួនសំនួរ";
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(571, 71);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(100, 34);
            this.btn_clear.TabIndex = 34;
            this.btn_clear.Tag = "1";
            this.btn_clear.Text = "button1";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(461, 71);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(100, 34);
            this.btn_add.TabIndex = 33;
            this.btn_add.Tag = "1";
            this.btn_add.Text = "button1";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // col_auto_id
            // 
            this.col_auto_id.DataPropertyName = "auto_id";
            this.col_auto_id.HeaderText = "col_auto_id";
            this.col_auto_id.Name = "col_auto_id";
            this.col_auto_id.ReadOnly = true;
            this.col_auto_id.Visible = false;
            // 
            // col_edit
            // 
            this.col_edit.FillWeight = 60F;
            this.col_edit.HeaderText = "";
            this.col_edit.Name = "col_edit";
            this.col_edit.ReadOnly = true;
            this.col_edit.Text = "កែប្រែ";
            this.col_edit.UseColumnTextForButtonValue = true;
            this.col_edit.Width = 60;
            // 
            // col_lr
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_lr.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_lr.FillWeight = 50F;
            this.col_lr.HeaderText = "ល.រ";
            this.col_lr.Name = "col_lr";
            this.col_lr.ReadOnly = true;
            this.col_lr.Width = 50;
            // 
            // col_skill_id
            // 
            this.col_skill_id.DataPropertyName = "skill_id";
            this.col_skill_id.HeaderText = "skill_id";
            this.col_skill_id.Name = "col_skill_id";
            this.col_skill_id.ReadOnly = true;
            this.col_skill_id.Visible = false;
            // 
            // col_skill
            // 
            this.col_skill.DataPropertyName = "skill";
            this.col_skill.FillWeight = 200F;
            this.col_skill.HeaderText = "ជំនាញ";
            this.col_skill.Name = "col_skill";
            this.col_skill.ReadOnly = true;
            this.col_skill.Width = 200;
            // 
            // col_sub_id
            // 
            this.col_sub_id.DataPropertyName = "sub_id";
            this.col_sub_id.HeaderText = "sub_id";
            this.col_sub_id.Name = "col_sub_id";
            this.col_sub_id.ReadOnly = true;
            this.col_sub_id.Visible = false;
            // 
            // col_subject
            // 
            this.col_subject.DataPropertyName = "sub_title";
            this.col_subject.FillWeight = 200F;
            this.col_subject.HeaderText = "មុខវិជ្ជា";
            this.col_subject.Name = "col_subject";
            this.col_subject.ReadOnly = true;
            this.col_subject.Width = 200;
            // 
            // col_question
            // 
            this.col_question.DataPropertyName = "quiz_limit";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_question.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_question.HeaderText = "ចំនួនសំនួរ";
            this.col_question.Name = "col_question";
            this.col_question.ReadOnly = true;
            // 
            // frm_subject_edit_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 482);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.txt_answer_num);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvlst);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.cbo_subject);
            this.Controls.Add(this.cbo_skill);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_subject_edit_";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "កំណត់មុខវិជ្ជា";
            this.Load += new System.EventHandler(this.frm_subject_edit__Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dgvlst;
        private System.Windows.Forms.ComboBox cbo_skill;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_subject;
        private System.Windows.Forms.TextBox txt_answer_num;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_auto_id;
        private System.Windows.Forms.DataGridViewButtonColumn col_edit;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lr;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_skill_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_skill;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_sub_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_question;
    }
}