﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_certificate_skill : Form
    {
        public frm_certificate_skill()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ព័ត៌មានជំនាញ");
        }

        private void frm_certificate_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_certificate);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            bds_certificate_skill.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();

        }

        private void frm_certificate_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_certificate_skill_edit frm = new frm_certificate_skill_edit();
            frm.ShowDialog();
            frm_certificate_Load(null, null);
        }

        private void dgv_certificate_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                frm_certificate_skill_edit frm = new frm_certificate_skill_edit();
                frm.cer_skill_id = (int)dgv_certificate.CurrentRow.Cells["cert_skill_id"].Value;
                frm.ShowDialog();
                frm_certificate_Load(null, null);
            }
        }
    }
}
