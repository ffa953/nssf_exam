﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_skill_edit : Form
    {
        public int skill_id=0;
        public frm_skill_edit()
        {
            InitializeComponent();
        }

        private void frm_skill_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_new);
            ClsSetting.sty_btn_close(btn_close);
            bds_quiz_type.DataSource = ClsConfig.glo_local_db.tbl_quiz_type.ToList();
            cbo_quiz_type.SelectedIndex = -1;

            if (skill_id > 0)
            {
                var sk = (from s in ClsConfig.glo_local_db.tbl_skill where s.skill_id == skill_id select s).SingleOrDefault();
                txt_skill.Text = sk.skill;
                txt_skill_note.Text = sk.skill_note;
                cbo_quiz_type.SelectedValue = sk.type_id;
                chk_box.Checked = (bool) sk.skill_active;

            }
                
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            try
            {
                if (skill_id > 0)
                {
                    var sk = (from s in ClsConfig.glo_local_db.tbl_skill where s.skill_id == skill_id select s).SingleOrDefault();
                    sk.skill = txt_skill.Text;
                    sk.skill_note = txt_skill_note.Text;
                    sk.type_id = Convert.ToInt32(cbo_quiz_type.SelectedValue);
                    sk.skill_active = (chk_box.Checked) ? true : false;
                    sk.user_id = ClsConfig.glo_use_id;
                    sk.user_edit = ClsFun.getCurrentDate();            
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក");
                    this.Close();
                }
                else
                {
                    if (IsFieldValidate())
                    {
                        tbl_skill sk = new tbl_skill();
                        sk.skill = txt_skill.Text;
                        sk.skill_note = txt_skill_note.Text;
                        sk.type_id =Convert.ToInt32(cbo_quiz_type.SelectedValue);
                        sk.skill_active = (chk_box.Checked) ? true : false;
                        sk.user_id = ClsConfig.glo_use_id;
                        sk.user_edit = ClsFun.getCurrentDate();
                        ClsConfig.glo_local_db.tbl_skill.Add(sk);
                        ClsConfig.glo_local_db.SaveChanges();
                        ClsMsg.Success("ពត៍មានត្រូវបានរក្សាទុក");
                    }
                }
               
            }
            catch(Exception ex)
            {
                ClsMsg.Success("" + ex);
            }

            

        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (txt_skill.Text == "")
            {
                msgString += "-ផ្នែកត្រូវតែបញ្ចូល\n";
            }

            if (txt_skill.Text == "")
            {
                msgString += "-អក្សរកាត់ត្រូវតែបញ្ចូល\n";
            }

            if (cbo_quiz_type.Text == "")
            {
                msgString += "-ប្រភេទសំនួរត្រូវតែបញ្ចូល\n";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
