﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;
using nssf_hr_system.Forms.FingerPrint;
using System.Transactions;
namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_emp_import_edit : Form
    {
       
        public List<int> emp_lst = new List<int>();
        public frm_emp_import_edit()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string fun_select_emp_option(int pos_id)
        {
            switch (pos_id)
            {
                case 1 :
                case 2:
                case 7:
                case 21:
                case 22:
                case 23:                   
                    return "0";
                default :
                    return "1";
            }
        }

        private void frm_emp_import_edit_Load(object sender, EventArgs e)
        {
            try
            {

                lb_emp.Text = emp_lst.Count.ToString();
                ClsSetting.sty_btn_save(btn_save);
                ClsSetting.sty_btn_close(btn_close);

                bds_position.DataSource =ClsConfig.glo_local_db.vw_position.Where(m => m.pos_active == true).ToList();
                bds_branch.DataSource = ClsConfig.glo_local_db.vw_branch.Where(m => m.bra_active == true).OrderBy(m => m.bra_order).ToList();
                
               
                cbo_branch.SelectedIndex = -1;
                cbo_division.SelectedIndex = -1;
                cbo_pos.SelectedValue = 6;

              
            }
            catch(Exception ex)
            {
                ClsMsg.Success(""+ex);
            }
        }

        private void cbo_branch_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cbo_branch_SelectionChangeCommitted(object sender, EventArgs e)
        {
            
            string bra_id = cbo_branch.SelectedValue.ToString();
            var division = (from d in ClsConfig.glo_local_db.vie_division where d.bra_id == bra_id select d).ToList();
            cbo_division.DataSource = division;
            cbo_division.DisplayMember = "div_name_khmer";
            cbo_division.ValueMember = "div_id";
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            add_emp();
            Cursor.Current = Cursors.Default;
          
        }

       public string getFirstname(string Fullname)
        {
            return Fullname.Substring(0, Fullname.IndexOf(" "));
        }

       public string getLastname(string Fullname)
        {
            return Fullname.Substring(Fullname.IndexOf(" ") + 1, Fullname.Length - (Fullname.IndexOf(" ") + 1));
        }

       public bool IsFieldValid()
       {
           string str="";
           if(cbo_pos.SelectedItem==null)
           {
               str += "សូមជ្រើសរើសឋានៈ !!!\n";
           }
           if(cbo_division.SelectedItem==null)
           {
               str += "សូមជ្រើសរើសការិយាល័យ  !!!\n";
           }
           if(str!="")
           {
               ClsMsg.Warning(str);
               return false;
           }
           return true;
       }
       public void add_emp()
       {

           List<string> dup_lst = new List<string>();
           if (IsFieldValid())
           {
               for (int i = 0; i < emp_lst.Count; i++)
               {
                 

                       int id = Convert.ToInt32(emp_lst[i]);
                       var us = (from u in ClsConfig.glo_local_db.UserProfiles where u.UserId == id select u).SingleOrDefault();
                       tbl_user_fingerprint us_fin = ClsConfig.glo_local_db.tbl_user_fingerprint.Find(us.UserId);
                       string title="";
                       

                           if (us.GenderId == 1)
                           {
                               title = "លោក";
                           }
                           else
                           {
                               title = "កញ្ញា";
                           }

                           
                            ClsConfig.glo_local_db.pro_import_emp(title, getFirstname(us.Name_khmer),
                                                                  getLastname(us.Name_khmer),
                                                                  getFirstname(us.Name_latin),
                                                                  getLastname(us.Name_latin),
                                                                  us.GenderId, us.DateOfBirth,
                                                                  Convert.ToString(cbo_division.SelectedValue),
                                                                  Convert.ToInt32(cbo_pos.SelectedValue),
                                                                  DateTime.Now, us.Phone,
                                                                  " ",
                                                                  fun_select_emp_option(Convert.ToInt32(cbo_pos.SelectedValue)),
                                                                  ClsConfig.glo_use_id, 
                                                                  ClsFun.getCurrentDate());

                
                   ClsMsg.Success(ClsMsg.STR_SUCCESS);

                 

                   this.Close();

               }

           }
       }
        public void sub_cancel(object sender, EventArgs e)
        {
            ClsMsg.Warning(ClsMsg.STR_UNSUCCESS);
        }
    }
}
