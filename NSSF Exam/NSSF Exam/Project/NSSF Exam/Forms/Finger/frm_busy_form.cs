﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nssf_hr_system.Forms.FingerPrint
{
    public partial class frm_busy_form : Form
    {

        private DoWorkEventHandler thr_call_back;
        private object thr_arg_object;
        private RunWorkerCompletedEventArgs thr_result;
        private Exception thr_error;
        protected BackgroundWorker thr_worker = new BackgroundWorker();


        private frm_busy_form(string title, DoWorkEventHandler callback) 
            : this(title, callback, false, null, null)
        {

        }

        private frm_busy_form(string title, DoWorkEventHandler callback, object args)
           : this(title, callback, false, args, null)
        {
            
        }

        private frm_busy_form(string title, DoWorkEventHandler callback, bool reportsProgress)
            : this(title, callback, reportsProgress, null, null)
        {

        }
        
        public frm_busy_form(string title, DoWorkEventHandler callback, bool reportsProgress, object args, EventHandler cancelHandler)
        {
            InitializeComponent();


            if (!reportsProgress)
            {
                progressBar.Style = ProgressBarStyle.Marquee;
            }
            SetExecutionText(title);
            this.thr_call_back = callback;
            thr_arg_object = args;
            thr_worker.WorkerReportsProgress = reportsProgress;
            thr_worker.DoWork += BusyForm_DoWork;
            thr_worker.RunWorkerCompleted += BusyForm_RunWorkerCompleted;
            thr_worker.ProgressChanged += BusyForm_ProgressChanged;

            if (cancelHandler != null)
            {
                thr_worker.WorkerSupportsCancellation = true;
                btn_close.Click += cancelHandler;
                btn_close.Click += new EventHandler(btn_close_Click);
                btn_close.Enabled = true;
                btn_close.Visible = true;
            }
        }

        private void frm_busy_form_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_close(btn_close);
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback)
        {
            return RunLongTask(title, callback, false);
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback, bool reportsProgress)
        {
            using (frm_busy_form frmLongTask = new frm_busy_form(title, callback, reportsProgress))
            {
                frmLongTask.ShowDialog();
                if (frmLongTask.thr_error != null)
                {
                    throw frmLongTask.thr_error;
                }
                return frmLongTask.thr_result;
            }
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback, bool reportsProgress, object args, EventHandler cancelHandler)
        {
            using (frm_busy_form frmLongTask = new frm_busy_form(title, callback, reportsProgress, args, cancelHandler))
            {
                frmLongTask.ShowDialog();
                if (frmLongTask.thr_error != null)
                {
                    throw frmLongTask.thr_error;
                }
                return frmLongTask.thr_result;
            }
        }

        private void frm_busy_form_Shown(object sender, EventArgs e)
        {
            thr_worker.RunWorkerAsync(thr_arg_object);
        }

#region "Background worker"

        private void BusyForm_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                thr_call_back(sender, e);
            }
            catch (Exception ex)
            {
                thr_error = ex;
            }
        }

        private void BusyForm_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string text = e.UserState as string;
            if (text != null)
            {
                SetExecutionText(text);
            }
            SetExecutionValue(e.ProgressPercentage);
        }

        private void BusyForm_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            thr_result = e;
            Close();
        }

#endregion

#region "Setters/Getters"

        public delegate void StringMethod(string value);

        public void SetExecutionText(string text)
        {
            try
            {
                if (lblOperation.InvokeRequired)
                {
                    lblOperation.Invoke((StringMethod)SetExecutionText);
                }
                else
                {
                    lblOperation.Text = text;
                }
            }
            finally
            {
            }
        }

        public delegate void IntegerMethod(int value);

        public void SetExecutionValue(int value)
        {
            try
            {
                if (progressBar.InvokeRequired)
                {
                    progressBar.Invoke((IntegerMethod)SetExecutionValue, value);
                }
                else
                {
                    progressBar.Value = value;
                }
            }
            finally
            {
            }
        }

 #endregion

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
