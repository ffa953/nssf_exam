﻿using DPFP;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Finger
{
    public partial class frm_registration_scan : Form
    {
        List<ClsFingerPrint> fin_list = new List<ClsFingerPrint>();
        public string fing_quality = "";
        public int user_id;

        public frm_registration_scan()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "បញ្ចូលស្នាមម្រាមដៃ");
        }

       

        private void btn_save_finger_Click(object sender, EventArgs e)
        {
            

            string str_string = "";
            if (fin_list.Count == 2) {

	            foreach (var cls in fin_list) {
		            str_string += (cls.fin_verify == false ? " - " + fun_select_finger_name(cls.fin_index) + " មិនត្រឹមត្រូវ, សូមធ្វើការផ្លាស់ប្តូរម្រាមដៃនេះជាមុនសិន\n" : "");
	            }
	            if (string.IsNullOrEmpty(str_string)) {
		            try {
                        var result = ClsConfig.glo_local_db.tbl_user_fingerprint.Where(f => f.UserId == user_id);
                        if (result.Count() > 0)
                        {
                            if (ClsMsg.Question("បេក្ខជននេះ​មានស្នាមម្រាមដៃរួចហើយ តើអ្នកចង់ធ្វើបច្ចុប្បន្នភាពឬទេ?")) {

                                tbl_user_fingerprint tbl = result.FirstOrDefault();

                                MemoryStream mem_stream = default(MemoryStream);
                                Template tem_left = fin_list[0].fin_template;
                                Template tem_right = fin_list[1].fin_template;
                                FeatureSet fea_left = fin_list[0].fin_feature_set;
                                FeatureSet fea_right = fin_list[1].fin_feature_set;

                                byte[] finger_left = null;
                                byte[] finger_right = null;
                                byte[] verify_left = null;
                                byte[] verify_right = null;

                                mem_stream = new MemoryStream();
                                tem_left.Serialize(mem_stream);
                                finger_left = mem_stream.ToArray();
                                mem_stream = new MemoryStream();
                                tem_right.Serialize(mem_stream);
                                finger_right = mem_stream.ToArray();

                                mem_stream = new MemoryStream();
                                fea_left.Serialize(mem_stream);
                                verify_left = mem_stream.ToArray();

                                mem_stream = new MemoryStream();
                                fea_right.Serialize(mem_stream);
                                verify_right = mem_stream.ToArray();

                                tbl.use_finger_left = finger_left;
                                tbl.use_finger_left_index = fin_list[0].fin_index;
                                tbl.use_finger_right = finger_right;
                                tbl.use_finger_right_index = fin_list[1].fin_index;
                                tbl.user_id = ClsConfig.glo_use_id;
                                tbl.user_edit = ClsFun.getCurrentDate();
                                ClsConfig.glo_local_db.SaveChanges();

                                ClsMsg.Success(ClsMsg.STR_SUCCESS);
                                this.Close();
                            }
                        }
                        else
                        {
                            tbl_user_fingerprint tbl = new tbl_user_fingerprint();
                            MemoryStream mem_stream = default(MemoryStream);
                            Template tem_left = fin_list[0].fin_template;
                            Template tem_right = fin_list[1].fin_template;
                            FeatureSet fea_left = fin_list[0].fin_feature_set;
                            FeatureSet fea_right = fin_list[1].fin_feature_set;

                            byte[] finger_left = null;
                            byte[] finger_right = null;
                            byte[] verify_left = null;
                            byte[] verify_right = null;

                            mem_stream = new MemoryStream();
                            tem_left.Serialize(mem_stream);
                            finger_left = mem_stream.ToArray();
                            mem_stream = new MemoryStream();
                            tem_right.Serialize(mem_stream);
                            finger_right = mem_stream.ToArray();

                            mem_stream = new MemoryStream();
                            fea_left.Serialize(mem_stream);
                            verify_left = mem_stream.ToArray();

                            mem_stream = new MemoryStream();
                            fea_right.Serialize(mem_stream);
                            verify_right = mem_stream.ToArray();

                            tbl.UserId = user_id;
                            tbl.use_finger_left = finger_left;
                            tbl.use_finger_left_index = fin_list[0].fin_index;
                            tbl.use_finger_right = finger_right;
                            tbl.use_finger_right_index = fin_list[1].fin_index;
                            tbl.user_id = ClsConfig.glo_use_id;
                            tbl.user_edit = ClsFun.getCurrentDate();
                            ClsConfig.glo_local_db.tbl_user_fingerprint.Add(tbl);
                            ClsConfig.glo_local_db.SaveChanges();

                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            this.Close();
                        }

		            } catch (Exception ex) {
		            }
	            } else {
                    ClsMsg.Error(str_string);
	            }
            }
        }

        public string fun_select_finger_name(int fin_id)
        {
            string result = "";
            var que = ClsConfig.glo_local_db.tbl_finger_print.Where(fin => fin.fin_id == fin_id).ToList();
            if (que.Count() > 0)
            {
                result = que.First().fin_name_khmer;
            }
            return result;
        }

        private void frm_registration_scan_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save_finger);
            ClsSetting.sty_btn_close(btn_cancel);
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EnrollmentControl_OnEnroll(object Control, int FingerMask, Template Template, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            ClsFingerPrint cls = new ClsFingerPrint();
            cls.fin_index = FingerMask;
            cls.fin_template = Template;
            cls.fin_quaity = fing_quality;
            //fin_list.Add(cls)
            frm_registration_verify frm = new frm_registration_verify();
            frm.cls = cls;
            frm.ShowDialog();
            fin_list.Add(frm.cls);
        }

        private void EnrollmentControl_OnDelete(object Control, int FingerMask, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            ClsFingerPrint cls = default(ClsFingerPrint);
            for (int i = fin_list.Count() - 1; i >= 0; i += -1)
            {
                cls = fin_list[i];
                if (cls.fin_index == FingerMask)
                {
                    fin_list.Remove(cls);
                }
            }
        }

        private void EnrollmentControl_OnComplete(object Control, string ReaderSerialNumber, int Finger)
        {

        }

        private void EnrollmentControl_OnSampleQuality(object Control, string ReaderSerialNumber, int Finger, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
            fing_quality = CaptureFeedback.ToString();
        }

        private void EnrollmentControl_OnReaderDisconnect(object Control, string ReaderSerialNumber, int Finger)
        {
            MessageBox.Show("Cannot connect to finger print", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
