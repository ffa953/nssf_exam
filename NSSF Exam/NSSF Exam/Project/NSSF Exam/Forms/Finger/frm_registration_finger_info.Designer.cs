﻿namespace NSSF_Exam.Forms.Finger
{
    partial class frm_registration_finger_info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_description = new System.Windows.Forms.Label();
            this.VerificationControl = new DPFP.Gui.Verification.VerificationControl();
            this.pic_result = new System.Windows.Forms.PictureBox();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_result)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.pic_result);
            this.GroupBox2.Controls.Add(this.lbl_description);
            this.GroupBox2.Controls.Add(this.VerificationControl);
            this.GroupBox2.Location = new System.Drawing.Point(23, 12);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(262, 229);
            this.GroupBox2.TabIndex = 104;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "ព័ត៌មានស្នាមម្រាមដៃ";
            // 
            // lbl_description
            // 
            this.lbl_description.AutoSize = true;
            this.lbl_description.Location = new System.Drawing.Point(96, 154);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(61, 24);
            this.lbl_description.TabIndex = 79;
            this.lbl_description.Text = "លទ្ធផល៖";
            // 
            // VerificationControl
            // 
            this.VerificationControl.Active = true;
            this.VerificationControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.VerificationControl.Location = new System.Drawing.Point(99, 47);
            this.VerificationControl.Margin = new System.Windows.Forms.Padding(5, 11, 5, 11);
            this.VerificationControl.Name = "VerificationControl";
            this.VerificationControl.ReaderSerialNumber = "00000000-0000-0000-0000-000000000000";
            this.VerificationControl.Size = new System.Drawing.Size(64, 87);
            this.VerificationControl.TabIndex = 76;
            this.VerificationControl.OnComplete += new DPFP.Gui.Verification.VerificationControl._OnComplete(this.VerificationControl_OnComplete);
            // 
            // pic_result
            // 
            this.pic_result.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_result.Location = new System.Drawing.Point(103, 106);
            this.pic_result.Name = "pic_result";
            this.pic_result.Size = new System.Drawing.Size(41, 38);
            this.pic_result.TabIndex = 82;
            this.pic_result.TabStop = false;
            // 
            // frm_registration_finger_info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 265);
            this.Controls.Add(this.GroupBox2);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_registration_finger_info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ផ្ទៀងផ្ទាត់ស្នាមម្រាមដៃ";
            this.Load += new System.EventHandler(this.frm_registration_finger_info_Load);
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_result)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.PictureBox pic_result;
        internal System.Windows.Forms.Label lbl_description;
        internal DPFP.Gui.Verification.VerificationControl VerificationControl;
    }
}