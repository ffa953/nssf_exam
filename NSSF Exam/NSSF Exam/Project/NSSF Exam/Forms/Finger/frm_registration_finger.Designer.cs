﻿namespace NSSF_Exam.Forms.Finger
{
    partial class frm_registration_finger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_verify = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_add_finger = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Label8 = new System.Windows.Forms.Label();
            this.cbo_gender = new System.Windows.Forms.ComboBox();
            this.bds_gender = new System.Windows.Forms.BindingSource(this.components);
            this.Label7 = new System.Windows.Forms.Label();
            this.cbo_nationality = new System.Windows.Forms.ComboBox();
            this.bds_nationality = new System.Windows.Forms.BindingSource(this.components);
            this.Label6 = new System.Windows.Forms.Label();
            this.cbo_skill = new System.Windows.Forms.ComboBox();
            this.bds_skill = new System.Windows.Forms.BindingSource(this.components);
            this.Label5 = new System.Windows.Forms.Label();
            this.txt_phone = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.dtp_dob = new System.Windows.Forms.DateTimePicker();
            this.Label3 = new System.Windows.Forms.Label();
            this.txt_name_en = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.txt_name_kh = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.txt_user_code = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_gender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_nationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_verify);
            this.panel1.Controls.Add(this.btn_cancel);
            this.panel1.Controls.Add(this.btn_add_finger);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 268);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(724, 65);
            this.panel1.TabIndex = 0;
            // 
            // btn_verify
            // 
            this.btn_verify.Location = new System.Drawing.Point(391, 14);
            this.btn_verify.Name = "btn_verify";
            this.btn_verify.Size = new System.Drawing.Size(100, 36);
            this.btn_verify.TabIndex = 37;
            this.btn_verify.Text = "ផ្ទៀងផ្ទាត់";
            this.btn_verify.UseVisualStyleBackColor = true;
            this.btn_verify.Click += new System.EventHandler(this.btn_verify_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(608, 14);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(100, 36);
            this.btn_cancel.TabIndex = 33;
            this.btn_cancel.Text = "បោះបង់";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_add_finger
            // 
            this.btn_add_finger.Location = new System.Drawing.Point(500, 14);
            this.btn_add_finger.Name = "btn_add_finger";
            this.btn_add_finger.Size = new System.Drawing.Size(100, 36);
            this.btn_add_finger.TabIndex = 34;
            this.btn_add_finger.Text = "បញ្ចូល";
            this.btn_add_finger.UseVisualStyleBackColor = true;
            this.btn_add_finger.Click += new System.EventHandler(this.btn_add_finger_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.Label8);
            this.panel2.Controls.Add(this.cbo_gender);
            this.panel2.Controls.Add(this.Label7);
            this.panel2.Controls.Add(this.cbo_nationality);
            this.panel2.Controls.Add(this.Label6);
            this.panel2.Controls.Add(this.cbo_skill);
            this.panel2.Controls.Add(this.Label5);
            this.panel2.Controls.Add(this.txt_phone);
            this.panel2.Controls.Add(this.Label4);
            this.panel2.Controls.Add(this.dtp_dob);
            this.panel2.Controls.Add(this.Label3);
            this.panel2.Controls.Add(this.txt_name_en);
            this.panel2.Controls.Add(this.Label2);
            this.panel2.Controls.Add(this.txt_name_kh);
            this.panel2.Controls.Add(this.Label1);
            this.panel2.Controls.Add(this.txt_user_code);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(724, 268);
            this.panel2.TabIndex = 1;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(378, 58);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(32, 24);
            this.Label8.TabIndex = 36;
            this.Label8.Text = "ភេទ";
            // 
            // cbo_gender
            // 
            this.cbo_gender.DataSource = this.bds_gender;
            this.cbo_gender.DisplayMember = "GenderKH";
            this.cbo_gender.Enabled = false;
            this.cbo_gender.FormattingEnabled = true;
            this.cbo_gender.Location = new System.Drawing.Point(488, 50);
            this.cbo_gender.Name = "cbo_gender";
            this.cbo_gender.Size = new System.Drawing.Size(200, 32);
            this.cbo_gender.TabIndex = 35;
            this.cbo_gender.ValueMember = "GenderId";
            // 
            // bds_gender
            // 
            this.bds_gender.DataSource = typeof(NSSF_Exam.Entities.tbl_gender);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(378, 192);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(50, 24);
            this.Label7.TabIndex = 32;
            this.Label7.Text = "សញ្ជាតិ";
            // 
            // cbo_nationality
            // 
            this.cbo_nationality.DataSource = this.bds_nationality;
            this.cbo_nationality.DisplayMember = "nat_name_kh";
            this.cbo_nationality.Enabled = false;
            this.cbo_nationality.FormattingEnabled = true;
            this.cbo_nationality.Location = new System.Drawing.Point(488, 184);
            this.cbo_nationality.Name = "cbo_nationality";
            this.cbo_nationality.Size = new System.Drawing.Size(200, 32);
            this.cbo_nationality.TabIndex = 31;
            this.cbo_nationality.ValueMember = "nat_id";
            // 
            // bds_nationality
            // 
            this.bds_nationality.DataSource = typeof(NSSF_Exam.Entities.vw_nationality);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(31, 190);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(32, 24);
            this.Label6.TabIndex = 30;
            this.Label6.Text = "ផ្នែក";
            // 
            // cbo_skill
            // 
            this.cbo_skill.DataSource = this.bds_skill;
            this.cbo_skill.DisplayMember = "skill";
            this.cbo_skill.Enabled = false;
            this.cbo_skill.FormattingEnabled = true;
            this.cbo_skill.Location = new System.Drawing.Point(133, 182);
            this.cbo_skill.Name = "cbo_skill";
            this.cbo_skill.Size = new System.Drawing.Size(200, 32);
            this.cbo_skill.TabIndex = 29;
            this.cbo_skill.ValueMember = "skill_id";
            // 
            // bds_skill
            // 
            this.bds_skill.DataSource = typeof(NSSF_Exam.Entities.tbl_skill);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(378, 145);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(71, 24);
            this.Label5.TabIndex = 28;
            this.Label5.Text = "លេខទូរស័ព្ទ";
            // 
            // txt_phone
            // 
            this.txt_phone.BackColor = System.Drawing.SystemColors.Info;
            this.txt_phone.Location = new System.Drawing.Point(488, 141);
            this.txt_phone.Name = "txt_phone";
            this.txt_phone.ReadOnly = true;
            this.txt_phone.Size = new System.Drawing.Size(200, 32);
            this.txt_phone.TabIndex = 27;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(31, 141);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(89, 24);
            this.Label4.TabIndex = 26;
            this.Label4.Text = "ថ្ងៃខែឆ្នាំកំណើត";
            // 
            // dtp_dob
            // 
            this.dtp_dob.CustomFormat = "dd/MM/yyyy";
            this.dtp_dob.Enabled = false;
            this.dtp_dob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dob.Location = new System.Drawing.Point(133, 138);
            this.dtp_dob.Name = "dtp_dob";
            this.dtp_dob.Size = new System.Drawing.Size(200, 32);
            this.dtp_dob.TabIndex = 25;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(378, 100);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(94, 24);
            this.Label3.TabIndex = 24;
            this.Label3.Text = "ឈ្មោះជាឡាតាំង";
            // 
            // txt_name_en
            // 
            this.txt_name_en.BackColor = System.Drawing.SystemColors.Info;
            this.txt_name_en.Location = new System.Drawing.Point(488, 96);
            this.txt_name_en.Name = "txt_name_en";
            this.txt_name_en.ReadOnly = true;
            this.txt_name_en.Size = new System.Drawing.Size(200, 32);
            this.txt_name_en.TabIndex = 23;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(31, 99);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(71, 24);
            this.Label2.TabIndex = 22;
            this.Label2.Text = "ឈ្មោះជាខ្មែរ";
            // 
            // txt_name_kh
            // 
            this.txt_name_kh.BackColor = System.Drawing.SystemColors.Info;
            this.txt_name_kh.Location = new System.Drawing.Point(133, 95);
            this.txt_name_kh.Name = "txt_name_kh";
            this.txt_name_kh.ReadOnly = true;
            this.txt_name_kh.Size = new System.Drawing.Size(200, 32);
            this.txt_name_kh.TabIndex = 21;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(31, 54);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(75, 24);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "លេខសំគាល់";
            // 
            // txt_user_code
            // 
            this.txt_user_code.BackColor = System.Drawing.SystemColors.Info;
            this.txt_user_code.Location = new System.Drawing.Point(133, 50);
            this.txt_user_code.Name = "txt_user_code";
            this.txt_user_code.ReadOnly = true;
            this.txt_user_code.Size = new System.Drawing.Size(200, 32);
            this.txt_user_code.TabIndex = 19;
            // 
            // frm_registration_finger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 333);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_registration_finger";
            this.Text = "frm_registration_finger";
            this.Load += new System.EventHandler(this.frm_registration_finger_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_gender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_nationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_skill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Button btn_verify;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.ComboBox cbo_gender;
        internal System.Windows.Forms.Button btn_add_finger;
        internal System.Windows.Forms.Button btn_cancel;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.ComboBox cbo_nationality;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.ComboBox cbo_skill;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txt_phone;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.DateTimePicker dtp_dob;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txt_name_en;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txt_name_kh;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txt_user_code;
        internal System.Windows.Forms.BindingSource bds_gender;
        internal System.Windows.Forms.BindingSource bds_nationality;
        internal System.Windows.Forms.BindingSource bds_skill;
    }
}