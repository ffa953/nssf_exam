﻿using DPFP.Verification;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Finger
{
    public partial class frm_verify_fingerprint : Form
    {
        private List<ClsFingerPrint> cls = new List<ClsFingerPrint>();
        DPFP.Template template = new DPFP.Template();
        public frm_verify_fingerprint()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ផ្ទៀងផ្ទាត់ស្នាមម្រាមដៃ");
        }

        private void frm_verify_fingerprint_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_close(btn_close);         
            bds_exam_date.DataSource = ClsConfig.glo_local_db.tbl_exam_date.OrderByDescending(m=>m.exam_id).ToList();
            cbo_exam_date.SelectedIndex = -1;

        }

        

        private void frm_verify_fingerprint_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void VerificationControl_OnComplete(object Control, DPFP.FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            Cursor.Current = Cursors.WaitCursor;
            Verification ver = new Verification();
            DPFP.Verification.Verification.Result res = new DPFP.Verification.Verification.Result();
            if (cls.Count() > 0)
            {
                foreach (var tb in cls)
                {

                    ver.Verify(FeatureSet, tb.fin_template, ref res);
                    //   Compare feature set with particular template.
                    if (res.Verified)
                    {
                        EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Success;
                        lbl_description.Text = " ត្រឹមត្រូវ!";
                        tb.fin_feature_set = FeatureSet;
                        tb.fin_verify = true;
                        pic_result.BackgroundImage = Properties.Resources.Accept;

                        var re = (from us in ClsConfig.glo_local_db.UserProfiles
                                  join sk in ClsConfig.glo_local_db.tbl_skill on us.skill_id equals sk.skill_id
                                  join ge in ClsConfig.glo_local_db.tbl_gender on us.GenderId equals ge.GenderId
                                  join na in ClsConfig.glo_local_db.vw_nationality on us.nat_id equals na.nat_id
                                  where us.UserId == tb.fin_id
                                  select new { us,sk,ge,na}).FirstOrDefault();
 
                        lbl_name.Text = re.us.Name_khmer;
                        lbl_name_latin.Text = re.us.Name_latin;
                        lbl_gender.Text = re.ge.GenderKH;
                        lbl_dob.Text = re.us.DateOfBirth.Value.ToString("dd-MMM-yyyy");
                        lbl_nationality.Text = re.na.nat_name_kh;
                        lbl_skill.Text = re.sk.skill;
                        
                        break; // TODO: might not be correct. Was : Exit For
                    }
                    else
                    {
                        tb.fin_verify = false;
                        lbl_description.Text = "មិនត្រឹមត្រូវ!";
                        pic_result.BackgroundImage = Properties.Resources.invalid;

                        lbl_name.Text = "";
                        lbl_name_latin.Text = "";
                        lbl_gender.Text = "";
                        lbl_dob.Text = "";
                        lbl_nationality.Text = "";
                        lbl_skill.Text = "";
                    }
                    if (!res.Verified)
                    {
                        EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            else
            {
                EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;
                lbl_description.Text = "មិនមាន!";
                pic_result.BackgroundImage = Properties.Resources.invalid;


                lbl_name.Text = "";
                lbl_name_latin.Text = "";
                lbl_gender.Text = "";
                lbl_dob.Text = "";
                lbl_nationality.Text = "";
                lbl_skill.Text = "";
            }

            Cursor.Current = Cursors.Default;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      


       

        private void cbo_exam_date_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbo_exam_date.SelectedIndex>-1)
            {
                cls.Clear();
                Cursor.Current = Cursors.WaitCursor;
                int exam_id = Convert.ToInt32(cbo_exam_date.SelectedValue);
                var userFinger = (from data in ClsConfig.glo_local_db.tbl_user_fingerprint join us in ClsConfig.glo_local_db.UserProfiles on data.UserId equals us.UserId where us.exam_id == exam_id select data);

                if (userFinger.Count() > 0)
                {
                    foreach (var item in userFinger)
                    {

                        MemoryStream memStreamTemplate = new MemoryStream(item.use_finger_left);
                        template = new DPFP.Template(memStreamTemplate);
                        ClsFingerPrint clsFingerRight = new ClsFingerPrint();
                        clsFingerRight.fin_id = item.UserId;
                        clsFingerRight.fin_index = (int)item.use_finger_left_index;
                        clsFingerRight.fin_template = template;
                        cls.Add(clsFingerRight);

                        MemoryStream memStreamTemplateRight = new MemoryStream(item.use_finger_right);
                        template = new DPFP.Template(memStreamTemplateRight);
                        ClsFingerPrint clsFingerLeft = new ClsFingerPrint();
                        clsFingerLeft.fin_id = item.UserId;
                        clsFingerLeft.fin_index = (int)item.use_finger_right_index;
                        clsFingerLeft.fin_template = template;
                        cls.Add(clsFingerLeft);
                    }
                    Cursor.Current = Cursors.Default;
                    ClsMsg.Success("ទាញយកទិន្នន័យបានជោគជ័យ។");
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    ClsMsg.Warning("ទាញយកទិន្នន័យមិនបានជោគជ័យ។");
                }
            }

        }

       
    }
}
