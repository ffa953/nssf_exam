﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.Forms.Finger
{
    public partial class frm_registration_finger : Form
    {
        public int user_id = 0;
        public frm_registration_finger()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "ព័ត៌មានស្នាមម្រាមដៃ");
        }

        private void frm_registration_finger_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_add(btn_add_finger);
            ClsSetting.sty_btn_close(btn_cancel);
            ClsSetting.sty_btn_verify(btn_verify);

            bds_gender.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            bds_nationality.DataSource = ClsConfig.glo_local_db.vw_nationality.ToList();
            bds_skill.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();
            cbo_gender.SelectedIndex = -1;
            cbo_nationality.SelectedIndex = -1;
            cbo_skill.SelectedIndex = -1;

            var result = ClsConfig.glo_local_db.fun_user_by_role(2).Where(u => u.UserId == user_id).FirstOrDefault();
            txt_user_code.Text = result.UserName;
            txt_name_kh.Text = result.Name_khmer;
            txt_name_en.Text = result.Name_latin;
            txt_phone.Text = result.Phone;
            cbo_skill.SelectedValue = result.skill_id;
            cbo_nationality.SelectedValue = result.nat_id;
            cbo_gender.SelectedValue = result.GenderId;
            dtp_dob.Value = (DateTime)result.DateOfBirth;
            
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_add_finger_Click(object sender, EventArgs e)
        {
            frm_registration_scan frm = new frm_registration_scan();
            frm.user_id = user_id;
            frm.ShowDialog();
        }

        private void btn_verify_Click(object sender, EventArgs e)
        {
            frm_registration_finger_info frm = new frm_registration_finger_info();
            frm.user_id = user_id;
            frm.ShowDialog();
        }
    }
}
