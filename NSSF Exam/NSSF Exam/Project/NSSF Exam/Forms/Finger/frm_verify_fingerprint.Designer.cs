﻿namespace NSSF_Exam.Forms.Finger
{
    partial class frm_verify_fingerprint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.VerificationControl = new DPFP.Gui.Verification.VerificationControl();
            this.lbl_description = new System.Windows.Forms.Label();
            this.pic_result = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbo_exam_date = new System.Windows.Forms.ComboBox();
            this.bds_exam_date = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbl_skill = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_nationality = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_gender = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_dob = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_name_latin = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_result)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 516);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1021, 61);
            this.panel1.TabIndex = 107;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(909, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 0;
            this.btn_close.Text = "button1";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // VerificationControl
            // 
            this.VerificationControl.Active = true;
            this.VerificationControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.VerificationControl.Location = new System.Drawing.Point(76, 46);
            this.VerificationControl.Margin = new System.Windows.Forms.Padding(5, 11, 5, 11);
            this.VerificationControl.Name = "VerificationControl";
            this.VerificationControl.ReaderSerialNumber = "00000000-0000-0000-0000-000000000000";
            this.VerificationControl.Size = new System.Drawing.Size(64, 87);
            this.VerificationControl.TabIndex = 76;
            this.VerificationControl.OnComplete += new DPFP.Gui.Verification.VerificationControl._OnComplete(this.VerificationControl_OnComplete);
            // 
            // lbl_description
            // 
            this.lbl_description.AutoSize = true;
            this.lbl_description.Location = new System.Drawing.Point(73, 154);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(61, 24);
            this.lbl_description.TabIndex = 79;
            this.lbl_description.Text = "លទ្ធផល៖";
            // 
            // pic_result
            // 
            this.pic_result.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_result.Location = new System.Drawing.Point(80, 106);
            this.pic_result.Name = "pic_result";
            this.pic_result.Size = new System.Drawing.Size(41, 38);
            this.pic_result.TabIndex = 82;
            this.pic_result.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbo_exam_date);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.GroupBox2);
            this.groupBox1.Location = new System.Drawing.Point(26, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(965, 457);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ព័ត៌មានបេក្ខជន";
            // 
            // cbo_exam_date
            // 
            this.cbo_exam_date.DataSource = this.bds_exam_date;
            this.cbo_exam_date.DisplayMember = "exam_date";
            this.cbo_exam_date.FormatString = "dd-MM-yyyy";
            this.cbo_exam_date.FormattingEnabled = true;
            this.cbo_exam_date.Location = new System.Drawing.Point(133, 55);
            this.cbo_exam_date.Name = "cbo_exam_date";
            this.cbo_exam_date.Size = new System.Drawing.Size(224, 32);
            this.cbo_exam_date.TabIndex = 108;
            this.cbo_exam_date.ValueMember = "exam_id";
            this.cbo_exam_date.SelectedIndexChanged += new System.EventHandler(this.cbo_exam_date_SelectedIndexChanged);
            // 
            // bds_exam_date
            // 
            this.bds_exam_date.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_date);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 24);
            this.label2.TabIndex = 107;
            this.label2.Text = "ជ្រើសរើសជំនាន់";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl_skill);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lbl_nationality);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.lbl_gender);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.lbl_dob);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.lbl_name_latin);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lbl_name);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(27, 107);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(683, 217);
            this.groupBox3.TabIndex = 106;
            this.groupBox3.TabStop = false;
            // 
            // lbl_skill
            // 
            this.lbl_skill.AutoSize = true;
            this.lbl_skill.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_skill.Location = new System.Drawing.Point(91, 162);
            this.lbl_skill.Name = "lbl_skill";
            this.lbl_skill.Size = new System.Drawing.Size(0, 27);
            this.lbl_skill.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(36, 163);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 27);
            this.label12.TabIndex = 22;
            this.label12.Text = "ផ្នែក៖";
            // 
            // lbl_nationality
            // 
            this.lbl_nationality.AutoSize = true;
            this.lbl_nationality.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nationality.Location = new System.Drawing.Point(398, 103);
            this.lbl_nationality.Name = "lbl_nationality";
            this.lbl_nationality.Size = new System.Drawing.Size(0, 27);
            this.lbl_nationality.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(325, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 27);
            this.label10.TabIndex = 20;
            this.label10.Text = "សញ្ជាតិ៖";
            // 
            // lbl_gender
            // 
            this.lbl_gender.AutoSize = true;
            this.lbl_gender.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gender.Location = new System.Drawing.Point(610, 42);
            this.lbl_gender.Name = "lbl_gender";
            this.lbl_gender.Size = new System.Drawing.Size(0, 27);
            this.lbl_gender.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(558, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 27);
            this.label8.TabIndex = 18;
            this.label8.Text = "ភេទ៖";
            // 
            // lbl_dob
            // 
            this.lbl_dob.AutoSize = true;
            this.lbl_dob.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dob.Location = new System.Drawing.Point(154, 103);
            this.lbl_dob.Name = "lbl_dob";
            this.lbl_dob.Size = new System.Drawing.Size(0, 27);
            this.lbl_dob.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 27);
            this.label6.TabIndex = 16;
            this.label6.Text = "ថ្ងៃខែឆ្នាំកំណើត៖";
            // 
            // lbl_name_latin
            // 
            this.lbl_name_latin.AutoSize = true;
            this.lbl_name_latin.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name_latin.Location = new System.Drawing.Point(416, 44);
            this.lbl_name_latin.Name = "lbl_name_latin";
            this.lbl_name_latin.Size = new System.Drawing.Size(0, 27);
            this.lbl_name_latin.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(327, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 27);
            this.label4.TabIndex = 14;
            this.label4.Text = "ជាឡាតាំង៖";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(172, 43);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(0, 27);
            this.lbl_name.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Content", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 27);
            this.label1.TabIndex = 12;
            this.label1.Text = "គោត្តនាម និងនាម៖";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.pic_result);
            this.GroupBox2.Controls.Add(this.lbl_description);
            this.GroupBox2.Controls.Add(this.VerificationControl);
            this.GroupBox2.Location = new System.Drawing.Point(729, 107);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(205, 217);
            this.GroupBox2.TabIndex = 105;
            this.GroupBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1021, 516);
            this.panel2.TabIndex = 108;
            // 
            // frm_verify_fingerprint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 577);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_verify_fingerprint";
            this.Text = "frm_verify_fingerprint";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_verify_fingerprint_FormClosed);
            this.Load += new System.EventHandler(this.frm_verify_fingerprint_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_result)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam_date)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        internal DPFP.Gui.Verification.VerificationControl VerificationControl;
        internal System.Windows.Forms.Label lbl_description;
        internal System.Windows.Forms.PictureBox pic_result;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_skill;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_nationality;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_gender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_dob;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_name_latin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.GroupBox GroupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbo_exam_date;
        private System.Windows.Forms.BindingSource bds_exam_date;

    }
}