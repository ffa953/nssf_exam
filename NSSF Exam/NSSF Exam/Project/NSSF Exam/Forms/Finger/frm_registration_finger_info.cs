﻿using DPFP.Verification;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Finger
{
    public partial class frm_registration_finger_info : Form
    {
        private List<ClsFingerPrint> cls = new List<ClsFingerPrint>();
        DPFP.Template template = new DPFP.Template();
        public int user_id;

        public frm_registration_finger_info()
        {
            InitializeComponent();
        }

        private void frm_registration_finger_info_Load(object sender, EventArgs e)
        {
            var userFinger = (from data in ClsConfig.glo_local_db.tbl_user_fingerprint where data.UserId == user_id select data);
            if (userFinger.Count() > 0)
            {
                MemoryStream memStreamTemplate = new MemoryStream(userFinger.FirstOrDefault().use_finger_left);
                template = new DPFP.Template(memStreamTemplate);
                ClsFingerPrint clsFingerRight = new ClsFingerPrint();
                clsFingerRight.fin_index = (int)userFinger.FirstOrDefault().use_finger_left_index;
                clsFingerRight.fin_template = template;
                cls.Add(clsFingerRight);

                MemoryStream memStreamTemplateRight = new MemoryStream(userFinger.FirstOrDefault().use_finger_right);
                template = new DPFP.Template(memStreamTemplateRight);
                ClsFingerPrint clsFingerLeft = new ClsFingerPrint();
                clsFingerLeft.fin_index = (int)userFinger.FirstOrDefault().use_finger_right_index;
                clsFingerLeft.fin_template = template;
                cls.Add(clsFingerLeft);
            }
        }

        private void VerificationControl_OnComplete(object Control, DPFP.FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            Verification ver = new Verification();
            DPFP.Verification.Verification.Result res = new DPFP.Verification.Verification.Result();
            if (cls.Count == 2)
            {
                foreach (var tb in cls)
                {

                    ver.Verify(FeatureSet, tb.fin_template, ref res);
                    //   Compare feature set with particular template.
                    if (res.Verified)
                    {
                        EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Success;
                        lbl_description.Text = " ត្រឹមត្រូវ!";
                        tb.fin_feature_set = FeatureSet;
                        tb.fin_verify = true;
                        pic_result.BackgroundImage = Properties.Resources.Accept;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                    else
                    {
                        tb.fin_verify = false;
                        lbl_description.Text = "មិនត្រឹមត្រូវ!";
                        pic_result.BackgroundImage = Properties.Resources.invalid;
                    }
                    if (!res.Verified)
                    {
                        EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            else
            {
                EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;
                lbl_description.Text = "មិនមាន!";
                pic_result.BackgroundImage = Properties.Resources.invalid;
            }

        }
        

    }
}
