﻿using DPFP.Verification;
using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Finger
{
    public partial class frm_registration_verify : Form
    {
        public ClsFingerPrint cls;
        Verification ver = new Verification();
        DPFP.Verification.Verification.Result res = new DPFP.Verification.Verification.Result();

        public frm_registration_verify()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "បញ្ជាក់ស្នាមម្រាមដៃ");
        }

        private void frm_registration_verify_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_close(btn_close);
        }      

        public string fun_select_finger_name(int fin_id)
        {
            string result = "";
            var que = ClsConfig.glo_local_db.tbl_finger_print.Where(fin => fin.fin_id == fin_id).ToList();
            if (que.Count() > 0)
            {
                result = que.First().fin_name_khmer;
            }
            return result;
        }

        private void VerificationControl_OnComplete(object Control, DPFP.FeatureSet FeatureSet, ref DPFP.Gui.EventHandlerStatus EventHandlerStatus)
        {
            ver.Verify(FeatureSet, cls.fin_template, ref res);
            //   Compare feature set with particular template.
            if (res.Verified)
            {
                EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Success;
                lbl_description.Text = " - " + fun_select_finger_name(cls.fin_index) + " ត្រឹមត្រូវ!";
                cls.fin_feature_set = FeatureSet;
                cls.fin_verify = true;
            }
            else
            {
                cls.fin_verify = false;
                lbl_description.Text = " - " + fun_select_finger_name(cls.fin_index) + " មិនត្រឹមត្រូវ!";
            }

            if (!res.Verified)
            {
                EventHandlerStatus = DPFP.Gui.EventHandlerStatus.Failure;

            }

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
