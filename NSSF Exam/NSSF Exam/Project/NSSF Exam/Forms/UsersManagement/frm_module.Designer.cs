﻿namespace NSSF_Exam.Forms.UsersManagement
{
    partial class frm_module
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_new = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.dgv_module = new System.Windows.Forms.DataGridView();
            this.bds_module = new System.Windows.Forms.BindingSource(this.components);
            this.កែប្រែ = new System.Windows.Forms.DataGridViewImageColumn();
            this.mod_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moddescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modformnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modcontrolnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(903, 56);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Controls.Add(this.btn_new);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(903, 56);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv_module);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(903, 443);
            this.panel3.TabIndex = 1;
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(684, 11);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 35);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(792, 11);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // dgv_module
            // 
            this.dgv_module.AutoGenerateColumns = false;
            this.dgv_module.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_module.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.កែប្រែ,
            this.mod_id,
            this.moddescriptionDataGridViewTextBoxColumn,
            this.modformnameDataGridViewTextBoxColumn,
            this.modcontrolnameDataGridViewTextBoxColumn});
            this.dgv_module.DataSource = this.bds_module;
            this.dgv_module.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_module.Location = new System.Drawing.Point(0, 0);
            this.dgv_module.Name = "dgv_module";
            this.dgv_module.Size = new System.Drawing.Size(903, 443);
            this.dgv_module.TabIndex = 0;
            this.dgv_module.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_module_CellClick);
            // 
            // bds_module
            // 
            this.bds_module.DataSource = typeof(NSSF_Exam.Entities.tbl_module);
            // 
            // កែប្រែ
            // 
            this.កែប្រែ.HeaderText = "កែប្រែ";
            this.កែប្រែ.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.កែប្រែ.Name = "កែប្រែ";
            this.កែប្រែ.Width = 80;
            // 
            // mod_id
            // 
            this.mod_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mod_id.DataPropertyName = "mod_id";
            this.mod_id.HeaderText = "ល.រ";
            this.mod_id.Name = "mod_id";
            this.mod_id.Width = 59;
            // 
            // moddescriptionDataGridViewTextBoxColumn
            // 
            this.moddescriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.moddescriptionDataGridViewTextBoxColumn.DataPropertyName = "mod_description";
            this.moddescriptionDataGridViewTextBoxColumn.HeaderText = "ពិពណ៌នា";
            this.moddescriptionDataGridViewTextBoxColumn.Name = "moddescriptionDataGridViewTextBoxColumn";
            this.moddescriptionDataGridViewTextBoxColumn.Width = 90;
            // 
            // modformnameDataGridViewTextBoxColumn
            // 
            this.modformnameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.modformnameDataGridViewTextBoxColumn.DataPropertyName = "mod_formname";
            this.modformnameDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះ Form";
            this.modformnameDataGridViewTextBoxColumn.Name = "modformnameDataGridViewTextBoxColumn";
            this.modformnameDataGridViewTextBoxColumn.Width = 108;
            // 
            // modcontrolnameDataGridViewTextBoxColumn
            // 
            this.modcontrolnameDataGridViewTextBoxColumn.DataPropertyName = "mod_controlname";
            this.modcontrolnameDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះ Control";
            this.modcontrolnameDataGridViewTextBoxColumn.Name = "modcontrolnameDataGridViewTextBoxColumn";
            this.modcontrolnameDataGridViewTextBoxColumn.Width = 200;
            // 
            // frm_module
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 499);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_module";
            this.Text = "frm_module";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_module_FormClosed);
            this.Load += new System.EventHandler(this.frm_module_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.DataGridView dgv_module;
        private System.Windows.Forms.BindingSource bds_module;
        private System.Windows.Forms.DataGridViewImageColumn កែប្រែ;
        private System.Windows.Forms.DataGridViewTextBoxColumn mod_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn moddescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modformnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modcontrolnameDataGridViewTextBoxColumn;
    }
}