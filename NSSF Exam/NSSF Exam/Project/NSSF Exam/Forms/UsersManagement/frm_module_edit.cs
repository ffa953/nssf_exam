﻿using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_module_edit : Form
    {
        public int mod_id = 0;
        public frm_module_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "កំណត់ការងារ");
        }

        private void frm_module_edit_Load(object sender, EventArgs e)
        {
            try
            {
                ClsSetting.sty_btn_save(btn_save);
                ClsSetting.sty_btn_close(btn_close);

                if (mod_id > 0)
                {
                    var result = ClsConfig.glo_local_db.tbl_module.Where(m => m.mod_id == mod_id).FirstOrDefault();
                    txt_form_name.Text = result.mod_formname;
                    txt_description.Text = result.mod_description;
                    txt_control_name.Text = result.mod_controlname;
                }
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFieldValidate())
                {
                    if (mod_id == 0)
                    {
                        tbl_module module = new tbl_module();
                        module.mod_formname = txt_form_name.Text;
                        module.mod_description = txt_description.Text;
                        module.mod_controlname = txt_control_name.Text;

                        ClsConfig.glo_local_db.tbl_module.Add(module);
                        
                    }
                    else {
                        var module = ClsConfig.glo_local_db.tbl_module.Where(m => m.mod_id == mod_id).FirstOrDefault();
                        module.mod_formname = txt_form_name.Text;
                        module.mod_description = txt_description.Text;
                        module.mod_controlname = txt_control_name.Text;

                       
                    }
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    this.Close();
                }  
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
            
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (txt_form_name.Text == "") {
                msgString += "- សូមបញ្ចូលឈ្មោះ Form ជាមុនសិន\n";
            }

            if (txt_control_name.Text == "")
            {
                msgString += "- សូមបញ្ចូលឈ្មោះ Control ជាមុនសិន\n";
            }

            if (txt_description.Text == "")
            {
                msgString += "- សូមបញ្ចូល​ឈ្មោះការងារជាមុនសិន\n";
            }

           
            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
