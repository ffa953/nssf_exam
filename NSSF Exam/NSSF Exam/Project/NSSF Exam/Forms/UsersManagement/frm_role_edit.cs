﻿using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_role_edit : Form
    {
        public int role_id = 0;
        public frm_role_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "ក្រុមអ្នកប្រើប្រាស់");
        }

        private void frm_role_edit_Load(object sender, EventArgs e)
        {
            try
            {
                ClsSetting.sty_btn_save(btn_save);
                ClsSetting.sty_btn_close(btn_close);

                if (role_id > 0)
                {
                    var result = ClsConfig.glo_local_db.tbl_role.Where(r => r.rol_id == role_id).FirstOrDefault();
                    txt_role.Text = result.rol_name;
                    txt_description.Text = result.rol_description;
                }
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFieldValidate())
                {
                    if (role_id == 0)
                    {
                        tbl_role role = new tbl_role();
                        role.rol_name = txt_role.Text;
                        role.rol_description = txt_description.Text;
                        role.rol_isactive = true;
                        role.use_id = ClsConfig.glo_use_id;
                        role.use_editdate = DateTime.Now;

                        ClsConfig.glo_local_db.tbl_role.Add(role);
                        
                    }
                    else {
                        var role = ClsConfig.glo_local_db.tbl_role.Where(r => r.rol_id == role_id).FirstOrDefault();
                        role.rol_name = txt_role.Text;
                        role.rol_description = txt_description.Text;
                       
                    }
                    ClsConfig.glo_local_db.SaveChanges();
                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    this.Close();
                }  
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
            
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (txt_role.Text == "") {
                msgString += "- សូមបញ្ចូលឈ្មោះក្រុមជាមុនសិន\n";
            }

            if (txt_description.Text == "")
            {
                msgString += "- សូមបញ្ចូលពិពណ៌នាជាមុនសិន\n";
            }
           
            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
