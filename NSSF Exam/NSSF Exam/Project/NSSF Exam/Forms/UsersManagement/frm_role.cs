﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_role : Form
    {
        public frm_role()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ក្រុមអ្នកប្រើប្រាស់");
        }

        private void frm_role_Load(object sender, EventArgs e)
        {
            try
            {
                ClsSetting.sty_dgv(dgv_role);
                ClsSetting.sty_btn_add(btn_new);
                ClsSetting.sty_btn_close(btn_close);
                bds_role.DataSource = ClsConfig.glo_local_db.tbl_role.ToList();
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void frm_role_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_role_edit frm = new frm_role_edit();
            frm.ShowDialog();
            frm_role_Load(null, null);
        }

        private void dgv_role_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    int roleId = (int)dgv_role.CurrentRow.Cells["role_id"].Value;

                    frm_role_edit frm = new frm_role_edit();
                    frm.role_id = roleId;
                    frm.ShowDialog();
                    frm_role_Load(null, null);
                }
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }
    }
}
