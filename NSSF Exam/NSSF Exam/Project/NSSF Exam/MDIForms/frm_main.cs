﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Forms.Exam;
using NSSF_Exam.Forms.UsersManagement;
using NSSF_Exam.Classes;
using NSSF_Exam.Forms.Finger;
using NSSF_Exam.Reports;
using NSSF_Exam.Quiz;
using NSSF_Exam.Forms.Exam.EQ_Test;
using NSSF_Exam.Forms.Exam.IQ_Quiz;

namespace NSSF_Exam.MDIForms
{
    public partial class frm_main : Form
    {
        public frm_main()
        {
            InitializeComponent();
            this.Text = ClsConfig.glo_app_name;
        }

        private void frm_main_Load(object sender, EventArgs e)
        {
            ClsSetting.OpenPermission(this, this.Name, ClsConfig.glo_use_privilege);
            ClsSetting.tpctrl = tabControl1;
            ClsSetting.OpenInParentForm(new frm_home(), this);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClsSetting.TabSelected();
        }

        private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ClsSetting.isClose == false)
            {
                if (ClsSetting.IsExitApplication())
                {
                    ClsSetting.isClose = true;
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void tsm_logout_Click(object sender, EventArgs e)
        {
            if (ClsSetting.IsExitApplication())
            {
                ClsSetting.isClose = true;
                Application.Exit();
            }
        }

        private void tsm_switch_user_Click(object sender, EventArgs e)
        {
            ClsUserClient.SetUserLogout();
        }

        private void tsm_change_password_Click(object sender, EventArgs e)
        {
            frm_change_password frm = new frm_change_password();
            frm.ShowDialog();
        }

        private void tsp_switch_user_Click(object sender, EventArgs e)
        {
            ClsUserClient.SetUserLogout();
        }

        private void tsm_add_module_role_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_module_to_role(), this);
        }

        private void tsm_role_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_role(), this);
        }

        private void tsb_refresh_Click(object sender, EventArgs e)
        {
            ClsFun.RefreshEntity();
        }

        private void tsm_module_list_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_module(), this);
        }

        private void tsm_account_list_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_user(), this);
        }

        public void ShowLoginInfo()
        {
            tsl_app_name.Text = ClsConfig.glo_app_name;
            tsl_server_name.Text = (ClsConfig.is_real_database == true ? "ទីស្នាក់ការកណ្តាលរាជធានីភ្នំពេញ" : "ប្រព័ន្ធសាកល្បង");
            tsl_user_name.Text = "ឈ្មោះអ្នកប្រើប្រាស់៖ " + ClsConfig.glo_local_db.vie_employee.Where(em => em.emp_id == ClsConfig.glo_local_db.tbl_user.Where(u => u.use_id == ClsConfig.glo_use_id).FirstOrDefault().emp_id).FirstOrDefault().name_kh;
            var user_role = (from ur in ClsConfig.glo_local_db.tbl_user_role join r in ClsConfig.glo_local_db.tbl_role on ur.rol_id equals r.rol_id where ur.use_id == ClsConfig.glo_use_id select r.rol_name);
            tsl_role_name.Text = "ក្រុម៖ " + (user_role.Count() > 0? user_role.FirstOrDefault().ToString():"មិនទាន់កំណត់ក្រុម");
            tsl_login_date.Text = "ម៉ោងចូលប្រើប្រាស់៖ " + ClsConfig.glo_local_db.tbl_audit_log.Where(l => l.log_id == ClsConfig.glo_log_id).FirstOrDefault().log_date_in.Value.ToString("dd-MMM-yyyy HH:mm:ss");
        }

        private void tsb_home_Click(object sender, EventArgs e)
        {
            ClsSetting.CloseAllTabs();
        }

        private void tsm_student_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_student(), this);
        }

        private void tsm_certificate_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_certificate_skill(), this);
        }

        private void tsm_exam_date_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_exam(), this);
        }

        private void tsm_verify_fingerprint_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_verify_fingerprint(), this);
        }

        private void tsm_rpt_student_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_rpt_student(), this);
        }

        private void tsm_certificate_Click_1(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_certificate(), this);
        }

        private void tsm_skill_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_skill(), this);
        }

        private void tsm_subject_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_subject(), this);
        }

        private void tsm_set_quiz_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_quiz(), this);
        }

        private void tsm_typing_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_typing(), this);
        }

        private void tsm_import_stu_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_student_import(), this);
        }

        private void tsm_emp_import_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_emp_import(), this);
        }

        private void tsm_question_quiz_Click(object sender, EventArgs e)
        {
            //frm_question_quiz frm = new frm_question_quiz();
            //frm.ShowDialog();
        }

        private void tsm_certificate_all_subject_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_subject(), this);
        }

        private void tsm_certificate_group_subject_with_skill_Click(object sender, EventArgs e)
        {
            frm_subject_edit_ frm = new frm_subject_edit_();
            frm.ShowDialog();
            //frm_subject_Load(null, null);
        }

        private void ts_quiz_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_quiz(), this);
        }

        private void tsm_section_in_subject_Click(object sender, EventArgs e)
        {
            frm_section_in_subject frm = new frm_section_in_subject();
            frm.ShowDialog();
        }

        private void ts_quiz_mcq_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_quiz(), this);
        }

        private void ts_quiz_eq_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_main_eq_test(), this);
        }

        private void tsm_question_eq_test_Click(object sender, EventArgs e)
        {
            frm_eq_exam frm = new frm_eq_exam();
            frm.ShowDialog();
        }

        private void ts_quiz_iq_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_iq_testing(), this);
        }
    }
}
