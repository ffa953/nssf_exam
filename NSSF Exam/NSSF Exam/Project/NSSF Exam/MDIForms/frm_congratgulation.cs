﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam
{
    public partial class frm_congratgulation : Form
    {
        string str = "";
        public frm_congratgulation()
        {
            InitializeComponent();
        }

        public frm_congratgulation(string str)
        {
            this.str = str;
            InitializeComponent();
        }
        int count = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
           
            count++;
            if (count == 7)
            {
                Environment.Exit(1);
            }
        }

        private void frm_congratgulation_Load(object sender, EventArgs e)
        {
            label2.Text = str;
        }
    }
}
