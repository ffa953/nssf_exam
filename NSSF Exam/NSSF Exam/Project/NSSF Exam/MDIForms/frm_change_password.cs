﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam.MDIForms
{
    public partial class frm_change_password : Form
    {
        public frm_change_password()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "ផ្លាស់ប្តូរលេខសំងាត់");
        }

        private void frm_change_password_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_close);
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFieldValidate())
                {
                    if(IsValidData()){
                        var result = ClsConfig.glo_local_db.tbl_user.Where(u => u.use_id == ClsConfig.glo_use_id && u.use_password == txt_old_passwrod.Text);
                        if (result.Count() > 0)
                        {
                            result.FirstOrDefault().use_password = txt_new_password.Text;
                            ClsConfig.glo_local_db.SaveChanges();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            this.Close();
                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool IsFieldValidate()
        {
            string msgString = "";

            if (txt_old_passwrod.Text == "") {
                msgString += "សូមបញ្ចូលលេខសំងាត់ចាស់ជាមុនសិន!\n";
            }

            if (txt_new_password.Text == "") {
                msgString += "សូមបញ្ចូលលេខសំងាត់ថ្មីជាមុនសិន!\n";
            }

            if (txt_confirm_password.Text == "") {
                msgString += "សូមបញ្ចូលលេខសំងាត់ថ្មីម្តងទៀត!\n";
            }
            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool IsValidData() {
            string msgString = "";
            if (txt_new_password.Text != txt_confirm_password.Text)
            {
                msgString += "លេខសំងាត់​ថ្មី និងលេខសំងាត់​ដែលបាន​បញ្ជាក់មិនដូចគ្នាទេ";
            }
           
            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
