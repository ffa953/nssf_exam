﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
namespace NSSF_Exam.MDIForms
{
    public partial class frm_message : Form
    {
        public string msgStr;
        public Bitmap opt;
        public bool yes;
        public bool result;
        public frm_message()
        {
            InitializeComponent();
            ClsSetting.sty_form_message(this, "Message");
        }

        private void frm_message_Load(object sender, EventArgs e)
        {
            
            ClsSetting.sty_btn_no(btn_no);
            ClsSetting.sty_btn_submit(btn_yes);
            pb_icon.Image = opt;
            txt_msg.Text = msgStr;
            if (!yes) btn_no.Hide();

        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            result = true;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            result = false;
            this.Close();
        }
    }
}
