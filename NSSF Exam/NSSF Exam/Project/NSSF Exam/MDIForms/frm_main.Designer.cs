﻿namespace NSSF_Exam.MDIForms
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsm_file = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_refresh_db = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_change_password = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_switch_user = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_logout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_setting = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_exam_date = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_student = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_certificate_subject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_all_subject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_subject_with_skill = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_section_in_subject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_certificate_skill = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_skill = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_certificate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_import_stu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_emp_import = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_topic = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_quiz = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_quiz_eq = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_quiz_iq = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_quiz_mcq = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_typing = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_activity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_verify_fingerprint = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_report = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_rpt_student = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_user_management = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_account_list = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_module_list = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_role_list = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_add_module_role = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_exam = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_question_quiz = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_question_eq_test = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_typing_quiz = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_home = new System.Windows.Forms.ToolStripButton();
            this.tsp_switch_user = new System.Windows.Forms.ToolStripButton();
            this.tsb_refresh = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsl_app_name = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_server_name = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_user_name = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_role_name = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsl_login_date = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_file,
            this.tsm_setting,
            this.tsm_activity,
            this.tsm_report,
            this.tsm_user_management,
            this.tsm_exam});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(956, 37);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsm_file
            // 
            this.tsm_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_refresh_db,
            this.tsm_change_password,
            this.tsm_switch_user,
            this.tsm_logout});
            this.tsm_file.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.tsm_file.Name = "tsm_file";
            this.tsm_file.Size = new System.Drawing.Size(57, 29);
            this.tsm_file.Text = "ហ្វាល់";
            // 
            // tsm_refresh_db
            // 
            this.tsm_refresh_db.Name = "tsm_refresh_db";
            this.tsm_refresh_db.Size = new System.Drawing.Size(210, 30);
            this.tsm_refresh_db.Text = "ធ្វើបច្ចុប្បន្នភាពទិន្នន័យ";
            // 
            // tsm_change_password
            // 
            this.tsm_change_password.Name = "tsm_change_password";
            this.tsm_change_password.Size = new System.Drawing.Size(210, 30);
            this.tsm_change_password.Text = "ផ្លាស់ប្តូរលេខសំងាត់";
            this.tsm_change_password.Click += new System.EventHandler(this.tsm_change_password_Click);
            // 
            // tsm_switch_user
            // 
            this.tsm_switch_user.Name = "tsm_switch_user";
            this.tsm_switch_user.Size = new System.Drawing.Size(210, 30);
            this.tsm_switch_user.Text = "ប្តូរអ្នកប្រើប្រាស់";
            this.tsm_switch_user.Click += new System.EventHandler(this.tsm_switch_user_Click);
            // 
            // tsm_logout
            // 
            this.tsm_logout.Name = "tsm_logout";
            this.tsm_logout.Size = new System.Drawing.Size(210, 30);
            this.tsm_logout.Text = "ចាកចេញ";
            this.tsm_logout.Click += new System.EventHandler(this.tsm_logout_Click);
            // 
            // tsm_setting
            // 
            this.tsm_setting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_exam_date,
            this.tsm_student,
            this.tsm_certificate_subject,
            this.tsm_certificate_skill,
            this.tsm_skill,
            this.tsm_certificate,
            this.tsm_import_stu,
            this.tsm_emp_import,
            this.tsm_topic});
            this.tsm_setting.Name = "tsm_setting";
            this.tsm_setting.Size = new System.Drawing.Size(63, 29);
            this.tsm_setting.Text = "កំណត់";
            // 
            // tsm_exam_date
            // 
            this.tsm_exam_date.Name = "tsm_exam_date";
            this.tsm_exam_date.Size = new System.Drawing.Size(208, 30);
            this.tsm_exam_date.Text = "សម័យប្រឡង";
            this.tsm_exam_date.Click += new System.EventHandler(this.tsm_exam_date_Click);
            // 
            // tsm_student
            // 
            this.tsm_student.Name = "tsm_student";
            this.tsm_student.Size = new System.Drawing.Size(208, 30);
            this.tsm_student.Text = "ព័ត៌មានបេក្ខជន";
            this.tsm_student.Click += new System.EventHandler(this.tsm_student_Click);
            // 
            // tsm_certificate_subject
            // 
            this.tsm_certificate_subject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_all_subject,
            this.tsm_subject_with_skill,
            this.tsm_section_in_subject});
            this.tsm_certificate_subject.Name = "tsm_certificate_subject";
            this.tsm_certificate_subject.Size = new System.Drawing.Size(208, 30);
            this.tsm_certificate_subject.Text = "ព័ត៌មានមុខវិជ្ជា";
            // 
            // tsm_all_subject
            // 
            this.tsm_all_subject.Name = "tsm_all_subject";
            this.tsm_all_subject.Size = new System.Drawing.Size(170, 30);
            this.tsm_all_subject.Text = "មុខវិជ្ជាទាំងអស់";
            this.tsm_all_subject.Click += new System.EventHandler(this.tsm_certificate_all_subject_Click);
            // 
            // tsm_subject_with_skill
            // 
            this.tsm_subject_with_skill.Name = "tsm_subject_with_skill";
            this.tsm_subject_with_skill.Size = new System.Drawing.Size(170, 30);
            this.tsm_subject_with_skill.Text = "កំណត់មុខវិជ្ជា";
            this.tsm_subject_with_skill.Click += new System.EventHandler(this.tsm_certificate_group_subject_with_skill_Click);
            // 
            // tsm_section_in_subject
            // 
            this.tsm_section_in_subject.Name = "tsm_section_in_subject";
            this.tsm_section_in_subject.Size = new System.Drawing.Size(170, 30);
            this.tsm_section_in_subject.Text = "ផ្នែកក្នុងមុខវិជ្ជា";
            this.tsm_section_in_subject.Click += new System.EventHandler(this.tsm_section_in_subject_Click);
            // 
            // tsm_certificate_skill
            // 
            this.tsm_certificate_skill.Name = "tsm_certificate_skill";
            this.tsm_certificate_skill.Size = new System.Drawing.Size(208, 30);
            this.tsm_certificate_skill.Text = "ព័ត៌មានជំនាញ";
            this.tsm_certificate_skill.Click += new System.EventHandler(this.tsm_certificate_Click);
            // 
            // tsm_skill
            // 
            this.tsm_skill.Name = "tsm_skill";
            this.tsm_skill.Size = new System.Drawing.Size(208, 30);
            this.tsm_skill.Text = "ពត៍មានផ្នែក";
            this.tsm_skill.Click += new System.EventHandler(this.tsm_skill_Click);
            // 
            // tsm_certificate
            // 
            this.tsm_certificate.Name = "tsm_certificate";
            this.tsm_certificate.Size = new System.Drawing.Size(208, 30);
            this.tsm_certificate.Text = "ពត៍មានសញ្ញាប័ត្រ";
            this.tsm_certificate.Click += new System.EventHandler(this.tsm_certificate_Click_1);
            // 
            // tsm_import_stu
            // 
            this.tsm_import_stu.Name = "tsm_import_stu";
            this.tsm_import_stu.Size = new System.Drawing.Size(208, 30);
            this.tsm_import_stu.Text = "ទាញយកបេក្ខជនចាស់";
            this.tsm_import_stu.Click += new System.EventHandler(this.tsm_import_stu_Click);
            // 
            // tsm_emp_import
            // 
            this.tsm_emp_import.Name = "tsm_emp_import";
            this.tsm_emp_import.Size = new System.Drawing.Size(208, 30);
            this.tsm_emp_import.Text = "ទាញយកបុគ្គលិក";
            this.tsm_emp_import.Click += new System.EventHandler(this.tsm_emp_import_Click);
            // 
            // tsm_topic
            // 
            this.tsm_topic.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ts_quiz,
            this.tsm_typing});
            this.tsm_topic.Name = "tsm_topic";
            this.tsm_topic.Size = new System.Drawing.Size(208, 30);
            this.tsm_topic.Text = "វិញ្ញាសារ";
            // 
            // ts_quiz
            // 
            this.ts_quiz.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ts_quiz_eq,
            this.ts_quiz_iq,
            this.ts_quiz_mcq});
            this.ts_quiz.Name = "ts_quiz";
            this.ts_quiz.Size = new System.Drawing.Size(193, 30);
            this.ts_quiz.Text = "វិញ្ញាសារទូរទៅ";
            // 
            // ts_quiz_eq
            // 
            this.ts_quiz_eq.Name = "ts_quiz_eq";
            this.ts_quiz_eq.Size = new System.Drawing.Size(290, 30);
            this.ts_quiz_eq.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (EQ)";
            this.ts_quiz_eq.Click += new System.EventHandler(this.ts_quiz_eq_Click);
            // 
            // ts_quiz_iq
            // 
            this.ts_quiz_iq.Name = "ts_quiz_iq";
            this.ts_quiz_iq.Size = new System.Drawing.Size(290, 30);
            this.ts_quiz_iq.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (IQ)";
            this.ts_quiz_iq.Click += new System.EventHandler(this.ts_quiz_iq_Click);
            // 
            // ts_quiz_mcq
            // 
            this.ts_quiz_mcq.Name = "ts_quiz_mcq";
            this.ts_quiz_mcq.Size = new System.Drawing.Size(290, 30);
            this.ts_quiz_mcq.Text = "សំនួរជ្រើសរើស (MCQ)";
            this.ts_quiz_mcq.Click += new System.EventHandler(this.ts_quiz_mcq_Click);
            // 
            // tsm_typing
            // 
            this.tsm_typing.Name = "tsm_typing";
            this.tsm_typing.Size = new System.Drawing.Size(193, 30);
            this.tsm_typing.Text = "វិញ្ញាសារវាយអត្ថបទ";
            this.tsm_typing.Click += new System.EventHandler(this.tsm_typing_Click);
            // 
            // tsm_activity
            // 
            this.tsm_activity.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_verify_fingerprint});
            this.tsm_activity.Name = "tsm_activity";
            this.tsm_activity.Size = new System.Drawing.Size(85, 29);
            this.tsm_activity.Text = "ព្រឹត្តិការណ៍";
            // 
            // tsm_verify_fingerprint
            // 
            this.tsm_verify_fingerprint.Name = "tsm_verify_fingerprint";
            this.tsm_verify_fingerprint.Size = new System.Drawing.Size(207, 30);
            this.tsm_verify_fingerprint.Text = "ផ្ទៀងផ្ទាត់ស្នាមម្រាមដៃ";
            this.tsm_verify_fingerprint.Click += new System.EventHandler(this.tsm_verify_fingerprint_Click);
            // 
            // tsm_report
            // 
            this.tsm_report.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_rpt_student});
            this.tsm_report.Name = "tsm_report";
            this.tsm_report.Size = new System.Drawing.Size(93, 29);
            this.tsm_report.Text = "របាយការណ៍";
            // 
            // tsm_rpt_student
            // 
            this.tsm_rpt_student.Name = "tsm_rpt_student";
            this.tsm_rpt_student.Size = new System.Drawing.Size(289, 30);
            this.tsm_rpt_student.Text = "របាយការណ៍ស្តីពីការចុះឈ្មោះបេក្ខជន";
            this.tsm_rpt_student.Click += new System.EventHandler(this.tsm_rpt_student_Click);
            // 
            // tsm_user_management
            // 
            this.tsm_user_management.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_account_list,
            this.tsm_module_list,
            this.tsm_role_list,
            this.tsm_add_module_role});
            this.tsm_user_management.Name = "tsm_user_management";
            this.tsm_user_management.Size = new System.Drawing.Size(174, 29);
            this.tsm_user_management.Text = "គ្រប់គ្រងការប្រើប្រាស់ប្រព័ន្ធ";
            // 
            // tsm_account_list
            // 
            this.tsm_account_list.Name = "tsm_account_list";
            this.tsm_account_list.Size = new System.Drawing.Size(247, 30);
            this.tsm_account_list.Text = "គណនីប្រើប្រាស់ប្រព័ន្ធ";
            this.tsm_account_list.Click += new System.EventHandler(this.tsm_account_list_Click);
            // 
            // tsm_module_list
            // 
            this.tsm_module_list.Name = "tsm_module_list";
            this.tsm_module_list.Size = new System.Drawing.Size(247, 30);
            this.tsm_module_list.Text = "កំណត់ការងារ";
            this.tsm_module_list.Click += new System.EventHandler(this.tsm_module_list_Click);
            // 
            // tsm_role_list
            // 
            this.tsm_role_list.Name = "tsm_role_list";
            this.tsm_role_list.Size = new System.Drawing.Size(247, 30);
            this.tsm_role_list.Text = "ក្រុមអ្នកប្រើប្រាស់";
            this.tsm_role_list.Click += new System.EventHandler(this.tsm_role_Click);
            // 
            // tsm_add_module_role
            // 
            this.tsm_add_module_role.Name = "tsm_add_module_role";
            this.tsm_add_module_role.Size = new System.Drawing.Size(247, 30);
            this.tsm_add_module_role.Text = "កំណត់ការងារឲ្យក្រុមប្រើប្រាស់";
            this.tsm_add_module_role.Click += new System.EventHandler(this.tsm_add_module_role_Click);
            // 
            // tsm_exam
            // 
            this.tsm_exam.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_question_quiz,
            this.tsm_question_eq_test,
            this.tsm_typing_quiz});
            this.tsm_exam.Name = "tsm_exam";
            this.tsm_exam.Size = new System.Drawing.Size(80, 29);
            this.tsm_exam.Text = "ការប្រឡង";
            // 
            // tsm_question_quiz
            // 
            this.tsm_question_quiz.Name = "tsm_question_quiz";
            this.tsm_question_quiz.Size = new System.Drawing.Size(290, 30);
            this.tsm_question_quiz.Text = "សំនួរជ្រើសរើស";
            this.tsm_question_quiz.Click += new System.EventHandler(this.tsm_question_quiz_Click);
            // 
            // tsm_question_eq_test
            // 
            this.tsm_question_eq_test.Name = "tsm_question_eq_test";
            this.tsm_question_eq_test.Size = new System.Drawing.Size(290, 30);
            this.tsm_question_eq_test.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (EQ)";
            this.tsm_question_eq_test.Click += new System.EventHandler(this.tsm_question_eq_test_Click);
            // 
            // tsm_typing_quiz
            // 
            this.tsm_typing_quiz.Name = "tsm_typing_quiz";
            this.tsm_typing_quiz.Size = new System.Drawing.Size(290, 30);
            this.tsm_typing_quiz.Text = "វាយអត្ថបទ";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_home,
            this.tsp_switch_user,
            this.tsb_refresh});
            this.toolStrip1.Location = new System.Drawing.Point(0, 37);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(956, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_home
            // 
            this.tsb_home.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsb_home.Image = global::NSSF_Exam.Properties.Resources.btn_home;
            this.tsb_home.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_home.Name = "tsb_home";
            this.tsb_home.Size = new System.Drawing.Size(23, 22);
            this.tsb_home.Text = "Home";
            this.tsb_home.Click += new System.EventHandler(this.tsb_home_Click);
            // 
            // tsp_switch_user
            // 
            this.tsp_switch_user.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsp_switch_user.Image = global::NSSF_Exam.Properties.Resources.btn_user_color;
            this.tsp_switch_user.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsp_switch_user.Name = "tsp_switch_user";
            this.tsp_switch_user.Size = new System.Drawing.Size(23, 22);
            this.tsp_switch_user.Text = "Switch User";
            this.tsp_switch_user.Click += new System.EventHandler(this.tsp_switch_user_Click);
            // 
            // tsb_refresh
            // 
            this.tsb_refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsb_refresh.Image = global::NSSF_Exam.Properties.Resources.btn_refresh;
            this.tsb_refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_refresh.Name = "tsb_refresh";
            this.tsb_refresh.Size = new System.Drawing.Size(23, 22);
            this.tsb_refresh.Text = "Refresh";
            this.tsb_refresh.Click += new System.EventHandler(this.tsb_refresh_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(956, 30);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Khmer OS Battambang", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsl_app_name,
            this.toolStripStatusLabel1,
            this.tsl_server_name,
            this.toolStripStatusLabel3,
            this.tsl_user_name,
            this.toolStripStatusLabel2,
            this.tsl_role_name,
            this.toolStripStatusLabel4,
            this.tsl_login_date});
            this.statusStrip1.Location = new System.Drawing.Point(0, 435);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(956, 27);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsl_app_name
            // 
            this.tsl_app_name.Name = "tsl_app_name";
            this.tsl_app_name.Size = new System.Drawing.Size(103, 22);
            this.tsl_app_name.Text = "Application Name";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(13, 22);
            this.toolStripStatusLabel1.Text = "|";
            // 
            // tsl_server_name
            // 
            this.tsl_server_name.Name = "tsl_server_name";
            this.tsl_server_name.Size = new System.Drawing.Size(83, 22);
            this.tsl_server_name.Text = "Server Name";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(13, 22);
            this.toolStripStatusLabel3.Text = "|";
            // 
            // tsl_user_name
            // 
            this.tsl_user_name.Name = "tsl_user_name";
            this.tsl_user_name.Size = new System.Drawing.Size(68, 22);
            this.tsl_user_name.Text = "User Login";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(13, 22);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // tsl_role_name
            // 
            this.tsl_role_name.Font = new System.Drawing.Font("Khmer OS Battambang", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsl_role_name.Name = "tsl_role_name";
            this.tsl_role_name.Size = new System.Drawing.Size(71, 22);
            this.tsl_role_name.Text = "Role Name";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(13, 22);
            this.toolStripStatusLabel4.Text = "|";
            // 
            // tsl_login_date
            // 
            this.tsl_login_date.Name = "tsl_login_date";
            this.tsl_login_date.Size = new System.Drawing.Size(68, 22);
            this.tsl_login_date.Text = "Login Date";
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 462);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ប្រព័ន្ធគ្រប់គ្រងរបបថែទាំសុខភាព";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_main_FormClosing);
            this.Load += new System.EventHandler(this.frm_main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsm_file;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem tsm_user_management;
        private System.Windows.Forms.ToolStripMenuItem tsm_setting;
        private System.Windows.Forms.ToolStripButton tsb_refresh;
        private System.Windows.Forms.ToolStripButton tsb_home;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsl_app_name;
        private System.Windows.Forms.ToolStripMenuItem tsm_refresh_db;
        private System.Windows.Forms.ToolStripMenuItem tsm_change_password;
        private System.Windows.Forms.ToolStripMenuItem tsm_switch_user;
        private System.Windows.Forms.ToolStripMenuItem tsm_logout;
        private System.Windows.Forms.ToolStripButton tsp_switch_user;
        private System.Windows.Forms.ToolStripMenuItem tsm_add_module_role;
        private System.Windows.Forms.ToolStripMenuItem tsm_role_list;
        private System.Windows.Forms.ToolStripMenuItem tsm_account_list;
        private System.Windows.Forms.ToolStripMenuItem tsm_module_list;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsl_user_name;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tsl_role_name;
        private System.Windows.Forms.ToolStripStatusLabel tsl_server_name;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tsl_login_date;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripMenuItem tsm_student;
        private System.Windows.Forms.ToolStripMenuItem tsm_exam_date;
        private System.Windows.Forms.ToolStripMenuItem tsm_activity;
        private System.Windows.Forms.ToolStripMenuItem tsm_verify_fingerprint;
        private System.Windows.Forms.ToolStripMenuItem tsm_report;
        private System.Windows.Forms.ToolStripMenuItem tsm_rpt_student;
        private System.Windows.Forms.ToolStripMenuItem tsm_certificate;
        private System.Windows.Forms.ToolStripMenuItem tsm_skill;
        private System.Windows.Forms.ToolStripMenuItem tsm_topic;
        private System.Windows.Forms.ToolStripMenuItem ts_quiz;
        private System.Windows.Forms.ToolStripMenuItem tsm_typing;
        private System.Windows.Forms.ToolStripMenuItem tsm_import_stu;
        private System.Windows.Forms.ToolStripMenuItem tsm_emp_import;
        private System.Windows.Forms.ToolStripMenuItem tsm_exam;
        private System.Windows.Forms.ToolStripMenuItem tsm_question_quiz;
        private System.Windows.Forms.ToolStripMenuItem tsm_typing_quiz;
        private System.Windows.Forms.ToolStripMenuItem tsm_certificate_skill;
        private System.Windows.Forms.ToolStripMenuItem tsm_certificate_subject;
        private System.Windows.Forms.ToolStripMenuItem tsm_all_subject;
        private System.Windows.Forms.ToolStripMenuItem tsm_subject_with_skill;
        private System.Windows.Forms.ToolStripMenuItem tsm_section_in_subject;
        private System.Windows.Forms.ToolStripMenuItem ts_quiz_eq;
        private System.Windows.Forms.ToolStripMenuItem ts_quiz_iq;
        private System.Windows.Forms.ToolStripMenuItem ts_quiz_mcq;
        private System.Windows.Forms.ToolStripMenuItem tsm_question_eq_test;
    }
}

