﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Quiz;
using NSSF_Exam.MDIForms;

namespace NSSF_Exam.Quiz
{
    public partial class frm_exam_login : Form
    {


        public frm_exam_login()
        {
            InitializeComponent();
        }
        string id = "";


        private void frm_exam_login_Load(object sender, EventArgs e)
        {
            //ClsSetting.sty_btn_search_stu_exam(btn_search);
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txt_id.Text.Trim()))
            {
                string str_uid = txt_id.Text.Trim().ToLower();
                var qury = ClsConfig.glo_local_db.UserProfiles.Where(u => u.UserName.ToLower().Equals(str_uid)).SingleOrDefault();
                if (qury != null)
                {
                    ClsConfig.glo_stu_id = qury.UserId;
                    id = qury.UserName;
                    lbl_name.Text = qury.Name_khmer;
                    lbl_gen.Text = qury.GenderId == 0 ? "ស្រី" : "ប្រុស";
                    lbl_dob.Text = Convert.ToDateTime(qury.DateOfBirth).ToString("dd/MMM/yyyy");
                    pic_tick.Visible = true;
                    lbl_exam.Visible = true;
                }
                else
                {
                    ClsMsg.Warning("- លេខសម្គាល់របស់បេក្ខជននេះមិនមានក្នុងប្រព័ន្ធទេ\nសូមបំពេញលេខសម្គាល់ម្តងទៀតអោយបានត្រឹមត្រូវ!");
                    lbl_name.Text = lbl_gen.Text = lbl_dob.Text = "";
                    pic_tick.Visible = false;
                    lbl_exam.Visible = false;
                }
            }
            else { ClsMsg.Warning("- សូមបំពេញលេខសម្គាល់របស់បេក្ខជនប្រឡង!"); txt_id.Focus(); }
        }

        private void txt_id_KeyDown(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt_id.Text.Trim())) 
            {
                if (e.KeyCode == Keys.Enter) { btn_search.PerformClick();}
            }
        }

        private void frm_exam_login_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Environment.Exit(1);
        }

        private void txt_id_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void lbl_exam_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frm_test_menu frm = new frm_test_menu();
            frm.lbl_id.Text = this.id;
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }
    }
}
