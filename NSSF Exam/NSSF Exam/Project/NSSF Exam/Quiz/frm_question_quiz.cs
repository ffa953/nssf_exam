﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using System.Diagnostics;
using System.Threading;

namespace NSSF_Exam.Quiz
{
    public partial class frm_question_quiz : Form
    {
        public frm_question_quiz(string stu_id)
        {
            InitializeComponent();
            var query = ClsConfig.glo_local_db.fun_filter_studentprofile_by_id(stu_id).Single();
            lbl_id.Text = query.UserName;
            lbl_name.Text = query.Name_khmer;
            lbl_table_no.Text = query.skill_note;
            lbl_dob.Text = Convert.ToDateTime(query.DateOfBirth).ToString("dd/MM/yyyy");
            lbl_subject.Text = query.skill;
            lbl_skill.Text = query.cert_skill_name;
        }
        BindingSource bds_quiz = new BindingSource();
        Stopwatch timer = new Stopwatch();
        RadioButton rad_option;

        private void Select_Quiz(int subject_id) 
        {
            var query = ClsConfig.glo_local_db.fun_filter_question_by_subject(subject_id).Select(quiz => new {quiz.quiz_id,quiz.quiz_title}).ToList();
            bds_quiz.DataSource = query;
            lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_quiz.Position + 1).ToString());
        }

        private void Select_Question_Option(int quiz_id)
        {
            var query = ClsConfig.glo_local_db.fun_filter_answer_by_question(quiz_id).ToList();
            container.Controls.Clear();
            foreach (var item in query)
            {
                rad_option= new RadioButton();
                rad_option.Text = item.ans_title;
                rad_option.Tag = item.ans_id.ToString()+" "+item.ans_choice;
                rad_option.AutoSize = true;
                rad_option.Margin = new Padding(0, 0, 0, 15);
                //rad_option.Size = new Size(900,38);
                rad_option.Cursor = Cursors.Hand;
                rad_option.MouseEnter +=new EventHandler(rad_option_MouseEnter);
                rad_option.MouseLeave += new EventHandler(rad_option_MouseLeave);
                rad_option.Click += new EventHandler(rad_option_Click);
                container.Controls.Add(rad_option);
            }
        }

        void rad_option_MouseLeave(object sender, EventArgs e)
        {
            var val_check = (RadioButton)sender;
            //val_check.Font = new Font("Khmer OS Content", 10, FontStyle.Regular);
            val_check.ForeColor = Color.Black;
        }

        void rad_option_MouseEnter(object sender, EventArgs e)
        {
            var val_check = (RadioButton)sender;
            val_check.ForeColor = Color.Navy;
            //val_check.Font = new Font("Khmer OS Content", 10, FontStyle.Bold);
        }

        void rad_option_Click(object sender, EventArgs e)
        {
            var val_check = (RadioButton)sender;
            lbl.Text = val_check.Tag.ToString();
        }

        private void frm_question_quiz_Load(object sender, EventArgs e)
        {
            var query = (from user in ClsConfig.glo_local_db.UserProfiles
                         join skill in ClsConfig.glo_local_db.tbl_skill on user.skill_id equals skill.skill_id
                         join sub in ClsConfig.glo_local_db.tbl_subject on user.subject_no equals sub.sub_id
                         where skill.skill_id != null && user.subject_no != null && user.UserName.Equals(lbl_id.Text)
                         select new {user.UserName}).SingleOrDefault();

            Select_Quiz(1);
            lbl_question.DataBindings.Add(new Binding("Text",bds_quiz,"quiz_title"));           
            lbl_question_id.DataBindings.Add(new Binding("Text", bds_quiz, "quiz_id"));         
            ClsSetting.sty_btn_next(btn_next);                              
            ClsSetting.sty_btn_previous(btn_previous);                                          
            Select_Question_Option(Convert.ToInt32(lbl_question_id.Text));     
            quiz_timer.Start();                 
            timer.Start();                      

        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            bds_quiz.MoveNext();
            lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_quiz.Position + 1).ToString());
            Select_Question_Option(Convert.ToInt32(lbl_question_id.Text));
        }

        private void btn_previous_Click(object sender, EventArgs e)
        {
            bds_quiz.MovePrevious();
            lbl_question_pos.Text = ClsSetting.toKhmerNum((bds_quiz.Position + 1).ToString());
            Select_Question_Option(Convert.ToInt32(lbl_question_id.Text));
        }

        private void quiz_timer_Tick(object sender, EventArgs e)
        {
            TimeSpan elapsed = this.timer.Elapsed;
            lbl_timer.Text = string.Format("{0:00}:{1:00}:{2:00}", Math.Floor(elapsed.TotalHours), elapsed.Minutes, elapsed.Seconds);
        }

        private void frm_question_quiz_FormClosed(object sender, FormClosedEventArgs e)
        {
            quiz_timer.Stop();
            timer.Stop();
            frm_test_menu frm = new frm_test_menu();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }
    }
}
