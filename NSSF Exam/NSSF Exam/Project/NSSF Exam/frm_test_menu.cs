﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Forms.Exam.EQ_Test;
using NSSF_Exam.Quiz;
using NSSF_Exam.MDIForms;
using NSSF_Exam.Forms.Exam.IQ_Test;
using NSSF_Exam.Classes.Config;

namespace NSSF_Exam
{
    public partial class frm_test_menu : Form
    {
        public frm_test_menu()
        {
            InitializeComponent();
        }

        private void btn_eq_Click(object sender, EventArgs e)
        {
            frm_eq_exam frm = new frm_eq_exam();
            frm.Text = "សំនួរសម្រាប់ការតេស្តភាពវៃឆ្លាត (EQ) សម្រាប់បេក្ខជនលេខសម្គាល់ [" + lbl_id.Text+"]";
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btn_mcq_Click(object sender, EventArgs e)
        {
            frm_question_quiz frm = new frm_question_quiz(lbl_id.Text);
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void frm_test_menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btn_iq_Click(object sender, EventArgs e)
        {
            frm_iq_exam frm = new frm_iq_exam();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void frm_test_menu_Load(object sender, EventArgs e)
        {
            lbl_id.Text = (from stu in ClsConfig.glo_local_db.UserProfiles where stu.UserId == ClsConfig.glo_stu_id select stu.UserName).SingleOrDefault();
        }
    }
}
