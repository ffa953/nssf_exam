﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using Microsoft.Reporting.WinForms;

namespace NSSF_Exam.Reports
{
    public partial class frm_rpt_student : Form
    {
        int stu_count;
        int stu_male_count;
        int stu_female_count;
        public frm_rpt_student()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "របាយ៍ការណ៍បេក្ខជន");
        }

        private void frm_view_stu_Load(object sender, EventArgs e)
        {

            ClsSetting.sty_btn_search(btn_print);
            ClsSetting.sty_btn_close(btn_close);
            lb_stu_num.Visible = false;
            lb_stu_type.Visible = false;
            lb_female.Visible = false;
            lb_female_num.Visible = false;
            lb_male.Visible = false;
            lb_male_num.Visible = false;

            cbo_user.DataSource = (from emp in ClsConfig.glo_local_db.vie_employee join us in ClsConfig.glo_local_db.tbl_user on emp.emp_id equals us.emp_id where us.pri_id !=1 select new {us.use_id,emp.name_kh }).ToList();
            cbo_user.DisplayMember = "name_kh";
            cbo_user.ValueMember = "use_id";
            cbo_user.SelectedIndex = -1;

            cbo_skill.DataSource= (from sk in ClsConfig.glo_local_db.tbl_skill select sk).ToList();
            cbo_skill.DisplayMember = "skill";
            cbo_skill.ValueMember = "skill_id";
            cbo_skill.SelectedIndex = -1;

            cbo_stu_id.DataSource=(from st in ClsConfig.glo_local_db.tbl_student_type select st).ToList();
            cbo_stu_id.DisplayMember = "stu_type_name";
            cbo_stu_id.ValueMember = "stu_type_id";
            cbo_stu_id.SelectedIndex = -1;

            cbo_exam_id.DataSource = ClsConfig.glo_local_db.tbl_exam_date.OrderByDescending(m=>m.exam_id).ToList();
            cbo_exam_id.DisplayMember = "exam_date";
            cbo_exam_id.ValueMember = "exam_id";
                       
        }
        private void DisplayData(DateTime start,DateTime end ,int user,int skill,int stu_type)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
                reportDataSource1.Name = "DataSet_stu";

                if (chk_all.Checked)
                {
                    var data = ClsConfig.glo_local_db.fun_get_stuDate(2,Convert.ToInt32(cbo_exam_id.SelectedValue)).ToList();
                    reportDataSource1.Value = data;
                }
                else
                {
                    string sql = "SELECT *  FROM fun_get_stuDate(2,"+Convert.ToInt32(cbo_exam_id.SelectedValue)+") WHERE ";
                    string con="";
                    con += "(CONVERT(date,LastActivityDate) >='" + start.Date + "' and CONVERT(date,LastActivityDate) <= '" + end.Date + "') ";

                    if (user != 0)
                    {
                        con += "AND user_id = " + user + " "; 
                    }

                    if(skill !=0)
                    {
                        con += "AND skill_id = " + skill + " "; 
                    }

                    if (stu_type != 0)
                    {
                        con += "AND stu_type_id = " + stu_type + " ";
                    }
                   
                    var data = ClsConfig.glo_local_db.Database.SqlQuery<fun_user_by_role_Result>(sql +con).ToList();               
                    reportDataSource1.Value = data;

                    stu_count = ClsConfig.glo_local_db.Database.SqlQuery<fun_user_by_role_Result>(sql + con).Count();
                    stu_male_count = ClsConfig.glo_local_db.Database.SqlQuery<fun_user_by_role_Result>(sql + con).Where(m=>m.GenderId == 1).Count();
                    stu_female_count = ClsConfig.glo_local_db.Database.SqlQuery<fun_user_by_role_Result>(sql + con).Where(m => m.GenderId == 2).Count();
                }

              
               
                rpt_view_stu.LocalReport.DataSources.Clear();
                this.rpt_view_stu.LocalReport.ReportEmbeddedResource = "NSSF_Exam.Reports.rpt_student.rdlc";

                //ReportParameter[] parameters = new ReportParameter[1];
                //parameters[0] = new ReportParameter("pf_emp_id", emp_id.ToString());
                //this.rpt_view_stu.LocalReport.SetParameters(parameters);

                this.rpt_view_stu.LocalReport.DataSources.Add(reportDataSource1);


                this.rpt_view_stu.SetDisplayMode(DisplayMode.PrintLayout);
                this.rpt_view_stu.ZoomMode = ZoomMode.Percent;
                this.rpt_view_stu.ZoomPercent = 100;
                this.rpt_view_stu.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            DisplayData(dt_start.Value,dt_end.Value,Convert.ToInt32(cbo_user.SelectedValue),Convert.ToInt32(cbo_skill.SelectedValue),Convert.ToInt32(cbo_stu_id.SelectedValue));
            this.rpt_view_stu.RefreshReport();

            if (Convert.ToInt32(cbo_stu_id.SelectedValue) == 1)
            {
                lb_stu_num.Visible = true;
                lb_stu_type.Visible = true;
                lb_female.Visible = true;
                lb_female_num.Visible = true;
                lb_male.Visible = true;
                lb_male_num.Visible = true;

                lb_stu_type.Text = "មន្រី្ត៖​ ";
                lb_stu_num.Text = stu_count.ToString() + " នាក់";

                lb_male.Text = "ប្រុស៖";
                lb_male_num.Text = stu_male_count.ToString() + " នាក់";

                lb_female.Text = "ស្រី៖";
                lb_female_num.Text = stu_female_count.ToString() + " នាក់"; 
            }
            if (Convert.ToInt32(cbo_stu_id.SelectedValue) == 2)
            {
                lb_stu_num.Visible = true;
                lb_stu_type.Visible = true;
                lb_female.Visible = true;
                lb_female_num.Visible = true;
                lb_male.Visible = true;
                lb_male_num.Visible = true;

                lb_stu_type.Text = "បុគ្គលិក៖​ ";
                lb_stu_num.Text = stu_count.ToString() + " នាក់";

                lb_male.Text = "ប្រុស៖";
                lb_male_num.Text = stu_male_count.ToString() + " នាក់";

                lb_female.Text = "ស្រី៖";
                lb_female_num.Text = stu_female_count.ToString() + " នាក់"; 
                
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_view_stu_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
