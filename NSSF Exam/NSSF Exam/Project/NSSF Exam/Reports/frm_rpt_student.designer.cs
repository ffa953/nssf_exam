﻿namespace NSSF_Exam.Reports
{
    partial class frm_rpt_student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpt_view_stu = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_female_num = new System.Windows.Forms.Label();
            this.lb_female = new System.Windows.Forms.Label();
            this.lb_male_num = new System.Windows.Forms.Label();
            this.lb_male = new System.Windows.Forms.Label();
            this.lb_stu_num = new System.Windows.Forms.Label();
            this.lb_stu_type = new System.Windows.Forms.Label();
            this.cbo_exam_id = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbo_stu_id = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbo_skill = new System.Windows.Forms.ComboBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.chk_all = new System.Windows.Forms.CheckBox();
            this.btn_print = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_user = new System.Windows.Forms.ComboBox();
            this.dt_end = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dt_start = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rpt_view_stu
            // 
            this.rpt_view_stu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpt_view_stu.Location = new System.Drawing.Point(0, 0);
            this.rpt_view_stu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rpt_view_stu.Name = "rpt_view_stu";
            this.rpt_view_stu.Size = new System.Drawing.Size(1469, 502);
            this.rpt_view_stu.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lb_female_num);
            this.panel1.Controls.Add(this.lb_female);
            this.panel1.Controls.Add(this.lb_male_num);
            this.panel1.Controls.Add(this.lb_male);
            this.panel1.Controls.Add(this.lb_stu_num);
            this.panel1.Controls.Add(this.lb_stu_type);
            this.panel1.Controls.Add(this.cbo_exam_id);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cbo_stu_id);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbo_skill);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.chk_all);
            this.panel1.Controls.Add(this.btn_print);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbo_user);
            this.panel1.Controls.Add(this.dt_end);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dt_start);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1469, 123);
            this.panel1.TabIndex = 1;
            // 
            // lb_female_num
            // 
            this.lb_female_num.AutoSize = true;
            this.lb_female_num.Location = new System.Drawing.Point(875, 73);
            this.lb_female_num.Name = "lb_female_num";
            this.lb_female_num.Size = new System.Drawing.Size(42, 24);
            this.lb_female_num.TabIndex = 20;
            this.lb_female_num.Text = "label7";
            // 
            // lb_female
            // 
            this.lb_female.AutoSize = true;
            this.lb_female.Location = new System.Drawing.Point(827, 73);
            this.lb_female.Name = "lb_female";
            this.lb_female.Size = new System.Drawing.Size(42, 24);
            this.lb_female.TabIndex = 19;
            this.lb_female.Text = "label7";
            // 
            // lb_male_num
            // 
            this.lb_male_num.AutoSize = true;
            this.lb_male_num.Location = new System.Drawing.Point(759, 73);
            this.lb_male_num.Name = "lb_male_num";
            this.lb_male_num.Size = new System.Drawing.Size(42, 24);
            this.lb_male_num.TabIndex = 18;
            this.lb_male_num.Text = "label7";
            // 
            // lb_male
            // 
            this.lb_male.AutoSize = true;
            this.lb_male.Location = new System.Drawing.Point(711, 73);
            this.lb_male.Name = "lb_male";
            this.lb_male.Size = new System.Drawing.Size(42, 24);
            this.lb_male.TabIndex = 17;
            this.lb_male.Text = "label7";
            // 
            // lb_stu_num
            // 
            this.lb_stu_num.AutoSize = true;
            this.lb_stu_num.Location = new System.Drawing.Point(632, 73);
            this.lb_stu_num.Name = "lb_stu_num";
            this.lb_stu_num.Size = new System.Drawing.Size(42, 24);
            this.lb_stu_num.TabIndex = 16;
            this.lb_stu_num.Text = "label7";
            // 
            // lb_stu_type
            // 
            this.lb_stu_type.AutoSize = true;
            this.lb_stu_type.Location = new System.Drawing.Point(572, 73);
            this.lb_stu_type.Name = "lb_stu_type";
            this.lb_stu_type.Size = new System.Drawing.Size(42, 24);
            this.lb_stu_type.TabIndex = 15;
            this.lb_stu_type.Text = "label7";
            // 
            // cbo_exam_id
            // 
            this.cbo_exam_id.DropDownHeight = 120;
            this.cbo_exam_id.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_exam_id.FormatString = "dd-MM-yyyy";
            this.cbo_exam_id.FormattingEnabled = true;
            this.cbo_exam_id.IntegralHeight = false;
            this.cbo_exam_id.Location = new System.Drawing.Point(352, 70);
            this.cbo_exam_id.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo_exam_id.Name = "cbo_exam_id";
            this.cbo_exam_id.Size = new System.Drawing.Size(189, 32);
            this.cbo_exam_id.TabIndex = 14;
            this.cbo_exam_id.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 24);
            this.label6.TabIndex = 13;
            this.label6.Text = "ប្រភេទបេក្ខជន";
            // 
            // cbo_stu_id
            // 
            this.cbo_stu_id.DropDownHeight = 120;
            this.cbo_stu_id.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_stu_id.FormattingEnabled = true;
            this.cbo_stu_id.IntegralHeight = false;
            this.cbo_stu_id.Location = new System.Drawing.Point(96, 70);
            this.cbo_stu_id.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo_stu_id.Name = "cbo_stu_id";
            this.cbo_stu_id.Size = new System.Drawing.Size(189, 32);
            this.cbo_stu_id.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(305, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 24);
            this.label5.TabIndex = 11;
            this.label5.Text = "ជំនាន់";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(827, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "ផ្នែក";
            // 
            // cbo_skill
            // 
            this.cbo_skill.DropDownHeight = 120;
            this.cbo_skill.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_skill.FormattingEnabled = true;
            this.cbo_skill.IntegralHeight = false;
            this.cbo_skill.Location = new System.Drawing.Point(865, 18);
            this.cbo_skill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo_skill.Name = "cbo_skill";
            this.cbo_skill.Size = new System.Drawing.Size(169, 32);
            this.cbo_skill.TabIndex = 9;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.Location = new System.Drawing.Point(1349, 74);
            this.btn_close.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(114, 39);
            this.btn_close.TabIndex = 8;
            this.btn_close.Text = "button1";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // chk_all
            // 
            this.chk_all.AutoSize = true;
            this.chk_all.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_all.Location = new System.Drawing.Point(1057, 22);
            this.chk_all.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chk_all.Name = "chk_all";
            this.chk_all.Size = new System.Drawing.Size(121, 28);
            this.chk_all.TabIndex = 7;
            this.chk_all.Text = "ទាញយកទាំងអស់";
            this.chk_all.UseVisualStyleBackColor = true;
            // 
            // btn_print
            // 
            this.btn_print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_print.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print.Location = new System.Drawing.Point(1228, 74);
            this.btn_print.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(114, 39);
            this.btn_print.TabIndex = 6;
            this.btn_print.Text = "button1";
            this.btn_print.UseVisualStyleBackColor = true;
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(560, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "អ្នកប្រើប្រាស់";
            // 
            // cbo_user
            // 
            this.cbo_user.DropDownHeight = 120;
            this.cbo_user.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_user.FormattingEnabled = true;
            this.cbo_user.IntegralHeight = false;
            this.cbo_user.Location = new System.Drawing.Point(641, 18);
            this.cbo_user.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo_user.Name = "cbo_user";
            this.cbo_user.Size = new System.Drawing.Size(169, 32);
            this.cbo_user.TabIndex = 4;
            // 
            // dt_end
            // 
            this.dt_end.CustomFormat = "dd-MM-yyyy";
            this.dt_end.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_end.Location = new System.Drawing.Point(352, 18);
            this.dt_end.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dt_end.Name = "dt_end";
            this.dt_end.Size = new System.Drawing.Size(189, 32);
            this.dt_end.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(301, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "ដល់ថ្ងៃ";
            // 
            // dt_start
            // 
            this.dt_start.CustomFormat = "dd-MM-yyyy";
            this.dt_start.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dt_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_start.Location = new System.Drawing.Point(96, 18);
            this.dt_start.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dt_start.Name = "dt_start";
            this.dt_start.Size = new System.Drawing.Size(189, 32);
            this.dt_start.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "ចាប់ពីថ្ងៃ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rpt_view_stu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 123);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1469, 502);
            this.panel2.TabIndex = 2;
            // 
            // frm_rpt_student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1469, 625);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frm_rpt_student";
            this.Text = "frm_view_stu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_view_stu_FormClosed);
            this.Load += new System.EventHandler(this.frm_view_stu_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpt_view_stu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_print;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_user;
        private System.Windows.Forms.DateTimePicker dt_end;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dt_start;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chk_all;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbo_skill;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbo_exam_id;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbo_stu_id;
        private System.Windows.Forms.Label lb_stu_num;
        private System.Windows.Forms.Label lb_stu_type;
        private System.Windows.Forms.Label lb_male_num;
        private System.Windows.Forms.Label lb_male;
        private System.Windows.Forms.Label lb_female_num;
        private System.Windows.Forms.Label lb_female;
    }
}