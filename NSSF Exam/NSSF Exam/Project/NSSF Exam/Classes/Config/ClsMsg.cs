﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSSF_Exam.MDIForms;

namespace NSSF_Exam.Classes.Config
{
    class ClsMsg
    {
        public static string STR_SUCCESS = "ប្រតិបត្តិការទទួលបានជោគជ័យ!";
        public static string STR_UNSUCCESS = "ប្រតិបត្តិការ​មិនទទួលបានជោគជ័យទេ!";

        public static void Success(string messageText)
        {
            frm_message frm = new frm_message();
            frm.msgStr = messageText;
            frm.opt = Properties.Resources.information;
            frm.yes = false;
            frm.ShowDialog();
        }
        public static bool Question(string messageText)
        {
            frm_message frm = new frm_message();
            bool re = false;
            frm.msgStr = messageText;
            frm.opt = Properties.Resources.question;
            frm.yes = true;
            frm.ShowDialog();
            re = frm.result;
            return re;
        }
        public static void Error(string messageText)
        {
            frm_message frm = new frm_message();
            frm.msgStr = messageText;
            frm.opt = Properties.Resources.error;
            frm.yes = false;
            frm.ShowDialog();
        }
        public static void Warning(string messageText)
        {
            frm_message frm = new frm_message();
            frm.msgStr = messageText;
            frm.opt = Properties.Resources.warnin;
            frm.yes = false;
            frm.ShowDialog();
        }
    }
}
