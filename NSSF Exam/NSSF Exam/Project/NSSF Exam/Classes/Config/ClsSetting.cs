﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Classes.Config
{
    class ClsSetting
    {
        static int tpcount = 0;
        public static bool isClose = false;
        public static TabControl tpctrl;

        public static void sty_btn_refresh(Button btn)
        {
            btn.Image = Properties.Resources.btn_reload;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(150, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ទាញយកម្ដងទៀត";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_search_stu_exam(Button btn)
        {
            btn.Image = Properties.Resources.Zoom;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(78, 31);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ស្វែងរក";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_save(Button btn)
        {
            btn.Image = Properties.Resources.btn_save;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "រក្សាទុក";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_add(Button btn)
        {
            btn.Image = Properties.Resources.btn_add;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "បន្ថែមថ្មី";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }
        public static void sty_btn_update(Button btn)
        {
            btn.Image = Properties.Resources.btn_update;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "កែប្រែ";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_delete(Button btn)
        {
            btn.Image = Properties.Resources.btn_delete;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "លុប";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_clear(Button btn)
        {
            btn.Image = Properties.Resources.btn_delete;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "សំអាត";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_next(Button btn)
        {
            btn.Image = Properties.Resources.play_symbol;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.Size = new System.Drawing.Size(47, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            //btn.Text = "ទៅមុខ";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_previous(Button btn)
        {
            btn.Image = Properties.Resources.previous;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.Size = new System.Drawing.Size(47, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            //btn.Text = "ថយក្រោយ";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_close(Button btn)
        {
            btn.Image = Properties.Resources.btn_close;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ចាកចេញ";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_no(Button btn)
        {
            btn.Image = Properties.Resources.btn_close;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "បដិសេធ";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_submit(Button btn)
        {
            btn.Image = Properties.Resources.btn_submit;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "យល់ព្រម";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_next_eq(Button btn)
        {
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(90, 33);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ផ្នែកបន្ទាប់ ⮞⮞";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }


        public static void sty_btn_search(Button btn)
        {
            btn.Image = Properties.Resources.btn_search;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ស្វែងរក";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_print(Button btn)
        {
            btn.Image = Properties.Resources.btn_print;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "បោះពុម្ព";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_verify(Button btn)
        {
            btn.Image = Properties.Resources.btn_reload;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "ផ្ទៀងផ្ទាត់";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_cancel(Button btn)
        {
            btn.Image = Properties.Resources.btn_delete;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "បោះបង់";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_btn_setting(Button btn)
        {
            btn.Image = Properties.Resources.btn_setting;
            btn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.Size = new System.Drawing.Size(100, 35);
            btn.FlatStyle = FlatStyle.Flat;
            btn.BackColor = SystemColors.MenuHighlight;
            btn.ForeColor = Color.Transparent;
            btn.Text = "កំណត់";
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            btn.UseVisualStyleBackColor = true;
            btn.FlatAppearance.BorderSize = 0;
            btn.Cursor = Cursors.Hand;
        }

        public static void sty_dgv(DataGridView dgv)
        {
            DataGridViewCellStyle sty_1 = new DataGridViewCellStyle();
            DataGridViewCellStyle sty_2 = new DataGridViewCellStyle();
            DataGridViewCellStyle sty_3 = new DataGridViewCellStyle();

            sty_1.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(224)), Convert.ToInt32(Convert.ToByte(224)), Convert.ToInt32(Convert.ToByte(224)));
            sty_1.Font = new System.Drawing.Font("Khmer OS Content", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            sty_1.ForeColor = System.Drawing.SystemColors.WindowText;
            sty_1.SelectionBackColor = System.Drawing.Color.LightBlue;
            sty_1.SelectionForeColor = System.Drawing.Color.Black;
            sty_1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;

            sty_2.BackColor = System.Drawing.SystemColors.Control;
            sty_2.Font = new System.Drawing.Font("Khmer OS Content", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            sty_2.ForeColor = System.Drawing.SystemColors.WindowText;
            sty_2.SelectionBackColor = System.Drawing.Color.LightBlue;
            sty_2.SelectionForeColor = System.Drawing.Color.Black;
            sty_2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;

            sty_3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            sty_3.BackColor = System.Drawing.SystemColors.Control;
            sty_3.Font = new System.Drawing.Font("Khmer OS Content", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            sty_3.ForeColor = System.Drawing.SystemColors.WindowText;
            sty_3.SelectionBackColor = System.Drawing.Color.LightBlue;
            sty_3.SelectionForeColor = System.Drawing.Color.Black;
            sty_3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dgv.AllowUserToAddRows = false;
            dgv.AllowUserToDeleteRows = false;
            dgv.AllowUserToResizeColumns = false;
            dgv.AllowUserToResizeRows = false;

            dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedVertical;
            dgv.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dgv.RowsDefaultCellStyle = sty_2;
            dgv.AlternatingRowsDefaultCellStyle = sty_1;
            dgv.ColumnHeadersDefaultCellStyle = sty_3;
            dgv.RowHeadersDefaultCellStyle = sty_3;
            dgv.ColumnHeadersHeight = 36;
            dgv.RowHeadersWidth = 45;
            dgv.RowTemplate.Height = 36;
            dgv.TabIndex = 0;
            dgv.ScrollBars = ScrollBars.Both;
            dgv.ReadOnly = true;
            dgv.RowHeadersVisible = false;
            dgv.BackgroundColor = System.Drawing.Color.White;
        }

        public static void sty_form_view(Form frm, string frm_name)
        {
            frm.WindowState = FormWindowState.Maximized;
            frm.AutoScaleDimensions = new System.Drawing.SizeF(7f, 24f);
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            frm.ClientSize = new System.Drawing.Size(783, 271);
            frm.ControlBox = false;
            frm.Font = new System.Drawing.Font("Khmer Os Content", 10f);
            frm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            frm.Text = frm_name;
            frm.ResumeLayout(false);
        }

        public static void sty_form_dialog(Form frm, string frm_name)
        {
            //frm.Icon = Properties.Resources.icon;
            frm.AutoScaleDimensions = new System.Drawing.SizeF(7f, 24f);
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            frm.ControlBox = false;
            frm.Font = new System.Drawing.Font("Khmer OS Content", 10f);
            frm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            frm.Text = frm_name;
            frm.ResumeLayout(false);
        }

        public static void sty_form_message(Form frm, string frm_name)
        {
           // frm.Icon = Properties.Resources.icon;
            frm.AutoScaleDimensions = new System.Drawing.SizeF(7f, 24f);
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            frm.ControlBox = false;
            frm.Font = new System.Drawing.Font("Khmer OS Content", 10f);
            frm.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            frm.Text = frm_name;
            frm.ResumeLayout(false);
        }

        public static void sty_combobox(ComboBox cbo)
        {
            cbo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            cbo.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbo.DropDownHeight = 250;
        }

        public static void OpenInParentForm(Form frmName,Form frmMain)
        {
            bool b = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Name == frmName.Name)
                {
                    b = true;
                    break;
                }
            }
            if (b == true)
            {
                frmName.Activate();
                SelectForm(frmName.Text, frmName.Name, b);
            }
            else
            {
                SelectForm(frmName.Text, frmName.Name, b);
                frmName.MdiParent = frmMain;
                frmName.Show();
            }

        }
        public static void SelectForm(string fText, string fName, bool exist)
        {

            if (exist)
            {
                foreach (TabPage tp in tpctrl.TabPages)
                {
                    if (tp.Tag.ToString() == fName)
                    {
                        tpctrl.SelectedTab = tp;
                        break;
                    }
                }
            }
            else
            {
                tpcount += 1;
                TabPage tp = new TabPage();
                tp.Name = "Tabpage" + tpcount;
                tp.Text = fText;
                tp.Tag = fName;
                if (tp.Tag.ToString() != "frm_home")
                {
                    tpctrl.TabPages.Add(tp);
                    tpctrl.SelectedTab = tp;
                }
            }
        }
        public static void CloseTab()
        {
            if (isClose == false)
            {
                tpctrl.TabPages.Remove(tpctrl.SelectedTab);
            }
        }

        public static void TabSelected()
        {
            if (tpctrl.TabPages.Count > 0)
            {
                TabPage ttp = tpctrl.SelectedTab;
                foreach (Form frm in ClsConfig.glo_main_form.MdiChildren)
                {
                    if (frm.Name == ttp.Tag.ToString())
                    {
                        frm.Activate();
                        tpctrl.SelectedTab = ttp;
                        break;
                    }
                }
            }
        }

        public static void CloseAllTabs()
        {
            if (tpctrl.TabPages.Count > 0)
            {
                for (int i = tpctrl.TabPages.Count - 1; i >= 0; i += -1)
                {
                    TabPage TP = default(TabPage);
                    tpctrl.SelectedIndex = i;
                    TP = tpctrl.SelectedTab;

                    foreach (Form ChildForm in ClsConfig.glo_main_form.MdiChildren)
                    {
                        if (ChildForm.Name == TP.Tag.ToString())
                        {
                            ChildForm.Close();
                            break; 
                        }
                    }
                }

            }
        }

        public static bool IsExitApplication()
        {

            bool result = ClsMsg.Question("តើអ្នកពិតជាចង់​ចាកចេញមែនទេ?");
            if (result) {
                return true;
            }
        
            return false;
        }

        public static void OpenPermission(Control parentCtr,string formName,int priv)
        {

            DataTable dt = GetModulePermissionByForm(formName);

            foreach (Control ctr in parentCtr.Controls)
            {
                if (ctr is MenuStrip)
                {
                    if (priv == 1)
                    {
                        SetMenuItems(ctr, formName, true);
                    }
                    else
                    {
                        ScanItem(ctr, formName, dt);
                    }
                }
                else if(ctr is Button)
                {
                    if (priv == 1)
                    {
                        SetButtonItems(ctr, formName, true);
                    }
                    else {
                        SetButtonItems(ctr, formName, false);
                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            if (ctr.Name == dt.Rows[i][0].ToString())
                            {
                                ctr.Enabled = true;
                                break;
                            }
                        }
                    }
                        
                }
                
                OpenPermission(ctr,formName,priv);
            }
            
        }

        private static DataTable GetModulePermissionByForm(string formName)
        {
            DataTable dt = new DataTable();
            DataRow dr;
            DataColumn dc = new DataColumn();
            dc.ColumnName = "mod_controlname";
            dt.Columns.Add(dc);
           
            var lst1 = (from ur in ClsConfig.glo_local_db.tbl_user_role
                        join up in ClsConfig.glo_local_db.tbl_role_permission on ur.rol_id equals up.rol_id
                        join m in ClsConfig.glo_local_db.tbl_module on up.mod_id equals m.mod_id
                        where m.mod_formname == formName && ur.use_id == ClsConfig.glo_use_id
                        select new
                        {
                            m.mod_controlname,
                        }
                            ).ToList();
            if (lst1.Count > 0)
            {
                for (var i = 0; i < lst1.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["mod_controlname"] = lst1[i].mod_controlname;
                    dt.Rows.Add(dr);
                }
            }
            
            return dt;
        }

        private static void SetMenuItems(Control ctr, string formName, bool enable)
        {
            DataTable dt = GetModuleByForm(formName); // get module by form to check if control exits or not to visible it
            foreach (ToolStripMenuItem crt in ((MenuStrip)ctr).Items)
            {
                if (crt is ToolStripMenuItem)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (crt.Name == dt.Rows[i][0].ToString())
                        {
                            crt.Visible = enable;
                        }
                    }

                    foreach (ToolStripMenuItem subcrt in ((ToolStripMenuItem)crt).DropDownItems)
                    {
                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            if (subcrt.Name == dt.Rows[i][0].ToString())
                            {
                                subcrt.Visible = enable;
                            }
                        }

                        foreach (ToolStripMenuItem dropcrt in ((ToolStripMenuItem)subcrt).DropDownItems)
                        {
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                if (dropcrt.Name == dt.Rows[i][0].ToString())
                                {
                                    dropcrt.Visible = enable;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void SetButtonItems(Control ctr, string formName, bool enable)
        {
            DataTable dt = GetModuleByForm(formName); // get module by form to check if control exits or not to visible it
            Button crt = (Button)ctr;
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (crt.Name == dt.Rows[i][0].ToString())
                {
                    crt.Enabled = enable;
                }
            }
        }

        private static void ScanItem(Control ctr,string formName, DataTable dt)
        {
            SetMenuItems(ctr, formName, false);

            foreach (ToolStripMenuItem crt in ((MenuStrip)ctr).Items)
            {
                if (crt is ToolStripMenuItem)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (crt.Name == dt.Rows[i][0].ToString())
                        {
                            crt.Visible = true;
                            break;
                        }
                    }

                    foreach (ToolStripMenuItem subcrt in ((ToolStripMenuItem)crt).DropDownItems)
                    {                    

                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            if (subcrt.Name == dt.Rows[i][0].ToString())
                            {
                                subcrt.Visible = true;
                                break; 
                            }
                        }
                        foreach (ToolStripMenuItem dropcrt in ((ToolStripMenuItem)subcrt).DropDownItems)
                        {
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                if (dropcrt.Name == dt.Rows[i][0].ToString())
                                {
                                    dropcrt.Visible = true;
                                    break; 
                                }
                            }
                        }
                    }
                }
            }
        }

        private static DataTable GetModuleByForm(string formName)
        {
            DataTable dt = new DataTable();
            DataRow dr;
            DataColumn dc = new DataColumn();
            dc.ColumnName = "mod_controlname";
            dt.Columns.Add(dc);
            var q = (from m in ClsConfig.glo_local_db.tbl_module where m.mod_formname == formName select m).ToList();

            if (q.Count > 0)
            {
                for (var i = 0; i < q.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["mod_controlname"] = q[i].mod_controlname;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        public static void ChangeKeyToEN()
        {
            foreach (InputLanguage Ipl in InputLanguage.InstalledInputLanguages)
            {
                if (Ipl.Culture.EnglishName.ToUpper().StartsWith("EN"))
                {
                    InputLanguage.CurrentInputLanguage = Ipl;
                    break;
                }
            }
        }
        public static void ChangeKeyToKH()
        {
            foreach (InputLanguage Ipl in InputLanguage.InstalledInputLanguages)
            {
                if (Ipl.Culture.DisplayName.ToUpper().StartsWith("KH"))
                {
                    InputLanguage.CurrentInputLanguage = Ipl;
                    break;
                }
                else if (Ipl.Culture.DisplayName.ToUpper().StartsWith("CA"))
                {
                    InputLanguage.CurrentInputLanguage = Ipl;
                    break;
                }
            }
        }

        public static string FunGetSystemAddress()
        {
            string Result = "";
            try
            {
                System.Net.IPHostEntry LocalIPList = System.Net.Dns.GetHostEntry(Environment.MachineName);
                foreach (System.Net.IPAddress LocalIP in LocalIPList.AddressList)
                {
                    if (LocalIP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        Result = LocalIP.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Result;
        }

        public static string FunGetSystemMac()
        {
            string Result = "";
            try
            {
                //Dim mac As System.Net.NetworkInformation.NetworkInterface = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
                foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                    {
                        //MessageBox.Show(String.Format("The MAC address of {0} is{1}{2}", nic.Description, Environment.NewLine, nic.GetPhysicalAddress()))
                        Result = string.Format("{0}", nic.GetPhysicalAddress());
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Result;
        }

        public static string FunGetSystemHostname()
        {
            string Result = "";
            try
            {
                Result = Environment.MachineName;

            }
            catch (Exception ex)
            {
            }
            return Result;
        }

        public static void rowGrid_AutoNum(DataGridView dgvlst,string col_name)
        {
            for (int i = 0; i < dgvlst.RowCount; i++)
            {
                dgvlst.Rows[i].Cells[col_name].Value = i+1;
            }
        }

        public static string toKhmerNum(string num)
        {
            string str = "";
            foreach (char item in num)
            {
                switch (item)
                {
                    case '0':
                        str += "០";
                        break;
                    case '1':
                        str += "១";
                        break;
                    case '2':
                        str += "២";
                        break;
                    case '3':
                        str += "៣";
                        break;
                    case '4':
                        str += "៤";
                        break;
                    case '5':
                        str += "៥";
                        break;
                    case '6':
                        str += "៦";
                        break;
                    case '7':
                        str += "៧";
                        break;
                    case '8':
                        str += "៨";
                        break;
                    case '9':
                        str += "៩";
                        break;
                }
            }
            return str;
        }

        public static string FunGetSystemVersion()
        {
            string Result = "";
            try
            {
                dynamic m_xmld = new System.Xml.XmlDocument();
                m_xmld.Load("NSSF_Exam.exe.manifest");
                //Application.ExecutablePath & ".vshost.exe" & ".manifest")
                Result = m_xmld.ChildNodes.Item(1).ChildNodes.Item(0).Attributes.GetNamedItem("version").Value;

            }
            catch (Exception ex)
            {
            }
            return Result;
        }

    }
}
