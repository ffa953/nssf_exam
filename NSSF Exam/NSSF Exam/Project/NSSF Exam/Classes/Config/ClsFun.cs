﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Classes.Config
{
    class ClsFun
    {
        public static DateTime FormatDateTime(string datetime)
        {
            DateTime d_result = DateTime.Now;
            try
            {
                string[] d_t_arr = datetime.Split(' ');
                string[] d_arr = d_t_arr[0].Split('/');
                int d = Convert.ToInt16(d_arr[0]);
                int m = Convert.ToInt16(d_arr[1]);
                int y = Convert.ToInt16(d_arr[2]);
                if (d_t_arr.Count() == 1)
                {
                    d_result = new DateTime(y, m, d);
                }
                else if (d_t_arr.Count() == 2)
                {
                    string[] t_arr = d_t_arr[1].Split(':');
                    int h = Convert.ToInt16(t_arr[0]);
                    int i = Convert.ToInt16(t_arr[1]);
                    d_result = new DateTime(y, m, d, h, i, 0, 0);

                }
            }
            catch (Exception ex)
            {
                d_result = DateTime.Now;
            }
            return d_result;

        }

        public static DateTime getCurrentDate() {
            return (DateTime)ClsConfig.glo_local_db.pro_get_current_date().FirstOrDefault().Value;
        }

        public static void RefreshEntity() {
            ClsConfig.glo_local_db = ClsDatabase.Connection;
        }

        public static string Encrypt(string cha_value)
        {
            System.Security.Cryptography.RijndaelManaged AES = new System.Security.Cryptography.RijndaelManaged();
            System.Security.Cryptography.MD5CryptoServiceProvider Hash_AES = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string encrypted = "";
            try
            {
                byte[] hash = new byte[32];
                byte[] temp = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes("#"));
                Array.Copy(temp, 0, hash, 0, 16);
                Array.Copy(temp, 0, hash, 15, 16);
                AES.Key = hash;
                AES.Mode = System.Security.Cryptography.CipherMode.ECB;
                System.Security.Cryptography.ICryptoTransform DESEncrypter = AES.CreateEncryptor();
                byte[] Buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(cha_value);
                encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length));
            }
            catch (Exception ex)
            {
            }
            return encrypted;
        }

        public static string Decrypt(string cha_value)
        {
            System.Security.Cryptography.RijndaelManaged AES = new System.Security.Cryptography.RijndaelManaged();
            System.Security.Cryptography.MD5CryptoServiceProvider Hash_AES = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string decrypted = "";
            try
            {
                byte[] hash = new byte[32];
                byte[] temp = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes("#"));
                Array.Copy(temp, 0, hash, 0, 16);
                Array.Copy(temp, 0, hash, 15, 16);
                AES.Key = hash;
                AES.Mode = System.Security.Cryptography.CipherMode.ECB;
                System.Security.Cryptography.ICryptoTransform DESDecrypter = AES.CreateDecryptor();
                byte[] Buffer = Convert.FromBase64String(cha_value);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length));
            }
            catch (Exception ex)
            {
            }
            return decrypted;
        }

        public Bitmap GetBarcodeBitmap(string BarCode)
        {
            Bitmap result = new Bitmap(1, 1);
            try
            {
                BarCode = "*" + BarCode + "*";
                Font font = new Font("Free 3 of 9 Extended", 150, FontStyle.Regular, GraphicsUnit.Pixel);
                Graphics graphics = Graphics.FromImage(result);
                int width = Convert.ToInt32(graphics.MeasureString(BarCode, font).Width);
                int height = Convert.ToInt32(graphics.MeasureString(BarCode, font).Height);
                result = new Bitmap(result, new Size(width, height));
                graphics = Graphics.FromImage(result);
                graphics.Clear(Color.White);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                graphics.DrawString(BarCode, font, new SolidBrush(Color.FromArgb(0, 0, 0)), 0, 0);
                graphics.Flush();
                graphics.Dispose();

            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public string ConvertCharTokhmer(string cha_values, int cha_digit = 0)
        {
            string TemResult = "";
            Char[] CharArray = cha_values.ToString().ToCharArray();
            string Result = "";
            for (int i = 0; i <= CharArray.Length - 1; i++)
            {
                TemResult = CharArray[i].ToString();
                switch (TemResult)
                {
                    case "0":
                        Result += "០";
                        break;
                    case "1":
                        Result += "១";
                        break;
                    case "2":
                        Result += "២";
                        break;
                    case "3":
                        Result += "៣";
                        break;
                    case "4":
                        Result += "៤";
                        break;
                    case "5":
                        Result += "៥";
                        break;
                    case "6":
                        Result += "៦";
                        break;
                    case "7":
                        Result += "៧";
                        break;
                    case "8":
                        Result += "៨";
                        break;
                    case "9":
                        Result += "៩";
                        break;
                    case "k":
                        Result += "ក";
                        break;
                    case "x":
                        Result += "ខ";
                        break;
                    case "K":
                        Result += "គ";
                        break;
                    case "X":
                        Result += "ឃ";
                        break;
                    case "g":
                        Result += "ង";
                        break;
                    case "c":
                        Result += "ច";
                        break;
                    case "q":
                        Result += "ឆ";
                        break;
                    case "C":
                        Result += "ជ";
                        break;
                    case "Q":
                        Result += "ឈ";
                        break;
                    case "j":
                        Result += "ញ";
                        break;
                    case "d":
                        Result += "ដ";
                        break;
                    case "z":
                        Result += "ឋ";
                        break;
                    case "D":
                        Result += "ឌ";
                        break;
                    case "Z":
                        Result += "ឍ";
                        break;
                    case "N":
                        Result += "ណ";
                        break;
                    case "t":
                        Result += "ត";
                        break;
                    case "f":
                        Result += "ថ";
                        break;
                    case "T":
                        Result += "ទ";
                        break;
                    case "F":
                        Result += "ធ";
                        break;
                    case "n":
                        Result += "ន";
                        break;
                    case "b":
                        Result += "ប";
                        break;
                    case "p":
                        Result += "ផ";
                        break;
                    case "B":
                        Result += "ព";
                        break;
                    case "P":
                        Result += "ភ";
                        break;
                    case "m":
                        Result += "ម";
                        break;
                    case "y":
                        Result += "យ";
                        break;
                    case "r":
                        Result += "រ";
                        break;
                    case "l":
                        Result += "ល";
                        break;
                    case "v":
                        Result += "វ";
                        break;
                    case "s":
                        Result += "ស";
                        break;
                    case "h":
                        Result += "ហ";
                        break;
                    case "L":
                        Result += "ឡ";
                        break;
                    case "G":
                        Result += "អ";
                        break;
                    default:
                        Result += TemResult;
                        break;
                }
            }
            if (cha_digit > 0)
            {
                Result = (Result + ("០០០០០០០០០"));
                Result = Result.Substring(Result.Length - cha_digit, cha_digit);
            }
            return Result;
        }

        public static bool CheckAge(DateTime today, DateTime bd)
        {
            return today.AddYears(-18) < bd;
        }
        
    }
}
