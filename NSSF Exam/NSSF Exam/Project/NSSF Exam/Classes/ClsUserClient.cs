﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.MDIForms;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Classes
{
    class ClsUserClient
    {
        public static int switchUser = 0; //0 new login, 1 switch user
        public static int CheckMachineClient()
        {
            try
            {
                ObjectParameter mac_id = new ObjectParameter("mac_id", typeof(Int32));
                ObjectParameter mac_active = new ObjectParameter("mac_active", typeof(bool));
                ClsConfig.glo_local_db.pro_machine_client(ClsSetting.FunGetSystemHostname(), ClsSetting.FunGetSystemMac(), ClsSetting.FunGetSystemMac(), ClsSetting.FunGetSystemVersion(), ClsConfig.glo_use_id, "0", mac_id, mac_active);
                if ((bool)mac_active.Value == true)
                {
                    ClsConfig.glo_mac_id = (int)mac_id.Value;
                }
            }
            catch (Exception ex)
            {
                ClsConfig.glo_mac_id = 0;
            }
            return ClsConfig.glo_mac_id;
        }
        public static void SetUserLogout() {
            ClsUserClient.Logout();
            frm_login frm = new frm_login();
            switchUser = 1;
            frm.ShowDialog();
        }

        public static Int64 Login(int use_id, int mac_id, string div_id)
        {
            Int64 result = 0;
            try
            {
                ObjectParameter log_id = new ObjectParameter("log_id", typeof(Int64));
                ClsConfig.glo_local_db.pro_login(use_id, mac_id, div_id, log_id);
                result = Convert.ToInt64(log_id.Value);

            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public static bool Logout()
        {
            bool result = false;
            try
            {
                var query = ClsConfig.glo_local_db.tbl_audit_log.Where(log => log.log_id == ClsConfig.glo_log_id);
                tbl_audit_log cls = query.FirstOrDefault();
                cls.log_status = false;
                cls.log_date_out = ClsFun.getCurrentDate();
                ClsConfig.glo_local_db.SaveChanges();
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }


    }
}
