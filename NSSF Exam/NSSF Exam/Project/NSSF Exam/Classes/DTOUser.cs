﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSSF_Exam.Classes
{
    class DTOUser
    {
        public int user_id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string div_name { get; set; }
        public bool user_status { get; set; }
    }
}
