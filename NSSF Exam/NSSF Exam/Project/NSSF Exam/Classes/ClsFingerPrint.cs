﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPFP;

namespace NSSF_Exam.Classes
{
    public class ClsFingerPrint
    {
        public int fin_id { get; set; }
        public int fin_index {get;set;}
        public string fin_quaity {get;set;}
        public Template fin_template {get;set;}
        public FeatureSet fin_feature_set { get; set; }
        public bool fin_verify { get; set; }
    }
}
