﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;


namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_student : Form
    {
        public frm_student()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "ពត៍មានបេក្ខជន");
        }

        private void frm_student_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_student);
            ClsSetting.sty_btn_add(btn_add);
            ClsSetting.sty_btn_print(btn_export);
            ClsSetting.sty_btn_update(btn_edit);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_btn_search(btn_search);

            btn_edit.Enabled = false;

            GenBindingSource.DataSource = ClsConfig.glo_local_db.tbl_gender.ToList();
            SkillBindingSource.DataSource = ClsConfig.glo_local_db.tbl_skill.ToList();          
            CertBindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate.ToList();
            CertSkill_BindingSource.DataSource = ClsConfig.glo_local_db.tbl_certificate_skill.ToList();
            NatBindindSource.DataSource = ClsConfig.glo_local_db.vw_nationality.ToList();
            StatusBindingSource.DataSource = ClsConfig.glo_local_db.tbl_user_type.ToList();

            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_user_by_role(2).OrderByDescending(f => f.UserId).ToList();

           
        }

        private void dgv_student_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dgv_student.RowCount; i++)
            {
                dgv_student.Rows[i].Cells["col"].Value = i + 1;             
            }
            dgv_student.ClearSelection();
        }

       

        private void btn_add_Click(object sender, EventArgs e)
        {
            frm_student_edit frm = new frm_student_edit();
            frm.ShowDialog();
            frm_student_Load(null, null);
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            int rowindex = dgv_student.CurrentCell.RowIndex;
            int id = Convert.ToInt32(dgv_student.Rows[rowindex].Cells[0].Value);
            frm_student_edit frm = new frm_student_edit();
            frm.userID = id;
            frm.ShowDialog();
            frm_student_Load(null, null);
        }

        private void dgv_student_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv_student.Columns[e.ColumnIndex].Name == "finger")
            {
                if (dgv_student.Rows[e.RowIndex].Cells["finger"].Value != null)
                {

                    e.CellStyle.BackColor = SystemColors.Info;

                }
                else
                {
                    e.CellStyle.BackColor = SystemColors.Control;
                  
                }
            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            StuBindingSource.DataSource = ClsConfig.glo_local_db.fun_user_by_role(2).Where(m => m.Name_khmer.Contains(txt_search.Text) || m.Name_latin.Contains(txt_search.Text) || m.UserName.Contains(txt_search.Text)).ToList();
        }


        private void btn_export_Click(object sender, EventArgs e)
        {
            

  
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_student_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btn_edit.Enabled = true;
          
        }

        private void frm_student_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }
    }
}
