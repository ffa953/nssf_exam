﻿namespace NSSF_Exam.Forms.Exam
{
    partial class frm_exam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.dgv_exam = new System.Windows.Forms.DataGridView();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.bds_exam = new System.Windows.Forms.BindingSource(this.components);
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.exam_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.examdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.examactiveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usereditDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userProfileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbluseranswerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblusercontrolDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_exam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_new);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(705, 62);
            this.panel1.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(593, 14);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 34);
            this.btn_close.TabIndex = 3;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(485, 14);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 34);
            this.btn_new.TabIndex = 2;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // dgv_exam
            // 
            this.dgv_exam.AutoGenerateColumns = false;
            this.dgv_exam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_exam.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.exam_id,
            this.examdateDataGridViewTextBoxColumn,
            this.examactiveDataGridViewTextBoxColumn,
            this.useridDataGridViewTextBoxColumn,
            this.usereditDataGridViewTextBoxColumn,
            this.userProfileDataGridViewTextBoxColumn,
            this.tbluseranswerDataGridViewTextBoxColumn,
            this.tblusercontrolDataGridViewTextBoxColumn});
            this.dgv_exam.DataSource = this.bds_exam;
            this.dgv_exam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_exam.Location = new System.Drawing.Point(0, 62);
            this.dgv_exam.Name = "dgv_exam";
            this.dgv_exam.Size = new System.Drawing.Size(705, 388);
            this.dgv_exam.TabIndex = 1;
            this.dgv_exam.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_exam_CellClick);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 90;
            // 
            // bds_exam
            // 
            this.bds_exam.DataSource = typeof(NSSF_Exam.Entities.tbl_exam_date);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "កែប្រែ";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Width = 90;
            // 
            // exam_id
            // 
            this.exam_id.DataPropertyName = "exam_id";
            this.exam_id.HeaderText = "exam_id";
            this.exam_id.Name = "exam_id";
            this.exam_id.Visible = false;
            // 
            // examdateDataGridViewTextBoxColumn
            // 
            this.examdateDataGridViewTextBoxColumn.DataPropertyName = "exam_date";
            dataGridViewCellStyle1.Format = "dd-MMM-yyyy";
            this.examdateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.examdateDataGridViewTextBoxColumn.HeaderText = "សម័យប្រឡង";
            this.examdateDataGridViewTextBoxColumn.Name = "examdateDataGridViewTextBoxColumn";
            this.examdateDataGridViewTextBoxColumn.Width = 300;
            // 
            // examactiveDataGridViewTextBoxColumn
            // 
            this.examactiveDataGridViewTextBoxColumn.DataPropertyName = "exam_active";
            this.examactiveDataGridViewTextBoxColumn.HeaderText = "exam_active";
            this.examactiveDataGridViewTextBoxColumn.Name = "examactiveDataGridViewTextBoxColumn";
            this.examactiveDataGridViewTextBoxColumn.Visible = false;
            // 
            // useridDataGridViewTextBoxColumn
            // 
            this.useridDataGridViewTextBoxColumn.DataPropertyName = "user_id";
            this.useridDataGridViewTextBoxColumn.HeaderText = "user_id";
            this.useridDataGridViewTextBoxColumn.Name = "useridDataGridViewTextBoxColumn";
            this.useridDataGridViewTextBoxColumn.Visible = false;
            // 
            // usereditDataGridViewTextBoxColumn
            // 
            this.usereditDataGridViewTextBoxColumn.DataPropertyName = "user_edit";
            this.usereditDataGridViewTextBoxColumn.HeaderText = "user_edit";
            this.usereditDataGridViewTextBoxColumn.Name = "usereditDataGridViewTextBoxColumn";
            this.usereditDataGridViewTextBoxColumn.Visible = false;
            // 
            // userProfileDataGridViewTextBoxColumn
            // 
            this.userProfileDataGridViewTextBoxColumn.DataPropertyName = "UserProfile";
            this.userProfileDataGridViewTextBoxColumn.HeaderText = "UserProfile";
            this.userProfileDataGridViewTextBoxColumn.Name = "userProfileDataGridViewTextBoxColumn";
            this.userProfileDataGridViewTextBoxColumn.Visible = false;
            // 
            // tbluseranswerDataGridViewTextBoxColumn
            // 
            this.tbluseranswerDataGridViewTextBoxColumn.DataPropertyName = "tbl_user_answer";
            this.tbluseranswerDataGridViewTextBoxColumn.HeaderText = "tbl_user_answer";
            this.tbluseranswerDataGridViewTextBoxColumn.Name = "tbluseranswerDataGridViewTextBoxColumn";
            this.tbluseranswerDataGridViewTextBoxColumn.Visible = false;
            // 
            // tblusercontrolDataGridViewTextBoxColumn
            // 
            this.tblusercontrolDataGridViewTextBoxColumn.DataPropertyName = "tbl_user_control";
            this.tblusercontrolDataGridViewTextBoxColumn.HeaderText = "tbl_user_control";
            this.tblusercontrolDataGridViewTextBoxColumn.Name = "tblusercontrolDataGridViewTextBoxColumn";
            this.tblusercontrolDataGridViewTextBoxColumn.Visible = false;
            // 
            // frm_exam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 450);
            this.Controls.Add(this.dgv_exam);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_exam";
            this.Text = "frm_exam";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_exam_FormClosed);
            this.Load += new System.EventHandler(this.frm_exam_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_exam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_exam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_exam;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.BindingSource bds_exam;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn exam_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn examdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn examactiveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usereditDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn userProfileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbluseranswerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblusercontrolDataGridViewTextBoxColumn;
    }
}