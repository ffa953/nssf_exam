﻿using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_exam_edit : Form
    {
        public int exam_id = 0;
        public frm_exam_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "បញ្ចូលសម័យប្រឡង");
        }

        private void frm_exam_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_close);
            if (exam_id != 0) {
                tbl_exam_date exam = ClsConfig.glo_local_db.tbl_exam_date.Where(ex => ex.exam_id == exam_id).FirstOrDefault();
                dtp_exam_date.Value = (DateTime)exam.exam_date;
            }


        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try {
                if (exam_id == 0)
                {                  

                    using (TransactionScope tra = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0)))
                    {
                        try
                        {
                            tbl_exam_date exam = new tbl_exam_date();
                            exam.exam_date = dtp_exam_date.Value;
                            exam.exam_active = true;
                            exam.user_id = ClsConfig.glo_use_id;
                            exam.user_edit = ClsFun.getCurrentDate();
                            ClsConfig.glo_local_db.tbl_exam_date.Add(exam);
                            ClsConfig.glo_local_db.SaveChanges();

                            var oldExam = ClsConfig.glo_local_db.tbl_exam_date.Where(ex => ex.exam_id != exam.exam_id).ToList();
                            foreach (tbl_exam_date item in oldExam)
                            {
                                item.exam_active = false;
                                
                            }
                            ClsConfig.glo_local_db.SaveChanges();

                            tra.Complete();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            this.Close();

                        }
                        catch (Exception ex)
                        {
                            tra.Dispose();
                            ClsMsg.Error(ex.Message);
                        }
                    }

                }
                else {
                    tbl_exam_date exam = ClsConfig.glo_local_db.tbl_exam_date.Where(ex => ex.exam_id == exam_id).FirstOrDefault();
                    exam.exam_date = dtp_exam_date.Value;
                    exam.user_id = ClsConfig.glo_use_id;
                    exam.user_edit = ClsFun.getCurrentDate();
                    ClsConfig.glo_local_db.SaveChanges();

                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    this.Close();
                }

                
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
