﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.Exam
{
    public partial class frm_certificate_skill_edit : Form
    {
        public int cer_skill_id = 0;
        public frm_certificate_skill_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "បញ្ចូលជំនាញ");
        }

        private void frm_certificate_skill_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_close);
            if (cer_skill_id != 0) {
                tbl_certificate_skill cerSkill = ClsConfig.glo_local_db.tbl_certificate_skill.Where(c => c.cert_skill_id == cer_skill_id).FirstOrDefault();
                txt_certificate_skill.Text = cerSkill.cert_skill_name;
            } 
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsFieldValidate())
                {
                    if (cer_skill_id == 0)
                    {
                        tbl_certificate_skill cerSkill = new tbl_certificate_skill();
                        cerSkill.cert_skill_name = txt_certificate_skill.Text;
                        ClsConfig.glo_local_db.tbl_certificate_skill.Add(cerSkill);
                        ClsConfig.glo_local_db.SaveChanges();
                    }
                    else
                    {
                        tbl_certificate_skill cerSkill = ClsConfig.glo_local_db.tbl_certificate_skill.Where(c => c.cert_skill_id == cer_skill_id).FirstOrDefault();
                        cerSkill.cert_skill_name = txt_certificate_skill.Text;
                        ClsConfig.glo_local_db.SaveChanges();
                    }

                    ClsMsg.Success(ClsMsg.STR_SUCCESS);
                    this.Close();
                }
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private bool IsFieldValidate()
        {
            string msgString = "";
            if (txt_certificate_skill.Text == "") {
                msgString += "- ជំនាញត្រូវតែបញ្ចូល\n";
            }

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
