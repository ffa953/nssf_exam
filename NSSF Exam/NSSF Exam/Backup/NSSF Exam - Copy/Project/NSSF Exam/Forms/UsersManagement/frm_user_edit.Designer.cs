﻿namespace NSSF_Exam.Forms.UsersManagement
{
    partial class frm_user_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbo_emp_id = new System.Windows.Forms.ComboBox();
            this.bds_employee = new System.Windows.Forms.BindingSource(this.components);
            this.cbo_privilege = new System.Windows.Forms.ComboBox();
            this.bds_privilege = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chk_active = new System.Windows.Forms.CheckBox();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbo_role = new System.Windows.Forms.ComboBox();
            this.bds_role_combo = new System.Windows.Forms.BindingSource(this.components);
            this.dgv_role = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.rolidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.useidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_role_grid = new System.Windows.Forms.BindingSource(this.components);
            this.cbo_division = new System.Windows.Forms.ComboBox();
            this.bds_division = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_employee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_privilege)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role_combo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_division)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 467);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(827, 56);
            this.panel1.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(716, 9);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button2";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(606, 9);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(100, 35);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "button1";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cbo_role);
            this.panel2.Controls.Add(this.dgv_role);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(827, 467);
            this.panel2.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbo_division);
            this.groupBox1.Controls.Add(this.cbo_emp_id);
            this.groupBox1.Controls.Add(this.cbo_privilege);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chk_active);
            this.groupBox1.Controls.Add(this.txt_username);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_password);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(20, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 172);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // cbo_emp_id
            // 
            this.cbo_emp_id.DataSource = this.bds_employee;
            this.cbo_emp_id.DisplayMember = "name_kh";
            this.cbo_emp_id.FormattingEnabled = true;
            this.cbo_emp_id.Location = new System.Drawing.Point(180, 28);
            this.cbo_emp_id.Name = "cbo_emp_id";
            this.cbo_emp_id.Size = new System.Drawing.Size(204, 32);
            this.cbo_emp_id.TabIndex = 12;
            this.cbo_emp_id.ValueMember = "emp_id";
            // 
            // bds_employee
            // 
            this.bds_employee.DataSource = typeof(NSSF_Exam.Entities.vie_employee);
            // 
            // cbo_privilege
            // 
            this.cbo_privilege.DataSource = this.bds_privilege;
            this.cbo_privilege.DisplayMember = "pri_name_khmer";
            this.cbo_privilege.FormattingEnabled = true;
            this.cbo_privilege.Location = new System.Drawing.Point(545, 27);
            this.cbo_privilege.Name = "cbo_privilege";
            this.cbo_privilege.Size = new System.Drawing.Size(204, 32);
            this.cbo_privilege.TabIndex = 11;
            this.cbo_privilege.ValueMember = "pri_id";
            // 
            // bds_privilege
            // 
            this.bds_privilege.DataSource = typeof(NSSF_Exam.Entities.tbl_privilege);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "គោត្តនាម និងនាម";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "ឈ្មោះអ្នកប្រើប្រាស់";
            // 
            // chk_active
            // 
            this.chk_active.AutoSize = true;
            this.chk_active.Checked = true;
            this.chk_active.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_active.Location = new System.Drawing.Point(545, 114);
            this.chk_active.Name = "chk_active";
            this.chk_active.Size = new System.Drawing.Size(86, 29);
            this.chk_active.TabIndex = 10;
            this.chk_active.Text = "ដំណើរការ";
            this.chk_active.UseVisualStyleBackColor = true;
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(180, 72);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(204, 32);
            this.txt_username.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "លេខសំងាត់";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(429, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "ការិយាល័យ";
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(180, 115);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(204, 32);
            this.txt_password.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(429, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "សិទ្ធិប្រើប្រាស់";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 25);
            this.label6.TabIndex = 13;
            this.label6.Text = "ក្រុមប្រើប្រាស់";
            // 
            // cbo_role
            // 
            this.cbo_role.DataSource = this.bds_role_combo;
            this.cbo_role.DisplayMember = "rol_name";
            this.cbo_role.FormattingEnabled = true;
            this.cbo_role.Location = new System.Drawing.Point(118, 188);
            this.cbo_role.Name = "cbo_role";
            this.cbo_role.Size = new System.Drawing.Size(222, 32);
            this.cbo_role.TabIndex = 12;
            this.cbo_role.ValueMember = "rol_id";
            this.cbo_role.SelectedIndexChanged += new System.EventHandler(this.cbo_role_SelectedIndexChanged);
            // 
            // bds_role_combo
            // 
            this.bds_role_combo.DataSource = typeof(NSSF_Exam.Entities.tbl_role);
            // 
            // dgv_role
            // 
            this.dgv_role.AutoGenerateColumns = false;
            this.dgv_role.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_role.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.rolidDataGridViewTextBoxColumn,
            this.useidDataGridViewTextBoxColumn});
            this.dgv_role.DataSource = this.bds_role_grid;
            this.dgv_role.Location = new System.Drawing.Point(20, 236);
            this.dgv_role.Name = "dgv_role";
            this.dgv_role.Size = new System.Drawing.Size(780, 211);
            this.dgv_role.TabIndex = 11;
            this.dgv_role.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_role_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "លុប";
            this.Column1.Image = global::NSSF_Exam.Properties.Resources.btn_delete_color;
            this.Column1.Name = "Column1";
            // 
            // rolidDataGridViewTextBoxColumn
            // 
            this.rolidDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.rolidDataGridViewTextBoxColumn.DataPropertyName = "rol_id";
            this.rolidDataGridViewTextBoxColumn.DataSource = this.bds_role_combo;
            this.rolidDataGridViewTextBoxColumn.DisplayMember = "rol_name";
            this.rolidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.rolidDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះក្រុមប្រើប្រាស់";
            this.rolidDataGridViewTextBoxColumn.Name = "rolidDataGridViewTextBoxColumn";
            this.rolidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.rolidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.rolidDataGridViewTextBoxColumn.ValueMember = "rol_id";
            this.rolidDataGridViewTextBoxColumn.Width = 145;
            // 
            // useidDataGridViewTextBoxColumn
            // 
            this.useidDataGridViewTextBoxColumn.DataPropertyName = "use_id";
            this.useidDataGridViewTextBoxColumn.HeaderText = "use_id";
            this.useidDataGridViewTextBoxColumn.Name = "useidDataGridViewTextBoxColumn";
            this.useidDataGridViewTextBoxColumn.Visible = false;
            // 
            // bds_role_grid
            // 
            this.bds_role_grid.DataSource = typeof(NSSF_Exam.Entities.tbl_user_role);
            // 
            // cbo_division
            // 
            this.cbo_division.DataSource = this.bds_division;
            this.cbo_division.DisplayMember = "div_name_khmer";
            this.cbo_division.FormattingEnabled = true;
            this.cbo_division.Location = new System.Drawing.Point(544, 69);
            this.cbo_division.Name = "cbo_division";
            this.cbo_division.Size = new System.Drawing.Size(204, 32);
            this.cbo_division.TabIndex = 13;
            this.cbo_division.ValueMember = "div_id";
            // 
            // bds_division
            // 
            this.bds_division.DataSource = typeof(NSSF_Exam.Entities.vie_division);
            // 
            // frm_user_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 523);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_user_edit";
            this.Text = "frm_user_edit";
            this.Load += new System.EventHandler(this.frm_user_edit_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_employee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_privilege)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role_combo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_division)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chk_active;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_role;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbo_role;
        private System.Windows.Forms.BindingSource bds_role_combo;
        private System.Windows.Forms.BindingSource bds_role_grid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn rolidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useidDataGridViewTextBoxColumn;
        private System.Windows.Forms.ComboBox cbo_privilege;
        private System.Windows.Forms.BindingSource bds_privilege;
        private System.Windows.Forms.ComboBox cbo_emp_id;
        private System.Windows.Forms.BindingSource bds_employee;
        private System.Windows.Forms.ComboBox cbo_division;
        private System.Windows.Forms.BindingSource bds_division;
    }
}