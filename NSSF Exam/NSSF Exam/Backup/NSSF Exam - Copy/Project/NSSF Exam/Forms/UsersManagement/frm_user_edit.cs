﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_user_edit : Form
    {
        public int user_id = 0;
        public frm_user_edit()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "គណនីប្រើប្រាស់ប្រព័ន្ធ");
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            
            if (IsFieldValidate()) {

                if (user_id == 0)
                {
                    using (TransactionScope tra = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0)))
                    {
                        try
                        {
                            tbl_user user = new tbl_user();
                            user.emp_id = Convert.ToInt16(cbo_emp_id.SelectedValue);
                            user.use_username = txt_username.Text;
                            user.use_password = txt_password.Text;
                            user.pri_id = (int)cbo_privilege.SelectedValue;
                            user.div_id = cbo_division.SelectedValue.ToString();
                            user.use_active = chk_active.Checked;
                            ClsConfig.glo_local_db.tbl_user.Add(user);
                            ClsConfig.glo_local_db.SaveChanges();

                            foreach (tbl_user_role item in bds_role_grid.List) {
                                tbl_user_role userRole = new tbl_user_role();
                                userRole.rol_id = item.rol_id;
                                userRole.use_id = user.use_id;
                                ClsConfig.glo_local_db.tbl_user_role.Add(userRole);
                            }

                            ClsConfig.glo_local_db.SaveChanges();

                            tra.Complete();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            this.Close();

                        }
                        catch (Exception ex)
                        {
                            tra.Dispose();
                            ClsMsg.Error(ex.Message);
                        }
                    }
                }
                else {
                    using (TransactionScope tra = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 15, 0)))
                    {
                        try
                        {
                            var user = ClsConfig.glo_local_db.tbl_user.Where(u => u.use_id == user_id).FirstOrDefault();
                            user.emp_id = Convert.ToInt16(cbo_emp_id.SelectedValue);
                            user.use_username = txt_username.Text;
                            user.use_password = txt_password.Text;
                            user.pri_id = (int)cbo_privilege.SelectedValue;
                            user.div_id = cbo_division.SelectedValue.ToString();
                            user.use_active = chk_active.Checked;
                            ClsConfig.glo_local_db.SaveChanges();

                            foreach (tbl_user_role userRole in ClsConfig.glo_local_db.tbl_user_role.Where(u => u.use_id == user_id))
                            {
                                ClsConfig.glo_local_db.tbl_user_role.Remove(userRole);
                            }
                            ClsConfig.glo_local_db.SaveChanges();

                            foreach (tbl_user_role item in bds_role_grid.List)
                            {
                                tbl_user_role userRole = new tbl_user_role();
                                userRole.rol_id = item.rol_id;
                                userRole.use_id = user.use_id;
                                ClsConfig.glo_local_db.tbl_user_role.Add(userRole);
                            }

                            ClsConfig.glo_local_db.SaveChanges();

                            tra.Complete();
                            ClsMsg.Success(ClsMsg.STR_SUCCESS);
                            this.Close();

                        }
                        catch (Exception ex)
                        {
                            tra.Dispose();
                            ClsMsg.Error(ex.Message);
                        }
                    }
                   
                }
            }
            
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_user_edit_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_role);
            ClsSetting.sty_btn_save(btn_save);
            ClsSetting.sty_btn_close(btn_close);
            ClsSetting.sty_combobox(cbo_role);
            ClsSetting.sty_combobox(cbo_emp_id);

            bds_role_combo.DataSource = ClsConfig.glo_local_db.tbl_role.ToList();
            bds_privilege.DataSource = ClsConfig.glo_local_db.tbl_privilege.ToList();
            bds_employee.DataSource = ClsConfig.glo_local_db.vie_employee.Where(em => em.emp_active == true).ToList();
            bds_division.DataSource = ClsConfig.glo_local_db.vie_division.Where(d => d.div_active == true).ToList();

            cbo_role.SelectedIndex = -1;
            cbo_privilege.SelectedIndex = -1;

            if (user_id != 0) { 

                var result = ClsConfig.glo_local_db.tbl_user.Where(u => u.use_id == user_id).FirstOrDefault();
                cbo_emp_id.SelectedValue = result.emp_id;
                txt_username.Text = result.use_username;
                txt_password.Text = result.use_password;
                cbo_privilege.SelectedValue = result.pri_id;
                cbo_division.SelectedValue = result.div_id;
                chk_active.Checked = (bool)result.use_active;

                bds_role_grid.DataSource = result.tbl_user_role.ToList();
                
            }
        }

        private void cbo_role_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_role.SelectedIndex != -1) {
                int roleId = (int)cbo_role.SelectedValue;
                foreach (tbl_user_role item in bds_role_grid.List) {
                    if (item.rol_id == roleId) {
                        ClsMsg.Warning("ក្រុមអ្នកប្រើប្រាស់នេះមានម្តងរួចហើយ");
                        return;
                    }
                }
                tbl_user_role role = new tbl_user_role();
                role.rol_id = (int)cbo_role.SelectedValue;
                bds_role_grid.Add(role);
            }
        }

        private void dgv_role_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 & e.ColumnIndex == 0)
            {
                bds_role_grid.RemoveAt(e.RowIndex);
            }

        }

        private bool IsFieldValidate()
        {
            string msgString = "";

           

            if (!string.IsNullOrEmpty(msgString))
            {
                ClsMsg.Warning(msgString);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
