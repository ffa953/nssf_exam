﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_module : Form
    {
        public frm_module()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "កំណត់ការងារ");
        }

        private void frm_module_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_module);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            bds_module.DataSource = ClsConfig.glo_local_db.tbl_module.ToList();

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_module_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_module_edit frm = new frm_module_edit();
            frm.ShowDialog();
            frm_module_Load(null, null);
        }

        private void dgv_module_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    int modId = (int)dgv_module.CurrentRow.Cells["mod_id"].Value;

                    frm_module_edit frm = new frm_module_edit();
                    frm.mod_id = modId;
                    frm.ShowDialog();
                    frm_module_Load(null, null);
                }
            }
            catch (Exception ex)
            {
                ClsMsg.Error(ex.Message);
            }
        }
    }
}
