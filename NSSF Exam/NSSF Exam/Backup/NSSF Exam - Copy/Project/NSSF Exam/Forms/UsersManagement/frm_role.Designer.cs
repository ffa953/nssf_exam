﻿namespace NSSF_Exam.Forms.UsersManagement
{
    partial class frm_role
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgv_role_ = new System.Windows.Forms.Panel();
            this.btn_new = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_role = new System.Windows.Forms.DataGridView();
            this.btn_close = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.bds_role = new System.Windows.Forms.BindingSource(this.components);
            this.role_edit = new System.Windows.Forms.DataGridViewImageColumn();
            this.role_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roldescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolisactiveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useeditdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblrolepermissionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbluserroleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_role_.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_role_
            // 
            this.dgv_role_.Controls.Add(this.btn_close);
            this.dgv_role_.Controls.Add(this.btn_new);
            this.dgv_role_.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgv_role_.Location = new System.Drawing.Point(0, 0);
            this.dgv_role_.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.dgv_role_.Name = "dgv_role_";
            this.dgv_role_.Size = new System.Drawing.Size(1083, 57);
            this.dgv_role_.TabIndex = 0;
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_new.Location = new System.Drawing.Point(862, 12);
            this.btn_new.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(100, 35);
            this.btn_new.TabIndex = 0;
            this.btn_new.Text = "button1";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_role);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 57);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1083, 427);
            this.panel2.TabIndex = 1;
            // 
            // dgv_role
            // 
            this.dgv_role.AutoGenerateColumns = false;
            this.dgv_role.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_role.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.role_edit,
            this.role_id,
            this.rolnameDataGridViewTextBoxColumn,
            this.roldescriptionDataGridViewTextBoxColumn,
            this.rolisactiveDataGridViewTextBoxColumn,
            this.useidDataGridViewTextBoxColumn,
            this.useeditdateDataGridViewTextBoxColumn,
            this.tblrolepermissionDataGridViewTextBoxColumn,
            this.tbluserroleDataGridViewTextBoxColumn});
            this.dgv_role.DataSource = this.bds_role;
            this.dgv_role.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_role.Location = new System.Drawing.Point(0, 0);
            this.dgv_role.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.dgv_role.Name = "dgv_role";
            this.dgv_role.Size = new System.Drawing.Size(1083, 427);
            this.dgv_role.TabIndex = 0;
            this.dgv_role.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_role_CellClick);
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(972, 12);
            this.btn_close.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "button1";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "កែប្រែ";
            this.dataGridViewImageColumn1.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            // 
            // bds_role
            // 
            this.bds_role.DataSource = typeof(NSSF_Exam.Entities.tbl_role);
            // 
            // role_edit
            // 
            this.role_edit.HeaderText = "កែប្រែ";
            this.role_edit.Image = global::NSSF_Exam.Properties.Resources.btn_edit_color;
            this.role_edit.Name = "role_edit";
            // 
            // role_id
            // 
            this.role_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.role_id.DataPropertyName = "rol_id";
            this.role_id.HeaderText = "ល.រ";
            this.role_id.Name = "role_id";
            this.role_id.Width = 59;
            // 
            // rolnameDataGridViewTextBoxColumn
            // 
            this.rolnameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.rolnameDataGridViewTextBoxColumn.DataPropertyName = "rol_name";
            this.rolnameDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះក្រុមអ្នកប្រើប្រាស់";
            this.rolnameDataGridViewTextBoxColumn.Name = "rolnameDataGridViewTextBoxColumn";
            this.rolnameDataGridViewTextBoxColumn.Width = 165;
            // 
            // roldescriptionDataGridViewTextBoxColumn
            // 
            this.roldescriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.roldescriptionDataGridViewTextBoxColumn.DataPropertyName = "rol_description";
            this.roldescriptionDataGridViewTextBoxColumn.HeaderText = "ពិព៌ណនា";
            this.roldescriptionDataGridViewTextBoxColumn.Name = "roldescriptionDataGridViewTextBoxColumn";
            this.roldescriptionDataGridViewTextBoxColumn.Width = 90;
            // 
            // rolisactiveDataGridViewTextBoxColumn
            // 
            this.rolisactiveDataGridViewTextBoxColumn.DataPropertyName = "rol_isactive";
            this.rolisactiveDataGridViewTextBoxColumn.HeaderText = "rol_isactive";
            this.rolisactiveDataGridViewTextBoxColumn.Name = "rolisactiveDataGridViewTextBoxColumn";
            this.rolisactiveDataGridViewTextBoxColumn.Visible = false;
            // 
            // useidDataGridViewTextBoxColumn
            // 
            this.useidDataGridViewTextBoxColumn.DataPropertyName = "use_id";
            this.useidDataGridViewTextBoxColumn.HeaderText = "use_id";
            this.useidDataGridViewTextBoxColumn.Name = "useidDataGridViewTextBoxColumn";
            this.useidDataGridViewTextBoxColumn.Visible = false;
            // 
            // useeditdateDataGridViewTextBoxColumn
            // 
            this.useeditdateDataGridViewTextBoxColumn.DataPropertyName = "use_editdate";
            this.useeditdateDataGridViewTextBoxColumn.HeaderText = "use_editdate";
            this.useeditdateDataGridViewTextBoxColumn.Name = "useeditdateDataGridViewTextBoxColumn";
            this.useeditdateDataGridViewTextBoxColumn.Visible = false;
            // 
            // tblrolepermissionDataGridViewTextBoxColumn
            // 
            this.tblrolepermissionDataGridViewTextBoxColumn.DataPropertyName = "tbl_role_permission";
            this.tblrolepermissionDataGridViewTextBoxColumn.HeaderText = "tbl_role_permission";
            this.tblrolepermissionDataGridViewTextBoxColumn.Name = "tblrolepermissionDataGridViewTextBoxColumn";
            this.tblrolepermissionDataGridViewTextBoxColumn.Visible = false;
            // 
            // tbluserroleDataGridViewTextBoxColumn
            // 
            this.tbluserroleDataGridViewTextBoxColumn.DataPropertyName = "tbl_user_role";
            this.tbluserroleDataGridViewTextBoxColumn.HeaderText = "tbl_user_role";
            this.tbluserroleDataGridViewTextBoxColumn.Name = "tbluserroleDataGridViewTextBoxColumn";
            this.tbluserroleDataGridViewTextBoxColumn.Visible = false;
            // 
            // frm_role
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 484);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dgv_role_);
            this.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_role";
            this.Text = "frm_role";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_role_FormClosed);
            this.Load += new System.EventHandler(this.frm_role_Load);
            this.dgv_role_.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel dgv_role_;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv_role;
        private System.Windows.Forms.BindingSource bds_role;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn role_edit;
        private System.Windows.Forms.DataGridViewTextBoxColumn role_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn roldescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolisactiveDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn useeditdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tblrolepermissionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbluserroleDataGridViewTextBoxColumn;

    }
}