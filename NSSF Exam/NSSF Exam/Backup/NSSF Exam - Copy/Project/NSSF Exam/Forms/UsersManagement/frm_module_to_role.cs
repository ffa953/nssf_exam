﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Classes;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_module_to_role : Form
    {
        public frm_module_to_role()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "កំណត់​ការងារឲ្យក្រុមប្រើប្រាស់");
        }

        private void frm_module_to_role_Load(object sender, EventArgs e)
        {
            try {
                ClsSetting.sty_dgv(dgv_module);
                ClsSetting.sty_dgv(dgv_module_role);
                ClsSetting.sty_btn_close(btn_close);
                bds_role.DataSource = ClsConfig.glo_local_db.tbl_role.ToList();
                cbo_role.SelectedIndex = -1;
            }
            catch (Exception ex) { 
                ClsMsg.Error(ex.Message); 
            }
           

        }

        private void cbo_role_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbo_role.SelectedIndex != -1)
                {
                    int roleId = (int)cbo_role.SelectedValue;
                    RefreshDgvModule(roleId);
                }
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_assign_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (dgv_module.RowCount > 0)
                {
                    int roleId = (int)cbo_role.SelectedValue;

                    for (int i = 0; i < dgv_module.SelectedRows.Count; i++)
                    {
                        int modId = (int)dgv_module.SelectedRows[i].Cells["mod_id"].Value;
                        ClsUserManagement.AddUserToRole(modId, roleId);
                    }

                    RefreshDgvModule(roleId);
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_remove_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (dgv_module_role.RowCount > 0)
                {
                    int roleId = (int)cbo_role.SelectedValue;

                    for (int i = 0; i < dgv_module_role.SelectedRows.Count; i++)
                    {
                        int modId = (int)dgv_module_role.SelectedRows[i].Cells["mod_role_id"].Value;
                        ClsUserManagement.RemoveModuleFromRole(modId, roleId);
                    }

                    RefreshDgvModule(roleId);
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex) {
                ClsMsg.Error(ex.Message);
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RefreshDgvModule(int roleId) {
            var exception = ClsConfig.glo_local_db.tbl_role_permission.Where(mr => mr.rol_id == roleId).Select(mr => mr.mod_id);

            var moduleList = ClsConfig.glo_local_db.tbl_module.Where(m => !exception.Contains(m.mod_id)).Select(m => new DTOModule { modId = m.mod_id, modName = m.mod_description }).ToList();
            var moduleRoleList = ClsConfig.glo_local_db.tbl_module.Where(m => exception.Contains(m.mod_id)).Select(m => new DTOModule { modId = m.mod_id, modName = m.mod_description }).ToList();

            bds_module.DataSource = moduleList;
            bds_module_role.DataSource = moduleRoleList;
        }

        private void frm_module_to_role_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        
    }
}
