﻿using NSSF_Exam.Classes;
using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSSF_Exam.Forms.UsersManagement
{
    public partial class frm_user : Form
    {
        public frm_user()
        {
            InitializeComponent();
            ClsSetting.sty_form_view(this, "គណនីប្រើប្រាស់ប្រព័ន្ធ");
        }

        private void frm_user_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_dgv(dgv_user);
            ClsSetting.sty_btn_add(btn_new);
            ClsSetting.sty_btn_close(btn_close);

            bds_user.DataSource = (from u in ClsConfig.glo_local_db.tbl_user
                                  join emp in ClsConfig.glo_local_db.vie_employee on u.emp_id equals emp.emp_id
                                  join d in ClsConfig.glo_local_db.vie_division on u.div_id equals d.div_id
                                  where u.use_active == true
                                  select new DTOUser
                                  {
                                      user_id = u.use_id,
                                      name = emp.name_kh,
                                      username = u.use_username,
                                      div_name = d.div_name_khmer,
                                      user_status = (bool)u.use_active
                                  }).ToList();
            

        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            frm_user_edit frm = new frm_user_edit();
            frm.ShowDialog();
            frm_user_Load(null, null);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_user_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClsSetting.CloseTab();
        }

        private void dgv_user_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                frm_user_edit frm = new frm_user_edit();
                frm.user_id = (int)dgv_user.CurrentRow.Cells["user_id"].Value;
                frm.ShowDialog();
                frm_user_Load(null, null);
            }
        }
    }
}
