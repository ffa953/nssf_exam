﻿namespace NSSF_Exam.Forms.UsersManagement
{
    partial class frm_module_to_role
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label Label2;
            System.Windows.Forms.Label Label1;
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_role = new System.Windows.Forms.ComboBox();
            this.bds_role = new System.Windows.Forms.BindingSource(this.components);
            this.btn_remove = new System.Windows.Forms.Button();
            this.btn_assign = new System.Windows.Forms.Button();
            this.dgv_module_role = new System.Windows.Forms.DataGridView();
            this.mod_role_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_module_role = new System.Windows.Forms.BindingSource(this.components);
            this.dgv_module = new System.Windows.Forms.DataGridView();
            this.mod_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modNameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bds_module = new System.Windows.Forms.BindingSource(this.components);
            Label2 = new System.Windows.Forms.Label();
            Label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module_role)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            Label2.AutoSize = true;
            Label2.Location = new System.Drawing.Point(413, 12);
            Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            Label2.Name = "Label2";
            Label2.Size = new System.Drawing.Size(65, 25);
            Label2.TabIndex = 15;
            Label2.Text = "កំណត់រួច";
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new System.Drawing.Point(21, 12);
            Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            Label1.Name = "Label1";
            Label1.Size = new System.Drawing.Size(95, 25);
            Label1.TabIndex = 16;
            Label1.Text = "មិនទាន់កំណត់";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btn_close);
            this.panel1.Controls.Add(this.cbo_role);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(751, 60);
            this.panel1.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Location = new System.Drawing.Point(639, 13);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(100, 35);
            this.btn_close.TabIndex = 0;
            this.btn_close.Text = "button1";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.btn_remove);
            this.panel2.Controls.Add(this.btn_assign);
            this.panel2.Controls.Add(this.dgv_module_role);
            this.panel2.Controls.Add(this.dgv_module);
            this.panel2.Controls.Add(Label2);
            this.panel2.Controls.Add(Label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(751, 516);
            this.panel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 25);
            this.label3.TabIndex = 22;
            this.label3.Text = "ក្រុមអ្នកប្រើប្រាស់";
            // 
            // cbo_role
            // 
            this.cbo_role.DataSource = this.bds_role;
            this.cbo_role.DisplayMember = "rol_name";
            this.cbo_role.FormattingEnabled = true;
            this.cbo_role.Location = new System.Drawing.Point(131, 15);
            this.cbo_role.Name = "cbo_role";
            this.cbo_role.Size = new System.Drawing.Size(264, 32);
            this.cbo_role.TabIndex = 21;
            this.cbo_role.ValueMember = "rol_id";
            this.cbo_role.SelectedIndexChanged += new System.EventHandler(this.cbo_role_SelectedIndexChanged);
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(351, 253);
            this.btn_remove.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(44, 40);
            this.btn_remove.TabIndex = 19;
            this.btn_remove.Text = "<<";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // btn_assign
            // 
            this.btn_assign.Location = new System.Drawing.Point(351, 199);
            this.btn_assign.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_assign.Name = "btn_assign";
            this.btn_assign.Size = new System.Drawing.Size(44, 43);
            this.btn_assign.TabIndex = 20;
            this.btn_assign.Text = ">>";
            this.btn_assign.UseVisualStyleBackColor = true;
            this.btn_assign.Click += new System.EventHandler(this.btn_assign_Click);
            // 
            // dgv_module_role
            // 
            this.dgv_module_role.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_module_role.AutoGenerateColumns = false;
            this.dgv_module_role.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_module_role.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mod_role_id,
            this.modNameDataGridViewTextBoxColumn});
            this.dgv_module_role.DataSource = this.bds_module_role;
            this.dgv_module_role.Location = new System.Drawing.Point(406, 43);
            this.dgv_module_role.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.dgv_module_role.Name = "dgv_module_role";
            this.dgv_module_role.Size = new System.Drawing.Size(326, 436);
            this.dgv_module_role.TabIndex = 17;
            // 
            // mod_role_id
            // 
            this.mod_role_id.DataPropertyName = "modId";
            this.mod_role_id.HeaderText = "modId";
            this.mod_role_id.Name = "mod_role_id";
            this.mod_role_id.Visible = false;
            // 
            // modNameDataGridViewTextBoxColumn
            // 
            this.modNameDataGridViewTextBoxColumn.DataPropertyName = "modName";
            this.modNameDataGridViewTextBoxColumn.HeaderText = "ការងារ";
            this.modNameDataGridViewTextBoxColumn.Name = "modNameDataGridViewTextBoxColumn";
            this.modNameDataGridViewTextBoxColumn.Width = 275;
            // 
            // bds_module_role
            // 
            this.bds_module_role.DataSource = typeof(NSSF_Exam.Classes.DTOModule);
            // 
            // dgv_module
            // 
            this.dgv_module.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_module.AutoGenerateColumns = false;
            this.dgv_module.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_module.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mod_id,
            this.modNameDataGridViewTextBoxColumn2});
            this.dgv_module.DataSource = this.bds_module;
            this.dgv_module.Location = new System.Drawing.Point(18, 43);
            this.dgv_module.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.dgv_module.Name = "dgv_module";
            this.dgv_module.Size = new System.Drawing.Size(322, 436);
            this.dgv_module.TabIndex = 18;
            // 
            // mod_id
            // 
            this.mod_id.DataPropertyName = "modId";
            this.mod_id.HeaderText = "modId";
            this.mod_id.Name = "mod_id";
            this.mod_id.Visible = false;
            // 
            // modNameDataGridViewTextBoxColumn2
            // 
            this.modNameDataGridViewTextBoxColumn2.DataPropertyName = "modName";
            this.modNameDataGridViewTextBoxColumn2.HeaderText = "ការងារ";
            this.modNameDataGridViewTextBoxColumn2.Name = "modNameDataGridViewTextBoxColumn2";
            this.modNameDataGridViewTextBoxColumn2.Width = 275;
            // 
            // bds_module
            // 
            this.bds_module.DataSource = typeof(NSSF_Exam.Classes.DTOModule);
            // 
            // frm_module_to_role
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 576);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS Content", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frm_module_to_role";
            this.Text = "frm_module_to_role";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_module_to_role_FormClosed);
            this.Load += new System.EventHandler(this.frm_module_to_role_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bds_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module_role)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_module)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_module)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Button btn_remove;
        internal System.Windows.Forms.Button btn_assign;
        internal System.Windows.Forms.DataGridView dgv_module_role;
        internal System.Windows.Forms.DataGridView dgv_module;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbo_role;
        private System.Windows.Forms.BindingSource bds_module;
        private System.Windows.Forms.BindingSource bds_module_role;
        private System.Windows.Forms.BindingSource bds_role;
        private System.Windows.Forms.DataGridViewTextBoxColumn mod_role_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn modNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mod_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn modNameDataGridViewTextBoxColumn2;

    }
}