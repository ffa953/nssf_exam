﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Classes;

namespace NSSF_Exam.MDIForms
{
    public partial class frm_login : Form
    {
        public frm_login()
        {
            InitializeComponent();
            ClsSetting.sty_form_dialog(this, "ប្រព័ន្ធគ្រប់គ្រង​របបថែទាំសុខភាព");
        }

        private void frm_login_Load(object sender, EventArgs e)
        {
            ClsSetting.sty_btn_submit(btn_submit);
            ClsSetting.sty_btn_close(btn_cancel);

            tsl_system_name.Text = ClsConfig.glo_app_name_en;
            tsl_version.Text = "Version " + (ClsSetting.FunGetSystemVersion() == ""? "1.0.0.1":ClsSetting.FunGetSystemVersion());

            txt_username.Text = "system";
            txt_password.Text = "systemadmin";

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                string strMsg = "";
                if (txt_username.Text == "")
                {
                    strMsg += "- សូមបញ្ជូលឈ្មោះអ្នកប្រើប្រាស់ជាមុន!\n";
                }
                if (txt_password.Text == "")
                {
                    strMsg += "- សូមបញ្ជូលលេខសំងាត់ជាមុន!";
                }

                if (strMsg != "")
                {
                    ClsMsg.Warning(strMsg);
                }
                else
                {
                    var result = (from u in ClsConfig.glo_local_db.tbl_user where u.use_username == txt_username.Text && u.use_password == txt_password.Text && u.use_active == true select u).ToList();
                    if (result.Count() > 0)
                    {
                        ClsConfig.glo_use_id = result.FirstOrDefault().use_id;
                        ClsConfig.glo_use_privilege = (int)result.FirstOrDefault().pri_id;
                        ClsConfig.glo_use_div_id = result.FirstOrDefault().div_id;
                        ClsConfig.glo_use_bra_id = 12;
                        if (ClsUserClient.CheckMachineClient() == 0)
                        {
                            ClsMsg.Warning("ម៉ាស៊ីនឈ្មោះ " + ClsSetting.FunGetSystemHostname() + " មិនអាចដំណើរការបានទេ។ សូមធ្វើការទំទាក់ទំនងការិយាល័យព័ត៌មានវិទ្យា!");
                        }
                        else
                        {
                            ClsConfig.glo_log_id = ClsUserClient.Login(ClsConfig.glo_use_id, ClsConfig.glo_mac_id, ClsConfig.glo_use_div_id);

                            if (ClsUserClient.switchUser == 1) // Switch User
                            {
                                this.Close();
                                ClsSetting.CloseAllTabs();
                                ClsSetting.OpenPermission(ClsConfig.glo_main_form, ClsConfig.glo_main_form.Name, ClsConfig.glo_use_privilege);

                            }
                            else { //New Login
                                this.Hide();
                                ClsConfig.glo_main_form = new frm_main();
                                ClsConfig.glo_main_form.Show();
                                
                            }

                            ClsConfig.glo_main_form.ShowLoginInfo();
                           
                        }
                    }
                    else
                    {
                        ClsMsg.Warning("ឈ្មោះអ្នកប្រើប្រាស់ ឬលេខសំងាត់មិនត្រឹមត្រូវទេ!");

                    }
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception exc) {
                ClsMsg.Error(exc.Message);
            }
        }

        

        private void txtUsername_Enter(object sender, EventArgs e)
        {
            ClsSetting.ChangeKeyToEN();   
            
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            ClsSetting.ChangeKeyToEN();
        }
    }
}
