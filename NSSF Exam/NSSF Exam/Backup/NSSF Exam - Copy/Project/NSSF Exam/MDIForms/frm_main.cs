﻿using NSSF_Exam.Classes.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.Forms.Exam;
using NSSF_Exam.Forms.UsersManagement;
using NSSF_Exam.Classes;

namespace NSSF_Exam.MDIForms
{
    public partial class frm_main : Form
    {
        public frm_main()
        {
            InitializeComponent();
            this.Text = ClsConfig.glo_app_name;
        }

        private void frm_main_Load(object sender, EventArgs e)
        {
            ClsSetting.OpenPermission(this, this.Name, ClsConfig.glo_use_privilege);
            ClsSetting.tpctrl = tabControl1;
            ClsSetting.OpenInParentForm(new frm_home(), this);
            
        }

     

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClsSetting.TabSelected();
        }

        private void frm_main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ClsSetting.isClose == false)
            {
                if (ClsSetting.IsExitApplication())
                {
                    ClsSetting.isClose = true;
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void tsm_logout_Click(object sender, EventArgs e)
        {
            if (ClsSetting.IsExitApplication())
            {
                ClsSetting.isClose = true;
                Application.Exit();
            }
        }

        private void tsm_switch_user_Click(object sender, EventArgs e)
        {
            ClsUserClient.SetUserLogout();
        }

        private void tsm_change_password_Click(object sender, EventArgs e)
        {
            frm_change_password frm = new frm_change_password();
            frm.ShowDialog();
        }

        private void tsp_switch_user_Click(object sender, EventArgs e)
        {
            ClsUserClient.SetUserLogout();
        }

        private void tsm_add_module_role_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_module_to_role(), this);
        }

        private void tsm_role_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_role(), this);
        }

        private void tsb_refresh_Click(object sender, EventArgs e)
        {
            ClsFun.RefreshEntity();
        }

        private void tsm_module_list_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_module(), this);
        }

        private void tsm_account_list_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_user(), this);
        }

        public void ShowLoginInfo()
        {
            tsl_app_name.Text = ClsConfig.glo_app_name;
            tsl_server_name.Text = (ClsConfig.is_real_database == true ? "ទីស្នាក់ការកណ្តាលរាជធានីភ្នំពេញ" : "ប្រព័ន្ធសាកល្បង");
            tsl_user_name.Text = "ឈ្មោះអ្នកប្រើប្រាស់៖ " + ClsConfig.glo_local_db.tbl_user.Where(u => u.use_id == ClsConfig.glo_use_id).FirstOrDefault().use_username;
            var user_role = (from ur in ClsConfig.glo_local_db.tbl_user_role join r in ClsConfig.glo_local_db.tbl_role on ur.rol_id equals r.rol_id where ur.use_id == ClsConfig.glo_use_id select r.rol_name);
            tsl_role_name.Text = "ក្រុម៖ " + (user_role.Count() > 0? user_role.FirstOrDefault().ToString():"មិនទាន់កំណត់ក្រុម");
            tsl_login_date.Text = "ម៉ោងចូលប្រើប្រាស់៖ " + ClsConfig.glo_local_db.tbl_audit_log.Where(l => l.log_id == ClsConfig.glo_log_id).FirstOrDefault().log_date_in.Value.ToString("dd-MMM-yyyy HH:mm:ss");
        }

        private void tsb_home_Click(object sender, EventArgs e)
        {
            ClsSetting.CloseAllTabs();
        }

        private void tsm_student_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_student(), this);
        }

        private void tsm_certificate_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_certificate_skill(), this);
        }

        private void tsm_exam_date_Click(object sender, EventArgs e)
        {
            ClsSetting.OpenInParentForm(new frm_exam(), this);
        }

    }
}
