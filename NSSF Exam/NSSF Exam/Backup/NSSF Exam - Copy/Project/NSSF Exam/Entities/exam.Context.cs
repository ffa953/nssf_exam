﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NSSF_Exam.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class nssf_exam_onlineEntitie : DbContext
    {
        public nssf_exam_onlineEntitie()
            : base("name=nssf_exam_onlineEntitie")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<tbl_account> tbl_account { get; set; }
        public DbSet<tbl_applicant> tbl_applicant { get; set; }
        public DbSet<tbl_audit_log> tbl_audit_log { get; set; }
        public DbSet<tbl_audit_machine> tbl_audit_machine { get; set; }
        public DbSet<tbl_certificate> tbl_certificate { get; set; }
        public DbSet<tbl_certificate_skill> tbl_certificate_skill { get; set; }
        public DbSet<tbl_employee_exam> tbl_employee_exam { get; set; }
        public DbSet<tbl_exam_date> tbl_exam_date { get; set; }
        public DbSet<tbl_exam_place> tbl_exam_place { get; set; }
        public DbSet<tbl_exam_type> tbl_exam_type { get; set; }
        public DbSet<tbl_finger_print> tbl_finger_print { get; set; }
        public DbSet<tbl_gender> tbl_gender { get; set; }
        public DbSet<tbl_log_description> tbl_log_description { get; set; }
        public DbSet<tbl_log_transaction> tbl_log_transaction { get; set; }
        public DbSet<tbl_log_type> tbl_log_type { get; set; }
        public DbSet<tbl_module> tbl_module { get; set; }
        public DbSet<tbl_permission> tbl_permission { get; set; }
        public DbSet<tbl_privilege> tbl_privilege { get; set; }
        public DbSet<tbl_quiz_answer> tbl_quiz_answer { get; set; }
        public DbSet<tbl_quiz_type> tbl_quiz_type { get; set; }
        public DbSet<tbl_role> tbl_role { get; set; }
        public DbSet<tbl_role_permission> tbl_role_permission { get; set; }
        public DbSet<tbl_skill> tbl_skill { get; set; }
        public DbSet<tbl_skill_branch> tbl_skill_branch { get; set; }
        public DbSet<tbl_skill_exam> tbl_skill_exam { get; set; }
        public DbSet<tbl_skill_typing> tbl_skill_typing { get; set; }
        public DbSet<tbl_subject> tbl_subject { get; set; }
        public DbSet<tbl_subject_in_skill> tbl_subject_in_skill { get; set; }
        public DbSet<tbl_subject_quiz> tbl_subject_quiz { get; set; }
        public DbSet<tbl_typing> tbl_typing { get; set; }
        public DbSet<tbl_user> tbl_user { get; set; }
        public DbSet<tbl_user_answer> tbl_user_answer { get; set; }
        public DbSet<tbl_user_control> tbl_user_control { get; set; }
        public DbSet<tbl_user_fingerprint> tbl_user_fingerprint { get; set; }
        public DbSet<tbl_user_quiz> tbl_user_quiz { get; set; }
        public DbSet<tbl_user_role> tbl_user_role { get; set; }
        public DbSet<tbl_user_type> tbl_user_type { get; set; }
        public DbSet<tbl_user_typing> tbl_user_typing { get; set; }
        public DbSet<vie_quiz_list> vie_quiz_list { get; set; }
        public DbSet<vw_nationality> vw_nationality { get; set; }
        public DbSet<webpages_Membership> webpages_Membership { get; set; }
        public DbSet<webpages_Roles> webpages_Roles { get; set; }
        public DbSet<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<webpages_OAuthMembership> webpages_OAuthMembership { get; set; }
        public DbSet<vie_employee> vie_employee { get; set; }
        public DbSet<vie_division> vie_division { get; set; }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_get_exam_by_user")]
        public virtual IQueryable<fun_get_exam_by_user_Result> fun_get_exam_by_user(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_get_exam_by_user_Result>("[nssf_exam_onlineEntitie].[fun_get_exam_by_user](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_get_quiz_list")]
        public virtual IQueryable<fun_get_quiz_list_Result> fun_get_quiz_list(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_get_quiz_list_Result>("[nssf_exam_onlineEntitie].[fun_get_quiz_list](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_get_total_result")]
        public virtual IQueryable<fun_get_total_result_Result> fun_get_total_result(Nullable<int> exam_id)
        {
            var exam_idParameter = exam_id.HasValue ?
                new ObjectParameter("exam_id", exam_id) :
                new ObjectParameter("exam_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_get_total_result_Result>("[nssf_exam_onlineEntitie].[fun_get_total_result](@exam_id)", exam_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_quiz_result")]
        public virtual IQueryable<fun_quiz_result_Result> fun_quiz_result(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_quiz_result_Result>("[nssf_exam_onlineEntitie].[fun_quiz_result](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_subject_quiz_check")]
        public virtual IQueryable<fun_subject_quiz_check_Result> fun_subject_quiz_check(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_subject_quiz_check_Result>("[nssf_exam_onlineEntitie].[fun_subject_quiz_check](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_subject_skill_user_list")]
        public virtual IQueryable<fun_subject_skill_user_list_Result> fun_subject_skill_user_list(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_subject_skill_user_list_Result>("[nssf_exam_onlineEntitie].[fun_subject_skill_user_list](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_typing_result")]
        public virtual IQueryable<fun_typing_result_Result> fun_typing_result(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_typing_result_Result>("[nssf_exam_onlineEntitie].[fun_typing_result](@account_id)", account_idParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_user_admin_by_role")]
        public virtual IQueryable<fun_user_admin_by_role_Result> fun_user_admin_by_role(Nullable<int> role)
        {
            var roleParameter = role.HasValue ?
                new ObjectParameter("role", role) :
                new ObjectParameter("role", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_user_admin_by_role_Result>("[nssf_exam_onlineEntitie].[fun_user_admin_by_role](@role)", roleParameter);
        }
    
        [EdmFunction("nssf_exam_onlineEntitie", "fun_user_by_role")]
        public virtual IQueryable<fun_user_by_role_Result> fun_user_by_role(Nullable<int> role)
        {
            var roleParameter = role.HasValue ?
                new ObjectParameter("role", role) :
                new ObjectParameter("role", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fun_user_by_role_Result>("[nssf_exam_onlineEntitie].[fun_user_by_role](@role)", roleParameter);
        }
    
        public virtual int pro_generate_user_quiz(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("pro_generate_user_quiz", account_idParameter);
        }
    
        public virtual ObjectResult<Nullable<System.DateTime>> pro_get_current_date()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<System.DateTime>>("pro_get_current_date");
        }
    
        public virtual int pro_login(Nullable<int> use_id, Nullable<int> mac_id, string div_id, ObjectParameter log_id)
        {
            var use_idParameter = use_id.HasValue ?
                new ObjectParameter("use_id", use_id) :
                new ObjectParameter("use_id", typeof(int));
    
            var mac_idParameter = mac_id.HasValue ?
                new ObjectParameter("mac_id", mac_id) :
                new ObjectParameter("mac_id", typeof(int));
    
            var div_idParameter = div_id != null ?
                new ObjectParameter("div_id", div_id) :
                new ObjectParameter("div_id", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("pro_login", use_idParameter, mac_idParameter, div_idParameter, log_id);
        }
    
        public virtual int pro_machine_client(string mac_hostname, string mac_macaddress, string mac_systemserial, string mac_version, Nullable<int> use_id, string bra_id, ObjectParameter mac_active, ObjectParameter mac_id)
        {
            var mac_hostnameParameter = mac_hostname != null ?
                new ObjectParameter("mac_hostname", mac_hostname) :
                new ObjectParameter("mac_hostname", typeof(string));
    
            var mac_macaddressParameter = mac_macaddress != null ?
                new ObjectParameter("mac_macaddress", mac_macaddress) :
                new ObjectParameter("mac_macaddress", typeof(string));
    
            var mac_systemserialParameter = mac_systemserial != null ?
                new ObjectParameter("mac_systemserial", mac_systemserial) :
                new ObjectParameter("mac_systemserial", typeof(string));
    
            var mac_versionParameter = mac_version != null ?
                new ObjectParameter("mac_version", mac_version) :
                new ObjectParameter("mac_version", typeof(string));
    
            var use_idParameter = use_id.HasValue ?
                new ObjectParameter("use_id", use_id) :
                new ObjectParameter("use_id", typeof(int));
    
            var bra_idParameter = bra_id != null ?
                new ObjectParameter("bra_id", bra_id) :
                new ObjectParameter("bra_id", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("pro_machine_client", mac_hostnameParameter, mac_macaddressParameter, mac_systemserialParameter, mac_versionParameter, use_idParameter, bra_idParameter, mac_active, mac_id);
        }
    
        public virtual ObjectResult<pro_user_detail_Result> pro_user_detail()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<pro_user_detail_Result>("pro_user_detail");
        }
    
        public virtual int pro_user_finished_exam(Nullable<int> account_id)
        {
            var account_idParameter = account_id.HasValue ?
                new ObjectParameter("account_id", account_id) :
                new ObjectParameter("account_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("pro_user_finished_exam", account_idParameter);
        }
    
        public virtual int sp_insert_SubNo(Nullable<int> user_id, Nullable<int> skil_id, Nullable<int> exam_id)
        {
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("user_id", user_id) :
                new ObjectParameter("user_id", typeof(int));
    
            var skil_idParameter = skil_id.HasValue ?
                new ObjectParameter("skil_id", skil_id) :
                new ObjectParameter("skil_id", typeof(int));
    
            var exam_idParameter = exam_id.HasValue ?
                new ObjectParameter("exam_id", exam_id) :
                new ObjectParameter("exam_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_insert_SubNo", user_idParameter, skil_idParameter, exam_idParameter);
        }
    }
}
