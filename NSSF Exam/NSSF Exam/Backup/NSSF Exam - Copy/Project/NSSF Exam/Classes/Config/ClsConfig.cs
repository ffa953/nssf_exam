﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NSSF_Exam.MDIForms;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Classes.Config
{
    class ClsConfig
    {

        //============== Database Connection =====================
        public static bool is_real_database = false;

        public static nssf_exam_onlineEntitie glo_local_db = ClsDatabase.Connection;

        //=============== End Connection =========================

        //Global Variable

        public static string glo_app_name = "ប្រព័ន្ធគ្រប់គ្រងរបប​ថែទាំសុខភាព";
        public static string glo_app_name_en = "HSPIS System";
        public static int glo_use_id = 0;
        public static string glo_use_div_id = "";
        public static int glo_use_bra_id = 0;
        public static int glo_use_privilege = 0;
        public static int glo_bra_id = 0;      
        public static int glo_mac_id = 0;
        public static long glo_log_id = 0;
        public static frm_main glo_main_form;

        //============= End Global Variable ======================

        

    }
}
