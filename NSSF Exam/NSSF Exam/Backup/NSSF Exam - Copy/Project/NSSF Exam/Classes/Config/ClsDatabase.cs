﻿using NSSF_Exam.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
    

namespace NSSF_Exam.Classes.Config
{
    class ClsDatabase : nssf_exam_onlineEntitie
    {
        public ClsDatabase(string serverName, string databaseName, string username, string password)
        {
            string constr = "";
            try
            {
                constr = "data source=" + serverName + ";initial catalog=" + databaseName + ";persist security info=True;user id=" + username + ";password=" + password + ";MultipleActiveResultSets=True;App=EntityFramework;Connection Timeout=5000";
            }
            catch (Exception ex) { 
                MessageBox.Show(ex.Message, "NSSF Message", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }

            base.Database.Connection.ConnectionString = constr;
        }
        public static nssf_exam_onlineEntitie Connection
        {
            get
            {
                nssf_exam_onlineEntitie db;

                if (ClsConfig.is_real_database)
                {
                    db = new ClsDatabase("", "", "", ""); //database real
                }
                else
                {
                    db = new ClsDatabase("192.168.201.238", "nssf_exam_online", "developer", "Nssf@2017"); //database test
                }
                return db;
            }
            
        }

    }
}
