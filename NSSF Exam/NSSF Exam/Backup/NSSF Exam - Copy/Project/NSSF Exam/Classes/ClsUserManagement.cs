﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSSF_Exam.Classes.Config;
using NSSF_Exam.Entities;

namespace NSSF_Exam.Classes
{
    class ClsUserManagement
    {
        public static void AddUserToRole(int modId, int roleid)
        {
            try
            {
                var userRole = ClsConfig.glo_local_db.tbl_role_permission.Where(ur => ur.mod_id == modId & ur.rol_id == roleid).ToList();
                if (userRole.Count() > 0)
                {
                    return;
                }
                else
                {
                    tbl_role_permission tbl = new tbl_role_permission();
                    tbl.rol_id = roleid;
                    tbl.mod_id = modId;
                    ClsConfig.glo_local_db.tbl_role_permission.Add(tbl);
                    ClsConfig.glo_local_db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
            }
        }

        public static void RemoveModuleFromRole(int modId, int roleid)
        {
            try
            {
                var userRole = ClsConfig.glo_local_db.tbl_role_permission.Where(ur => ur.mod_id == modId & ur.rol_id == roleid).SingleOrDefault();

                if (userRole != null)
                {
                    ClsConfig.glo_local_db.tbl_role_permission.Remove(userRole);
                    ClsConfig.glo_local_db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                
            }

        }

    }
}
